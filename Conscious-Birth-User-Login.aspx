﻿<%@ Page Title="Account User Login" Language="C#" MasterPageFile="~/ConsciousBirth.master" AutoEventWireup="true" CodeFile="Conscious-Birth-User-Login.aspx.cs" Inherits="Conscious_Birth_User_Login" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link href="web/css/style.css" rel="stylesheet" />

    <script lang="javascript" type="text/javascript">

        function clearStylingEmail(target) {
            if (target.value != '' || target.value != null) {
                var emailExpression = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
                if (target.value.length > 0) {
                    if (target.value.match(emailExpression)) {
                        document.getElementById("<%= divImageHolderLogin.ClientID %>").className = 'validationImageCorrectLogin';
                    }
                    else {
                        document.getElementById("<%= divImageHolderLogin.ClientID %>").className = 'validationImageIncorrectLogin';
                    }
                }
                else {
                    document.getElementById("<%= divImageHolderLogin.ClientID %>").className = 'validationImageIncorrectLogin';
                }
            }
            else {
                document.getElementById("<%= divImageHolderLogin.ClientID %>").className = 'validationImageIncorrectLogin';
            }
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="main_wrapper">
        <!-- C O N T E N T -->
        <div class="content_wrapper">
            <div class="page_title_block">
                <div class="container">
                    <h2 class="title">LogIn</h2>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="content_block no-sidebar row">
                <!-- .row-fluid -->
                <div class="row-fluid">
                    <div class="span7 module_cont module_feedback_form">
                        <br />

                        <div id="mandatoryDiv" class="mandatoryInvalidDiv" runat="server">
                            <div class ="bold_red_text" style="position: relative;top: 16px;left: 0px; font-size: 13px; margin-bottom: 35px;">
                                <asp:Literal ID="litValidationMessage" runat="server" />
                            </div>
                          </div>

                        <div class="controlDiv">
                            <div class="labelDiv">Username:</div>
                            <div id="divImageHolderLogin" runat="server" class="divImageHolderLogin"></div>
                            <div class="fieldDiv">
                                <asp:TextBox ID="txtUsername" runat="server" CssClass="" onblur="" onKeyUp="return SetMaxLength(this,50)" />
                            </div>
                            <br class="clearfix" />
                        </div>

                        <div id="divPassword" runat="server" class="controlDiv">
                            <div id="passwordLabel" class="labelDiv">Password:</div>
                            <div class="divImageHolderLogin"></div>
                            <div class="fieldDiv">
                                <asp:TextBox ID="txtPassword" runat="server" onfocus="javascript:changeFocusStylingPassword(this)" onblur="javascript:clearStylingPassword(this)" CssClass="roundedTextboxPassword" TextMode="Password" />
                            </div>
                            <br class="clearfix" />
                        </div>

                        <div style="float:right;">
                            <br style="clear: both;" />
                            <a href="Conscious-Birth-User-Register.aspx">Register</a><br />
                            <asp:LinkButton ID="lnkbtnForgottenPassword" runat="server" CssClass="" Text="I forgot my password" OnClick="lnkbtnForgottenPassword_Click" />
                            <asp:LinkButton ID="lnkbtnBackToLogin" runat="server" CssClass="" Text="Back to login" OnClick="lnkbtnBackToLogin_Click" Visible="false" />
                        </div>

                        

                        <asp:Button ID="btnLogin" Text="Login" runat="server" CssClass="" OnClick="btnLogin_Click" />
                        <asp:Button ID="btnSubmit" Text="Submit" runat="server" CssClass="" Visible="false" OnClick="btnSubmit_Click" />

                    </div>
                </div>
            </div>
            <div class="left-sidebar-block span5">
                <aside class="sidebar">
                </aside>
            </div>
        </div>
    </div>

</asp:Content>



