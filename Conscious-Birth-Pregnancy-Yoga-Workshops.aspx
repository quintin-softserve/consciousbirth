﻿<%@ Page Title="Pregnancy Yoga Workshopm :: Conscious Birth" Language="C#" MasterPageFile="~/ConsciousBirth.master" AutoEventWireup="true" CodeFile="Conscious-Birth-Pregnancy-Yoga-Workshops.aspx.cs" Inherits="Conscious_Birth_Pregnancy_Yoga_Workshop" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date(); a = s.createElement(o),
            m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-53160457-1', 'auto');
        ga('send', 'pageview');

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="main_wrapper">
        <!-- C O N T E N T -->
        <div class="content_wrapper">
            <div class="page_title_block">
                <div class="container">
                    <h2 class="title">Pregnancy Yoga Workshops</h2>
                </div>
            </div>
            <div class="container">
                <div class="content_block no-sidebar row">
                    <div class="fl-container span12">
                        <div class="row">
                            <div class="posts-block span12">
                                <div class="contentarea">

                                    <div class="row-fluid">
                                        <div class="span4 module_cont module_text_area module_none_padding">
                                            <img src="img/connect-with-baby.jpg" alt="" />
                                        </div>
                                        <div class="span8 module_cont module_text_area module_none_padding">
                                            <blockquote class="type2">
                                                <h6>Connect with your Baby</h6>
                                                <p>
                                                    A workshop that helps you explore the mystery that you and your baby share in a fun and expressive way.
                                                </p>
                                                <p><strong>What to expect:</strong></p>
                                                <p>
                                                    In this workshop, using simple, holistic tools you will be guided to access your intuition, trust your 
                                                    body and bond with your baby. Whether you are experiencing your first, second or third pregnancy this 
                                                    workshop is an opportunity to give this particular baby your attention and focus for one afternoon. 
                                                    It is also a chance to establish a deep (re)connection with yourself and the incorporation of some important me-time. 
                                                   
                                                </p>
                                                <p>
                                                    You can enjoy this workshop at any stage during your pregnancy. The workshop utilises the transformative qualities of 
                                                    pregnancy yoga and breathing, visualization, deep relaxation and sound. NEXT DATES: 30th July 2016 & 22nd October 2016
                                                </p>
                                                <br />
                                                <div class="sectionContainerLeft innerBoxWithShadow">
                                                    <div class="span12 module_cont module_text_area module_none_padding">
                                                        <blockquote class="type2">
                                                            <p>
                                                                Connecting with baby workshop was a moving experience for me. 
                                                                I had had a couple of really tough weeks at work and when I got there I realized that I had not been connecting or 
                                                                conversing with my baby at all - I felt terrible. Theoni reminded me that even if I thought I wasn't - I was still connected. 
                                                                It was beautiful to go through the meditation and just to take the time to focus solely on the miracle inside me. 
                                                                I thoroughly enjoyed it and would recommend it to all moms to be!
                                                            </p>
                                                            <div class="author italic">Debbie-Lee Cheetham</div>
                                                        </blockquote>
                                                    </div>
                                                    <br class="clear" />
                                                </div>
                                                <br />
                                                <br />
                                                <h6>Mom’s and Dad’s Workshop</h6>
                                                <p>A time to experience calmness and connection with your partner.</p>
                                                <p>
                                                    This class is especially designed for moms and their partners to experience pregnancy yoga together and to connect with 
                                                    their baby. Partners will be shown tools to support and nurture the pregnant party and baby during the rest of the pregnancy 
                                                    and during birthing. You will learn to breathe, stretch, relax and feel totally restored.
                                                </p>
                                                <p>
                                                    Your pregnancy is so unique and so short. This workshop provides the space for you to make time to consciously connect with 
                                                    each other before your baby arrives. Even though dads or partners are not physically experiencing the pregnancy, they can make 
                                                    an important contribution with their presence. Join this workshop and embrace a precious space together.<br />
                                                    
                                                </p>

                                                <br />
                                                <div class="sectionContainerLeft innerBoxWithShadow">
                                                    <div class="span12 module_cont module_text_area module_none_padding">
                                                        <blockquote class="type2">
                                                            <p>
                                                                The ‘Mom & Dads’ workshop provided an opportunity for my husband and I to bond with each other. When you are pregnant and 
                                                                not feeling terribly attractive, it is a great thing to be able to bond with your partner on a spiritual, mental and even 
                                                                physical level through yoga. Through the yoga postures we found that our minds and bodies are in tune with each other. 
                                                                We shared a lot of good laughs and thoroughly enjoyed the afternoon. My husband loved meeting the other couples as this 
                                                                gave him a chance to experience our pregnancy with them. Leaving the workshop we felt connected as a couple, ready to take on parenthood together.

                                                            </p>
                                                            <div class="author italic">Sharon, mother of Matthew</div>
                                                        </blockquote>
                                                    </div>
                                                    <br class="clear" />
                                                </div>
                                            </blockquote>
                                        </div>
                                    </div>
                                    <div class="clear"></div>
                                    <div class="contentarea">
                                        
                                    </div>
                                    <!-- .row-fluid -->
                                    <div class="clear"></div>
                                    
                                    <div class="module_line_trigger" data-background="#ffeec9 url(img/VV.png) no-repeat center" data-top-padding="bottom_padding_huge" data-bottom-padding="module_big_padding">
                                        <div class="row-fluid">
                                            <div class="span12 module_cont module_promo_text module_huge_padding">
                                                <div class="shortcode_promoblock">
                                                    <div class="promo_button_block type2">
                                                        <a href="Conscious-Birth-Products.aspx" class="promo_button"> View our Products </a>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--.module_cont -->
                                            <div class="clear">
                                                <!-- ClearFIX -->
                                            </div>
                                        </div>
                                        <!-- .row-fluid -->
                                    </div>
                                    <!-- .module_line_trigger -->
                                    <!-- <div class="row-fluid">
                                        <div class="span12 module_cont module_partners center_title module_big_padding2">
                                            <div class="bg_title">
                                                <div class="title_wrapper">
                                                    <a href="javascript:void(0)" class="btn_carousel_left"></a>
                                                    <h5 class="headInModule">Our Partners</h5>
                                                    <a href="javascript:void(0)" class="btn_carousel_right"></a>
                                                </div>
                                            </div>
                                            <div class="module_content sponsors_works carouselslider items5" data-count="5">
                                                <ul>
                                                    <li>
                                                        <div class="item">
                                                            <a href="#charity-html5css3-website-template/3180454" target="_blank">
                                                                <img src="img/pictures/partners2.png" alt=""></a>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="item">
                                                            <a href="#point-business-responsive-wp-theme/4319087" target="_blank">
                                                                <img src="img/pictures/partners4.png" alt=""></a>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="item">
                                                            <a href="#cleanspace-retina-ready-business-wp-theme/3776000" target="_blank">
                                                                <img src="img/pictures/partners1.png" alt=""></a>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="item">
                                                            <a href="#yellowproject-multipurpose-retina-wp-theme/4066662" target="_blank">
                                                                <img src="img/pictures/partners3.png" alt=""></a>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="item">
                                                            <a href="#showroom-portfolio-retina-ready-wp-theme/3473628" target="_blank">
                                                                <img src="img/pictures/partners5.png" alt=""></a>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="item">
                                                            <a href="themeforest.net/item/incipiens-responsive-portfolio-wordpress-theme/2762691" target="_blank">
                                                                <img src="img/pictures/partners6.png" alt=""></a>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="item">
                                                            <a href="themeforest.net/item/hq-photography-responsive-wp-theme/3200962" target="_blank">
                                                                <img src="img/pictures/partners9.png" alt=""></a>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="item">
                                                            <a href="#incipiens-responsive-portfolio-wordpress-theme/2762691" target="_blank">
                                                                <img src="img/pictures/partners11.png" alt=""></a>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- .row-fluid -->

                                    <!-- </div> -->
                                    <!-- .contentarea -->

                                    <div class="left-sidebar-block span3">
                                        <aside class="sidebar">
                                            //Sidebar Text
                                        </aside>
                                    </div>
                                    <!-- .left-sidebar -->
                                </div>
                            </div>
                            <!-- .fl-container -->
                            <div class="right-sidebar-block span3">
                                <aside class="sidebar">
                                </aside>
                            </div>
                            <!-- .right-sidebar -->
                        </div>
                    </div>
                    <!-- .container -->
                </div>
                <!-- .content_wrapper -->
            </div>
            <!-- .main_wrapper -->
        </div>
    </div>
</asp:Content>

