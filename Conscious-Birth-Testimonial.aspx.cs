﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Conscious_Birth_Testimonial : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        popTestimonials();
    }
    private void popTestimonials()
    {
        string strCustomerName = "";
        string strTestimonial = "";

        DataTable dtTestimonials = clsCustomerTestimonials.GetCustomerTestimonialsList("", "");
        StringBuilder strbTestimonial = new StringBuilder();

        foreach (DataRow dtrTestimonialRow in dtTestimonials.Rows)
        {
            strTestimonial = (dtrTestimonialRow["strTestimonial"].ToString());
            strCustomerName = (dtrTestimonialRow["strCustomerName"].ToString());
          
            strbTestimonial.AppendLine("<div class='sectionContainerLeft innerBoxWithShadow'>");
                strbTestimonial.AppendFormat("<div class='span12 module_cont module_text_area module_none_padding'>");
                    strbTestimonial.AppendFormat("<blockquote class='type2'>");
                        strbTestimonial.AppendLine("<p>" + strTestimonial + "</p>");
                         strbTestimonial.AppendFormat("<div class='author italic'>"+ strCustomerName +"</div>");
                    strbTestimonial.AppendFormat("</blockquote>");
                    strbTestimonial.AppendLine("</div><br class='clear'/>");
                strbTestimonial.AppendFormat("</div>");
            strbTestimonial.AppendLine("<br/>");
           
        }

        litTestimonial.Text = strbTestimonial.ToString();
    }
   
    
}