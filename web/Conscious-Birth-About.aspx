﻿<%@ Page Title="About :: Conscious Birth" Language="C#" MasterPageFile="~/ConsciousBirth.master" AutoEventWireup="true" CodeFile="Conscious-Birth-About.aspx.cs" Inherits="Conscious_Birth_About" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <title>Conscious Birth - Products</title>

  
    
  


     <script type="text/javascript" src="js/jquery.min.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="main_wrapper">
        <!-- C O N T E N T -->
        <div class="content_wrapper">
            <div class="page_title_block">
                <div class="container">
                    <h2 class="title">About</h2>
                </div>
            </div>
            <div class="container">
                <div class="content_block no-sidebar row">
                    <div class="fl-container span12">
                        <div class="row">
                            <div class="posts-block span12">
                                <div class="contentarea">

                                    <div class="row-fluid">
                                        <div class="span4 module_cont module_text_area module_none_padding">
                                            <img src="img/pictures/dvd.png" alt="" />
                                        </div>
                                        <div class="span8 module_cont module_text_area module_none_padding">
                                            <blockquote class="type2">
                                                <p>
                                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam nec sagittis nulla. Aenean consequat aliquet malesuada cras lacinia augue. Praesent in viverra erat, sit 
                                                amet facilisis lacus. Sed congue augue molestie tincidunt porta. Integer pretiumt eget  gravida sem sit amet placerat. Nulla dignissim tincidunt velit accumsan semper. Maecenas 
                                                eget odio ultricies, pulvinar purus sed, tempus orci. Nullam auctor risus nunc, sed gravida mi dapibus mollis phasellus eu rhoncus lorem, sed dapibus.
                                                </p>
                                                <div class="author">Anna Smith, Company inc.</div>
                                            </blockquote>
                                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam nec sagittis nulla. Aenean consequat aliquet malesuada cras ut lacinia augue. Praesent in viverra erat, sit amet 
                                            facilisis lacus. Sed congue augue molestie tincidunt porta. Integer pretium gravida sem sit amet placerat. Nulla dignissim tincidunt velit accumsan semper. Maecenas eget odio 
                                            ultricies, pulvinar purus sed, tempus orci tnullam auctor risus nunc, sed gravida mi dapibus mollis. Phasellus eu rhoncus lorem, sed dapibus erat. Fusce malesuada, odio eget feugiat 
                                            adipiscing, massa nisl molestie felis, et sagittis eros leo et lorem. Integer lacinia leo nunc, nec lacinia justo volutpat et nam at metus ut ligula tincidunt pulvinar. Ut eget est 
                                            quis sem vulputate ultrices non et magna. In in egestas magna aliquam erat volutpat. donec sit amet ipsum tincidunt, tempor purus non, accumsan lacus. Mauris egestas mauris id 
                                            tellus eleifend, vel tincidunt nibh aliquam Vivamus non erat ut metus gravida aliquet. Integer malesuada gravida nisi sed fringilla. Etiam auctor lectus orci, et pretium mauris 
                                            eleifend sed. Nunc in sollicitudin felis. Cras nec nisl nisl.
                                        </div>
                                    </div>
                                    <!-- .row-fluid -->
                                    <div class="module_line_trigger" data-background="#ffeec9 url(img/VV.png) no-repeat center" data-top-padding="bottom_padding_huge" data-bottom-padding="module_big_padding">
                                        <div class="row-fluid">
                                            <div class="span12 module_cont module_promo_text module_huge_padding">
                                                <div class="shortcode_promoblock">
                                                    <div class="promo_button_block type2">
                                                        <a href="Conscious-Birth-Products.aspx" class="promo_button">View our Products</a>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--.module_cont -->
                                            <div class="clear">
                                                <!-- ClearFIX -->
                                            </div>
                                        </div>
                                        <!-- .row-fluid -->
                                    </div>
                                    <!-- .module_line_trigger -->
                                    <div class="row-fluid">
                                        <div class="span12 module_cont module_partners center_title module_big_padding2">
                                            <div class="bg_title">
                                                <div class="title_wrapper"><a href="javascript:void(0)" class="btn_carousel_left"></a>
                                                    <h5 class="headInModule">Our Partners</h5>
                                                    <a href="javascript:void(0)" class="btn_carousel_right"></a></div>
                                            </div>
                                            <div class="module_content sponsors_works carouselslider items5" data-count="5">
                                                <ul>
                                                    <li>
                                                        <div class="item">
                                                            <a href="#charity-html5css3-website-template/3180454" target="_blank">
                                                                <img src="img/pictures/partners2.png" alt=""></a>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="item">
                                                            <a href="#point-business-responsive-wp-theme/4319087" target="_blank">
                                                                <img src="img/pictures/partners4.png" alt=""></a>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="item">
                                                            <a href="#cleanspace-retina-ready-business-wp-theme/3776000" target="_blank">
                                                                <img src="img/pictures/partners1.png" alt=""></a>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="item">
                                                            <a href="#yellowproject-multipurpose-retina-wp-theme/4066662" target="_blank">
                                                                <img src="img/pictures/partners3.png" alt=""></a>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="item">
                                                            <a href="#showroom-portfolio-retina-ready-wp-theme/3473628" target="_blank">
                                                                <img src="img/pictures/partners5.png" alt=""></a>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="item">
                                                            <a href="themeforest.net/item/incipiens-responsive-portfolio-wordpress-theme/2762691" target="_blank">
                                                                <img src="img/pictures/partners6.png" alt=""></a>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="item">
                                                            <a href="themeforest.net/item/hq-photography-responsive-wp-theme/3200962" target="_blank">
                                                                <img src="img/pictures/partners9.png" alt=""></a>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="item">
                                                            <a href="#incipiens-responsive-portfolio-wordpress-theme/2762691" target="_blank">
                                                                <img src="img/pictures/partners11.png" alt=""></a>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- .row-fluid -->

                                </div>
                                <!-- .contentarea -->
                            </div>
                            <div class="left-sidebar-block span3">
                                <aside class="sidebar">
                                    //Sidebar Text
                                </aside>
                            </div>
                            <!-- .left-sidebar -->
                        </div>
                        <div class="clear">
                            <!-- ClearFix -->
                        </div>
                    </div>
                    <!-- .fl-container -->
                    <div class="right-sidebar-block span3">
                        <aside class="sidebar">
                        </aside>
                    </div>
                    <!-- .right-sidebar -->
                    <div class="clear">
                        <!-- ClearFix -->
                    </div>
                </div>
            </div>
            <!-- .container -->
        </div>
        <!-- .content_wrapper -->

    </div>
    <!-- .main_wrapper -->
</asp:Content>

