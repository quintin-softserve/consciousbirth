
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.Services;
using System.Web.Script.Services;

/// <summary>
/// Summary description for clsColoursView
/// </summary>
public partial class CMS_clsColoursView : System.Web.UI.Page
{
    #region CLASSES AND VARIABLES

    clsUsers clsUsers;
    DataTable dtColoursList;

    List<clsColours> glstColours;

    #endregion

    #region EVENT MODULES

    protected void Page_Load(object sender, EventArgs e)
    {
        //### Check if session clsUser exists
        if (Session["clsUsers"] == null)
        {
            {
                //### Redirect back to login
                Response.Redirect("../CMSLogin.aspx");
            }
        }
        clsUsers = (clsUsers)Session["clsUsers"];

dgrGrid.ItemCreated += new DataGridItemEventHandler(clsCommonFunctions.dgrGrid_PagerPopulate);

        if (!Page.IsPostBack)
        {
            PopulateFormData();
        }
        else
        {
            if (Session["glstColours"] == null)
            {
                glstColours = new List<clsColours>();
                Session["glstColours"] = glstColours;
            }
            else
                glstColours = (List<clsColours>)Session["glstColours"];
        }
            PopulateFormData();

    }

    protected void lnkbtnSearch_Click(object sender, EventArgs e)
    {
        string FilterExpression ="(strTitle) LIKE '%" + txtSearch.Text + "%'";
        DataTable dtColoursList = clsColours.GetColoursList(FilterExpression, "");

        Session["dtColoursList"] = dtColoursList;

        PopulateFormData();
    }

    protected void lnkbtnAdd_Click(object sender, EventArgs e)
    {
        Response.Redirect("frmColoursAddModify.aspx");
    }

    #endregion

    #region DATA GRID METHODS

    private void PopulateFormData()
    {
        //### Populate datagrid using clsColoursList object
        try
        {
            dtColoursList = new DataTable();

            if (Session["dtColoursList"] == null)
                dtColoursList = clsColours.GetColoursList("","strTitle ASC");
            else
                dtColoursList = (DataTable)Session["dtColoursList"];

            dgrGrid.DataSource = dtColoursList;

            //### Add this check to trap if the last item on the page is deleted
            if (Convert.ToInt16(dgrGrid.CurrentPageIndex) < Convert.ToInt16(Session["ColoursView_CurrentPageIndex"]))
            {
                dgrGrid.CurrentPageIndex = 0;
            }
            else
            {
                if (Session["ColoursView_CurrentPageIndex"] != null)
                {
                    dgrGrid.CurrentPageIndex = (int)Session["ColoursView_CurrentPageIndex"];
                }
            }

            dgrGrid.DataBind();

        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    protected void lnkDeleteItem_Click(object sender, EventArgs e)
    {
        int iColourID = int.Parse((sender as LinkButton).CommandArgument);

        clsColours.Delete(iColourID);

        PopulateFormData();
    }

    protected void dgrGrid_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
    {
        //### Method is used when the page index is changed.
        try
        {
            Session["ColoursView_CurrentPageIndex"] = e.NewPageIndex;
            dgrGrid.CurrentPageIndex = e.NewPageIndex;
            PopulateFormData();
        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    protected void dgrGrid_SortCommand(object source, DataGridSortCommandEventArgs e)
    {
        //### Sort Method
        dtColoursList = new DataTable();

        if (Session["dtColoursList"] == null)
            dtColoursList = clsColours.GetColoursList();
        else
            dtColoursList = (DataTable)Session["dtColoursList"];

        DataView dvTemp = dtColoursList.DefaultView;

        dvTemp.Sort = e.SortExpression;
        dtColoursList = dvTemp.ToTable();
        Session["dtColoursList"] = dtColoursList;

        PopulateFormData();
    }

    public void dgrGrid_PopEditLink(object sender, EventArgs e)
    {
        try
        {
            //### Add hyperlink to datagrid item
            HyperLink lnkEditItem = (HyperLink)sender;
            DataGridItem dgrItemEdit = (DataGridItem)lnkEditItem.Parent.Parent;

            //### Get the iColourID
            int iColourID = 0;
            iColourID = int.Parse(dgrItemEdit.Cells[0].Text);

            //### Add attributes to edit link
            lnkEditItem.Attributes.Add("href", "frmColoursAddModify.aspx?action=edit&iColourID=" + iColourID);
        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

     public void dgrGrid_PopDeleteLink(object sender, EventArgs e)
     {
          try
          {
             //### Add hyperlink to datagrid item
             HyperLink lnkDeleteItem = (HyperLink)sender;
             DataGridItem dgrItemDelete = (DataGridItem)lnkDeleteItem.Parent.Parent;

             //### Get the iColourID
             int iColourID = 0;
             iColourID = int.Parse(dgrItemDelete.Cells[0].Text);

             //### Add attributes to delete link
             lnkDeleteItem.CssClass = "dgrDeleteLink";
             lnkDeleteItem.Attributes.Add("href", "frmColoursView.aspx?action=delete&iColourID=" + iColourID);
           }
           catch (Exception ex)
           {
                 Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
           }
     }

    #endregion

    #region Colours FILTERING

    /// <summary>
    /// Enables the AutoCompleteExtender
    /// </summary>
    /// <param name="prefixText"></param>
    /// <param name="count"></param>
    /// <returns></returns>
    /// 
    [WebMethod]
    [ScriptMethod]
    public static string[] FilterBusinessRule(string prefixText, int count)
    {
        string[] strReturnList = new string[] { };

        string FilterExpression = "(strTitle) LIKE '%" + prefixText + "%'";
        DataTable dtColours = clsColours.GetColoursList(FilterExpression, "");
        List<string> glstColours = new List<string>();

        if (dtColours.Rows.Count > 0)
        {
            foreach (DataRow dtrColours in dtColours.Rows)
            {
                glstColours.Add(AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem("" + dtrColours["strTitle"].ToString(), dtrColours["iColourID"].ToString()));
            }
        }
        else
            glstColours.Add("No Colours Available.");
        strReturnList = glstColours.ToArray();
        return strReturnList;
    }

    #endregion
}
