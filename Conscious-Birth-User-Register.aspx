﻿<%@ Page Title="Register :: Conscious Birth " Language="C#" MasterPageFile="~/ConsciousBirth.master" AutoEventWireup="true" CodeFile="Conscious-Birth-User-Register.aspx.cs" Inherits="web_Conscious_Birth_User_Register" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.6/jquery.min.js" type="text/javascript"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js" type="text/javascript"></script>
    <link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" rel="Stylesheet" type="text/css" />
    <script type="text/javascript">
        $(function () {
            $("[id$=txtDate]").datepicker({
                showOn: 'button',
                buttonImageOnly: true,
                buttonImage: 'img/datePicker.jpg'
            });
        });
    </script>
    <link href="web/css/style.css" rel="stylesheet" />
    <style>
        .module_feedback_form input[type=text],
        .module_feedback_form input[type=password],
        .module_feedback_form textarea {
            margin-bottom: 5px;
            width: 95%;
        }

        .hasDatepicker {
            width: 90%;
            float: left;
        }

        .ui-datepicker-trigger {
            float: right;
            width: 33px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="main_wrapper">
        <!-- C O N T E N T -->
        <div class="content_wrapper">
            <div class="page_title_block">
                <div class="container">
                    <h2 class="title">Register</h2>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="content_block no-sidebar row">
                <!-- .row-fluid -->
                <div class="row-fluid">
                    <div id="HideRegester" runat="server">
                        <div class="span7 module_cont module_feedback_form">
                            <br />
                            <div id="mandatoryDiv" class="mandatoryInvalidDiv" runat="server" visible="false">
                                <asp:Label ID="lblValidationMessage" runat="server"></asp:Label>
                            </div>

                            <div class="controlDiv">
                                <asp:RequiredFieldValidator ID="RequiredFieldValidatorName" runat="server" ErrorMessage="Required" ControlToValidate="txtName"></asp:RequiredFieldValidator>
                                <br class="clearingSpacer" />
                                <div class="labelDiv">Name:</div>
                                <div class="fieldDiv">
                                    <asp:TextBox Style="width: 95%;" ID="txtName" runat="server" CssClass="" onblur="" onKeyUp="return SetMaxLength(this,50)" />
                                </div>

                            </div>


                            <div class="controlDiv">
                                <asp:RequiredFieldValidator ID="RequiredFieldValidatortxtSurname" runat="server" ErrorMessage="Required" ControlToValidate="txtSurname"></asp:RequiredFieldValidator>
                                <br class="clearingSpacer" />
                                <div class="labelDiv">Surname:</div>
                                <div class="fieldDiv">
                                    <asp:TextBox ID="txtSurname" Style="width: 95%;" runat="server" CssClass="" onblur="" onKeyUp="return SetMaxLength(this,100)" />
                                </div>


                                <%-- <div class="controlDiv">
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Required" ControlToValidate="txtDate"></asp:RequiredFieldValidator>
                                <br class="clearingSpacer" />
                                <div class="labelDiv">Date of event:</div>
                                <div class="fieldDiv">
                                    <asp:TextBox ID="txtDate" Style="width:90%" runat="server" ReadOnly="true"></asp:TextBox>
                                </div>

                            </div>--%>
                            </div>

                            <div class="controlDiv">
                                <asp:RequiredFieldValidator ID="RequiredFieldValidatorEmail" runat="server" ErrorMessage="Required" ControlToValidate="txtEmail"></asp:RequiredFieldValidator>
                                <br />
                                <asp:RegularExpressionValidator ID="regexEmailValid" runat="server" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ControlToValidate="txtEmail" ErrorMessage="Invalid Email Address?"></asp:RegularExpressionValidator>
                                <br class="clearingSpacer" />
                                <div class="labelDiv">Email:</div>
                                <div class="fieldDiv">
                                    <asp:TextBox ID="txtEmail" Style="width: 95%;" runat="server" CssClass="" onblur="" onKeyUp="return SetMaxLength(this,80)" />
                                </div>

                                <asp:HiddenField ID="hfCanSave" runat="server" Value="false" />
                            </div>

                            <div id="divPassword" runat="server" class="controlDiv">
                                <asp:RequiredFieldValidator ID="RequiredFieldValidatorPassword" runat="server" ErrorMessage="Required" ControlToValidate="txtPassword"></asp:RequiredFieldValidator>
                                <br class="clearingSpacer" />
                                <div id="passwordLabel" class="labelDiv">Password:</div>
                                <div class="fieldDiv">
                                    <div id="passwordField" class="fieldDiv">
                                        <asp:TextBox ID="txtPassword" Style="width: 95%;" runat="server" CssClass="" TextMode="Password" onblur="" onKeyUp="return SetMaxLength(this,30)" />
                                    </div>
                                </div>

                            </div>

                            <div id="divConfirmPassword" runat="server" class="controlDiv">
                                <asp:CompareValidator ID="CompareValidatorPassword" runat="server" ControlToCompare="txtConfirmPassword" ControlToValidate="txtPassword" ErrorMessage="Password das not match?"></asp:CompareValidator>
                                <br class="clearingSpacer" />
                                <div id="confirmPasswordLabel" class="labelDiv">Confirm Password:</div>
                                <div id="confirmPasswordField" class="fieldDiv">
                                    <asp:TextBox ID="txtConfirmPassword" Style="width: 95%;" runat="server" CssClass="" TextMode="Password" onblur="" onKeyUp="return SetMaxLength(this,30)" />
                                </div>

                            </div>

                            <div class="controlDiv">
                                <asp:RequiredFieldValidator ID="RequiredFieldValidatorPhoneNumber" runat="server" ErrorMessage="Required" ControlToValidate="txtPhoneNumber"></asp:RequiredFieldValidator>
                                <br />
                                <asp:RegularExpressionValidator ID="RegularExpressionValidatorPhoneNumber" runat="server" ControlToValidate="txtPhoneNumber" ErrorMessage="Required?" ValidationExpression="[0-9]{10}"></asp:RegularExpressionValidator>
                                <div class="labelDiv">
                                    Phone Number: 
                            <div class="fieldDiv">
                                <asp:TextBox ID="txtPhoneNumber" Style="width: 95%;" runat="server" CssClass="" onblur="" onKeyUp="return SetMaxLength(this,80)" />

                            </div>
                                </div>
                                <br class="clearingSpacer" />
                            </div>

                            <br />

                            <asp:Button ID="btnSubmit" runat="server" Text="Submit" class="" OnClick="btnSubmit_Click1" />
                        </div>
                    </div>
                    <div id="addevent" visible="false" runat="server">
                        <div class="container">
                            <div class="content_block no-sidebar row">
                                <!-- .row-fluid -->
                                <div class="row-fluid">
                                    <div class="span7 module_cont module_feedback_form">
                                        <br />

                                        <div class="controlDiv">
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Required" ControlToValidate="txtName"></asp:RequiredFieldValidator>
                                            <br class="clearingSpacer" />
                                            <div class="labelDiv">Event Name:</div>
                                            <div class="fieldDiv">
                                                <asp:TextBox Style="width: 95%;" ID="TextBox1" runat="server" CssClass="" onblur="" onKeyUp="return SetMaxLength(this,50)" />
                                            </div>

                                        </div>
                                        <div class="controlDiv">
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Required" ControlToValidate="txtDate"></asp:RequiredFieldValidator>
                                            <br class="clearingSpacer" />
                                            <div class="labelDiv">Date of event:</div>
                                            <div class="fieldDiv">
                                                <asp:TextBox ID="txtDate" Style="width: 90%" runat="server" ReadOnly="true"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="controlDiv">
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Required" ControlToValidate="txtAddressOfEvent"></asp:RequiredFieldValidator>
                                            <br class="clearingSpacer" />
                                            <div class="labelDiv">Address of event:</div>
                                            <div class="fieldDiv">
                                                <asp:TextBox ID="txtAddressOfEvent" Style="width: 95%;" runat="server" CssClass="" onblur="" onKeyUp="return SetMaxLength(this,100)" />
                                            </div>
                                        </div>
                                    </div>
                                    <div style="clear:both"></div>
                                    <div class="controlDiv">
                                        <br class="clearingSpacer" />
                                        <div class="fieldDiv">
                                            <asp:Button ID="btnSubmitEvent" runat="server" Text="Submit" class="" OnClick="btnSubmitEvent_Click" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="left-sidebar-block span5">
                        <aside class="sidebar">
                        </aside>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>


