using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Data.SqlClient;

/// <summary>
/// Summary description for clsProductCategoriesLink
/// </summary>
public class clsProductCategoriesLink
{
    #region MEMBER VARIABLES

        private int m_iProductCategoryLinkID;
        private int m_iCategoryID;
        private int m_iProductID;
        private bool m_bIsDeleted;
        
    #endregion 

    #region PROPERTIES

        public int iProductCategoryLinkID
        {
            get
            {
                return m_iProductCategoryLinkID;
            }
        }

        public int iCategoryID
        {
            get
            {
                return m_iCategoryID;
            }
            set
            {
                m_iCategoryID = value;
            }
        }

        public int iProductID
        {
            get
            {
                return m_iProductID;
            }
            set
            {
                m_iProductID = value;
            }
        }

        public bool bIsDeleted
        {
            get
            {
                return m_bIsDeleted;
            }
            set
            {
                m_bIsDeleted = value;
            }
        }


    
    #endregion
    
    #region CONSTRUCTORS

    public clsProductCategoriesLink()
    {
        m_iProductCategoryLinkID = 0;
    }

    public clsProductCategoriesLink(int iProductCategoryLinkID)
    {
        m_iProductCategoryLinkID = iProductCategoryLinkID;
        GetData();
    }

    #endregion

    #region PUBLIC METHODS

        public virtual void Update()
        {
            try
            {
                if (iProductCategoryLinkID == 0)
                {
                    //### Assign values to the parameter list for each corresponding column in the DB
                    SqlParameter[] sqlParametersInsert = new SqlParameter[] 
                    {
                        new SqlParameter("@iCategoryID", m_iCategoryID),
                        new SqlParameter("@iProductID", m_iProductID)                  
                  };

                  //### Add
                  m_iProductCategoryLinkID = (int)clsDataAccess.ExecuteScalar("spProductCategoriesLinkInsert", sqlParametersInsert);                    
                    }
                    else
                    {
                    //### Assign values to the parameter list for each corresponding column in the DB
                    SqlParameter[] sqlParametersUpdate = new SqlParameter[] 
                    {
                         new SqlParameter("@iProductCategoryLinkID", m_iProductCategoryLinkID),
                         new SqlParameter("@iCategoryID", m_iCategoryID),
                         new SqlParameter("@iProductID", m_iProductID)
                    };
                    //### Update
                    clsDataAccess.Execute("spProductCategoriesLinkUpdate", sqlParametersUpdate);
                    }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void Delete(int iProductCategoryLinkID)
        {
        //### Populate
        SqlParameter[] sqlParameter = new SqlParameter[]
        {
            new SqlParameter("@iProductCategoryLinkID", iProductCategoryLinkID)
        };
        //### Executes delete sp
        clsDataAccess.Execute("spProductCategoriesLinkDelete", sqlParameter);
        }

        public static void Clear(int iProductID)
        {
            //### Populate
            SqlParameter[] sqlParameter = new SqlParameter[]
        {
            new SqlParameter("@iProductID", iProductID)
        };
            //### Executes delete sp
            clsDataAccess.Execute("spProductCategoriesLinkClear", sqlParameter);
        }

        public static DataTable GetProductCategoriesLinkList()
        {
            SqlParameter[] EmptySqlParameter = new SqlParameter[]{};
            return clsDataAccess.GetDataTable("spProductCategoriesLinkList", EmptySqlParameter);
        }

        public static DataTable GetProductCategoriesLinkList(string strFilterExpression, string strSortExpression)
        {
            DataView dvProductCategoriesLinkList = new DataView();

            SqlParameter[] EmptySqlParameter = new SqlParameter[] { };
            dvProductCategoriesLinkList = clsDataAccess.GetDataView("spProductCategoriesLinkList", EmptySqlParameter);
            dvProductCategoriesLinkList.RowFilter = strFilterExpression;
            dvProductCategoriesLinkList.Sort = strSortExpression;

            return dvProductCategoriesLinkList.ToTable();
        }
    #endregion

    #region PROTECTED METHODS

        protected virtual void GetData()
        {
            try
                {
                    //### Populate
                    SqlParameter[] sqlParameter = new SqlParameter[] 
                        {
                            new SqlParameter("@iProductCategoryLinkID", m_iProductCategoryLinkID)
                        };
                DataRow drRecord = clsDataAccess.GetRecord("spProductCategoriesLinkGetRecord", sqlParameter);

                m_iCategoryID = Convert.ToInt32(drRecord["iCategoryID"]);
                m_iProductID = Convert.ToInt32(drRecord["iProductID"]);
                m_bIsDeleted = Convert.ToBoolean(drRecord["bIsDeleted"]);
 
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    #endregion
}