﻿<%@ Page Title="Search for gift registry" Language="C#" MasterPageFile="~/ConsciousBirth.master" AutoEventWireup="true" CodeFile="Conscious-Birth-Gift-Registry.aspx.cs" Inherits="Conscious_Birth_Gift_Registry" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <%--    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <asp:UpdatePanel ID="udpOrderItemsView" runat="server" UpdateMode="Always">--%>
    <%--        <ContentTemplate>--%>
    <center>
                <div class='clr'>
                </div>
                <div style="width: 100%">
                    
                    <div class="feature" runat="server" id="divHeading" visible="true" style='margin-top: 20px;'>
                    <h2 class='headingText' style='margin-top: 15px;'> <asp:Label ID="labHeader" CssClass="headingText" Style="color:#464d52;font-weight:normal" runat="server" Text="Label"></asp:Label></h2>
                    <br />     
                    </div>
                    <div id="hideSearche" runat="server">
                        <div class="fieldDiv">
                        <asp:TextBox runat="server" ID="UserIdTexbox" CssClass="roundedCornerTextBoxMultiLine2" Height="12px" Width="397px" onblur="if(this.value=='')this.value=this.defaultValue;" value="Enter email address" onfocus="if(this.value==this.defaultValue)this.value='';"></asp:Textbox>
                        </div>
                        <div style='margin: 35px 0px 0px 0px;'>
                        <asp:Button ID='btnSurcheUser' runat="server" OnClick="btnSurcheUser_Click" Text="Search" />
                        </div>
                    </div>
                    <asp:Label ID="Labeleror" runat="server" Text=""></asp:Label>
                    <br />
                    <div id ="HideAcountData" visible="false" runat="server" >
                        <div class="container">
                            <div class="feature" runat="server" id="div1" visible="true">
                                <table cellpadding="3" cellspacing="0" class="dgr">
                                    <tr class="dgrHeader">
                                    <td align="center" width="200px">First Name
                                    </td>
                                    <td align="center" width="100px">Surname
                                    </td>
                                     <td align="center" width="100px">Event Name
                                    </td>
                                    <td align="center" width="100px">Date of Event
                                    </td>
                                    <td align="center" width="100px">Registry List
                                    </td>
                                    </tr>
                                    <asp:Repeater ID="rpAccountUsers" runat="server">
                                        <ItemTemplate>
                                            <tr>
                                                <td style="text-align:center ;border: none; height: 67px; width: 67px; padding-top: 5px;">
                                                    <%#Eval ("strFirstName") %>
                                                </td>
                                                <td style="text-align:center">
                                                    <%#Eval ("strSurname") %>
                                                </td>
                                                    <td style="text-align:center">
                                                    <%#Eval ("EventName") %>
                                                </td>
                                                <td style="text-align:center">
                                                    <%#Eval ("dtEvent", "{0:MM/dd/yyyy}") %>
                                                </td>
                                                <td style='text-align:center'>
                                                    <asp:LinkButton ID="linkbViewGiftRegestery" Style="color:white; border-radius: 5px;" OnClick="linkbViewGiftRegestery_Click" CommandArgument=<%#Eval ("iWishListEventID") %> CssClass="PienkButton" runat="server">View registry</asp:LinkButton>
                                                </td>
                                            </tr>
                                           
                                            
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div id="HideData" visible="false" runat="server">
                        <div class="container">
                                <div class="feature" runat="server" id="divGrid" visible="true">
                                    <table cellpadding="3" cellspacing="0" class="dgr">
                                        <tr class="dgrHeader">
                                            <td align="center" width="100px">&nbsp;
                                            </td>
                                            <td align="center" width="200px">Product Title
                                            </td>
                                             <td align="center" width="200px">Already Purchased
                                            </td>
                                            <td align="center" width="100px">Quantity
                                            </td>
                                            <td align="center" width="100px">Colour
                                            </td>
                                            <td align="center" width="100px">Size
                                            </td>
                                            <td align="center" width="100px">Total Cost
                                            </td>
                                            <td style="text-align:center; width:100px">Add to Cart
                                            </td>
                                        </tr>
                                      <asp:Literal runat="server" ID="litGridData"></asp:Literal>
                                    </table>
                                </div>
                            </div>
                         </div>
                </div>
                </center>
    <%--     </ContentTemplate>
    </asp:UpdatePanel>--%>

    <%--   <asp:UpdatePanel runat="server" ID="updStep2" UpdateMode="Always">
        <ContentTemplate>--%>
    <div style="width: 100%; margin-bottom: 30px;">
        <div class="feature" runat="server" id="step2" visible="false">
            <h2 class="headingText">Delivery Information</h2>
            <br />
            <div id="mandatoryDiv" class="mandatoryInvalidDiv" runat="server" visible="false">
                <asp:Label ID="lblValidationMessage" runat="server"></asp:Label>
            </div>
            <div class="controlDiv">
                <div class="imageHolderCommonDiv">
                    <div class="validationImageMandatory"></div>
                </div>
                <div class="labelDiv">
                    Name:
                </div>
                <div class="fieldDiv">
                    <asp:TextBox ID="txtContactPerson" runat="server" CssClass="roundedCornerTextBoxMini" onKeyUp="return SetMaxLength(this,50)" />
                </div>
                <br class="clear" />
            </div>
            <div class="controlDiv">
                <div class="imageHolderCommonDiv">
                    <div class="validationImageMandatory"></div>
                </div>
                <div class="labelDiv">Contact number:</div>
                <div class="fieldDiv">
                    <asp:TextBox ID="txtContactNumber" runat="server" CssClass="roundedCornerTextBoxMini" onKeyUp="return SetMaxLength(this,20)" onblur="CheckNumeric();" />
                </div>
                <br class="clear" />
            </div>
            <div class="controlDiv">
                <div class="imageHolderCommonDiv">
                    <div class="validationImageMandatory"></div>
                </div>
                <div class="labelDiv">
                    Email Address:
                </div>
                <div class="fieldDiv">
                    <asp:TextBox ID="txtEmailAddress" runat="server" CssClass="roundedCornerTextBoxMini" onKeyUp="return SetMaxLength(this,40)" onblur="emailValidator(this);" />
                </div>
            </div>
            <br class="clear" />
            <div class="controlDiv" style="margin-bottom: 16px;">
                <div class="imageHolderCommonDiv">
                    <div class="validationImageMandatory"></div>
                </div>
                <div class="labelDiv">
                    Physical delivery address:
                </div>
                <div class="fieldDiv">
                    <asp:TextBox ID="txtPhysicalAddress" runat="server" CssClass="roundedCornerTextBoxMultiLine2" Height="60px" Width="594px" onKeyUp="return SetMaxLength(this,250)" TextMode="MultiLine" Rows="2" />
                </div>
                <br class="clear" />
            </div>
            <div style='margin: 35px 0px 0px 0px;'>
                <%--<asp:Button ID='btnProceedToStep3' runat="server" OnClick="btnProceedToStep3_Click" Text="Process Order" />--%>
            </div>
        </div>
    </div>
    <%--   </ContentTemplate>
        <Triggers>
        </Triggers>
    </asp:UpdatePanel>--%>
</asp:Content>

