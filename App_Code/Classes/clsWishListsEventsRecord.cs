
using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Data.SqlClient;

/// <summary>
/// Summary description for clsWishListsEvents
/// </summary>
public class clsWishListsEvents
{
    #region MEMBER VARIABLES

    private int m_iWishListEventID;
    private DateTime m_dtAdded;
    private int m_iAddedBy;
    private DateTime m_dtEdited;
    private int m_iEditedBy;
    private int m_iAccountUserID;
    private string m_strEventName;
    private DateTime m_dtEvent;
    private string m_strAddressOfEvent;
    private bool m_blsDeleted;

    #endregion

    #region PROPERTIES

    public int iWishListEventID
    {
        get
        {
            return m_iWishListEventID;
        }
    }

    public DateTime dtAdded
    {
        get
        {
            return m_dtAdded;
        }
        set
        {
            m_dtAdded = value;
        }
    }

    public int iAddedBy
    {
        get
        {
            return m_iAddedBy;
        }
        set
        {
            m_iAddedBy = value;
        }
    }

    public DateTime dtEdited
    {
        get
        {
            return m_dtEdited;
        }
        set
        {
            m_dtEdited = value;
        }
    }

    public int iEditedBy
    {
        get
        {
            return m_iEditedBy;
        }
        set
        {
            m_iEditedBy = value;
        }
    }

    public int iAccountUserID
    {
        get
        {
            return m_iAccountUserID;
        }
        set
        {
            m_iAccountUserID = value;
        }
    }

    public string strEventName
    {
        get
        {
            return m_strEventName;
        }
        set
        {
            m_strEventName = value;
        }
    }

    public DateTime dtEvent
    {
        get
        {
            return m_dtEvent;
        }
        set
        {
            m_dtEvent = value;
        }
    }

    public string strAddressOfEvent
    {
        get
        {
            return m_strAddressOfEvent;
        }
        set
        {
            m_strAddressOfEvent = value;
        }
    }

    public bool blsDeleted
    {
        get
        {
            return m_blsDeleted;
        }
        set
        {
            m_blsDeleted = value;
        }
    }


    #endregion

    #region CONSTRUCTORS

    public clsWishListsEvents()
    {
        m_iWishListEventID = 0;
    }

    public clsWishListsEvents(int iWishListEventID)
    {
        m_iWishListEventID = iWishListEventID;
        GetData();
    }

    #endregion

    #region PUBLIC METHODS

    public virtual void Update()
    {
        try
        {
            if (iWishListEventID == 0)
            {
                //### Assign values to the parameter list for each corresponding column in the DB
                SqlParameter[] sqlParametersInsert = new SqlParameter[] 
                    {
                        new SqlParameter("@dtAdded", m_dtAdded),
                        new SqlParameter("@iAddedBy", m_iAddedBy),
                        new SqlParameter("@iAccountUserID", m_iAccountUserID),
                        new SqlParameter("@strEventName", m_strEventName),
                        new SqlParameter("@dtEvent", m_dtEvent),
                        new SqlParameter("@strAddressOfEvent", m_strAddressOfEvent),
                        new SqlParameter("@blsDeleted", m_blsDeleted)                  
                  };

                //### Add
                m_iWishListEventID = (int)clsDataAccess.ExecuteScalar("spWishListsEventsInsert", sqlParametersInsert);
            }
            else
            {
                //### Assign values to the parameter list for each corresponding column in the DB
                SqlParameter[] sqlParametersUpdate = new SqlParameter[] 
                    {
                         new SqlParameter("@iWishListEventID", m_iWishListEventID),
                         new SqlParameter("@dtEdited", m_dtEdited),
                         new SqlParameter("@iEditedBy", m_iEditedBy),
                         new SqlParameter("@iAccountUserID", m_iAccountUserID),
                         new SqlParameter("@strEventName", m_strEventName),
                         new SqlParameter("@dtEvent", m_dtEvent),
                         new SqlParameter("@strAddressOfEvent", m_strAddressOfEvent),
                         new SqlParameter("@blsDeleted", m_blsDeleted)
          };
                //### Update
                clsDataAccess.Execute("spWishListsEventsUpdate", sqlParametersUpdate);
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public static void Delete(int iWishListEventID)
    {
        //### Populate
        SqlParameter[] sqlParameter = new SqlParameter[]
        {
            new SqlParameter("@iWishListEventID", iWishListEventID)
        };
        //### Executes delete sp
        clsDataAccess.Execute("spWishListsEventsDelete", sqlParameter);
    }

    public static DataTable GetWishListsEventsList()
    {
        SqlParameter[] EmptySqlParameter = new SqlParameter[] { };
        return clsDataAccess.GetDataTable("spWishListsEventsList", EmptySqlParameter);
    }
    public static DataTable GetWishListsEventsList(string strFilterExpression, string strSortExpression)
    {
        DataView dvWishListsEventsList = new DataView();

        SqlParameter[] EmptySqlParameter = new SqlParameter[] { };
        dvWishListsEventsList = clsDataAccess.GetDataView("spWishListsEventsList", EmptySqlParameter);
        dvWishListsEventsList.RowFilter = strFilterExpression;
        dvWishListsEventsList.Sort = strSortExpression;

        return dvWishListsEventsList.ToTable();
    }
    #endregion

    #region PROTECTED METHODS

    protected virtual void GetData()
    {
        try
        {
            //### Populate
            SqlParameter[] sqlParameter = new SqlParameter[] 
                        {
                            new SqlParameter("@iWishListEventID", m_iWishListEventID)
                        };
            DataRow drRecord = clsDataAccess.GetRecord("spWishListsEventsGetRecord", sqlParameter);

            m_dtAdded = Convert.ToDateTime(drRecord["dtAdded"]);
            m_iAddedBy = Convert.ToInt32(drRecord["iAddedBy"]);

            if (drRecord["dtEdited"] != DBNull.Value)
                m_dtEdited = Convert.ToDateTime(drRecord["dtEdited"]);

            if (drRecord["iEditedBy"] != DBNull.Value)
                m_iEditedBy = Convert.ToInt32(drRecord["iEditedBy"]);

            m_iAccountUserID = Convert.ToInt32(drRecord["iAccountUserID"]);
            m_dtEvent = Convert.ToDateTime(drRecord["dtEvent"]);
            m_strAddressOfEvent = drRecord["strAddressOfEvent"].ToString();

        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    #endregion
}