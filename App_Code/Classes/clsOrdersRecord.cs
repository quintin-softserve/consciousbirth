using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Data.SqlClient;

/// <summary>
/// Summary description for clsOrders
/// </summary>
public class clsOrders
{
    #region MEMBER VARIABLES

        private int m_iOrderID;
        private DateTime m_dtAdded;
        private int m_iAddedBy;
        private DateTime m_dtEdited;
        private int m_iEditedBy;
        private int m_iOrderStatusID;
        private int m_iDeliveryInfoID;
        private String m_strOrderNumber;
        private double m_dblTotalAmount;
        private double m_dblDeliveryCost;
        private bool m_bIsDeleted;
        
    #endregion 

    #region PROPERTIES

        public int iOrderID
        {
            get
            {
                return m_iOrderID;
            }
        }

        public DateTime dtAdded
        {
            get
            {
                return m_dtAdded;
            }
            set
            {
                m_dtAdded = value;
            }
        }

        public int iAddedBy
        {
            get
            {
                return m_iAddedBy;
            }
            set
            {
                m_iAddedBy = value;
            }
        }

        public DateTime dtEdited
        {
            get
            {
                return m_dtEdited;
            }
            set
            {
                m_dtEdited = value;
            }
        }

        public int iEditedBy
        {
            get
            {
                return m_iEditedBy;
            }
            set
            {
                m_iEditedBy = value;
            }
        }

        public int iOrderStatusID
        {
            get
            {
                return m_iOrderStatusID;
            }
            set
            {
                m_iOrderStatusID = value;
            }
        }

        public int iDeliveryInfoID
        {
            get
            {
                return m_iDeliveryInfoID;
            }
            set
            {
                m_iDeliveryInfoID = value;
            }
        }

        public String strOrderNumber
        {
            get
            {
                return m_strOrderNumber;
            }
            set
            {
                m_strOrderNumber = value;
            }
        }

        public double dblTotalAmount
        {
            get
            {
                return m_dblTotalAmount;
            }
            set
            {
                m_dblTotalAmount = value;
            }
        }

        public double dblDeliveryCost
        {
            get
            {
                return m_dblDeliveryCost;
            }
            set
            {
                m_dblDeliveryCost = value;
            }
        }

        public bool bIsDeleted
        {
            get
            {
                return m_bIsDeleted;
            }
            set
            {
                m_bIsDeleted = value;
            }
        }

    
    #endregion
    
    #region CONSTRUCTORS

    public clsOrders()
    {
        m_iOrderID = 0;
    }

    public clsOrders(int iOrderID)
    {
        m_iOrderID = iOrderID;
        GetData();
    }

    #endregion

    #region PUBLIC METHODS

        public virtual void Update()
        {
            try
            {
                if (iOrderID == 0)
                {
                    //### Assign values to the parameter list for each corresponding column in the DB
                    SqlParameter[] sqlParametersInsert = new SqlParameter[] 
                    {
                        new SqlParameter("@dtAdded", m_dtAdded),
                        new SqlParameter("@iAddedBy", m_iAddedBy),
                        new SqlParameter("@iOrderStatusID", m_iOrderStatusID),
                        new SqlParameter("@iDeliveryInfoID", m_iDeliveryInfoID),
                        new SqlParameter("@strOrderNumber", m_strOrderNumber),
                        new SqlParameter("@dblTotalAmount", m_dblTotalAmount),
                        new SqlParameter("@dblDeliveryCost", m_dblDeliveryCost)                  
                  };

                  //### Add
                  m_iOrderID = (int)clsDataAccess.ExecuteScalar("spOrdersInsert", sqlParametersInsert);                    
                    }
                    else
                    {
                    //### Assign values to the parameter list for each corresponding column in the DB
                    SqlParameter[] sqlParametersUpdate = new SqlParameter[] 
                    {
                         new SqlParameter("@iOrderID", m_iOrderID),
                         new SqlParameter("@dtEdited", m_dtEdited),
                         new SqlParameter("@iEditedBy", m_iEditedBy),
                         new SqlParameter("@iOrderStatusID", m_iOrderStatusID),
                         new SqlParameter("@iDeliveryInfoID", m_iDeliveryInfoID),
                         new SqlParameter("@strOrderNumber", m_strOrderNumber),
                         new SqlParameter("@dblTotalAmount", m_dblTotalAmount),
                         new SqlParameter("@dblDeliveryCost", m_dblDeliveryCost)
                    };
                    //### Update
                    clsDataAccess.Execute("spOrdersUpdate", sqlParametersUpdate);
                    }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void Delete(int iOrderID)
        {
        //### Populate
        SqlParameter[] sqlParameter = new SqlParameter[]
        {
            new SqlParameter("@iOrderID", iOrderID)
        };
        //### Executes delete sp
        clsDataAccess.Execute("spOrdersDelete", sqlParameter);
        }

        public static DataTable GetOrdersList()
        {
            SqlParameter[] EmptySqlParameter = new SqlParameter[]{};
            return clsDataAccess.GetDataTable("spOrdersList", EmptySqlParameter);
        }
        public static DataTable GetOrdersList(string strFilterExpression, string strSortExpression)
        {
            DataView dvOrdersList = new DataView();

            SqlParameter[] EmptySqlParameter = new SqlParameter[] { };
            dvOrdersList = clsDataAccess.GetDataView("spOrdersList", EmptySqlParameter);
            dvOrdersList.RowFilter = strFilterExpression;
            dvOrdersList.Sort = strSortExpression;

            return dvOrdersList.ToTable();
        }
    #endregion

    #region PROTECTED METHODS

        protected virtual void GetData()
        {
            try
                {
                    //### Populate
                    SqlParameter[] sqlParameter = new SqlParameter[] 
                        {
                            new SqlParameter("@iOrderID", m_iOrderID)
                        };
                DataRow drRecord = clsDataAccess.GetRecord("spOrdersGetRecord", sqlParameter);

                m_dtAdded = Convert.ToDateTime(drRecord["dtAdded"]);
                m_iAddedBy = Convert.ToInt32(drRecord["iAddedBy"]);

                if (drRecord["dtEdited"] != DBNull.Value)
                   m_dtEdited = Convert.ToDateTime(drRecord["dtEdited"]);

             if (drRecord["iEditedBy"] != DBNull.Value)
               m_iEditedBy = Convert.ToInt32(drRecord["iEditedBy"]);

                m_iOrderStatusID = Convert.ToInt32(drRecord["iOrderStatusID"]);
                m_iDeliveryInfoID = Convert.ToInt32(drRecord["iDeliveryInfoID"]);
                m_strOrderNumber = drRecord["strOrderNumber"].ToString();
                m_dblTotalAmount = Convert.ToDouble(drRecord["dblTotalAmount"]);
                m_dblDeliveryCost = Convert.ToDouble(drRecord["dblDeliveryCost"]);
                m_bIsDeleted = Convert.ToBoolean(drRecord["bIsDeleted"]);
 
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    #endregion
}