﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Conscious_Birth_Wishlist : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        int iWishlistID = Convert.ToInt32(Session["clsAccountUsers"]);
        if (!IsPostBack)
        {

            if (Request.QueryString["action"] == "delete")
            {
                int iUserProductWishlistLinkID = Convert.ToInt32(Request.QueryString["iUserProductWishlistLinkID"]);
                clsUserProductWishlistLink.Delete(iUserProductWishlistLinkID);
                 PopulateWishlist(Convert.ToInt32(Session["clsWishlistEvents"]));
            }
            else
            {
                divGrid.Visible = true;
                divTotalAndButtons.Visible = true;
                PopulateWishlist(Convert.ToInt32(Session["clsWishlistEvents"]));
            }
        }

        if (iWishlistID == null || iWishlistID == 0)
        {
            //### Redirect back to login
            Response.Redirect("Conscious-Birth-User-Login.aspx");
        }

    }
    private void PopulateWishlist(int iWishlistID)
    {
        double CartTotal = 0;
        StringBuilder sbGrid = new StringBuilder();
        DataTable dtUserWishlistLink = clsUserProductWishlistLink.GetUserProductWishlistLinkList("iWishListEventID='" + iWishlistID + "'", "");
        int iProductID = 0;
        foreach (DataRow dtrUserWishlistLink in dtUserWishlistLink.Rows)
        {

            iProductID = Convert.ToInt32(dtrUserWishlistLink["iProductID"]);
            clsProducts clsProducts = new clsProducts(iProductID);
            double total = Convert.ToInt32(dtrUserWishlistLink["iQuantity"]) * clsProducts.dblPrice;
            CartTotal = CartTotal + total;
            sbGrid.Append("<tr>");
            sbGrid.Append("<td style='text-align:center'>");
            sbGrid.Append("<img src= '" + getProductImage(Convert.ToInt32(iProductID)) + "' style='border: none; height: 120px; width: 120px; padding-top: 5px;' />");
            sbGrid.Append("</td>");
            sbGrid.Append("<td style='text-align:center'>");
            sbGrid.Append("<div style='padding-left:10px; padding-right: 10px;'>");
            sbGrid.Append("<span style='font-size: 18px;'>" + clsProducts.strTitle + "</span></div>");
            sbGrid.Append("<br/>");
            sbGrid.Append("</td>");
            sbGrid.Append("<td style='text-align:center'>" + dtrUserWishlistLink["iQuantity"] + "</td>");
            sbGrid.Append("<td style='text-align:center'>" + dtrUserWishlistLink["strColor"] + "</td>");
            sbGrid.Append("<td style='text-align:center'>" + dtrUserWishlistLink["strSize"] + "</td>");
            sbGrid.Append("<td style='text-align:center'>" + total + "</td>");
            sbGrid.Append("<td style='text-align:center'>");
            sbGrid.Append(" <a href='Conscious-Birth-WishList-Cart.aspx?action=delete&iUserProductWishlistLinkID=" + dtrUserWishlistLink["iUserProductWishlistLinkID"] + "'class='dgrLinkDelete'></a>");
            sbGrid.Append("</td>");
            sbGrid.Append("</tr>");
        }
        if (iProductID == 0)
        {
            labText.Text = " Your Wishlist is currently empty. <a href = 'Conscious-Birth-Products.aspx'>Click here to add a Product.</a>";
            HideData.Visible = false;
        }
        else
        {
            HideData.Visible = true;
            labText.Text = "You currently have the following items in added to your Wishlist:";
            litWishlist.Text = sbGrid.ToString();
            lblGridTotal.Text = CartTotal.ToString();
        }

    }
    protected string getProductImage(int iProductID)
    {
        clsProducts clsProducts = new clsProducts(Convert.ToInt32(iProductID));

        string strHTMLImageProduct = "";

        try
        {
            string[] strImagesProduct = Directory.GetFiles(ConfigurationManager.AppSettings["WebRootFullPath"] + "\\Products\\" + clsProducts.strPathToImages);

            foreach (string strImageProduct in strImagesProduct)
            {
                if (strImageProduct.Contains("_sml"))
                {
                    strHTMLImageProduct = strImageProduct.Replace(ConfigurationManager.AppSettings["WebRootFullPath"], "").Replace("\\", "/").Substring(1);
                    break;
                }
            }
        }
        catch
        {
        }

        return strHTMLImageProduct;
    }

    protected void linkDeliet_Click(object sender, EventArgs e)
    {
        LinkButton btn = (LinkButton)(sender);
        int iProductID = Convert.ToInt32(btn.CommandArgument);
        int iUserID = 0;

        DataTable dtWishlist = clsUserProductWishlistLink.GetUserProductWishlistLinkList("iProductID='" + iProductID + "'", "");
        foreach (DataRow dtrWishlist in dtWishlist.Rows)
        {
            iUserID = Convert.ToInt32(dtrWishlist["iUsersWishlistLinkID"]);
        }
        clsUserProductWishlistLink.Delete(iUserID);

        int iWishlistID = Convert.ToInt32(Session["clsAccountUsers"]);
        if (iWishlistID == null || iWishlistID == 0)
        {
            //### Redirect back to login
            Response.Redirect("Conscious-Birth-User-Login.aspx");
        }
        else
        {
            divGrid.Visible = true;
            divTotalAndButtons.Visible = true;
            PopulateWishlist(iWishlistID);
        }
    }
}