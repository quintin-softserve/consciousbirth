﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Conscious-Birth-Product-Info.aspx.cs" Inherits="Conscious_Birth_Product_Info" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Conscious Birth - Products</title>

    <meta charset="utf-8" />
    <meta name="description" content="" />
    <meta name="keywords" content="" />

    <!-- Mobile Specific Metas
    +++++++++++++++++++++++++++ -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <link rel="shortcut icon" href="img/consiousBirth.ico" />
    <%--<link rel="apple-touch-icon" href="img/apple_icons_57x57.png" />
    <link rel="apple-touch-icon" sizes="72x72" href="img/apple_icons_72x72.png" />
    <link rel="apple-touch-icon" sizes="114x114" href="img/apple_icons_114x114.png" />--%>

    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800,400italic,600italic,700italic' rel='stylesheet' type='text/css' />
    <link href='http://fonts.googleapis.com/css?family=Yellowtail' rel='stylesheet' type='text/css' />
    <link href='http://fonts.googleapis.com/css?family=Lobster' rel='stylesheet' type='text/css' />

    <link rel="stylesheet" type="text/css" href="css/bs_grid.css" />
    <link rel="stylesheet" type="text/css" href="css/theme.css" />
    <link rel="stylesheet" type="text/css" href="css/plugins.css" />
    <link rel="stylesheet" type="text/css" href="css/responsive.css" />
    <link rel="stylesheet" type="text/css" href="css/theme_settings.css" />
    <link rel="stylesheet" type="text/css" href="css/color_theme.css" id="theme_color" />

    <script type="text/javascript" src="js/jquery.min.js"></script>
    <link href="web/css/etalage.css" rel="stylesheet" />
    <script src="web/js/jquery.etalage.min.js"></script>

    <script type="text/javascript" src="js/move-top.js"></script>
    <script type="text/javascript" src="js/easing.js"></script>
    <script type="text/javascript">
        jQuery(document).ready(function ($) {
            $(".scroll").click(function (event) {
                event.preventDefault();
                $('html,body').animate({ scrollTop: $(this.hash).offset().top }, 1200);
            });
        });
    </script>
    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date(); a = s.createElement(o),
            m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-53160457-1', 'auto');
        ga('send', 'pageview');
    </script>
    <script>
        jQuery(document).ready(function ($) {

            $('#etalage').etalage({
                thumb_image_width: 315,
                thumb_image_height: 315,

                show_hint: true,
                click_callback: function (image_anchor, instance_id) {
                    alert('Callback example:\nYou clicked on an image with the anchor: "' + image_anchor + '"\n(in Etalage instance: "' + instance_id + '")');
                }
            });
            // This is for the dropdown list example:
            $('.dropdownlist').change(function () {
                etalage_show($(this).find('option:selected').attr('class'));
            });

        });
    </script>

    <style>
        .cbp-qtrotator {
            width: 300px;
            height: 180px;
            position: relative;
            float: left;
            margin-left: -104px;
            padding-top: 11px;
        }

        .cbp-qtcontent {
            width: 100%;
            height: auto;
            position: absolute;
            min-height: 180px;
            top: 0;
            z-index: 2;
            display: none;
        }

            .cbp-qtcontent span {
                color: #ff33cc;
            }


        .cbp-qtrotator .cbp-qtcontent.current {
            display: block;
        }

        .cbp-qtrotator blockquote {
            margin: 40px 0 0 0;
            padding: 0;
        }

            .cbp-qtrotator blockquote p {
                font-size: 2em;
                color: #888;
                font-weight: 300;
                margin: 0.4em 0 1em;
            }

            .cbp-qtrotator blockquote footer {
                font-size: 1.2em;
            }

                .cbp-qtrotator blockquote footer:before {
                    content: '― ';
                }

        .cbp-qtrotator .cbp-qtcontent img {
            float: right;
            margin: 50px 0 0 50px;
        }

        .cbp-qtprogress {
            position: absolute;
            background: #47a3da;
            height: 1px;
            width: 0%;
            z-index: 1000;
        }

        .cartPosition {
            z-index: 10000;
            position: fixed;
            right: 0px;
            top: 64px;
            width: 48px;
            height: 48px;
            background: url('img/imgCart.png') no-repeat 0px 0px;
        }

            .cartPosition:hover {
                background: url('img/imgCart.png') no-repeat 0px -48px;
            }
    </style>

</head>
<body>
    <form id="form1" runat="server">
        <header class="type6">
            <section class="type2">
                <div class=" container">
                    <div class="call_us">Call now: +27 (83) 229 3253</div>
                    <div class="slogan">Facilitating confident living</div>
                </div>
                <div class="clear"></div>
            </section>


            <div class="header_wrapper container">
                <a href="Conscious-Birth-Home.aspx" class="logo">
                    <img src="img/img-Consious-Birth-Logo.png" width="200" height="auto" class="logo_def" alt="Logo" title="Logo" />
                    <!--<img src="img/Logo_pink6.png" alt="" width="150" height="auto" class="logo_def"> -->
                    <img src="img/Logo_pink6.png" alt="" width="120" height="auto" class="logo_retina">
                    <%--<br />
                    <span>Conscious Birth</span>--%>
                </a>
                <nav>
                    <ul class="menu" style="text-align: center; float: none;">
                        <%--                    <li class="current-menu-parent">
                        <a href="Conscious-Birth-Home.aspx">HOME</a>
                    </li>--%>
                        <li><a href="Conscious-Birth-About.aspx">ABOUT</a>
                            <ul class="sub-menu">
                                <li><a href="Conscious-Birth-welcome.aspx">What is Conscious Birth?</a></li>
                                <%--   <li><a href="Conscious-Birth-Events.aspx">Event</a></li>--%>
                                <li><a href="Conscious-Birth-Join-Community-Tab.aspx">Join Community Tab</a></li>
                                <li><a href="Conscious-Birth-Google-Calendar.aspx">Calendar</a></li>
                                <li><a href="Conscious-Birth-Schedule-An-Appointment.aspx">Schedule An Appointment</a></li>
                                <li><a href="Conscious-Birth-Gift-Voucher.aspx">Gift Voucher</a></li>
                                <%--<li><a href="Conscious-Birth-Blog.aspx">Blog</a></li>--%>
                                <li><a href="Conscious-Birth-Blissful-Sessions.aspx">Blissful Sessions</a></li>
                                <%--   <li><a href="Conscious-Birth-FAQ.aspx">FAQ</a></li>--%>
                                <li><a href="Conscious-Birth-Modalities-And-Tools.aspx">Modalities & Tools</a></li>
                                <li><a href="Conscious-Birth-History.aspx">Our History</a></li>
                                <li><a href="Conscious-Birth-Testimonial.aspx">Testimonials</a></li>
                                <%--<li><a href="wall.html">Outcomes</a></li>                            
                            <li><a href="full-width.html">Stories &amp; Testimonials</a></li>--%>
                            </ul>
                        </li>
                        <%--<li>
                            <a href="Conscious-Birth-Blog.aspx">BLOG</a>
                        </li>--%>
                        <li><a href="javascript:void(0)">PREGNANCY</a>
                            <ul class="sub-menu">
                                <li><a href="Conscious-Birth-Hypnobirthing.aspx">Hypnobirthing®</a></li>
                                <li><a href="Conscious-Birth-Pregnancy-Yoga.aspx">Pregnancy Yoga</a></li>
                                <li><a href="Conscious-Birth-Pregnancy-Yoga-Workshops.aspx">Pregnancy Yoga Workshops</a></li>
                                <li><a href="Conscious-Birth-Blissful-Pregnancy.aspx">Blissful Pregnancy</a></li>
                                <li><a href="Conscious-Birth-Kahuna-Pregnancy-Massage.aspx">Pregnancy Kahuna Massage Experience</a></li>
                                <li><a href="Conscious-Birth-Blessing-Way.aspx">Blessing Way</a></li>
                                <li><a href="Conscious-Birth-Belly-Casting.aspx">Belly Casting</a></li>
                                <li><a href="Conscious-Birth-Birth-Release.aspx">Birth Release</a></li>
                                <li><a href="Conscious-Birth-Breech-Baby.aspx">Breech Baby</a></li>
                                <li><a href="Conscious-Birth-Pregnancy-Yoga-For-Teachers.aspx">Pregnancy Yoga For Teachers</a></li>
                                <li><a href="Conscious-Birth-Caesarian-Section-Preparation.aspx">Caesarian Section Preparation</a></li>
                                <li><a href="Conscious-Birth-Resources-For-pregnancy.aspx">Resources For Pregnancy</a></li>
                            </ul>
                        </li>
                        <li><a href="javascript:void(0)">MOTHERS & BABIES</a>
                            <ul class="sub-menu">
                                <li><a href="Conscious-Birth-Blissful-Mother.aspx">Blissful Mother</a></li>
                                <li><a href="Conscious-Birth-Postnatal-Massage.aspx">Postnatal Massage & Support</a></li>
                                <li><a href="Conscious-Birth-Kahuna-Massage-Experience-For-Mothers-And-Babies.aspx">Kahuna Massage Experience</a></li>
                                <li><a href="Conscious-Birth-Polynesian-Floor-Treatment-For-Mothers-And-Babies.aspx">Polynesian Floor Treatment</a></li>
                                <li><a href="Conscious-Birth-Moms-and-Babes-Yoga.aspx">Moms And Babes Yoga</a></li>
                                <li><a href="Conscious-Birth-Baby-Naming-Ceremonies.aspx">Baby Naming Ceremony</a></li>
                                <li><a href="Conscious-Birth-Mothering-Resources.aspx">Mothering Resources</a></li>
                                <li><a href="javascript:void(0)">WOMEN</a>
                                    <ul class="sub-menu">
                                        <li><a href="Conscious-Birth-Blissful-Women.aspx">Blissful Woman</a></li>
                                        <li><a href="Conscious-Birth-Kahuna-Massage-Experience-For-Women.aspx">Kahuna Massage Experience</a></li>
                                        <li><a href="Conscious-Birth-Polynesian-Floor-Treatment-For-Women.aspx">Polynesian floor treatment</a></li>
                                        <li><a href="Conscious-Birth-Womens-Resources.aspx">Woman Resources</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </li>


                        <li><a href="#">SHOP</a>
                            <ul class="sub-menu">
                                <li><a href="Conscious-Birth-Category-List.aspx">Categories</a></li>
                                <li><a href="Conscious-Birth-Products.aspx">View all Products</a></li>
                            </ul>
                        </li>
                        <li><a href="#">MY ACCOUNT</a>
                            <ul class="sub-menu">
                                <li><a href="Conscious-Birth-User-Login.aspx">
                                    <asp:Label Style="color: white" ID="LoginLabel" runat="server" Text=""></asp:Label></a></li>
                                <li><a href="Conscious-Birth-User-Register.aspx">Register</a></li>
                                <li><a href="Conscious-Birth-Gift-Registry.aspx">Search for a Gift Registry</a></li>
                                <li><a href="Conscious-Birth-My-Gift-Registry.aspx">View my Gift Registry</a></li>
                                <li><a href="Conscious-Birth-Cart.aspx">My Cart</a></li>

                            </ul>
                        </li>
                        <li>
                            <a href="Conscious-Birth-Contact.aspx">CONTACT</a>
                        </li>
                    </ul>
                    <!-- .menu -->
                    <div class="clear"></div>
                </nav>
                <div class="call_us">Call us now: +27 (83) 229 3253</div>
                <div class="socials">
                    <ul class="socials_list">
                        <li><a href="https://www.facebook.com/pages/Conscious-Birth/164361116909927" target="_blank" class="ico_social-facebook"></a></li>
                        <%--  <li><a href="#" class="ico_social-twitter"></a></li>--%>
                        <li><a href="#" class="ico_social-linked"></a></li>
                        <li><a href="https://www.pinterest.com/theonisharonpap/" target="_blank" class="ico_social-pinterest"></a></li>
                        <li><a href="https://www.youtube.com/channel/UCxW7BRiZx5PaJg2P7itX-Dw" target="_blank" class="ico_social-youtube" target="_blank"></a></li>
                        <li><a href="https://plus.google.com/u/0/116275672665544159572/posts" target="_blank" class="ico_social-gplus" target="_blank"></a></li>
                    </ul>
                </div>
                <!-- .social -->
            </div>

        </header>
        <asp:ScriptManager runat="server" ID="scrManager"></asp:ScriptManager>
        <div class="main_wrapper">
            <div class="content_wrapper">
                <div class="page_title_block">
                    <div class="container">
                        <h2 class="titleHead">Product Details</h2>
                    </div>
                </div>
                <%--        <div class="header-bottom">
            <div class="wrap">
                <!-- start header menu -->
                <div class="clear"></div>
            </div>
        </div>--%>
                <div class="container">
                    <div class="content_block no-sidebar row">
                        <div class="fl-container span12">

                            <div class="row">
                                <div class="posts-block span12">
                                    <center> 
                                       <div id="mandatoryDiv" class="mandatoryInvalidDiv" runat="server" visible="false">
                                                <asp:Label ID="lblValidationMessage" runat="server" Font-Size="20px" ForeColor="Red"></asp:Label>
                                            </div>

                                   </center>
                                    <div class="contentarea">
                                        <div class="row-fluid">

                                            <asp:Literal ID="litProducts" runat="server"></asp:Literal>
                                            <div class="clear"></div>
                                            <asp:UpdatePanel runat="server" ID="updP">
                                                <ContentTemplate>
                                                    <h7>Quantity:</h7>
                                                    <asp:TextBox runat="server" AutoPostBack="true" ID="txtQuantity" Width="35px" TextMode="Number" Text="1" BackColor="White"></asp:TextBox>
                                                    <br />
                                                    <br />
                                                    <div runat="server" id="colourDiv">
                                                        <h7>Select colour:</h7>
                                                        <asp:DropDownList runat="server" ID="ddlColor" AutoPostBack="true"></asp:DropDownList>
                                                    </div>
                                                    <br />
                                                    <div runat="server" id="sizeDiv">
                                                        <h7>Select size:</h7>
                                                        <asp:DropDownList runat="server" ID="ddlSizes" AutoPostBack="true"></asp:DropDownList>
                                                    </div>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                            <br />
                                            <br />
                                        </div>
                                    </div>
                                    <div class='span5 module_cont module_text_area module_none_padding' style="margin-left: 0px;">
                                        <div class='tn_form'>
                                            <div class='form' style="float: left; margin-right: 10px;">
                                                <asp:Button ID="btnBuyNow" runat="server" Text="buy now" OnClick="btnBuyNow_Click" OnClientClick="btnBuyNow_Click" />
                                            </div>
                                        </div>
                                        <div class='tn_form'>
                                            <div class='form'>
                                                <asp:Button ID="AddTOWISHLIST" runat="server" Text="add to WishList" OnClick="btnAddToWishlist_Click" OnClientClick="btnAddToWishlist_Click" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row-fluid"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <a href="#" id="toTop" style="display: block;"><span id="toTopHover" style="opacity: 1;"></span></a>
        </div>
        <div class="pre_footer">

            <div class="container">
                <aside id="footer_bar" class="row">
                    <div class="span3">
                        <div class="sidepanel widget_twitter">
                            <div class="bg_title">
                                <h6 class="sidebar_header">Contact Info</h6>
                            </div>
                            <ul class="tweet_list twitter_list tweet_1057">
                                <li>
                                    <span class="tweet_time">Phone: +27 (83) 229 3253</span>
                                </li>
                                <li>
                                    <span class="tweet_time">Email: <a href='mailto:theoni@consciousbirth.co.za'>theoni@consciousbirth.co.za</a></span>
                                </li>
                            </ul>
                            <script>
                                /*$(document).ready(function(){
                                    $('.tweet_1057').tweet({
                                        modpath: 'twitter/',
                                        count: 3,
                                        username : ''
                                     });
                                });*/
                            </script>
                        </div>
                        <!-- .widget_twitter -->
                    </div>
                    <%--<div class="span3">
                        <div class="sidepanel widget_twitter">
                            <div class="bg_title">
                                <h6 class="sidebar_header">Latest Tweets</h6>
                            </div>
                            <ul class="tweet_list twitter_list tweet_1057">
                                <li>
                                    <span class="tweet_time">Aptent taciti sociosqu litora torquente per vestibulum eget vestibulum egestas perestas.</span>
                                </li>
                                <li>
                                    <span class="tweet_time">Fusce nisi erat, ultrices in tincidunt id, sodales vitae sem proin sit amet. <a href="#">@hendrerit</a></span>
                                </li>
                                <li>
                                    <span class="tweet_time">Curabitur vitae lectus lacus, ut pulvinar justots vivamus metus dolor, ullamcorper adipiscing suscipit non, vulputate duis augue mi, egestas sit amet facilisis sed! rutrum sed <a href="#">@ac_purus</a></span>
                                </li>
                            </ul>
                            <script>
                                /*$(document).ready(function(){
                                    $('.tweet_1057').tweet({
                                        modpath: 'twitter/',
                                        count: 3,
                                        username : ''
                                     });
                                });*/
                            </script>
                        </div>
                        <!-- .widget_twitter -->
                    </div>--%>
                    <div class="span4">
                        <div class="sidepanel widget_posts">
                            <div class="bg_title">
                                <h6 class="sidebar_header">Testimonials</h6>
                            </div>
                            <ul class="recent_posts">
                                <li>
                                    <div id="cbp-qtrotator" class="cbp-qtrotator">
                                        <asp:Literal ID="litTestimonial" runat="server"></asp:Literal>
                                    </div>
                                    <div class="clear"></div>
                                </li>
                            </ul>
                        </div>
                        <!-- .sidepanel -->
                    </div>

                    <div class="span5">
                        <div class="sidepanel widget_mailchimpsf_widget">
                            <div class="bg_title">
                                <h6 class="sidebar_header">Join Our Mailing List</h6>
                            </div>
                            <div id="mandatoryDiv1" class="mandatoryInvalidDiv" runat="server" visible="false">
                                <asp:Label ID="Label1" runat="server"></asp:Label>
                            </div>
                            <div class="mc_form_inside">
                                <div class="mc_merge_var">
                                    <asp:TextBox runat="server" class="mc_input" ID="txtName" placeholder="Name" size="18"></asp:TextBox>
                                    <%--<asp:RequiredFieldValidator ID="rfvName" runat="server" ControlToValidate="txtName" ErrorMessage="Please enter name"></asp:RequiredFieldValidator>--%>
                                </div>
                                <div class="mc_merge_var">
                                    <asp:TextBox runat="server" class="mc_input" ID="txtEmail" placeholder="Email" size="18"></asp:TextBox>
                                    <%--<asp:RequiredFieldValidator ID="rfvEmail" ControlToValidate="txtEmail" runat="server" ErrorMessage="Please enter email"></asp:RequiredFieldValidator>--%>
                                </div>
                                <!-- /mc_merge_var -->
                                <div class="mc_signup_submit">
                                    <asp:Button ID="btnSubscribeNow" runat="server" Text="Join Now!" class="mc_submit" OnClick="btnSubscribeNow_Click" />
                                </div>
                                <!-- /mc_signup_submit -->
                            </div>
                        </div>
                        <!-- .sidepanel -->
                    </div>

                </aside>
            </div>
        </div>
        <!-- .pre_footer -->
        <footer>
            <div class="footer_line container">
                <div class="socials">
                    <ul class="socials_list">
                        <li><a href="https://www.facebook.com/pages/Conscious-Birth/164361116909927" class="ico_social-facebook" target="_blank"></a></li>
                        <%--   <li><a href="#" class="ico_social-twitter"></a></li>--%>
                        <li><a href="#" class="ico_social-linked"></a></li>
                        <li><a href="#" class="ico_social-pinterest"></a></li>
                        <li><a href="https://www.youtube.com/channel/UCxW7BRiZx5PaJg2P7itX-Dw" class="ico_social-youtube" target="_blank"></a></li>
                        <li><a href="https://plus.google.com/u/0/116275672665544159572/posts" class="ico_social-gplus" target="_blank"></a></li>
                    </ul>
                </div>
                <div class="copyright">
                    &copy; 2015 Conscious Birth | All Rights Reserved.<br />
                    Designed and developed by <a href="http://www.softservedigital.co.za" target="_blank">Softserve Digital Development</a>
                </div>
                <a href="javascript:void(0)" class="btn2top"></a>
                <div class="clear"></div>
            </div>
        </footer>
        <a href="Conscious-Birth-Cart.aspx" class="cartPosition"></a>
    </form>
    <script src="http://code.jquery.com/ui/1.10.2/jquery-ui.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="js/theme.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            //Quotes rotator
            var divs = $('.cbp-qtcontent');

            function fade() {
                var current = $('.current');
                var currentIndex = divs.index(current),
                nextIndex = currentIndex + 1;

                if (nextIndex >= divs.length) {
                    nextIndex = 0;
                }

                var next = divs.eq(nextIndex);

                next.stop().fadeIn(1500, function () {
                    $(this).addClass('current');
                });

                current.stop().fadeOut(1500, function () {
                    $(this).removeClass('current');
                    _startProgress()
                    setTimeout(fade, 10000);
                });
            }

            function _startProgress() {
                $(".cbp-qtprogress").removeAttr('style');
                $(".cbp-qtprogress").animate({
                    width: "800px",
                }, 10000);
            }

            _startProgress()
            setTimeout(fade, 10000);
        });
    </script>
</body>
</html>
