
using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Data.SqlClient;

/// <summary>
/// Summary description for clsOrderStatuses
/// </summary>
public class clsOrderStatuses
{
    #region MEMBER VARIABLES

        private int m_iOrderStatusID;
        private DateTime m_dtAdded;
        private int m_iAddedBy;
        private DateTime m_dtEdited;
        private int m_iEditedBy;
        private String m_strTitle;
        private bool m_bIsDeleted;
        
    #endregion 

    #region PROPERTIES

        public int iOrderStatusID
        {
            get
            {
                return m_iOrderStatusID;
            }
        }

        public DateTime dtAdded
        {
            get
            {
                return m_dtAdded;
            }
            set
            {
                m_dtAdded = value;
            }
        }

        public int iAddedBy
        {
            get
            {
                return m_iAddedBy;
            }
            set
            {
                m_iAddedBy = value;
            }
        }

        public DateTime dtEdited
        {
            get
            {
                return m_dtEdited;
            }
            set
            {
                m_dtEdited = value;
            }
        }

        public int iEditedBy
        {
            get
            {
                return m_iEditedBy;
            }
            set
            {
                m_iEditedBy = value;
            }
        }

        public String strTitle
        {
            get
            {
                return m_strTitle;
            }
            set
            {
                m_strTitle = value;
            }
        }

        public bool bIsDeleted
        {
            get
            {
                return m_bIsDeleted;
            }
            set
            {
                m_bIsDeleted = value;
            }
        }

    
    #endregion
    
    #region CONSTRUCTORS

    public clsOrderStatuses()
    {
        m_iOrderStatusID = 0;
    }

    public clsOrderStatuses(int iOrderStatusID)
    {
        m_iOrderStatusID = iOrderStatusID;
        GetData();
    }

    #endregion

    #region PUBLIC METHODS

        public virtual void Update()
        {
            try
            {
                if (iOrderStatusID == 0)
                {
                    //### Assign values to the parameter list for each corresponding column in the DB
                    SqlParameter[] sqlParametersInsert = new SqlParameter[] 
                    {
                        new SqlParameter("@dtAdded", m_dtAdded),
                        new SqlParameter("@iAddedBy", m_iAddedBy),
                        new SqlParameter("@strTitle", m_strTitle)                  
                  };

                  //### Add
                  m_iOrderStatusID = (int)clsDataAccess.ExecuteScalar("spOrderStatusesInsert", sqlParametersInsert);                    
                    }
                    else
                    {
                    //### Assign values to the parameter list for each corresponding column in the DB
                    SqlParameter[] sqlParametersUpdate = new SqlParameter[] 
                    {
                         new SqlParameter("@iOrderStatusID", m_iOrderStatusID),
                         new SqlParameter("@dtEdited", m_dtEdited),
                         new SqlParameter("@iEditedBy", m_iEditedBy),
                         new SqlParameter("@strTitle", m_strTitle)
                    };
                    //### Update
                    clsDataAccess.Execute("spOrderStatusesUpdate", sqlParametersUpdate);
                    }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void Delete(int iOrderStatusID)
        {
        //### Populate
        SqlParameter[] sqlParameter = new SqlParameter[]
        {
            new SqlParameter("@iOrderStatusID", iOrderStatusID)
        };
        //### Executes delete sp
        clsDataAccess.Execute("spOrderStatusesDelete", sqlParameter);
        }

        public static DataTable GetOrderStatusesList()
        {
            SqlParameter[] EmptySqlParameter = new SqlParameter[]{};
            return clsDataAccess.GetDataTable("spOrderStatusesList", EmptySqlParameter);
        }
        public static DataTable GetOrderStatusesList(string strFilterExpression, string strSortExpression)
        {
            DataView dvOrderStatusesList = new DataView();

            SqlParameter[] EmptySqlParameter = new SqlParameter[] { };
            dvOrderStatusesList = clsDataAccess.GetDataView("spOrderStatusesList", EmptySqlParameter);
            dvOrderStatusesList.RowFilter = strFilterExpression;
            dvOrderStatusesList.Sort = strSortExpression;

            return dvOrderStatusesList.ToTable();
        }
    #endregion

    #region PROTECTED METHODS

        protected virtual void GetData()
        {
            try
                {
                    //### Populate
                    SqlParameter[] sqlParameter = new SqlParameter[] 
                        {
                            new SqlParameter("@iOrderStatusID", m_iOrderStatusID)
                        };
                DataRow drRecord = clsDataAccess.GetRecord("spOrderStatusesGetRecord", sqlParameter);

                m_dtAdded = Convert.ToDateTime(drRecord["dtAdded"]);
                m_iAddedBy = Convert.ToInt32(drRecord["iAddedBy"]);

                if (drRecord["dtEdited"] != DBNull.Value)
                   m_dtEdited = Convert.ToDateTime(drRecord["dtEdited"]);

             if (drRecord["iEditedBy"] != DBNull.Value)
               m_iEditedBy = Convert.ToInt32(drRecord["iEditedBy"]);

                m_strTitle = drRecord["strTitle"].ToString();
                m_bIsDeleted = Convert.ToBoolean(drRecord["bIsDeleted"]);
 
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    #endregion
}