<%@ Page Title="WishListsEvents" Language="C#" MasterPageFile="~/CMS/CMS.master" AutoEventWireup="true" CodeFile="frmWishListsEventsAddModify.aspx.cs" Inherits="CMS_frmWishListsEventsAddModify" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <div id="mandatoryDiv" class="mandatoryInvalidDiv" runat="server" visible="false">
        <asp:Label ID="lblValidationMessage" runat="server"></asp:Label>
    </div>

     <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="validationImageMandatory"></div></div>
         <div class="labelDiv">WishListEvent:</div>
         <div class="fieldDiv">
             <asp:DropDownList ID="lstWishListEvent" runat="server" CssClass="roundedCornerDropDownList" onblur="setValid(this, 0);"></asp:DropDownList>
         </div>
         <br class="clearingSpacer" />
     </div>
     <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="validationImageMandatory"></div></div>
         <div class="labelDiv">AccountUser:</div>
         <div class="fieldDiv">
             <asp:DropDownList ID="lstAccountUser" runat="server" CssClass="roundedCornerDropDownList" onblur="setValid(this, 0);"></asp:DropDownList>
         </div>
         <br class="clearingSpacer" />
     </div>
     <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="validationImageMandatory"></div></div>
         <div class="labelDiv">Event:</div>
         <div class="fieldDiv">
             <img src="images/calendar.png" id="EventDateImg" alt="Select a date" title="Select a date" runat="server" class="calendarPosition"/>
                 <asp:TextBox ID="txtEvent" runat="server" CssClass="roundedCornerTextBoxMini"></asp:TextBox>
             <cc1:CalendarExtender ID="EventDateCal" runat="server" PopupButtonID="EventDateImg" TargetControlID="txtEvent" Format="dd MMM yyyy"></cc1:CalendarExtender>
         </div>
         <br class="clearingSpacer" />
     </div>

     <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="dummyHolder"></div></div>
         <div class="labelDiv">Address Of Event:</div>
         <div class="fieldDiv">
             <asp:TextBox ID="txtAddressOfEvent" runat="server" CssClass="roundedCornerTextBoxMultiLine8" onKeyUp="return SetMaxLength(this,1500)" TextMode="MultiLine" Rows="8" />
         </div>
         <br class="clearingSpacer" />
     </div>


    
    <div class="Line"></div>
    <div class="buttonsRightDiv">
        <asp:LinkButton ID="lnkbtnBack" runat="server" CssClass="backButton" onclick="lnkbtnBack_Click" OnClientClick='history.go(-1);return false;' />
        <asp:LinkButton ID="lnkbtnSave" runat="server" CssClass="saveButton" onclick="lnkbtnSave_Click" />
        <asp:LinkButton ID="lnkbtnClear" runat="server" CssClass="clearButton" onclick="lnkbtnClear_Click" />
    </div>
</asp:Content>
