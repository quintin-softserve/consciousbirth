﻿<%@ Page Title="Blissful Mother :: Conscious Birth" Language="C#" MasterPageFile="~/ConsciousBirth.master" AutoEventWireup="true" CodeFile="Conscious-Birth-Blissful-Mother.aspx.cs" Inherits="Conscious_Birth_Blissful_Mother" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript" src="js/jquery.min.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="main_wrapper">
        <!-- C O N T E N T -->
        <div class="content_wrapper">
            <div class="page_title_block">
                <div class="container">
                    <h2 class="title">Blissful mother </h2>
                </div>
            </div>
            <div class="container">
                <div class="content_block no-sidebar row">
                    <div class="fl-container span12">
                        <div class="row">
                            <div class="posts-block span12">
                                <div class="contentarea">

                                    <div class="row-fluid">
                                        <div class="span4 module_cont module_text_area module_none_padding">
                                            <img src="img/Blissful-mother.png" alt="" />
                                        </div>
                                        <div class="span8 module_cont module_text_area module_none_padding">
                                            <blockquote class="type2">
                                                <h6>What this is and who it is for:</h6>
                                                <br />
                                                <p>
                                                    When one becomes a mother life changes irrevocably. Some changes are easy to handle and some require extra adjustment. 
                                                    Blissful mother is about nurturing the mother in you and helping her deal with all the complex nuances that come with 
                                                    being a mother. The blissful mother journey is for all mothers who may need some guidance in dealing with any issue to 
                                                    do with motherhood. Some situations (certainly not all!) that can occur in your life when you are a mother may include:
                                                </p>
                                                <br />
                                                <ul>
                                                    <li><p>Redefining your sense of self as well as redefining other roles you may fulfill, such as wife, lover, friend, working woman or non-working woman.</p></li>
                                                    <li><p>Finding a new way of being as you leave previously comfortable modes of being to embark on a redefinition of self.</p></li>
                                                    <li><p>Finding confidence in your new roles or regaining lost confidence.</p></li>
                                                    <li><p>Accepting and embracing how your engagement with the world around you has changed as a result of becoming a mother.</p></li>
                                                    <li><p>Dealing with the numerous important decisions that need to be made regarding your baby, yourself and your family.</p></li>
                                                    <li><p>Learning how to manage your time now that it is spread even more thinly across your responsibilities.</p></li>
                                                    <li><p>Accommodating this new person in your life whilst still accommodating yourself and others.</p></li>
                                                    <li><p>Reassessing your value system now that you have another person’s well being to consider.</p></li>
                                                    <li><p>Finding creative way to go back to work and still care for your baby, or finding it within yourself to leave work behind – depending on your circumstance.</p></li>
                                                    <li><p>Finding balance between all the pressures competing for your attention.</p></li>
                                                    <li><p>When your children begin to become more independent you may need to redefine yourself once more as they begin to discover life more fully and without your constant presence.</p></li>
                                                    <li><p>Working on being the kind of mother you really want to be.</p></li>
                                                    <li><p>Dealing with the overflow of information and opinions on how to mother your child or children.</p></li>
                                                    <li><p>Locating and becoming comfortable with your own style of parenting that suits you and your family. </p></li>
                                                    <li><p>Finding your own stride as a mother and having the confidence to trust your instincts and intuition.</p></li>
                                                    <li><p>Finding strength to stand up for yourself and your child.</p></li>
                                                </ul>
                                                <br />
                                                <h6>What to expect and benefits:</h6>
                                                <p>
                                                    When consulting Theoni about your role as a mother you can expect compassionate care, professional advice and practical strategies for the challenges you face. The benefit of consulting 
                                                    with Theoni regarding any challenge you may have with motherhood is that you will be engaging with someone who is, first and foremost, a mother. But over and above all her experience 
                                                    as a mother she has also been engaging with mothers for more than two decades and has guided them as they embrace this role.
                                                </p>
                                            </blockquote>
                                            <br />
                                            <div class="sectionContainerLeft innerBoxWithShadow">
                                                <div class="span12 module_cont module_text_area module_none_padding">
                                                    <blockquote class="type2">
                                                        <p>
                                                            Thanks so much Theoni for all that you did for me after having my  daughter Kenzi. The birth and the 4 months after were very hard emotionally and 
                                                            physically.  I felt very scared, overwhelmed and quite down, that was until I met you just per chance for a massage.  It was only after chatting 
                                                            to you and being helped to process all that had happened to Kenzi and I, that I felt ready to start enjoying being a mother, stop feeling guilty 
                                                            for all that she had to endure, to stop feeling angry for what I endured, and to actually have fun with Kenzi and realise what a precious gift she 
                                                            really was to both my husband and I. I would still be stuck in a black whirlpool of  emotions if it were not for you helping me to climb out. 
                                                            I really do appreciate your support and empathy,  I really hope that every first time mom will get in touch with you to help them out on that 
                                                            wobbly road starting motherhood. 
                                                        </p>
                                                        <div class="author italic">Bianca mother to Kenzi</div>
                                                    </blockquote>
                                                </div>
                                                <br class="clear" />
                                            </div>
                                        </div>
                                    </div>
                                    <!-- .row-fluid -->
                                    <div class="module_line_trigger" data-background="#ffeec9 url(img/VV.png) no-repeat center" data-top-padding="bottom_padding_huge" data-bottom-padding="module_big_padding">
                                        <div class="row-fluid">
                                            <div class="span12 module_cont module_promo_text module_huge_padding">
                                                <div class="shortcode_promoblock">
                                                    <div class="promo_button_block type2">
                                                        <a href="Conscious-Birth-Products.aspx" class="promo_button"> View our Products </a>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--.module_cont -->
                                            <div class="clear">
                                                <!-- ClearFIX -->
                                            </div>
                                        </div>
                                        <!-- .row-fluid -->
                                    </div>
                                    <!-- .module_line_trigger -->
                                    <!--<div class="row-fluid">
                                        <div class="span12 module_cont module_partners center_title module_big_padding2">
                                            <div class="bg_title">
                                                <div class="title_wrapper">
                                                    <a href="javascript:void(0)" class="btn_carousel_left"></a>
                                                    <h5 class="headInModule">Our Partners</h5>
                                                    <a href="javascript:void(0)" class="btn_carousel_right"></a>
                                                </div>
                                            </div>
                                            <div class="module_content sponsors_works carouselslider items5" data-count="5">
                                                <ul>
                                                    <li>
                                                        <div class="item">
                                                            <a href="#charity-html5css3-website-template/3180454" target="_blank">
                                                                <img src="img/pictures/partners2.png" alt=""></a>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="item">
                                                            <a href="#point-business-responsive-wp-theme/4319087" target="_blank">
                                                                <img src="img/pictures/partners4.png" alt=""></a>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="item">
                                                            <a href="#cleanspace-retina-ready-business-wp-theme/3776000" target="_blank">
                                                                <img src="img/pictures/partners1.png" alt=""></a>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="item">
                                                            <a href="#yellowproject-multipurpose-retina-wp-theme/4066662" target="_blank">
                                                                <img src="img/pictures/partners3.png" alt=""></a>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="item">
                                                            <a href="#showroom-portfolio-retina-ready-wp-theme/3473628" target="_blank">
                                                                <img src="img/pictures/partners5.png" alt=""></a>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="item">
                                                            <a href="themeforest.net/item/incipiens-responsive-portfolio-wordpress-theme/2762691" target="_blank">
                                                                <img src="img/pictures/partners6.png" alt=""></a>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="item">
                                                            <a href="themeforest.net/item/hq-photography-responsive-wp-theme/3200962" target="_blank">
                                                                <img src="img/pictures/partners9.png" alt=""></a>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="item">
                                                            <a href="#incipiens-responsive-portfolio-wordpress-theme/2762691" target="_blank">
                                                                <img src="img/pictures/partners11.png" alt=""></a>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- .row-fluid -->

                                </div>
                                <!-- .contentarea -->
                            </div>
                            <div class="left-sidebar-block span3">
                                <aside class="sidebar">
                                    //Sidebar Text
                                </aside>
                            </div>
                            <!-- .left-sidebar -->
                        </div>
                        <div class="clear">
                            <!-- ClearFix -->
                        </div>
                    </div>
                    <!-- .fl-container -->
                    <div class="right-sidebar-block span3">
                        <aside class="sidebar">
                        </aside>
                    </div>
                    <!-- .right-sidebar -->
                    <div class="clear">
                        <!-- ClearFix -->
                    </div>
                </div>
            </div>
            <!-- .container -->
        </div>
        <!-- .content_wrapper -->

    </div>
    <!-- .main_wrapper -->
</asp:Content>

