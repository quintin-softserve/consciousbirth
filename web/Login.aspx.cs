﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Data.SqlClient;
using System.Net.Mail;

public partial class Login : System.Web.UI.Page
{
    clsAccountUsers clsAccountUsers;

    #region EVENT METHODS

    protected void Page_Load(object sender, EventArgs e)
    {
        txtUsername.Focus();
      
        //### Logout User
        if (Request.QueryString["action"] == "logout")
        {
            //### Clear all session variables
            Session.Clear();
        }
    }

    protected void lnkbtnForgottenPassword_Click(object sender, EventArgs e)
    {
        lnkbtnForgottenPassword.Visible = false;
        lnkbtnBackToLogin.Visible = true;
        divPassword.Visible = false;
        lnkbtnLogin.Visible = false;
        lnkbtnSubmit.Visible = true;
    }

    protected void lnkbtnBackToLogin_Click(object sender, EventArgs e)
    {
        lnkbtnForgottenPassword.Visible = true;
        lnkbtnBackToLogin.Visible = false;
        divPassword.Visible = true;
        lnkbtnLogin.Visible = true;
        lnkbtnSubmit.Visible = false;
    }

    protected void lnkbtnLogin_Click(object sender, EventArgs e)
    {
        try
        {
            clsAccountUsers = new clsAccountUsers(txtUsername.Text, clsCommonFunctions.GetMd5Sum(txtPassword.Text));

            Session["clsAccountUsers"] = clsAccountUsers;

            Response.Redirect("Client-Portal.aspx");
        }
        catch
        {
            litValidationMessage.Text = "Invalid Login Credentials.";

            if ((txtPassword.Text == "") || (txtPassword.Text == null))
            {
                litValidationMessage.Text = "Login requires a Password.";
            }
            else if ((txtUsername.Text == "") || (txtUsername.Text == null))
            {
                litValidationMessage.Text = "Login requires a username.";
            }

            if ((txtUsername.Text == "") && (txtPassword.Text == ""))
            {
                litValidationMessage.Text = "Login requires a username and matching password.";
            }

            Alert.Attributes["class"] = "alert alert-danger";
            litAlertHeading.Text = "Error";
        }
    }

    protected void lnkbtnSubmit_Click(object sender, EventArgs e)
    {
        //### Validate email
        if (txtUsername.Text == "")
        {
            litValidationMessage.Text = "Please enter your email address.";
            Alert.Attributes["class"] = "alert alert-danger";
            litAlertHeading.Text = "Error";
        }
        else
        {
            //### Check if email address exists
            if (clsCommonFunctions.DoesRecordExist("tblAccountUsers", "strPrimaryContactEmail = '" + txtUsername.Text + "' AND bIsDeleted = 0") == true)
            {
                //### Change password & Send mail
                string strRandomPassword = "";
                string strPassword = "";
                strRandomPassword = clsCommonFunctions.strCreateRandomPassword(8);
                strPassword = strRandomPassword;

                //### Hash random password
                strRandomPassword = clsCommonFunctions.GetMd5Sum(strPassword);

                ForgottenPassword(txtUsername.Text, strRandomPassword);

                StringBuilder strbMailBuilder = new StringBuilder();

                strbMailBuilder.AppendLine("<!-- Start of textbanner -->");
                strbMailBuilder.AppendLine("<table width='100%' bgcolor='#fff' cellpadding='0' cellspacing='0' border='0' id='backgroundTable' movable=''>");
                strbMailBuilder.AppendLine("<tbody>");
                strbMailBuilder.AppendLine("<tr>");
                strbMailBuilder.AppendLine("<td>");
                strbMailBuilder.AppendLine("<table bgcolor='#ffffff' width='650' cellpadding='0' cellspacing='0' border='0' align='center' class='devicewidth' options=''>"); //style='border-left:1px solid #333; border-right:1px solid #333;'
                strbMailBuilder.AppendLine("<tbody>");
                strbMailBuilder.AppendLine("<!-- Spacing -->");
                strbMailBuilder.AppendLine("<tr>");
                strbMailBuilder.AppendLine("<td height='50'></td>");
                strbMailBuilder.AppendLine("</tr>");
                strbMailBuilder.AppendLine("<!-- End of Spacing -->");
                strbMailBuilder.AppendLine("<tr>");
                strbMailBuilder.AppendLine("<td>");
                strbMailBuilder.AppendLine("<table width='100%' cellspacing='0' cellpadding='0'>");
                strbMailBuilder.AppendLine("<tbody>");

                strbMailBuilder.AppendLine("<!-- Content -->");
                strbMailBuilder.AppendLine("<tr>");
                strbMailBuilder.AppendLine("<td width='60'></td>");
                strbMailBuilder.AppendLine("<td valign='top' style='font-family: Helvetica, Arial, sans-serif;font-size: 14px; color: #333; text-align:left;line-height: 22px;' text=''>");
                strbMailBuilder.AppendLine("Dear User<br />");
                strbMailBuilder.AppendLine("Your password has been reset.<br />");
                strbMailBuilder.AppendLine("Your temporary password is shown below:<br />");
                strbMailBuilder.AppendLine("Password: " + strPassword + "<br/>");
                strbMailBuilder.AppendLine("</td>");
                strbMailBuilder.AppendLine("<td width='60'></td>");
                strbMailBuilder.AppendLine("</tr>");
                strbMailBuilder.AppendLine("<!-- End of Content -->");

                strbMailBuilder.AppendLine("</td>");
                strbMailBuilder.AppendLine("<td width='20'></td>");
                strbMailBuilder.AppendLine("</tr>");
                strbMailBuilder.AppendLine("</tbody>");
                strbMailBuilder.AppendLine("</table>");
                strbMailBuilder.AppendLine("</td>");
                strbMailBuilder.AppendLine("</tr>");
                strbMailBuilder.AppendLine("<!-- Spacing -->");
                strbMailBuilder.AppendLine("<tr>");
                strbMailBuilder.AppendLine("<td height='50'></td>");
                strbMailBuilder.AppendLine("</tr>");
                strbMailBuilder.AppendLine("<!-- End of Spacing -->");
                strbMailBuilder.AppendLine("</tbody>");
                strbMailBuilder.AppendLine("</table>");
                strbMailBuilder.AppendLine("</td>");
                strbMailBuilder.AppendLine("</tr>");
                strbMailBuilder.AppendLine("</tbody>");
                strbMailBuilder.AppendLine("</table>");
                strbMailBuilder.AppendLine("<!-- End of textbanner -->");

                //strbMailBuilder.AppendLine("Dear User<br /><br />");
                //strbMailBuilder.AppendLine("Your password has been reset.<br /><br />");
                //strbMailBuilder.AppendLine("Your temporary password is shown below:<br /><br />");
                //strbMailBuilder.AppendLine("Password: " + txtUsername.Text + "<br/>");

                string strContent = strbMailBuilder.ToString();

                //DataConnection.GetDataObject().ExecuteScalar("spForgottenPassword '" + txtUsername.Text + "', '" + strRandomPassword + "'");
                //emailComponent.SendMail("noreply@gobundu.co.za", txtUsername.Text, "", "", "Forgotten Password", strContent, true);

                Attachment[] empty = new Attachment[] { };

                try
                {
                    clsCommonFunctions.SendMail("no-reply@consciousbirth.co.za", txtUsername.Text, "", "", "Forgotten Password", strContent, empty, true);
                }
                catch { }

                //### Redirect
                litValidationMessage.Text = "Your temporary password has been sent.";
                Alert.Attributes["class"] = "alert alert-success";
                litAlertHeading.Text = "Success";
            }
            else
            {
                //### If no email address found
                litValidationMessage.Text = "The email you have entered is not a registered email address.";
                Alert.Attributes["class"] = "alert alert-dismissable";
                litAlertHeading.Text = "Sorry";
            }
        }
    }

    #endregion

    #region PASSWORD METHODS

    public static void ForgottenPassword(string strEmail, string strRandomPassword)
    {
        //### Populate
        try
        {
            SqlParameter[] sqlParameter = new SqlParameter[]
            {
                new SqlParameter("@strPrimaryContactEmail", strEmail),
                new SqlParameter("@strRandomPassword", strRandomPassword)
            };
            //### Executes Password Reset sp
            clsDataAccess.Execute("[spForgottenClientPassword]", sqlParameter);
        }
        catch
        {
        }
    }

    #endregion
}