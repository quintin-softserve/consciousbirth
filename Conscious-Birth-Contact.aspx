﻿<%@ Page Title="Contact :: Conscious Birth" Language="C#" MasterPageFile="~/ConsciousBirth.master" AutoEventWireup="true" CodeFile="Conscious-Birth-Contact.aspx.cs" Inherits="Conscious_Birth_Contact" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link rel="stylesheet" type="text/css" href="css/style.css" />
    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date(); a = s.createElement(o),
            m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-53160457-1', 'auto');
        ga('send', 'pageview');

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="main_wrapper">
        <!-- C O N T E N T -->
        <div class="content_wrapper">
            <div class="page_title_block">
                <div class="container">
                    <h2 class="title">Contacts</h2>
                </div>
            </div>
            <div class="container">
                <div class="content_block no-sidebar row">
                    <div class="fl-container span12">
                        <!-- <div class="row">
                            <div class="posts-block span12">
                                <div class="contentarea">
                                    <div class="blog_post">
                                        <!-- <div class="blog_head">
                                            <span class="blogpost_type_ico post_type_text"></span>
                                            <h1>Pregnancy Yoga - Atholl Road</h1>
                                            <div class="blogpost_meta">
                                                <span class="blog_date">Day: Tuesday, late morning</span><br />
                                                <span class="blog_date">Time: 9:30 to 11:00</span><br />
                                                <span class="blog_date">Venue: 121 Atholl Road, Atholl</span><br />
                                            </div>

                                            <div class="blogpost_share">
                                                <a href="#" class="ico_socialize_facebook2 ico_socialize type1"></a>
                                                <a href="#" class="ico_socialize_twitter2 ico_socialize type1"></a>
                                                <a href="#" class="ico_socialize_pinterest ico_socialize type1"></a>
                                                <a href="#" class="ico_socialize_google2 ico_socialize type1"></a>
                                            </div>
                                            <div class="row-fluid">
                                            </div>
                                            <div class="span12 module_cont module_google_map module_big_padding2">
                                                <iframe class="fullwidth" width="870" height="390" src="https://www.google.com/maps/embed?pb=!1m24!1m12!1m3!1d3582.2643326386897!2d28.069995!3d-26.122922999999705!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!4m9!1i0!3e0!4m0!4m5!1s0x1e950d214acb1a39%3A0x567a45a930c7cb4a!2s121+Atholl+Rd%2C+Johannesburg+2196!3m2!1d-26.12269!2d28.06994!5e0!3m2!1sen!2sza!4v1403253889315"></iframe>
                                            </div>
                                            <!-- .module_cont -->
                        <!-- </div> -->
                        <!-- </div> -->
                        <!-- <div class="blog_post">
                                        <div class="blog_head">
                                            <span class="blogpost_type_ico post_type_image"></span>
                                            <h1>Pregnancy Yoga - Roosevelt Park</h1>
                                            <div class="blogpost_meta">
                                                <span class="blog_date">Day: Saturday morning</span><br />
                                                <span class="blog_date">Time: 8:30 to 10:00</span><br />
                                                <span class="blog_date">Venue: 106 Beyers Naude Drive, Roosevelt Park(few blocks Linden)</span><br />
                                            </div>

                                            <div class="blogpost_share">
                                                <a href="#" class="ico_socialize_facebook2 ico_socialize type1"></a>
                                                <a href="#" class="ico_socialize_twitter2 ico_socialize type1"></a>
                                                <a href="#" class="ico_socialize_pinterest ico_socialize type1"></a>
                                                <a href="#" class="ico_socialize_google2 ico_socialize type1"></a>
                                            </div>
                                            <div class="row-fluid">
                                            </div>
                                            <div class="span12 module_cont module_google_map module_big_padding2">
                                                <iframe class="fullwidth" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d7162.613699407989!2d27.998933699348143!3d-26.15413433200041!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x1e950b0d1c2d5b1f%3A0xad4fce8e6100dac5!2s106+Beyers+Naude+Dr%2C+Randburg+2195!5e0!3m2!1sen!2sza!4v1406012121301" width="870" height="390" frameborder="0" style="border: 0"></iframe>
                                            </div>
                                            <!-- .module_cont


                                        </div> -->
                    </div>

                    <!-- .row-fluid -->

                    <div class="row-fluid">
                        <div class="span7 module_cont module_feedback_form">
                            <br />
                            <p>Subject</p>
                            <asp:DropDownList runat="server" CssClass="dropdown" Width="105%" ID="lstSubject">
                                <asp:ListItem Text="-=Subject=-" Value="0"></asp:ListItem>
                                <asp:ListItem Text="Compliment" Value="1"></asp:ListItem>
                                <asp:ListItem Text="Testimonial" Value="2"></asp:ListItem>
                                <asp:ListItem Text="Complaint" Value="3"></asp:ListItem>
                                <asp:ListItem Text="Other" Value="4"></asp:ListItem>
                            </asp:DropDownList>
                            <div class="clear"></div>
                            <br />
                            <div>
                                <p>Name</p>
                                <asp:TextBox ID="txtName" runat="server"></asp:TextBox>
                                <div class="clear"></div>
                                <br />
                                <p>Email</p>
                                <asp:TextBox ID="txtEmail" runat="server"></asp:TextBox>
                                <div class="clear"></div>
                                <br />
                                <p>Contact Number</p>
                                <asp:TextBox ID="txtContactNumber" runat="server"></asp:TextBox>
                                <div class="clear"></div>
                            </div>
                            <br />
                            <p>Message</p>
                            <asp:TextBox runat="server" TextMode="MultiLine" ID="txtMessage"></asp:TextBox>
                            <div class="clear"></div>
                            <%--<input type="reset" name="reset" id="reset2" value="Clear form" class="feedback_reset">
                                                <input type="button" name="submit" class="feedback_go" id="submit2" value="Send Message!">--%>
                            <asp:Button ID="btnSendMessage" runat="server" Text="Send Message" class="feedback_go" OnClick="btnSubmit_Click" />
                            <div class="ajaxanswer"></div>
                        </div>
                        <div class="span1 module_cont module_feedback_form">
                        </div>
                        <br />
                        <br />
                        <!-- .module_cont -->
                        <div class="span4 module_cont module_contact_info">
                            <div class="continfo_item"><span class="ico_contact ico_contact-address"></span>083 229 3253</div>
                            <div class="continfo_item"><span class="ico_contact ico_contact-mail"></span><a href="mailto:theoni@consciousbirth.co.za">theoni@consciousbirth.co.za</a></div>
                            <div class="continfo_item"><span class="ico_contact ico_contact-skype"></span><a href="skype:theoni.papoutsis1?add">theoni.papoutsis1</a></div>
                            <div class="continfo_item"><span class="ico_contact ico_contact-facebook"></span><a href="https://www.facebook.com/pages/Conscious-Birth/164361116909927" target="_blank">Facebook</a></div>
                            <div class="continfo_item"><span class="ico_contact ico_contact-youtube"></span><a href="https://www.youtube.com/channel/UCxW7BRiZx5PaJg2P7itX-Dw">YouTube</a></div>
                        </div>
                        <!-- .module_cont -->
                    </div>
                    <!-- .row-fluid -->


                    <!-- <div class="row-fluid">
                                        <div class="span12 module_cont module_partners center_title module_big_padding">
                                            <div class="bg_title">
                                                <div class="title_wrapper"><a href="javascript:void(0)" class="btn_carousel_left"></a>
                                                    <h5 class="headInModule">Our Partners</h5>
                                                    <a href="javascript:void(0)" class="btn_carousel_right"></a></div>
                                            </div>
                                            <div class="module_content sponsors_works carouselslider items5" data-count="5">
                                                <ul>
                                                    <li>
                                                        <div class="item">
                                                            <a href="#">
                                                                <img src="img/pictures/partners2.png" alt=""></a>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="item">
                                                            <a href="#">
                                                                <img src="img/pictures/partners4.png" alt=""></a>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="item">
                                                            <a href="#">
                                                                <img src="img/pictures/partners1.png" alt=""></a>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="item">
                                                            <a href="#">
                                                                <img src="img/pictures/partners3.png" alt=""></a>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="item">
                                                            <a href="#s">
                                                                <img src="img/pictures/partners5.png" alt=""></a>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="item">
                                                            <a href="3" target="_blank">
                                                                <img src="img/pictures/partners6.png" alt=""></a>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="item">
                                                            <a href="#">
                                                                <img src="img/pictures/partners9.png" alt=""></a>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="item">
                                                            <a href="#">
                                                                <img src="img/pictures/partners11.png" alt=""></a>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- .row-fluid -->

                </div>
                <!-- .contentarea -->
            </div>
            <div class="left-sidebar-block span3">
                <aside class="sidebar">
                </aside>
            </div>
            <!-- .left-sidebar -->
        </div>
    </div>
</asp:Content>

