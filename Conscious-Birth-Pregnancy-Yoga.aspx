﻿<%@ Page Title="Pregnancy Yoga :: Conscious Birth" Language="C#" MasterPageFile="~/ConsciousBirth.master" AutoEventWireup="true" CodeFile="Conscious-Birth-Pregnancy-Yoga.aspx.cs" Inherits="Conscious_Birth_Pregnancy_Yoga" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date(); a = s.createElement(o),
            m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-53160457-1', 'auto');
        ga('send', 'pageview');

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="main_wrapper">
        <!-- C O N T E N T -->
        <div class="content_wrapper">
            <div class="page_title_block">
                <div class="container">
                    <h2 class="title">Pregnancy Yoga</h2>
                </div>
            </div>
            <div class="container">
                <div class="content_block no-sidebar row">
                    <div class="fl-container span12">
                        <div class="row">
                            <div class="posts-block span12">
                                <div class="contentarea">

                                    <div class="row-fluid">
                                        <div class="span4 module_cont module_text_area module_none_padding">
                                            <img src="img/pregnancy-yoga.png" alt="" />
                                        </div>
                                        <div class="span8 module_cont module_text_area module_none_padding">
                                            <blockquote class="type2" >
                                                <h6 class="author italic">What this is and who it is for:</h6>
                                                <p>
                                                    Kundalini yoga for pregnancy is a holistic pregnancy yoga that focuses on physical, emotional, mental and spiritual elements 
                                                    in order to assist women during pregnancy and birth and to help them achieve optimum pregnancy health. Pregnancy yoga is the 
                                                    perfect way to safely exercise while you are pregnant and is recommended for any pregnant woman.
                                                </p>
                                                <br />
                                                <h6 class="author italic">What to expect & benefits:</h6>
                                                <p>
                                                    Over the last decade Theoni has honed her pregnancy yoga classes and she makes use of carefully selected yoga postures 
                                                    that are specifically designed to enhance physical strength. But the classes have emotional and spiritual benefits also. 
                                                </p>
                                                <br />
                                            </blockquote>
                                            <h5>Theoni’s pregnancy yoga classes will:</h5>
                                                <ul class="">
                                                    <li>Enhance flexibility and tone key muscles that support your pregnancy health and assist you during birth.</li>
                                                    <li>Release tension in the spine and body and expand your lungs.</li>
                                                    <li>Build the strength and stamina you will need for the birth and beyond. </li>
                                                    <li>Provide a supportive community of women who are experiencing similar things to you.</li>
                                                    <li>Help you to deal with any anxieties about pregnancy, labour, birth and motherhood.</li>
                                                    <li>Offer you important me-time and give you the skills to assist you to focus and connect with your baby.</li>
                                                </ul>
                                                <br />
                                                <p>
                                                    Whether you want to have a natural birth or not, these classes assist you in working through your fears, accessing 
                                                    your hidden strengths and leading you away from feelings of powerlessness to a place of belief in your power as a woman. 
                                                    In these classes Theoni acts as a guide on your journey towards greater belief in your power as a woman and she is also a 
                                                    reliable and knowledgeable source of information on all things related to pregnancy and babies.
                                                </p>
                                                <br />
                                                
                                            <h5>Kundalini meditation & relaxation CD</h5>
                                                <p>
                                                    The Kundalini music and chants that Theoni uses during her pregnancy yoga classes are also available for purchase on CD. 
                                                    Many babies have been born with Theoni’s music selection playing in the background and the CD can be used before, during 
                                                    and after labour to enhance your experience on the precious journey towards motherhood.
                                                </p>
                                                <a href="Conscious-Birth-Products.aspx">See products for more info</a>
                                                <br /><br /><br />
    
                                            <h5>Conscious Birth manual</h5>
                                                <p>
                                                    The Conscious Birth manual is a valuable addition to the Pregnancy Yoga classes. It is a comprehensive compilation of information 
                                                    that you need from conception to after birth, gathered in one manual. It includes yoga pregnancy postures that you can take away to 
                                                    deepen your practice at home. Alternatively if you cannot attend classes, the manual can be used as a guide for your own unique journey 
                                                    providing you with yoga and meditation practices that you can work on at your own pace, in your own home.
                                                </p>
                                                <a href="Conscious-Birth-Products.aspx">See products for more info</a>
                                            <br /><br /><br />
                                            <div class="sectionContainerLeft innerBoxWithShadow">
                                                <div class="span12 module_cont module_text_area module_none_padding">
                                                    <blockquote class="type2">
                                                        <p>
                                                            Theoni’s pregnancy yoga class is the highlight of my week. I know that I am going to relax and bond with my baby. I also enjoy listening 
                                                            to the other moms and how their pregnancies are going. It makes me feel connected to women who are going through the same amazing journey. 
                                                            And I can relate to their stories, fear, joys, and excitement. My baby loves the yoga class and I know that the meditations, chanting and 
                                                            postures massage him and make him feel safe. The yoga class gives me a chance to focus on my pregnancy and grounds me for the rest of the week.
                                                        </p>
                                                        <div class="author italic">Sharon, mother of Matthew</div>
                                                    </blockquote>
                                                </div>
                                                <br class="clear" />
                                            </div>
                                        </div>
                                        
                                    </div>
                                    <br />
                                        <br />

                                     <div class="">
                                            <span class="blogpost_type_ico post_type_text"></span>
                                            <h1>Pregnancy Yoga - Atholl Road</h1>
                                            <div class="blogpost_meta">
                                                <span class="blog_date">Day: Tuesday, late morning</span><br />
                                                <span class="blog_date">Time: 9:30 to 11:00</span><br />
                                                <span class="blog_date">Venue: 121 Atholl Road, Atholl</span><br />

                                            </div>
                                           <span class="blogpost_type_ico post_type_text"></span>
                                        <h4 style="margin-top: 70px;">Saturday monthly classes for 2016 in Melville</h4> 
                                         <div class="blogpost_meta">
                                              <span class="blog_date">25th June </span><br />
                                              <span class="blog_date">16th July</span><br />
                                              <span class="blog_date">13th August</span><br />
                                              <span class="blog_date">3rd September</span><br />
                                              <span class="blog_date">8th October</span><br />
                                              <span class="blog_date">5th November</span><br />
                                             </div>








                                            <div class="blogpost_share">
                                                <a href="#" class="ico_socialize_facebook2 ico_socialize type1"></a>
                                                <a href="#" class="ico_socialize_twitter2 ico_socialize type1"></a>
                                                <a href="#" class="ico_socialize_pinterest ico_socialize type1"></a>
                                                <a href="#" class="ico_socialize_google2 ico_socialize type1"></a>
                                            </div>
                                            <div class="row-fluid">
                                                <div class="contentarea">
                                                <div class="span12 module_cont module_google_map module_big_padding2 ">
                                                    <iframe height="390" src="https://www.google.com/maps/embed?pb=!1m24!1m12!1m3!1d3582.2643326386897!2d28.069995!3d-26.122922999999705!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!4m9!1i0!3e0!4m0!4m5!1s0x1e950d214acb1a39%3A0x567a45a930c7cb4a!2s121+Atholl+Rd%2C+Johannesburg+2196!3m2!1d-26.12269!2d28.06994!5e0!3m2!1sen!2sza!4v1403253889315"></iframe>
                                                </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row-fluid">
                                        <div class="contentarea">
                                            <div class="span12 module_cont module_text_area module_none_padding">
                                                <h4>The cost is as follows:</h4>
                                                <p>R120 per class, OR</p>
                                                <p>R380 for a month, paid in advance for a 4 week month, OR</p>
                                                <p>R1080 for 3 months, paid in advance for 4 week months.</p>
                                                <br />
                                            </div>
                                        </div>
                                    </div>
                                    <!-- .row-fluid -->
                                    <!-- .row-fluid -->
                                    <div class="module_line_trigger" data-background="#ffeec9 url(img/VV.png) no-repeat center" data-top-padding="bottom_padding_huge" data-bottom-padding="module_big_padding">
                                        <div class="row-fluid">
                                            <div class="span12 module_cont module_promo_text module_huge_padding">
                                                <div class="shortcode_promoblock">
                                                    <div class="promo_button_block type2">
                                                        <a href="Conscious-Birth-Products.aspx" class="promo_button"> View our Products </a>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--.module_cont -->
                                            <div class="clear">
                                                <!-- ClearFIX -->
                                            </div>
                                        </div>
                                        <!-- .row-fluid -->
                                    </div>
                                    <!-- .module_line_trigger -->
                                    <!--<div class="row-fluid">
                                        <div class="span12 module_cont module_partners center_title module_big_padding2">
                                            <div class="bg_title">
                                                <div class="title_wrapper">
                                                    <a href="javascript:void(0)" class="btn_carousel_left"></a>
                                                    <h5 class="headInModule">Our Partners</h5>
                                                    <a href="javascript:void(0)" class="btn_carousel_right"></a>
                                                </div>
                                            </div>
                                            <div class="module_content sponsors_works carouselslider items5" data-count="5">
                                                <ul>
                                                    <li>
                                                        <div class="item">
                                                            <a href="#charity-html5css3-website-template/3180454" target="_blank">
                                                                <img src="img/pictures/partners2.png" alt=""></a>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="item">
                                                            <a href="#point-business-responsive-wp-theme/4319087" target="_blank">
                                                                <img src="img/pictures/partners4.png" alt=""></a>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="item">
                                                            <a href="#cleanspace-retina-ready-business-wp-theme/3776000" target="_blank">
                                                                <img src="img/pictures/partners1.png" alt=""></a>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="item">
                                                            <a href="#yellowproject-multipurpose-retina-wp-theme/4066662" target="_blank">
                                                                <img src="img/pictures/partners3.png" alt=""></a>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="item">
                                                            <a href="#showroom-portfolio-retina-ready-wp-theme/3473628" target="_blank">
                                                                <img src="img/pictures/partners5.png" alt=""></a>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="item">
                                                            <a href="themeforest.net/item/incipiens-responsive-portfolio-wordpress-theme/2762691" target="_blank">
                                                                <img src="img/pictures/partners6.png" alt=""></a>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="item">
                                                            <a href="themeforest.net/item/hq-photography-responsive-wp-theme/3200962" target="_blank">
                                                                <img src="img/pictures/partners9.png" alt=""></a>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="item">
                                                            <a href="#incipiens-responsive-portfolio-wordpress-theme/2762691" target="_blank">
                                                                <img src="img/pictures/partners11.png" alt=""></a>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- .row-fluid -->

                                </div>
                                <!-- .contentarea -->
                            </div>
                            <div class="left-sidebar-block span3">
                                <aside class="sidebar">
                                    //Sidebar Text
                                </aside>
                            </div>
                            <!-- .left-sidebar -->
                        </div>
                        <div class="clear">
                            <!-- ClearFix -->
                        </div>
                    </div>
                    <!-- .fl-container -->
                    <div class="right-sidebar-block span3">
                        <aside class="sidebar">
                        </aside>
                    </div>
                    <!-- .right-sidebar -->
                    <div class="clear">
                        <!-- ClearFix -->
                    </div>
                </div>
            </div>
            <!-- .container -->
        </div>
        <!-- .content_wrapper -->

    </div>
    <!-- .main_wrapper -->
</asp:Content>

