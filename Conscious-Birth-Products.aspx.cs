﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Products : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if ((Request.QueryString["iCategoryID"] != "") && (Request.QueryString["iCategoryID"] != null))
        {
            int iCategoryID = Convert.ToInt32(Request.QueryString["iCategoryID"]);

            //### Populate the form
            popProducts(iCategoryID);
        }
        else
        {
            popProducts();
        }
    }

    protected void btnAddToCart_Click(object sender, EventArgs e)
    {
        int iProductID = Convert.ToInt32(((LinkButton)sender).CommandArgument);

        DataTable dtColours = clsProductColoursLink.GetProductColoursLinkList("iProductID =" + iProductID, "");
        DataTable dtSizes = clsProductSizesLink.GetProductSizesLinkList("iProductID =" + iProductID, "");

        if ((dtColours.Rows.Count > 1) || (dtSizes.Rows.Count > 1) || (dtColours.Rows.Count > 1 && dtSizes.Rows.Count > 1))
        {
            Response.Redirect("Conscious-Birth-Product-Info.aspx?iProductID=" + iProductID);
        }
        else
        {
            clsCart.addToCart(iProductID, 1, "One Colour Only", "One Size Only");
            Response.Redirect("Conscious-Birth-Cart.aspx");
        }

    }
    protected void btnAddToWishList_Click(object sender, EventArgs e)
    {
        if (Session["clsAccountUsers"] == null)
        {
            
                //### Redirect back to login
            Response.Redirect("Conscious-Birth-User-Login.aspx");
            
        }
        else
        {
            int iProductID = Convert.ToInt32(((LinkButton)sender).CommandArgument);

            DataTable dtColours = clsProductColoursLink.GetProductColoursLinkList("iProductID =" + iProductID, "");
            DataTable dtSizes = clsProductSizesLink.GetProductSizesLinkList("iProductID =" + iProductID, "");

            if ((dtColours.Rows.Count > 1) || (dtSizes.Rows.Count > 1) || (dtColours.Rows.Count > 1 && dtSizes.Rows.Count > 1))
            {
                Response.Redirect("Conscious-Birth-Product-Info.aspx?iProductID=" + iProductID);
            }
            else
            {
                SaveWishlist(iProductID, 1, "One Colour Only", "One Size Only");
                Response.Redirect("Conscious-Birth-WishList-Cart.aspx");
            }
        }

    }

    private void SaveWishlist(int Productid, int Quantity, string Color, string Size)
    {
        if (Productid != null)
        {

            clsUserProductWishlistLink clsUserProductWishlistLink = new clsUserProductWishlistLink();
            clsUserProductWishlistLink.iProductID = Productid;
            clsUserProductWishlistLink.iWishListEventID = Convert.ToInt32(Session["clsWishlistEvents"]);
            clsUserProductWishlistLink.iQuantity = Quantity;
            clsUserProductWishlistLink.strColor = Color;
            clsUserProductWishlistLink.strSize = Size;
            clsUserProductWishlistLink.Update();
        }
        else
        {
            Response.Redirect("Conscious-Birth-User-Login.aspx");
        }
    }


    private void popProducts()
    {
        DataTable dtProducts = clsProducts.GetProductsList("", "strTitle ASC");

        int iBestSellerCount = dtProducts.Rows.Count;
        dtProducts.Columns.Add("FullPathForImage");
        dtProducts.Columns.Add("Link");
        dtProducts.Columns.Add("divReset");
        dtProducts.Columns.Add("classForColumn");

        int iCount = 0;

        foreach (DataRow dtrProduct in dtProducts.Rows)
        {
            ++iCount;

            if (iCount == 1)
            {
                //dtrProduct["firstProduct"] = "";
                dtrProduct["classForColumn"] = "col_1_of_3 span_1_of_3 alpha";
            }

            if (iCount % 3 == 1 && iCount != 1)
            {
                dtrProduct["classForColumn"] = "col_1_of_3 span_1_of_3";
            }

            if (iCount % 3 == 2)
            {
                dtrProduct["classForColumn"] = "col_1_of_3 span_1_of_3";
            }

            if (iCount % 3 == 0)
            {
                dtrProduct["classForColumn"] = "col_1_of_3 span_1_of_3";
            }

            if ((iCount % 3 == 0))
            {
                dtrProduct["divReset"] = "<div class=\"clear\"></div>";
            }
            else if (iCount==dtProducts.Rows.Count)
            {
                dtrProduct["divReset"] = "<div class=\"clear\"></div>";
            }

            if (!(dtrProduct["strMasterImage"].ToString() == "") || (dtrProduct["strMasterImage"] == null) || (dtrProduct["strPathToImages"] == null))
            {
                dtrProduct["FullPathForImage"] = "Products/" + dtrProduct["strPathToImages"] + "/" + dtrProduct["strMasterImage"];
                dtrProduct["Link"] = "Conscious-Birth-Product-Information.aspx?iProductID=" + dtrProduct["iProductID"].ToString();
            }
            else
            {
                dtrProduct["FullPathForImage"] = "img/no-image.png";
            }
        }
        rpProducts.DataSource = dtProducts;
        rpProducts.DataBind();
    }

    private void popProducts(int iCategoryID)
    {
        DataTable dtCategoryProducts = clsProductCategoriesLink.GetProductCategoriesLinkList("iCategoryID='" + iCategoryID + "'", "");

        DataTable dtProducts = clsProducts.GetProductsList("", "strTitle ASC");

        int iBestSellerCount = dtProducts.Rows.Count;
        dtProducts.Columns.Add("FullPathForImage");
        dtProducts.Columns.Add("Link");
        dtProducts.Columns.Add("divReset");
        dtProducts.Columns.Add("classForColumn");

        DataTable dtNewProductList = new DataTable();

        dtNewProductList = dtProducts.Clone();
        dtNewProductList.Clear();

        int iCount = 0;

        foreach (DataRow dtrCategoryProducts in dtCategoryProducts.Rows)
        {
            int iProductTotal = dtCategoryProducts.Rows.Count;

            foreach (DataRow dtrProduct in dtProducts.Rows)
            {
                if (!(dtrProduct["strMasterImage"].ToString() == "") || (dtrProduct["strMasterImage"] == null) || (dtrProduct["strPathToImages"] == null))
                {
                    dtrProduct["FullPathForImage"] = "Products/" + dtrProduct["strPathToImages"] + "/" + dtrProduct["strMasterImage"];

                    dtrProduct["Link"] = "Conscious-Birth-Product-Information.aspx?iProductID=" + dtrProduct["iProductID"].ToString();
                }
                else
                {
                    dtrProduct["FullPathForImage"] = "img/no-image.png";
                }

                if (Convert.ToInt32(dtrCategoryProducts["iProductID"]) == Convert.ToInt32(dtrProduct["iProductID"]))
                {
                    ++iCount;

                    if (iCount == 1)
                    {
                        //dtrProduct["firstProduct"] = "";
                        dtrProduct["classForColumn"] = "col_1_of_3 span_1_of_3 alpha";
                    }

                    if (iCount % 3 == 1 && iCount != 1)
                    {
                        dtrProduct["classForColumn"] = "col_1_of_3 span_1_of_3";
                    }

                    if (iCount % 3 == 2)
                    {
                        dtrProduct["classForColumn"] = "col_1_of_3 span_1_of_3";
                    }

                    if (iCount % 3 == 0)
                    {
                        dtrProduct["classForColumn"] = "col_1_of_3 span_1_of_3";
                    }

                    if ((iCount % 3 == 0) || (iCount == iProductTotal))
                    {
                        dtrProduct["divReset"] = "<div class=\"clear\"></div>";
                    }

                    dtNewProductList.ImportRow(dtrProduct);
                }
            }
        }
        rpProducts.DataSource = dtNewProductList;
        rpProducts.DataBind();

    }
}