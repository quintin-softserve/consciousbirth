﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Conscious_Birth_Blog : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        PopBlogs();
    }

    private void PopBlogs()
    {
        int iBlogPostID;
        int iBlogTypeID;
        string strBlogTitle = "";
        DateTime dtPosted;
        string strPathToImage = "";
        string strMasterImage = "";
        string strFullPath = "";
        string strDescription;

        DataTable dtBlogPosts = clsBlogPosts.GetBlogPostsList("", "");
        StringBuilder strbProducts = new StringBuilder();

        foreach (DataRow dtrBlogRow in dtBlogPosts.Rows)
        {
            iBlogPostID = Convert.ToInt32(dtrBlogRow["iBlogPostID"].ToString());
            iBlogTypeID = Convert.ToInt32(dtrBlogRow["iBlogTypeID"].ToString());
            strBlogTitle = (dtrBlogRow["strTitle"].ToString());
            dtPosted = Convert.ToDateTime(dtrBlogRow["dtAdded"].ToString());
            strPathToImage = dtrBlogRow["strPathToImages"].ToString();
            strMasterImage = dtrBlogRow["strMasterImage"].ToString();
            strFullPath = "BlogPosts/"+ strPathToImage + "/" + strMasterImage;
            strDescription = dtrBlogRow["strDescription"].ToString();
            

            switch (iBlogTypeID)
            {
                case 1:
                    {
                        //### BLOG TYPE IMAGE
                        strbProducts.AppendLine("<div class='blog_post_preview'>");
                            strbProducts.AppendLine("<div class='blog_head'>");
                                 strbProducts.AppendLine("<span class='blogpost_type_ico post_type_image'></span>");
                                 strbProducts.AppendLine("<div class='bg_title'><h4><a class='blogpost_title' href='blogpost_full.html'>" + strBlogTitle + "</a></h4></div>");
                                 strbProducts.AppendLine("<div class='blogpost_meta'>");
                                    strbProducts.AppendLine("<span>" + dtPosted.ToString("dd MMMM yyyy") + "</span>");
                                      //strbProducts.AppendLine("<span>by <a href='#'>GT3</a></span>");
                                      //strbProducts.AppendLine(" <span>In <a href='#'>ArtCore</a>, <a href='#'>Illustrations</a></span>");
                                      //strbProducts.AppendLine("<span>Tags: <a href='#'>art</a>, <a href='#'>artcore</a>, <a href='#'>illustrations</a>, <a href='#'>colors</a>, <a href='#'>monster</a></span>");
                                 strbProducts.AppendLine("</div>");
                            strbProducts.AppendLine("</div");
                            //strbProducts.AppendLine(" <div class='featured_image_full'>");
                            //    strbProducts.AppendLine("<img src='" + strFullPath + "' alt=''/>");
                            //strbProducts.AppendLine("</div");
                            //strbProducts.AppendLine("<article class='contentarea'>");
                            //    strbProducts.AppendLine("<p>"+strDescription+"");
                            //    //<a href='blogpost_full.html' class='more-link'>Read more...</a></p>
                            //strbProducts.AppendLine("</article>");
                        //strbProducts.AppendLine("</div>");

                        break;
                    }

                case 2:
                    {
                        //### BLOG TYPE TEXT
                        strbProducts.AppendLine("<div class='blog_post_preview'>");
                        strbProducts.AppendLine(" <div class='blog_head'>");
                        strbProducts.AppendLine("<span class='blogpost_type_ico post_type_text'></span>");
                        strbProducts.AppendLine("<div class='bg_title'><h4><a class='blogpost_title' href='blogpost_full.html'>" + strBlogTitle + "</a></h4></div>");
                        strbProducts.AppendLine("<div class='blogpost_meta'>");
                        strbProducts.AppendLine("<span>" + dtPosted.ToString("dd MMMM yyyy") + "</span>");
                        //strbProducts.AppendLine("<span>by <a href='#'>GT3</a></span>");
                        //strbProducts.AppendLine("<span>In <a href='#'>ArtCore</a>, <a href='#'>Illustrations</a></span>");
                        //strbProducts.AppendLine("<span>Tags: <a href='#'>art</a>, <a href='#'>artcore</a>, <a href='#'>illustrations</a>, <a href='#'>colors</a>, <a href='#'>monster</a></span>");
                        strbProducts.AppendLine("</div>");
                        strbProducts.AppendLine("</div>");
                        //strbProducts.AppendLine("<article class='contentarea'>");
                        //strbProducts.AppendLine("<p>" + strDescription + "");
                        //strbProducts.AppendLine(" </article>");
                        //strbProducts.AppendLine("</div>");

                        break;
                    }

                case 3:
                    {
                        //### BLOG TYPE Video
                        strbProducts.AppendLine("<div class='blog_post_preview'>");
                            strbProducts.AppendLine("<div class='blog_head'>");
                                 strbProducts.AppendLine("<span class='blogpost_type_ico post_type_video'></span>");
                                 strbProducts.AppendLine("<div class='bg_title'><h4><a class='blogpost_title' href='blogpost_full.html'>" + strBlogTitle + "</a></h4></div>");
                                 strbProducts.AppendLine(" <div class='blogpost_meta'>");
                                        strbProducts.AppendLine("<span>"+ dtPosted.ToString("dd MMMM yyyy") +"</span>");
                                        //strbProducts.AppendLine("<span>by <a href='#'>GT3</a></span>");
                                        //strbProducts.AppendLine(" <span>In <a href='#'>ArtCore</a>, <a href='#'>Illustrations</a></span>");
                                        //strbProducts.AppendLine("<span>Tags: <a href='#'>art</a>, <a href='#'>artcore</a>, <a href='#'>illustrations</a>, <a href='#'>colors</a>, <a href='#'>monster</a></span>");
                                 strbProducts.AppendLine("</div>");
                            strbProducts.AppendLine("</div>");
                        //    strbProducts.AppendLine("<div class='featured_image_full'>");
                        //         strbProducts.AppendLine("<iframe width='870' height='430' src='"+dtrBlogRow["strVideoLink"].ToString()+"'></iframe>");
                        //    strbProducts.AppendLine("</div>");
                        //    strbProducts.AppendLine("<article class='contentarea'>");
                        //        strbProducts.AppendLine("<p>"+ strDescription+"");
                        //    strbProducts.AppendLine("</article>");
                        //strbProducts.AppendLine("</div>");

                        break;
                    }
            //    case 4:
            //        {
            //             //### BLOG TYPE MULTI IMAGE POST
            //strbProducts.AppendLine("<div class='blog_post_preview'>");
            //   strbProducts.AppendLine("<div class='blog_head'>");
            //       strbProducts.AppendLine("<span class='blogpost_type_ico post_type_gallery'></span>");
            //       strbProducts.AppendLine("<div class='bg_title'><h4><a class='blogpost_title' href='blogpost_full.html'>" + strBlogTitle + "</a></h4></div>'");
            //       strbProducts.AppendLine("<div class='blogpost_meta'>");
            //       strbProducts.AppendLine("<span>" + dtPosted.ToString("dd MMMM yyyy") + "</span>");
            //            //strbProducts.AppendLine("<span>by <a href='#'>GT3</a></span>");
            //            //strbProducts.AppendLine("<span>In <a href='#'>ArtCore</a>, <a href='#'>Illustrations</a></span>");
            //            //strbProducts.AppendLine("<span>Tags: <a href='#'>art</a>, <a href='#'>artcore</a>, <a href='#'>illustrations</a>, <a href='#'>colors</a>, <a href='#'>monster</a></span>");
            //       strbProducts.AppendLine("</div>");
            //   strbProducts.AppendLine("</div>");
            //   strbProducts.AppendLine("<div class='featured_image_full'>");
            //         strbProducts.AppendLine("<div class='slider-wrapper theme-default'>");
            //             strbProducts.AppendLine("<div class='nivoSlider'>");
            //                strbProducts.AppendLine("<img src='" + strFullPath + "' alt=''/>");
            //                strbProducts.AppendLine("<img src='" + strFullPath + "' alt=''/>");
            //             strbProducts.AppendLine("</div>");
            //             strbProducts.AppendLine("<div class='img_inset'></div> ");
            //         strbProducts.AppendLine("</div>");
            //    strbProducts.AppendLine("</div>");
            //    strbProducts.AppendLine("<article class='contentarea'>");
            //       strbProducts.AppendLine("<p>" + strDescription + "");
            //    strbProducts.AppendLine("</article>");
            //strbProducts.AppendLine("</div>");
            //break;
            //        }
            }           
           

        }

        litBlog.Text = strbProducts.ToString();
    }
}