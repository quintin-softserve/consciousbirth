using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Data.SqlClient;

/// <summary>
/// Summary description for clsColours
/// </summary>
public class clsColours
{
    #region MEMBER VARIABLES

        private int m_iColourID;
        private DateTime m_dtAdded;
        private int m_iAddedBy;
        private DateTime m_dtEdited;
        private int m_iEditedBy;
        private String m_strTitle;
        private String m_strColourCode;
        private bool m_bIsDeleted;
        
    #endregion 

    #region PROPERTIES

        public int iColourID
        {
            get
            {
                return m_iColourID;
            }
        }

        public DateTime dtAdded
        {
            get
            {
                return m_dtAdded;
            }
            set
            {
                m_dtAdded = value;
            }
        }

        public int iAddedBy
        {
            get
            {
                return m_iAddedBy;
            }
            set
            {
                m_iAddedBy = value;
            }
        }

        public DateTime dtEdited
        {
            get
            {
                return m_dtEdited;
            }
            set
            {
                m_dtEdited = value;
            }
        }

        public int iEditedBy
        {
            get
            {
                return m_iEditedBy;
            }
            set
            {
                m_iEditedBy = value;
            }
        }

        public String strTitle
        {
            get
            {
                return m_strTitle;
            }
            set
            {
                m_strTitle = value;
            }
        }

        public String strColourCode
        {
            get
            {
                return m_strColourCode;
            }
            set
            {
                m_strColourCode = value;
            }
        }

        public bool bIsDeleted
        {
            get
            {
                return m_bIsDeleted;
            }
            set
            {
                m_bIsDeleted = value;
            }
        }

    
    #endregion
    
    #region CONSTRUCTORS

    public clsColours()
    {
        m_iColourID = 0;
    }

    public clsColours(int iColourID)
    {
        m_iColourID = iColourID;
        GetData();
    }

    #endregion

    #region PUBLIC METHODS

        public virtual void Update()
        {
            try
            {
                if (iColourID == 0)
                {
                    //### Assign values to the parameter list for each corresponding column in the DB
                    SqlParameter[] sqlParametersInsert = new SqlParameter[] 
                    {
                        new SqlParameter("@dtAdded", m_dtAdded),
                        new SqlParameter("@iAddedBy", m_iAddedBy),
                        new SqlParameter("@strTitle", m_strTitle),
                        new SqlParameter("@strColourCode", m_strColourCode)              
                  };

                  //### Add
                  m_iColourID = (int)clsDataAccess.ExecuteScalar("spColoursInsert", sqlParametersInsert);                    
                    }
                    else
                    {
                    //### Assign values to the parameter list for each corresponding column in the DB
                    SqlParameter[] sqlParametersUpdate = new SqlParameter[] 
                    {
                         new SqlParameter("@iColourID", m_iColourID),
                         new SqlParameter("@dtEdited", m_dtEdited),
                         new SqlParameter("@iEditedBy", m_iEditedBy),
                         new SqlParameter("@strTitle", m_strTitle),
                         new SqlParameter("@strColourCode", m_strColourCode)              
                    };
                    //### Update
                    clsDataAccess.Execute("spColoursUpdate", sqlParametersUpdate);
                    }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void Delete(int iColourID)
        {
        //### Populate
        SqlParameter[] sqlParameter = new SqlParameter[]
        {
            new SqlParameter("@iColourID", iColourID)
        };
        //### Executes delete sp
        clsDataAccess.Execute("spColoursDelete", sqlParameter);
        }

        public static DataTable GetColoursList()
        {
            SqlParameter[] EmptySqlParameter = new SqlParameter[]{};
            return clsDataAccess.GetDataTable("spColoursList", EmptySqlParameter);
        }
        public static DataTable GetColoursList(string strFilterExpression, string strSortExpression)
        {
            DataView dvColoursList = new DataView();

            SqlParameter[] EmptySqlParameter = new SqlParameter[] { };
            dvColoursList = clsDataAccess.GetDataView("spColoursList", EmptySqlParameter);
            dvColoursList.RowFilter = strFilterExpression;
            dvColoursList.Sort = strSortExpression;

            return dvColoursList.ToTable();
        }
    #endregion

    #region PROTECTED METHODS

        protected virtual void GetData()
        {
            try
                {
                    //### Populate
                    SqlParameter[] sqlParameter = new SqlParameter[] 
                        {
                            new SqlParameter("@iColourID", m_iColourID)
                        };
                DataRow drRecord = clsDataAccess.GetRecord("spColoursGetRecord", sqlParameter);

                m_dtAdded = Convert.ToDateTime(drRecord["dtAdded"]);
                m_iAddedBy = Convert.ToInt32(drRecord["iAddedBy"]);

                if (drRecord["dtEdited"] != DBNull.Value)
                   m_dtEdited = Convert.ToDateTime(drRecord["dtEdited"]);

             if (drRecord["iEditedBy"] != DBNull.Value)
               m_iEditedBy = Convert.ToInt32(drRecord["iEditedBy"]);

                m_strTitle = drRecord["strTitle"].ToString();
                m_strColourCode = drRecord["strColourCode"].ToString();
                m_bIsDeleted = Convert.ToBoolean(drRecord["bIsDeleted"]);
 
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    #endregion
}