
using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Data.SqlClient;

/// <summary>
/// Summary description for clsBlogTypes
/// </summary>
public class clsBlogTypes
{
    #region MEMBER VARIABLES

        private int m_iBlogTypeID;
        private DateTime m_dtAdded;
        private int m_iAddedBy;
        private DateTime m_dtEdited;
        private int m_iEditedBy;
        private String m_strTitle;
        private String m_strContent;
        private bool m_bIsDeleted;
        
    #endregion 

    #region PROPERTIES

        public int iBlogTypeID
        {
            get
            {
                return m_iBlogTypeID;
            }
        }

        public DateTime dtAdded
        {
            get
            {
                return m_dtAdded;
            }
            set
            {
                m_dtAdded = value;
            }
        }

        public int iAddedBy
        {
            get
            {
                return m_iAddedBy;
            }
            set
            {
                m_iAddedBy = value;
            }
        }

        public DateTime dtEdited
        {
            get
            {
                return m_dtEdited;
            }
            set
            {
                m_dtEdited = value;
            }
        }

        public int iEditedBy
        {
            get
            {
                return m_iEditedBy;
            }
            set
            {
                m_iEditedBy = value;
            }
        }

        public String strTitle
        {
            get
            {
                return m_strTitle;
            }
            set
            {
                m_strTitle = value;
            }
        }

        public String strContent
        {
            get
            {
                return m_strContent;
            }
            set
            {
                m_strContent = value;
            }
        }

        public bool bIsDeleted
        {
            get
            {
                return m_bIsDeleted;
            }
            set
            {
                m_bIsDeleted = value;
            }
        }

    
    #endregion
    
    #region CONSTRUCTORS

    public clsBlogTypes()
    {
        m_iBlogTypeID = 0;
    }

    public clsBlogTypes(int iBlogTypeID)
    {
        m_iBlogTypeID = iBlogTypeID;
        GetData();
    }

    #endregion

    #region PUBLIC METHODS

        public virtual void Update()
        {
            try
            {
                if (iBlogTypeID == 0)
                {
                    //### Assign values to the parameter list for each corresponding column in the DB
                    SqlParameter[] sqlParametersInsert = new SqlParameter[] 
                    {
                        new SqlParameter("@dtAdded", m_dtAdded),
                        new SqlParameter("@iAddedBy", m_iAddedBy),
                        new SqlParameter("@strTitle", m_strTitle),
                        new SqlParameter("@strContent", m_strContent)                  
                  };

                  //### Add
                  m_iBlogTypeID = (int)clsDataAccess.ExecuteScalar("spBlogTypesInsert", sqlParametersInsert);                    
                    }
                    else
                    {
                    //### Assign values to the parameter list for each corresponding column in the DB
                    SqlParameter[] sqlParametersUpdate = new SqlParameter[] 
                    {
                         new SqlParameter("@iBlogTypeID", m_iBlogTypeID),
                         new SqlParameter("@dtEdited", m_dtEdited),
                         new SqlParameter("@iEditedBy", m_iEditedBy),
                         new SqlParameter("@strTitle", m_strTitle),
                         new SqlParameter("@strContent", m_strContent)
                    };
                    //### Update
                    clsDataAccess.Execute("spBlogTypesUpdate", sqlParametersUpdate);
                    }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void Delete(int iBlogTypeID)
        {
        //### Populate
        SqlParameter[] sqlParameter = new SqlParameter[]
        {
            new SqlParameter("@iBlogTypeID", iBlogTypeID)
        };
        //### Executes delete sp
        clsDataAccess.Execute("spBlogTypesDelete", sqlParameter);
        }

        public static DataTable GetBlogTypesList()
        {
            SqlParameter[] EmptySqlParameter = new SqlParameter[]{};
            return clsDataAccess.GetDataTable("spBlogTypesList", EmptySqlParameter);
        }
        public static DataTable GetBlogTypesList(string strFilterExpression, string strSortExpression)
        {
            DataView dvBlogTypesList = new DataView();

            SqlParameter[] EmptySqlParameter = new SqlParameter[] { };
            dvBlogTypesList = clsDataAccess.GetDataView("spBlogTypesList", EmptySqlParameter);
            dvBlogTypesList.RowFilter = strFilterExpression;
            dvBlogTypesList.Sort = strSortExpression;

            return dvBlogTypesList.ToTable();
        }
    #endregion

    #region PROTECTED METHODS

        protected virtual void GetData()
        {
            try
                {
                    //### Populate
                    SqlParameter[] sqlParameter = new SqlParameter[] 
                        {
                            new SqlParameter("@iBlogTypeID", m_iBlogTypeID)
                        };
                DataRow drRecord = clsDataAccess.GetRecord("spBlogTypesGetRecord", sqlParameter);

                m_dtAdded = Convert.ToDateTime(drRecord["dtAdded"]);
                m_iAddedBy = Convert.ToInt32(drRecord["iAddedBy"]);

                if (drRecord["dtEdited"] != DBNull.Value)
                   m_dtEdited = Convert.ToDateTime(drRecord["dtEdited"]);

             if (drRecord["iEditedBy"] != DBNull.Value)
               m_iEditedBy = Convert.ToInt32(drRecord["iEditedBy"]);

                m_strTitle = drRecord["strTitle"].ToString();
                m_strContent = drRecord["strContent"].ToString();
                m_bIsDeleted = Convert.ToBoolean(drRecord["bIsDeleted"]);
 
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    #endregion
}