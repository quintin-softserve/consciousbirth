//using System;
//using System.Data;
//using System.Configuration;
//using System.Web;
//using System.Web.Security;
//using System.Web.UI;
//using System.Web.UI.HtmlControls;
//using System.Web.UI.WebControls;
//using System.Web.UI.WebControls.WebParts;
//using System.Data.SqlClient;

///// <summary>
///// Summary description for clsOrders
///// </summary>
//public class clsWishList
//{
//    #region MEMBER VARIABLES

//        private int m_iWishListID;
//        private DateTime m_dtAdded;
//        private int m_iAddedBy;
//        private DateTime m_dtEdited;
//        private int m_iEditedBy;
//        private int m_iProductID;
//        private int m_iAccountUserID;
//        private int m_iQuantity;
        
//    #endregion 

//    #region PROPERTIES

//        public int iWishListID
//        {
//            get
//            {
//                return m_iWishListID;
//            }
//        }

//        public DateTime dtAdded
//        {
//            get
//            {
//                return m_dtAdded;
//            }
//            set
//            {
//                m_dtAdded = value;
//            }
//        }

//        public int iAddedBy
//        {
//            get
//            {
//                return m_iAddedBy;
//            }
//            set
//            {
//                m_iAddedBy = value;
//            }
//        }

//        public DateTime dtEdited
//        {
//            get
//            {
//                return m_dtEdited;
//            }
//            set
//            {
//                m_dtEdited = value;
//            }
//        }

//        public int iEditedBy
//        {
//            get
//            {
//                return m_iEditedBy;
//            }
//            set
//            {
//                m_iEditedBy = value;
//            }
//        }

//        public int iProductID
//        {
//            get
//            {
//                return m_iProductID;
//            }
//            set
//            {
//                m_iProductID = value;
//            }
//        }

//        public int iAccountUserID
//        {
//            get
//            {
//                return m_iAccountUserID;
//            }
//            set
//            {
//                m_iAccountUserID = value;
//            }
//        }
//        public int iQuantity
//        {
//            get
//            {
//                return m_iQuantity;
//            }
//            set
//            {
//                m_iQuantity = value;
//            }
//        }

    
//    #endregion
    
//    #region CONSTRUCTORS

//    public clsWishList()
//    {
//        m_iWishListID = 0;
//    }

//    public clsWishList(int iWishListID)
//    {
//        m_iWishListID = iWishListID;
//        GetData();
//    }

//    #endregion

//    #region PUBLIC METHODS

//        public virtual void Update()
//        {
//            try
//            {
//                if (iWishListID == 0)
//                {
//                    //### Assign values to the parameter list for each corresponding column in the DB
//                    SqlParameter[] sqlParametersInsert = new SqlParameter[] 
//                    {
//                        new SqlParameter("@dtAdded", m_dtAdded),
//                        new SqlParameter("@iAddedBy", m_iAddedBy),
//                        new SqlParameter("@iProductID", m_iProductID),
//                        new SqlParameter("@iAccountUserID", m_iAccountUserID),
//                        new SqlParameter("@iQuantity", m_iQuantity)
//                  };

//                  //### Add
//                  m_iWishListID = (int)clsDataAccess.ExecuteScalar("spWishListInsert", sqlParametersInsert);                    
//                    }
//                    else
//                    {
//                    //### Assign values to the parameter list for each corresponding column in the DB
//                    SqlParameter[] sqlParametersUpdate = new SqlParameter[] 
//                    {
//                         new SqlParameter("@iWishListID", m_iWishListID),
//                         new SqlParameter("@dtEdited", m_dtEdited),
//                         new SqlParameter("@iEditedBy", m_iEditedBy),
//                         new SqlParameter("@iProductID", m_iProductID),
//                         new SqlParameter("@iAccountUserID", m_iAccountUserID),
//                         new SqlParameter("@iQuantity", m_iQuantity)
//                    };
//                    //### Update
//                    clsDataAccess.Execute("spWishListUpdate", sqlParametersUpdate);
//                    }
//            }
//            catch (Exception ex)
//            {
//                throw ex;
//            }
//        }

//        public static void Delete(int iWishListID)
//        {
//        //### Populate
//        SqlParameter[] sqlParameter = new SqlParameter[]
//        {
//            new SqlParameter("@iWishListID", iWishListID)
//        };
//        //### Executes delete sp
//        clsDataAccess.Execute("spWishListDelete", sqlParameter);
//        }

//        public static DataTable GetWishListList()
//        {
//            SqlParameter[] EmptySqlParameter = new SqlParameter[]{};
//            return clsDataAccess.GetDataTable("spWishListList", EmptySqlParameter);
//        }
//        public static DataTable GetWishListList(string strFilterExpression, string strSortExpression)
//        {
//            DataView dvOrdersList = new DataView();

//            SqlParameter[] EmptySqlParameter = new SqlParameter[] { };
//            dvOrdersList = clsDataAccess.GetDataView("spWishListList", EmptySqlParameter);
//            dvOrdersList.RowFilter = strFilterExpression;
//            dvOrdersList.Sort = strSortExpression;

//            return dvOrdersList.ToTable();
//        }
//    #endregion

//    #region PROTECTED METHODS

//        protected virtual void GetData()
//        {
//            try
//                {
//                    //### Populate
//                    SqlParameter[] sqlParameter = new SqlParameter[] 
//                        {
//                            new SqlParameter("@iWishListID", m_iWishListID)
//                        };
//                DataRow drRecord = clsDataAccess.GetRecord("spWishListGetRecord", sqlParameter);

//                m_dtAdded = Convert.ToDateTime(drRecord["dtAdded"]);
//                m_iAddedBy = Convert.ToInt32(drRecord["iAddedBy"]);

//                if (drRecord["dtEdited"] != DBNull.Value)
//                   m_dtEdited = Convert.ToDateTime(drRecord["dtEdited"]);

//             if (drRecord["iEditedBy"] != DBNull.Value)
//               m_iEditedBy = Convert.ToInt32(drRecord["iEditedBy"]);

//                m_iProductID = Convert.ToInt32(drRecord["iProductID"]);
//                m_iAccountUserID = Convert.ToInt32(drRecord["iAccountUserID"]);
//                m_iQuantity = Convert.ToInt32(drRecord["iQuantity"]);
 
//            }
//            catch (Exception ex)
//            {
//                throw ex;
//            }
//        }
//    #endregion
//}