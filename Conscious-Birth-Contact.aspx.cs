﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Net.Mail;

public partial class Conscious_Birth_Contact : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //### Value is passed via QueryString
        if (Request.QueryString["strVal"] != "" && Request.QueryString["strVal"] != null)
        {
            string strValue = (Request.QueryString["strVal"]);
            popOption(strValue);
        }
    }

    private void popOption(string strValue)
    {
        lstSubject.SelectedValue = "2";
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        bool bCanSave = true;

        bCanSave = clsValidation.IsNullOrEmpty(txtName, bCanSave);
        bCanSave = clsValidation.IsNullOrEmpty(txtEmail, bCanSave);
        bCanSave = clsValidation.IsNullOrEmpty(txtContactNumber, bCanSave);
        bCanSave = clsValidation.IsNullOrEmpty(txtMessage, bCanSave);

        if (lstSubject.SelectedValue == "0")
        {
            lstSubject.Attributes.Remove("style");
            lstSubject.Attributes.Add("style", "background: #c92031;");
            bCanSave = false;
        }
        else
        {
            lstSubject.Style.Clear();
        }


        if (txtName.Text == "Name")
        {
            txtName.Attributes.Remove("style");
            txtName.Attributes.Add("style", "background: #c92031;");
            bCanSave = false;
        }
        else
        {
            txtName.Style.Clear();
        }

        if (txtEmail.Text == "Email")
        {
            txtEmail.Attributes.Remove("style");
            txtEmail.Attributes.Add("style", "background: #c92031;");
            bCanSave = false;
        }
        else
        {
            txtEmail.Style.Clear();
        }

        if (txtContactNumber.Text == "Contact Number")
        {
            txtContactNumber.Attributes.Remove("style");
            txtContactNumber.Attributes.Add("style", "background: #c92031;");
            bCanSave = false;
        }
        else
        {
            txtContactNumber.Style.Clear();
        }

        if (txtMessage.Text == "Message")
        {
            txtMessage.Attributes.Remove("style");
            txtMessage.Attributes.Add("style", "background: #c92031;");
            bCanSave = false;
        }
        else
        {
            txtMessage.Style.Clear();
        }

        //### If the form passes validation Save the data.
        if (bCanSave == true)
        {
            try
            {
                SendUserMail();
                SendAdminMail();
            }
            catch { }

            if (lstSubject.SelectedValue == "2")
            {
                clsCustomerTestimonials clsCustomerTestimonials = new clsCustomerTestimonials();
                clsCustomerTestimonials.dtAdded = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
                clsCustomerTestimonials.iAddedBy = 0;
                clsCustomerTestimonials.strCustomerName = txtName.Text;
                clsCustomerTestimonials.strTestimonial = txtMessage.Text;
                clsCustomerTestimonials.bIsApproved = false;

                clsCustomerTestimonials.Update();
            }

            Response.Redirect("Conscious-Birth-Home.aspx");
        }
        else
        {
        }
    }

    #region EMAIL METHODS

    protected void SendUserMail()
    {
        Attachment[] empty = new Attachment[] { };
        string strContent = "";

        StringBuilder strbMail = new StringBuilder();
        strbMail.AppendLine("<table>");
        strbMail.AppendLine("<tr>");
        strbMail.AppendLine("<td>");
        strbMail.AppendLine("Dear " + txtName.Text + "<br />");
        strbMail.AppendLine("Thank you for your enquiry. ");
        strbMail.AppendLine("If you have not joined us on facebook, please visit our <a href=\"http://www.facebook.com/Conscious Birth/\" style='color:#21285f;'>Facebook page</a><br />");
        strbMail.AppendLine("</td>");
        strbMail.AppendLine("</tr>");
        strbMail.AppendLine("</table>");

        strContent = strbMail.ToString();

       clsCommonFunctions.SendMail("noreply@ConsciousBirth.co.za", txtEmail.Text, "", "", "Conscious Birth - Automated Response", strContent, empty, true);
    }

    protected void SendAdminMail()
    {
        Attachment[] empty = new Attachment[] { };
        string strContent = "";
        string strSubject = "";

        if (lstSubject.SelectedIndex == 1)
            strSubject = "Compliment";
        else if (lstSubject.SelectedIndex == 2)
            strSubject = "Testimonial";
        else if (lstSubject.SelectedIndex == 3)
            strSubject = "Complaint";
        else if (lstSubject.SelectedIndex == 4)
            strSubject = "Other";

        StringBuilder strbMail = new StringBuilder();
        strbMail.AppendLine("<table>");
        strbMail.AppendLine("<tr>");
        strbMail.AppendLine("<td>");
        strbMail.AppendLine("Dear Admin,<br />");
        strbMail.AppendLine("You have a enquiry message.<br />");
        strbMail.AppendLine("Subject: " + strSubject + "<br />");
        strbMail.AppendLine("Name: " + txtName.Text + "<br />");
        strbMail.AppendLine("Email: <span style='color:#666;'>" + txtEmail.Text + "</span><br />");
        strbMail.AppendLine("Message: " + txtMessage.Text + "<br /><br />");
        strbMail.AppendLine("</td>");
        strbMail.AppendLine("</tr>");
        strbMail.AppendLine("</table>");

        strContent = strbMail.ToString();

        clsCommonFunctions.SendMail(txtEmail.Text, "info@consciousbirth.co.za", "", "", "Conscious Birth - Online Enquiry - " + strSubject, strContent, empty, true);
    }

    #endregion
}