using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class CMS_frmWishListsEventsAddModify : System.Web.UI.Page
{
    clsUsers clsUsers;
    clsWishListsEvents clsWishListsEvents;
    #region EVENT METHODS

    protected void Page_Load(object sender, EventArgs e)
    {
        //### Check if session clsUser exists
        if (Session["clsUsers"] == null)
        {
            {
                //### Redirect back to login
                Response.Redirect("../CMSLogin.aspx");
            }
        }
        clsUsers = (clsUsers)Session["clsUsers"];

        if (!IsPostBack)
        {
             //popWishListEvent();
             popAccountUser();

            //### If the iWishListsEventID is passed through then we want to instantiate the object with that iWishListsEventID
            if ((Request.QueryString["iWishListsEventID"] != "") && (Request.QueryString["iWishListsEventID"] != null))
            {
                clsWishListsEvents = new clsWishListsEvents(Convert.ToInt32(Request.QueryString["iWishListsEventID"]));

                //### Populate the form
                popFormData();
            }
            else
            {
                clsWishListsEvents = new clsWishListsEvents();
            }
            Session["clsWishListsEvents"] = clsWishListsEvents;
        }
        else
        {
            clsWishListsEvents = (clsWishListsEvents)Session["clsWishListsEvents"];
        }
    }

    protected void lnkbtnBack_Click(object sender, EventArgs e)
    {
        //### Go back to previous page
        Response.Redirect("frmWishListsEventsView.aspx");
    }

    protected void lnkbtnSave_Click(object sender, EventArgs e)
    {
        //### Validate registration process
        bool bCanSave = true;

       bCanSave = clsValidation.IsNullOrEmpty(lstWishListEvent, bCanSave);
       bCanSave = clsValidation.IsNullOrEmpty(lstAccountUser, bCanSave);
       bCanSave = clsValidation.IsNullOrEmpty(txtEvent, bCanSave);

        if (bCanSave == true)
        {
            mandatoryDiv.Visible = false;
            lblValidationMessage.Text = "<div class=\"madatoryPaddingDiv\"><img src=\"images/Validation/imgFaceHappy.png\" alt='' title=''/><div class=\"validationMessage\">WishListsEvent added successfully</div></div>";
            SaveData();
        }
        else
        {
            mandatoryDiv.Visible = true;
            lblValidationMessage.Text = "<div class=\"madatoryPaddingDiv\"><img src=\"images/Validation/imgFaceSad.png\" alt='' title=''/><div class=\"validationMessage\">Please fill out all mandatory fields - WishListsEvent not added</div></div>";
        }
    }

    protected void lnkbtnClear_Click(object sender, EventArgs e)
    {
        mandatoryDiv.Visible = false;

        lstWishListEvent.SelectedValue = "0";
        clsValidation.SetValid(lstWishListEvent);
        lstAccountUser.SelectedValue = "0";
        clsValidation.SetValid(lstAccountUser);
        txtEvent.Text = "";
        clsValidation.SetValid(txtEvent);
        txtAddressOfEvent.Text = "";
        clsValidation.SetValid(txtAddressOfEvent);
    }

    #endregion 

    #region POPULATE DATA METHODS

    private void popFormData()
    {
         lstWishListEvent.SelectedValue = clsWishListsEvents.iWishListEventID.ToString();
         lstAccountUser.SelectedValue = clsWishListsEvents.iAccountUserID.ToString();
         txtEvent.Text = clsWishListsEvents.dtEvent.ToString("dd MMM yyyy");
         txtAddressOfEvent.Text = clsWishListsEvents.strAddressOfEvent;
    }
    
    //private void popWishListEvent()
    //{
    //     DataTable dtWishListEventsList = new DataTable();
    //     lstWishListEvent.DataSource = clsWishListEvents.GetWishListEventsList();

    //     //### Populates the drop down list with PK and TITLE;
    //     lstWishListEvent.DataValueField = "iWishListEventID";
    //     lstWishListEvent.DataTextField = "strTitle";

    //     //### Bind the data to the list;
    //     lstWishListEvent.DataBind();

    //     //### Add default select option;
    //     lstWishListEvent.Items.Insert(0, new ListItem("--Not Selected--", "0"));
    //}
    
    private void popAccountUser()
    {
         DataTable dtAccountUsersList = new DataTable();
         lstAccountUser.DataSource = clsAccountUsers.GetAccountUsersList();

         //### Populates the drop down list with PK and TITLE;
         lstAccountUser.DataValueField = "iAccountUserID";
         lstAccountUser.DataTextField = "strTitle";

         //### Bind the data to the list;
         lstAccountUser.DataBind();

         //### Add default select option;
         lstAccountUser.Items.Insert(0, new ListItem("--Not Selected--", "0"));
    }
    
    #endregion

    #region SAVE DATA METHODS

    private void SaveData()
    {
        //### Add / Update
        //clsWishListsEvents.iWishListEventID = Convert.ToInt32(lstWishListEvent.SelectedValue.ToString());
        clsWishListsEvents.dtAdded = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
        clsWishListsEvents.iAddedBy = clsUsers.iUserID;
        clsWishListsEvents.dtEdited = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
        clsWishListsEvents.iEditedBy = clsUsers.iUserID;
        clsWishListsEvents.iAccountUserID = Convert.ToInt32(lstAccountUser.SelectedValue.ToString());
        clsWishListsEvents.dtEvent = Convert.ToDateTime(txtEvent.Text);
        clsWishListsEvents.strAddressOfEvent = txtAddressOfEvent.Text;

        clsWishListsEvents.Update();

        Session["dtWishListsEventsList"] = null;

        //### Go back to view page
        Response.Redirect("frmWishListsEventsView.aspx");
    }

    #endregion
}