﻿<%@ Page Title="Blissful Pregnancy :: Conscious Birth" Language="C#" MasterPageFile="~/ConsciousBirth.master" AutoEventWireup="true" CodeFile="Conscious-Birth-ModalitiesAndToolsTheoniUses.aspx.cs" Inherits="Conscious_Birth_Blissful_Pregnancy" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript" src="js/jquery.min.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="main_wrapper">
        <!-- C O N T E N T -->
        <div class="content_wrapper">
            <div class="page_title_block">
                <div class="container">
                    <h2 class="title">Modalities and tools Theoni uses</h2>
                </div>
            </div>
            <div class="container">
                <div class="content_block no-sidebar row">
                    <div class="fl-container span12">
                        <div class="row">
                            <div class="posts-block span12">
                                <div class="contentarea">

                                    <div class="row-fluid">
                                        <div class="span4 module_cont module_text_area module_none_padding">
                                            <img src="img/blissful-pregnancy.png" alt="" />
                                        </div>
                                        <div class="span8 module_cont module_text_area module_none_padding">
                                            <blockquote class="type2">
                                                <h6>Modalities and tools Theoni uses:</h6>
                                                <p>
                                                    Theoni utilizes several modalities and tools in her work. She has two decades worth of 
                                                    experience in working with women and uses an integrated approach to incorporate all she
                                                    has learned in order to assist you in just the right way.
                                                    Her repertoire covers the physical body as well as the mind and obviously the emotions. 
                                                    Depending on the unique circumstance with which you require assistance, she will combine
                                                    and integrate the most appropriate blend of therapies to suit you. The approach she takes 
                                                    with each person is therefore totally unique and custom designed.
                                                </p>
                                                <br />
                                                <h6>NLP</h6>
                                                <p>
                                                    NLP focuses on possibilities and how the mind works to produce 
                                                    results and under Theoni’s guidance, 
                                                    NLP enables women to have better, fuller and richer lives.
                                                </p>
                                                <p>
                                                    In Theoni’s hands the NLP techniques guide women to get in touch with their core selves, helping 
                                                    them to cope better with all life situations, ultimately allowing them to flow with life. Theoni 
                                                    is passionate about teaching women to design their own best life, so that they may achieve their
                                                     goals, dreams and desires.
                                                </p>
                                                <br />
                                                <br />
                                                <h6>Life Alignment</h6>
                                                <p>
                                                    Life Alignment works with the innate intelligence of our bodies, tapping into powerful energy systems 
                                                    such as acupuncture meridians and chakras. Disease can result from an imbalance in these systems 
                                                    that manifests at a physical level.
                                                </p>

                                                <p>
                                                    Using applied kinesiology techniques and a pendulum, the specific energy blockages and related emotional 
                                                    stress can be identified. Life alignment clears and balances these energy pathways, restoring energy flow 
                                                    throughout our whole system, which in turn stimulates physical healing. Our thoughts create our emotions 
                                                    and our emotions create our physical state or being.
                                                </p>
                                                <br />
                                                <br />
                                                <h6>Hypnotherapy</h6>
                                                <p>
                                                    Hypnotherapy is really deep relaxation, and not as scary as the media makes it out to be. All hypnosis is 
                                                    actually self-hypnosis, meaning that you are always in control and aware of your surroundings. Hypnosis 
                                                    teaches people how to master their own states of awareness. By doing so they can affect their own bodily 
                                                    functions and psychological responses. The success of hypnotherapy as a technique is what lead Theoni to 
                                                    begin teaching HypnoBirthing 9 years ago.
                                                </p>
                                                <br />
                                                <br />
                                                <h6>Kahuna massage & bodywork</h6>
                                                <p>
                                                    This is a therapeutic massage based on the teachings and practices of ancient Hawaiian Kahunas. It is 
                                                    extremely relaxing and therapeutic. See the Kahuna Massage sections for more detail.
                                                </p>
                                                <br />
                                                <br />
                                                <h6>Kundalini yoga</h6>
                                                <p>
                                                    Kundalini Yoga is known as a dynamic form of yoga that awakens awareness. It uses sets of yogic postures 
                                                    and movements (asana) with meditative focus, breath work (pranayama), and chanting (mantra). In addition 
                                                    to strengthening the health and well-being of the physical body, it is a useful support for emotional balance, 
                                                    mental clarity, stress relief, and personal transformation. Kundalini Yoga is the original and most powerful 
                                                    system of yoga. It can be practiced by anyone if it is done according to instruction and within the tradition 
                                                    in which it was originally taught.
                                                </p>

                                            </blockquote>
                                        </div>
                                    </div>
                                    <!-- .row-fluid -->
                                    <div class="module_line_trigger" data-background="#ffeec9 url(img/VV.png) no-repeat center" data-top-padding="bottom_padding_huge" data-bottom-padding="module_big_padding">
                                        <div class="row-fluid">
                                            <div class="span12 module_cont module_promo_text module_huge_padding">
                                                <div class="shortcode_promoblock">
                                                    <div class="promo_button_block type2">
                                                        <a href="Conscious-Birth-Products.aspx" class="promo_button">View our Products</a>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--.module_cont -->
                                            <div class="clear">
                                                <!-- ClearFIX -->
                                            </div>
                                        </div>
                                        <!-- .row-fluid -->
                                    </div>
                                    <!-- .module_line_trigger -->
                                    <div class="row-fluid">
                                        <div class="span12 module_cont module_partners center_title module_big_padding2">
                                            <div class="bg_title">
                                                <div class="title_wrapper">
                                                    <a href="javascript:void(0)" class="btn_carousel_left"></a>
                                                    <h5 class="headInModule">Our Partners</h5>
                                                    <a href="javascript:void(0)" class="btn_carousel_right"></a>
                                                </div>
                                            </div>
                                            <div class="module_content sponsors_works carouselslider items5" data-count="5">
                                                <ul>
                                                    <li>
                                                        <div class="item">
                                                            <a href="#charity-html5css3-website-template/3180454" target="_blank">
                                                                <img src="img/pictures/partners2.png" alt=""></a>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="item">
                                                            <a href="#point-business-responsive-wp-theme/4319087" target="_blank">
                                                                <img src="img/pictures/partners4.png" alt=""></a>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="item">
                                                            <a href="#cleanspace-retina-ready-business-wp-theme/3776000" target="_blank">
                                                                <img src="img/pictures/partners1.png" alt=""></a>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="item">
                                                            <a href="#yellowproject-multipurpose-retina-wp-theme/4066662" target="_blank">
                                                                <img src="img/pictures/partners3.png" alt=""></a>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="item">
                                                            <a href="#showroom-portfolio-retina-ready-wp-theme/3473628" target="_blank">
                                                                <img src="img/pictures/partners5.png" alt=""></a>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="item">
                                                            <a href="themeforest.net/item/incipiens-responsive-portfolio-wordpress-theme/2762691" target="_blank">
                                                                <img src="img/pictures/partners6.png" alt=""></a>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="item">
                                                            <a href="themeforest.net/item/hq-photography-responsive-wp-theme/3200962" target="_blank">
                                                                <img src="img/pictures/partners9.png" alt=""></a>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="item">
                                                            <a href="#incipiens-responsive-portfolio-wordpress-theme/2762691" target="_blank">
                                                                <img src="img/pictures/partners11.png" alt=""></a>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- .row-fluid -->

                                </div>
                                <!-- .contentarea -->
                            </div>
                            <div class="left-sidebar-block span3">
                                <aside class="sidebar">
                                    //Sidebar Text
                                </aside>
                            </div>
                            <!-- .left-sidebar -->
                        </div>
                        <div class="clear">
                            <!-- ClearFix -->
                        </div>
                    </div>
                    <!-- .fl-container -->
                    <div class="right-sidebar-block span3">
                        <aside class="sidebar">
                        </aside>
                    </div>
                    <!-- .right-sidebar -->
                    <div class="clear">
                        <!-- ClearFix -->
                    </div>
                </div>
            </div>
            <!-- .container -->
        </div>
        <!-- .content_wrapper -->

    </div>
    <!-- .main_wrapper -->
</asp:Content>

