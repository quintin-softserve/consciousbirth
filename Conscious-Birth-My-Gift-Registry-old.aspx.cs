﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Conscious_Birth_MyWishlist : System.Web.UI.Page
{
    #region CLASSES AND VARIABLES

    clsAccountUsers clsAccountUsers;

    DataTable dtOrderItemsList;
    List<clsOrderItems> glstOrderItems;

    //clsAccountHolders clsAccountHolders;
    //clsDeliveryInfos clsDeliveryInfos;

    bool bHasDiscount;
    int iOrderID = 0;

    #endregion

    #region EVENT METHODS

    protected void Page_Load(object sender, EventArgs e)
    {
        //### Check if session clsUser exists
        if (Session["clsAccountUsers"] == null)
        {
            //### Redirect back to login
            Response.Redirect("Conscious-Birth-User-Login.aspx");
        }
        clsAccountUsers = (clsAccountUsers)Session["clsAccountUsers"];

        popMyWishlist();
        UserName.Text = clsAccountUsers.strFirstName;
    }

    protected void btnShopMore_Click(object sender, EventArgs e)
    {
        Response.Redirect("Conscious-Birth-Products.aspx");
    }

    protected void btnContinue_Click(object sender, EventArgs e)
    {


        //divValidationMain.Visible = false;
        ////divCheckout.Visible = false;

        //if (Request.Cookies["OrderItems"] == null)
        //{
        //    divData.Visible = false;
        //    divNoData.Visible = true;
        //}

        //if (Session["clsAccountHolders"] != null)
        //{
        //    //divSteps.Visible = true;
        //    step2.Visible = true;

        //    divHeading.Visible = false;
        //    divGrid.Visible = false;
        //    divTotalAndButtons.Visible = false;

        //    //Master.hidePopup();
        //}
        //else
        //{
        //    divValidationMain.Visible = true;
        //    //divSteps.Visible = true;

        //    divHeading.Visible = false;
        //    divGrid.Visible = false;
        //    divTotalAndButtons.Visible = false;

        //    //Master.hidePopup();
        //}
    }

    protected void btnEmptyBag_Click(object sender, EventArgs e)
    {
        clearCookie();

        Response.Redirect("Conscious-Birth-Cart.aspx");
    }

    //protected void btnProceedWithoutAccount_Click(object sender, EventArgs e)
    //{
    //    //divSteps.Visible = true;
    //    step2.Visible = true;

    //    divValidationMain.Visible = false;
    //    divHeading.Visible = false;
    //    divGrid.Visible = false;
    //    divTotalAndButtons.Visible = false;

    //    //Master.hidePopup();
    //}

    protected void btnProceedToStep3_Click(object sender, EventArgs e)
    {
        //### Validate registration process
        bool bCanSave = true;
        bool bIsValidDate = true;

        StringBuilder strbValidationMessage = new StringBuilder();

        bCanSave = clsValidation.IsNullOrEmpty(txtContactPerson, bCanSave);
        bCanSave = clsValidation.IsNullOrEmpty(txtContactNumber, bCanSave);
        bCanSave = clsValidation.IsNullOrEmpty(txtEmailAddress, bCanSave);
        bCanSave = clsValidation.IsNullOrEmpty(txtPhysicalAddress, bCanSave);

        string strDateError = "";
        string strGenericError = "";


        if (bCanSave == true)
        {
            Attachment[] empty = new Attachment[] { };
            try
            {
                SendMail();
                SendAdminMail();/*"", txtEmailAddress.Text, "", "", "Product Order", /*("", txtEmailAddress.Text, "", "", "Product Order", "", empty, true);*/
                SaveData();
            }
            catch
            {

            }

            //divSteps.Visible = true;
            mandatoryDiv.Visible = false;
            step2.Visible = false;
            divHeading.Visible = false;
            divGrid.Visible = false;
            divTotalAndButtons.Visible = false;
            Response.Redirect("Conscious-Birth-Thank-You.aspx");

        }
        else
        {
            //divSteps.Visible = true;
            step2.Visible = true;
            mandatoryDiv.Visible = true;
            divHeading.Visible = false;
            divGrid.Visible = false;
            divTotalAndButtons.Visible = false;

            strGenericError = "Sorry, not all required fields have been entered.<br />";
        }

        strbValidationMessage.AppendLine("<div class=\"madatoryPaddingDiv\" style='margin-left:0px;'><img src=\"images/imgSadBaby.png\" alt='Conscious Birth' title='Conscious Birth'/><div style=color:#401414;\">");

        if (!bCanSave)
        {
            strbValidationMessage.AppendLine(strGenericError);
            strbValidationMessage.AppendLine("</div></div><br class='clear' />");

            lblValidationMessage.Text = strbValidationMessage.ToString();
        }

        if (!bIsValidDate)
        {
            strbValidationMessage.AppendLine(strDateError);
            strbValidationMessage.AppendLine("</div></div><br class='clear' />");

            lblValidationMessage.Text = strbValidationMessage.ToString();
        }
    }



    #endregion

    #region SAVE DATA METHODS

    private void SaveData()
    {
        clsDeliveryInfos clsDeliveryInfos = new clsDeliveryInfos();

        //### Add / Update
        clsDeliveryInfos.dtAdded = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
        clsDeliveryInfos.iAddedBy = 0;
        clsDeliveryInfos.dtEdited = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
        clsDeliveryInfos.iEditedBy = 0;
        clsDeliveryInfos.strContactPerson = txtContactPerson.Text;
        clsDeliveryInfos.strContactNumber = txtContactNumber.Text;

        clsDeliveryInfos.strDeliveryAddress = txtPhysicalAddress.Text;
        clsDeliveryInfos.strEmailAddress = txtEmailAddress.Text;

        clsDeliveryInfos.Update();

        int iOrderID;

        iOrderID = clsCart.CartSetup(clsDeliveryInfos.iDeliveryInfoID, 0);

        // ADDS ALL THE PRODUCTS AND QUANTITIES TO THE ORDERS AND ORDER ITEMS TABLES
        DataTable dtCartItemsTest = clsCart.Cart();

        foreach (DataRow dtrProduct in dtCartItemsTest.Rows)
        {
            int iProductID = Convert.ToInt32(dtrProduct["iProductID"].ToString());
            int iQuantity = Convert.ToInt32(dtrProduct["iQuantity"].ToString());
            string strColour = dtrProduct["strColour"].ToString();
            string strSize = dtrProduct["strSize"].ToString();

            //clsCart.addCartItem(iProductID, iQuantity);
            clsOrderItems clsOrderItems = new clsOrderItems();

            //### Add / Update
            clsOrderItems.dtAdded = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            clsOrderItems.iAddedBy = 0;
            clsOrderItems.dtEdited = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            clsOrderItems.iEditedBy = 0;
            clsOrderItems.iOrderID = iOrderID;
            clsOrderItems.iProductID = iProductID;
            clsOrderItems.iQuantity = iQuantity;
            clsOrderItems.strColour = strColour;
            clsOrderItems.strSize = strSize;
            //strTest.AppendLine(iProductID.ToString() + " " + iQuantity.ToString() + "<br />");

            clsOrderItems.Update();
        }

        Session["dtDeliveryInfosList"] = null;

        //### show and hide pages
        step2.Visible = false;
        //step3.Visible = true;

        double dblTotalCostPlusDelivery = 0.0;
        clsOrders clsOrders = new clsOrders(iOrderID);

        //dblTotalCostPlusDelivery = clsOrders.dblTotalAmount;

        ////bHasDiscount = Convert.ToBoolean(Session["bHasDiscount"]);

        ////if (bHasDiscount)
        ////{
        ////    dblTotalCostPlusDelivery = ((clsOrders.dblTotalAmount) * 0.925);
        ////}

        //Session["dblTotalCostPlusDelivery"] = dblTotalCostPlusDelivery.ToString("N2");

        //OrderPayment(dblTotalCostPlusDelivery, clsOrders.strOrderNumber, "This order was processed at: " + DateTime.Now.ToString("HH:mm:ss on dd MMM yyyy"));

        //if (rbCustomTime.Checked)
        //{
        //    if (Session["clsAccountHolders"] != null)
        //    {
        //        if (clsAccountHolders.iOrderCount == 0)
        //            dblTotalCostPlusDelivery = ((clsOrders.dblTotalAmount + 100) * 0.95);
        //        else
        //            dblTotalCostPlusDelivery = clsOrders.dblTotalAmount + 100;
        //    }
        //    else
        //        dblTotalCostPlusDelivery = clsOrders.dblTotalAmount + 100;

        //    Session["dblTotalCostPlusDelivery"] = dblTotalCostPlusDelivery;

        //    OrderPayment(dblTotalCostPlusDelivery, clsOrders.strOrderNumber, "This order was processed at: " + DateTime.Now.ToString("dd MMM yyyy HH:mm:ss"));
        ////}
        //else
        //{
        //if (Session["clsAccountHolders"] != null)
        //{
        //    if (clsAccountHolders.iOrderCount == 0)
        //        dblTotalCostPlusDelivery = ((clsOrders.dblTotalAmount) * 0.95);
        //    else
        //dblTotalCostPlusDelivery = clsOrders.dblTotalAmount;
        //}
        //else
        //    dblTotalCostPlusDelivery = clsOrders.dblTotalAmount;

        Response.Redirect("Conscious-Birth-Thank-You.aspx");
    }

    private void OrderPayment(double dblOrderTotal, string strOrderNumber, string strDescription)
    {
        //try
        //{
        //    if (!Page.IsValid) return;

        //    // Create the order in your DB and get the ID
        //    string amount = dblOrderTotal.ToString();
        //    string orderId = strOrderNumber;//new CreateOrder( amount );
        //    string name = "Conscious Birth, " + orderId;
        //    string description = "";

        //    string site = "http://www.consciousbirth.co.za";
        //    string merchant_id = "10345226";
        //    string merchant_key = "xl5bxt5ocir3g";

        //    // Check if we are using the test or live system
        //    string paymentMode = System.Configuration.ConfigurationManager.AppSettings["PaymentMode"];

        //    if (paymentMode == "test")
        //    {
        //        site = "https://sandbox.payfast.co.za/eng/process?";
        //        merchant_id = "10000100";
        //        merchant_key = "46f0cd694581a";
        //    }
        //    else if (paymentMode == "live")
        //    {
        //        site = "https://www.payfast.co.za/eng/process?";
        //        merchant_id = System.Configuration.ConfigurationManager.AppSettings["PF_MerchantID"];
        //        merchant_key = System.Configuration.ConfigurationManager.AppSettings["PF_MerchantKey"];
        //    }
        //    else
        //    {
        //        throw new InvalidOperationException("Cannot process payment if PaymentMode (in web.config) value is unknown.");
        //    }

        //    // Build the query string for payment site

        //    StringBuilder str = new StringBuilder();
        //    str.Append("merchant_id=" + HttpUtility.UrlEncode(merchant_id));
        //    str.Append("&merchant_key=" + HttpUtility.UrlEncode(merchant_key));
        //    str.Append("&return_url=" + HttpUtility.UrlEncode(System.Configuration.ConfigurationManager.AppSettings["PF_ReturnURL"]));
        //    str.Append("&cancel_url=" + HttpUtility.UrlEncode(System.Configuration.ConfigurationManager.AppSettings["PF_CancelURL"]));
        //    str.Append("&notify_url=" + HttpUtility.UrlEncode(System.Configuration.ConfigurationManager.AppSettings["PF_NotifyURL"]));

        //    str.Append("&m_payment_id=" + HttpUtility.UrlEncode(orderId));
        //    str.Append("&amount=" + HttpUtility.UrlEncode(amount));
        //    str.Append("&item_name=" + HttpUtility.UrlEncode(name));
        //    str.Append("&item_description=" + HttpUtility.UrlEncode(description));

        //    // Redirect to PayFast
        //    Response.Redirect(site + str.ToString());
        //}
        //catch (Exception ex)
        //{
        //    // Handle your errors here (log them and tell the user that there was an error)
        //}
    }

    #endregion

    #region DATA GRID METHODS

    private void PopulateFormData()
    {
        //### Populate datagrid using clsOrderItemsList object
        try
        {
            StringBuilder sbBackOrder = new StringBuilder();

            dtOrderItemsList = new DataTable();

            if (Session["dtOrderItemsList"] == null)
                dtOrderItemsList = clsOrderItems.GetOrderItemsList("iOrderID = " + iOrderID + "", "");
            else
                dtOrderItemsList = (DataTable)Session["dtOrderItemsList"];

            foreach (DataRow dtrProducts in dtOrderItemsList.Rows)
            {
                clsProducts clsProducts = new clsProducts(Convert.ToInt32(dtrProducts["iProductID"]));

                if (clsProducts.dblPrice == 0.00)
                {
                    sbBackOrder.Append(@"<b>" + clsProducts.strTitle + "'s:</b> <span class='orange'><b>" + clsProducts.strStockCode + "</b></span> is back ordered.<br />");
                }
            }
        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    protected void lnkDeleteItem_Click(object sender, EventArgs e)
    {
        int iOrderItemID = int.Parse((sender as LinkButton).CommandArgument);
        string strColour = Request.QueryString["strColour"].ToString();
        string strSize = Request.QueryString["strSize"].ToString();
        int iQuantity = Convert.ToInt32(Request.QueryString["iQuantity"]);
        DeleteFromCookie(iOrderItemID, iQuantity, strColour, strSize);
    }

    protected void dgrGrid_SortCommand(object source, DataGridSortCommandEventArgs e)
    {
        //### Sort Method
        dtOrderItemsList = new DataTable();

        if (Session["dtOrderItemsList"] == null)
            dtOrderItemsList = clsOrderItems.GetOrderItemsList();
        else
            dtOrderItemsList = (DataTable)Session["dtOrderItemsList"];

        DataView dvTemp = dtOrderItemsList.DefaultView;

        dvTemp.Sort = e.SortExpression;
        dtOrderItemsList = dvTemp.ToTable();
        Session["dtOrderItemsList"] = dtOrderItemsList;

        PopulateFormData();
    }

    public void dgrGrid_PopImage(object sender, EventArgs e)
    {
        try
        {
            //### Add hyperlink to datagrid item
            Image imgProduct = (Image)sender;
            DataGridItem dgrItemEdit = (DataGridItem)imgProduct.Parent.Parent.Parent;

            //### Get the iOrderItemID
            int iOrderItemID = 0;
            iOrderItemID = int.Parse(dgrItemEdit.Cells[0].Text);

            clsOrderItems clsOrderItems = new clsOrderItems(iOrderItemID);

            //if (clsOrderItems.bIsJewellery == false)
            //{
            //    clsWatches clsWatches = new clsWatches(clsOrderItems.iWatchID);
            //    //### START: GET WATCH IMAGE

            //    string strHTMLImageWatch = "";

            //    try
            //    {
            //        string[] strImagesWatch = Directory.GetFiles(ConfigurationManager.AppSettings["WebRootFullPath"] + "\\Watches\\" + clsWatches.strPathToImages);

            //        foreach (string strImageProduct in strImagesWatch)
            //        {
            //            if (strImageProduct.Contains("_sml"))
            //            {
            //                strHTMLImageWatch = strImageProduct.Replace(ConfigurationManager.AppSettings["WebRootFullPath"], ConfigurationManager.AppSettings["WebRoot"]).Replace("\\", "/");
            //                break;
            //            }
            //        }
            //    }
            //    catch
            //    {

            //    }
            //    imgProduct.ImageUrl = strHTMLImageWatch;
            //    imgProduct.Height = 50;
            //}
            //else
            {
                clsProducts clsProducts = new clsProducts(clsOrderItems.iProductID);
                //### START: GET IMAGE

                string strHTMLImageProduct = "";

                try
                {
                    string[] strImagesProduct = Directory.GetFiles(ConfigurationManager.AppSettings["WebRootFullPath"] + "\\Products\\" + clsProducts.strPathToImages);

                    foreach (string strImageProduct in strImagesProduct)
                    {
                        if (strImageProduct.Contains("_sml"))
                        {
                            strHTMLImageProduct = strImageProduct.Replace(ConfigurationManager.AppSettings["WebRootFullPath"], "").Replace("\\", "/").Substring(1);//.Replace(ConfigurationManager.AppSettings["WebRoot"],"");
                            break;
                        }
                    }
                }
                catch
                {

                }
                imgProduct.ImageUrl = strHTMLImageProduct;
                imgProduct.Height = 50;
            }
        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    public void dgrGrid_PopLink(object sender, EventArgs e)
    {
        try
        {
            //### Add hyperlink to datagrid item
            LinkButton lnkProduct = (LinkButton)sender;
            DataGridItem dgrItemEdit = (DataGridItem)lnkProduct.Parent.Parent;

            //### Get the iOrderItemID
            int iOrderItemID = 0;
            iOrderItemID = int.Parse(dgrItemEdit.Cells[0].Text);

            clsOrderItems clsOrderItems = new clsOrderItems(iOrderItemID);

            //if (clsOrderItems.bIsJewellery == false)
            //{
            //    clsWatches clsWatches = new clsWatches(clsOrderItems.iWatchID);

            //    lnkProduct.Attributes.Add("href", "Luxury-Time-Product-Info.aspx?iWatchID=" + clsWatches.iWatchID + "&prodtype=watch");
            //    //lnkProduct. = "";
            //}
            //else
            {
                clsProducts clsProducts = new clsProducts(clsOrderItems.iProductID);

                lnkProduct.Attributes.Add("href", "Conscious-Birth-Product-Details.aspx?iProductID=" + clsProducts.iProductID);
                //lnkProduct.PostBackUrl = "";
            }
        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    public void dgrGrid_PopTotalCost(object sender, EventArgs e)
    {
        try
        {
            //### Add hyperlink to datagrid item
            Label lblTotalCost = (Label)sender;
            DataGridItem dgrItemDelete = (DataGridItem)lblTotalCost.Parent.Parent;

            //### Get the iOrderItemID
            int iOrderItemID = 0;
            iOrderItemID = int.Parse(dgrItemDelete.Cells[0].Text);

            clsOrderItems clsOrderItems = new clsOrderItems(iOrderItemID);

            {
                clsProducts clsProducts = new clsProducts(clsOrderItems.iProductID);

                lblTotalCost.Text = (clsProducts.dblPrice * clsOrderItems.iQuantity).ToString("N2"); //clsOrderItems.iQuantity
            }
        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    public void dgrGrid_PopQuantity(object sender, EventArgs e)
    {
        try
        {
            //### Add hyperlink to datagrid item
            TextBox txtQuantity = (TextBox)sender;
            DataGridItem dgrItemDelete = (DataGridItem)txtQuantity.Parent.Parent;

            //### Get the iOrderItemID
            int iOrderItemID = 0;
            iOrderItemID = int.Parse(dgrItemDelete.Cells[0].Text);

            clsOrderItems clsOrderItems = new clsOrderItems(iOrderItemID);

            //if (clsOrderItems.bIsJewellery == false)
            //{
            //    txtQuantity.Text = clsOrderItems.iQuantity.ToString();
            //}
            //else
            {
                txtQuantity.Text = clsOrderItems.iQuantity.ToString();
            }

            //txtQuantity.TextChanged += new EventHandler(txtQuantity_TextChanged);
        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    public void dgrGrid_PopDeleteLink(object sender, EventArgs e)
    {
        try
        {
            //### Add hyperlink to datagrid item
            HyperLink lnkDeleteItem = (HyperLink)sender;
            DataGridItem dgrItemDelete = (DataGridItem)lnkDeleteItem.Parent.Parent;

            //### Get the iOrderItemID
            int iOrderItemID = 0;
            iOrderItemID = int.Parse(dgrItemDelete.Cells[0].Text);

            //### Add attributes to delete link
            lnkDeleteItem.CssClass = "dgrDeleteLink";
            lnkDeleteItem.Attributes.Add("href", "Conscious-Birth-Cart.aspx?action=delete&iOrderItemID=" + iOrderItemID);
        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    #endregion

    #region POPULATE DATA TABLE

    protected void popMyWishlist()
    {
        StringBuilder strbWishlistGrid = new StringBuilder();

        int iAccountUserID = clsAccountUsers.iAccountUserID;

        DataTable dtWishlistProducts = clsUserProductWishlistLink.GetUserProductWishlistLinkList("iUserID='" + iAccountUserID + "'", "");

        foreach (DataRow dtWishlistProduct in dtWishlistProducts.Rows)
        {
            {
                clsProducts clsWislistProducts = new clsProducts(Convert.ToInt32(dtWishlistProduct["iProductID"]));
                String test = clsWislistProducts.strTitle;
                String strFullPathToImage = "Products/" + clsWislistProducts.strPathToImages + "/" + clsWislistProducts.strMasterImage;
                strbWishlistGrid.Append(@"<td align='center'
                            <a ><img src='" + strFullPathToImage + "' style='border: none; height: 120px; width: 120px; padding-top: 5px;' /></a>"
                            + "</td>"
                            + "<td align='center'>"
                            + "<div style='padding-left:10px; padding-right: 10px;'><span style='font-size: 18px;'>" + test + "</span></div><br/>"
                            + "</td>"
                            + "<td align='center'>"
                            + dtWishlistProduct["iQuantity"]
                            + "</td>"
                            + "<td align='center'>"
                            + dtWishlistProduct["strColor"]
                            + "</td>"
                            + "<td align='center'>"
                            + dtWishlistProduct["strSize"]
                            + "</td>"
                            + "<td align='center'>"
                            + clsWislistProducts.dblPrice
                            + "</td>"
                            + "<td>"
                              + "<a href='Conscious-Birth-Cart.aspx?action=delete&iProductID=" + clsWislistProducts.iProductID + "&iQuantity=" + dtWishlistProduct["iQuantity"] + "&strColour=" + dtWishlistProduct["strColor"] + "&strSize=" + dtWishlistProduct["strSize"] + "' onclick='return ConfirmRecordDeleteCart()' class='dgrLinkDelete'></a>"
                            + "</td>"
                            +"<br/>"
                             + "</tr>"
                            );
                litGridData.Text = strbWishlistGrid.ToString();
            }
        }
       
    }

    protected void btnProcceedWishlist_Click(object sender, EventArgs e)
    {
        if ((Session["loggedOutOrder"] != null) && (Session["loggedOutOrder"].ToString() != "0"))
        {
            StringBuilder sbRows = new StringBuilder();
            StringBuilder sbBackOrder = new StringBuilder();
            string iProductID = "";
            int iRowCount = 0;
            string strColour = "";
            string strSize = "";
            string iQuanity = "";
            string[] strCartItems = HttpContext.Current.Session["loggedOutOrder"].ToString().Split(new char[] { ',' });
            foreach (string value in strCartItems)
            {

                iRowCount++;


                string[] strCartItemDetails = value.ToString().Split(new char[] { '|' });
                iQuanity = strCartItemDetails[1];
                strColour = strCartItemDetails[2];
                strSize = strCartItemDetails[3];
                iProductID = strCartItemDetails[0];
                clsProducts clsProducts = new clsProducts(Convert.ToInt32(iProductID));

                clsUserProductWishlistLink clsUserProductWishlistLink = new clsUserProductWishlistLink();
                clsUserProductWishlistLink.iWishListEventID = Convert.ToInt32(Session["clsAccountUsers"]);
                clsUserProductWishlistLink.iProductID = Convert.ToInt32(iProductID);
                clsUserProductWishlistLink.strSize = strSize;
                clsUserProductWishlistLink.iQuantity = Convert.ToInt32(iQuanity);
                clsUserProductWishlistLink.Update();
                clearCookie();
                Response.Redirect("Conscious-Birth-Products.aspx");




            }

        }
    }






    protected string getProductImage(int iProductID)
    {
        clsProducts clsProducts = new clsProducts(Convert.ToInt32(iProductID));

        string strHTMLImageProduct = "";

        try
        {
            string[] strImagesProduct = Directory.GetFiles(ConfigurationManager.AppSettings["WebRootFullPath"] + "\\Products\\" + clsProducts.strPathToImages);

            foreach (string strImageProduct in strImagesProduct)
            {
                if (strImageProduct.Contains("_sml"))
                {
                    strHTMLImageProduct = strImageProduct.Replace(ConfigurationManager.AppSettings["WebRootFullPath"], "").Replace("\\", "/").Substring(1);
                    break;
                }
            }
        }
        catch
        {
        }

        return strHTMLImageProduct;
    }

    #endregion

    #region DELETE METHODS

    private static void clearCookie()
    {
        HttpCookie aCookie = new HttpCookie("OrderItems");
        aCookie.Values["OrderItemInfo"] = null;
        aCookie.Expires = DateTime.Now.AddMonths(-1);
        HttpContext.Current.Response.Cookies.Add(aCookie);
        HttpContext.Current.Response.Cookies["OrderItems"]["OrderItemInfo"] = null;
        HttpContext.Current.Response.Cookies["OrderItems"].Expires = DateTime.Now.AddMonths(-1);
        HttpContext.Current.Session["loggedOutOrder"] = "0";
    }

    protected void DeleteItem(int iOrderItemID)
    {
        clsOrderItems clsOrderItems = new clsOrderItems(iOrderItemID);

        clsOrders clsOrders = new clsOrders(clsOrderItems.iOrderID);

        clsOrders.dtEdited = DateTime.Now;
        clsOrders.iEditedBy = -15;//clsAccountHolders.iAccountHolderID;
        clsOrders.iOrderStatusID = 1;

        clsOrders.Update();

        clsOrderItems.Delete(iOrderItemID);

        if (clsOrderItems.GetOrderItemsList("iOrderID=" + iOrderID + " AND bIsDeleted=0", "").Rows.Count == 0)
        {
            clsOrders.Delete(iOrderID);
            Session["clsOrders"] = null;
            Session["iOrderID"] = null;

            PopulateFormData();

            lblGridTotal.Text = "0.00";

            divData.Visible = false;
            divNoData.Visible = true;

            Response.Redirect("Conscious-Birth-Cart.aspx");
        }
        else
        {

            PopulateFormData();

            lblGridTotal.Text = clsCart.GetOrderTotal(iOrderID).ToString("N2");
        }
    }

    protected void DeleteFromCookie(int iProductID, int iQuantity, string strColours, string strSizes)
    {
        int iCartCount = clsCart.GetLoggedOutCartCount();
        string strColour = strColours;
        string strSize = strSizes;

        if (iCartCount == 1)
        {
            if (Request.Cookies["OrderItems"] != null)
            {
                clsCart.removeItemFromCart(iProductID, iQuantity, strColour, strSize);
            }
            else
            {
                divData.Visible = false;
                divNoData.Visible = true;
            }

            //clearCookie();
            Response.Redirect("Conscious-Birth-Cart.aspx");
        }
        else
        {
            if (Request.Cookies["OrderItems"] != null)
            {
                clsCart.removeItemFromCart(iProductID, iQuantity, strColour, strSize);
            }
            else
            {
                divData.Visible = false;
                divNoData.Visible = true;
            }
        }
    }

    #endregion

    #region EMAIL METHODS

    protected string BuildCartOrderRows(int iProductID, int iQuantity, string strColour, string strSize)
    {
        StringBuilder sbGrid = new StringBuilder();
        {
            clsProducts clsProducts = new clsProducts(Convert.ToInt32(iProductID));

            sbGrid.Append(@"<td align='center'>
                                <img src='http://www.consciousbirth.co.za/" + getProductImage(Convert.ToInt32(iProductID)) + "' alt='' height='50px' style='border: none;' />"
                        + "</td>"
                        + "<td align='center'>"
                        + "<span style=\"font-family:Arial; font-size:13px;\"><div style='padding-left:10px; padding-right: 10px;'>" + clsProducts.strTitle + "</div><br/></span>"
                        + "</td>"
                        + "<td align='center'>"
                        + "<span  style=\"font-family:Arial; font-size:13px;\">" + iQuantity + "<s/pan>"
                        + "</td>"
                        + "<td align='center'>"
                        + "<span style=\"font-family:Arial; font-size:13px;\">" + strColour + "</span>"
                        + "</td>"
                        + "<td align='center'>"
                        + "<span  style=\"font-family:Arial; font-size:13px;\">" + strSize + "</span>"
                        + "</td>"
                        + "<td align='center'>"
                        + "<span  style=\"font-family:Arial; font-size:13px;\">" + (clsProducts.dblPrice * Convert.ToInt32(iQuantity)).ToString("N2") + "</span>"
                        + "</td>"
                        + "<td>"
                        + "<a href='Conscious-Birth-Cart.aspx?action=delete&iProductID=" + clsProducts.iProductID + "&iQuantity=" + iQuantity + "&strColour=" + strColour + "&strSize=" + strSize + "' onclick='return ConfirmRecordDeleteCart()' class='dgrLinkDelete'></a>"
                        + "</td>"
                        );

            return sbGrid.ToString();
        }
    }

    protected string BuildItemList()
    {
        //if ((Session["loggedOutOrder"] != null) && (Session["loggedOutOrder"].ToString() != "0"))
        //{
        StringBuilder sbRows = new StringBuilder();

        int iRowCount = 0;
        string iProductID = "";
        string iQuantity = "";
        string strColour = "";
        string strSize = "";

        string[] strCartItems = HttpContext.Current.Session["loggedOutOrder"].ToString().Split(new char[] { ',' });

        foreach (string value in strCartItems)
        {
            if (!String.IsNullOrEmpty(value))
            {
                iRowCount++;

                if (iRowCount % 2 == 0)
                {
                    sbRows.Append(@"<tr class='dgrAltItem'>");
                }
                else
                {
                    sbRows.Append(@"<tr class='dgrItem'>");
                }

                string[] strCartItemDetails = value.ToString().Split(new char[] { '|' });

                iProductID = strCartItemDetails[0];
                iQuantity = strCartItemDetails[1];
                strColour = strCartItemDetails[2];
                strSize = strCartItemDetails[3];
                sbRows.Append(BuildCartOrderRows(Convert.ToInt32(iProductID), Convert.ToInt32(iQuantity), strColour, strSize));
                sbRows.Append("</tr>");
            }
        }

        return sbRows.ToString();
    }

    protected void SendMail()
    {
        Attachment[] empty = new Attachment[] { };
        StringBuilder strbMail = new StringBuilder();

        string strContent = "";
        //noreply@consciousbirth.co.za
        string strFrom = "noreply@consciousbirth.co.za";
        string strTo = txtEmailAddress.Text; //txtEmailAddress.Text; //clsAccountHolders.strEmailAddress;
        string strName = txtContactPerson.Text; //clsAccountHolders.strEmailAddress;

        //string strTo = "renier@softservedigital.co.za";

        strbMail.AppendLine("<table>");
        strbMail.AppendLine("<tr colspan='3' style=\"width: 100%;\">");
        strbMail.AppendLine("<td>");
        strbMail.AppendLine("<span style=\"font-family:Arial; font-size:14px;\">Dear " + strName + "<br /><br /></span>");
        strbMail.AppendLine("<span style=\"font-family:Arial; font-size:14px;\">Thank you for your order.<br /><br /></sapn>");
        strbMail.AppendLine("</td>");
        strbMail.AppendLine("</tr>");
        strbMail.AppendLine("<tr colspan='3' style=\"width: 100%;\">");
        strbMail.AppendLine("<td>");
        strbMail.AppendLine("<b style=\"font-family:Arial; font-size:14px;\">Delivery address:</b><br />");
        strbMail.AppendLine("<span  style=\"font-family:Arial; font-size:14px;\">" + txtPhysicalAddress.Text + "</span><br/>");
        strbMail.AppendLine("<br/");
        strbMail.AppendLine("<b style=\"font-family:Arial; font-size:14px;\">Contact Number:</b><br />");
        strbMail.AppendLine("<span  style=\"font-family:Arial; font-size:14px;\">" + txtContactNumber.Text + "<br /></span");
        strbMail.AppendLine("<br/");
        strbMail.AppendLine("<table cellpadding=\"3\" cellspacing=\"0\">");
        strbMail.AppendLine("<tr class=\"dgrHeader\">");
        strbMail.AppendLine("<span style=\"font-family:Arial; font-size:14px;\"><td  align=\"center\"></td></span>");
        strbMail.AppendLine("<span style=\"font-family:Arial; font-size:14px;\"><td  align=\"center\">Product Name</td></span>");
        strbMail.AppendLine("<span style=\"font-family:Arial; font-size:14px;\"><td  align=\"center\">Quantity</td><span>");
        strbMail.AppendLine("<span style=\"font-family:Arial; font-size:14px;\"><td  align=\"center\">Colour</td><span>");
        strbMail.AppendLine("<span style=\"font-family:Arial; font-size:14px;\"><td  align=\"center\">Size</td><span>");
        strbMail.AppendLine("<span style=\"font-family:Arial; font-size:14px;\"><td  align=\"center\">Price</td></span>");
        strbMail.AppendLine("</tr>");
        strbMail.AppendLine(BuildItemList());
        strbMail.AppendLine("<span  style=\"font-family:Arial; font-size:14px;\"><tr><td><br />Total R" + (clsCart.GetLoggedOutOrderTotal().ToString("N2")) + "</td></tr></span>");
        strbMail.AppendLine("</table>");
        strbMail.AppendLine("</td>");
        strbMail.AppendLine("</tr>");
        strbMail.AppendLine("<tr>");
        strbMail.AppendLine("<td>");
        strbMail.AppendLine("<br/>");
        strbMail.AppendLine("<span style=\"font-family:Arial; font-size:14px;\">If you have not joined us on facebook, please visit our  <a href='#' style='color:#21285f;'>Facebook page</a><br /></span>");
        strbMail.AppendLine("</td>");
        strbMail.AppendLine("</tr>");
        strbMail.AppendLine("</table>");
        strContent = strbMail.ToString();
        clsCommonFunctions.SendMail(strFrom, strTo, "", "", "Conscious Birth - Order", strContent, empty, true);//andrew@softservedigital.co.za
    }

    protected void SendAdminMail()
    {
        Attachment[] empty = new Attachment[] { };
        StringBuilder strbMail = new StringBuilder();

        //clsAccountHolders clsAccountHoldersRecord = new clsAccountHolders(clsAccountHolders.iAccountHolderID);

        string strContent = "";
        string strFrom = "noreply@consciousbirth.co.za";
        string strTo = "orders@consciousbirth.co.za"; //"orders@consciousbirth.co.za";//txtEmailAddress.Text; //clsAccountHolders.strEmailAddress;
        string strName = txtContactPerson.Text; //clsAccountHolders.strEmailAddress;

        strbMail.AppendLine("<table>");
        strbMail.AppendLine("<tr colspan='3' style=\"width: 100%;\">");
        strbMail.AppendLine("<td>");
        strbMail.AppendLine("<span style=\"font-family:Arial; font-size:14px;\">Dear Admin,<br /><br /></span>");
        strbMail.AppendLine("<span style=\"font-family:Arial; font-size:14px;\">You have a new order placed by " + strName + ".<br /><br /></span>");
        strbMail.AppendLine("<span style=\"font-family:Arial; font-size:14px;\"><b>Email:</b><br /></span>");
        strbMail.AppendLine("<span style=\"font-family:Arial; font-size:14px;\">" + txtEmailAddress.Text + "<br /><br /></span>");
        strbMail.AppendLine("<span style=\"font-family:Arial; font-size:14px;\"><b>Contact Number:</b><br /></span>");
        strbMail.AppendLine("<span style=\"font-family:Arial; font-size:14px;\">" + txtContactNumber.Text + "<br /><br /></span>");
        strbMail.AppendLine("<b style='font-family:Arial; font-size:14px;'>Delivery address:</b><br />");
        strbMail.AppendLine("<span  style=\"font-family:Arial; font-size:14px;\">" + txtPhysicalAddress.Text + "<br /><br /></span>");

        //strbMail.AppendLine("<b>Delivery address:</b><br />");
        //clsDeliveryInfos clsDeliveryInfos = new clsDeliveryInfos(clsAccountHoldersRecord.iBillingInfoID);
        //strbMail.AppendLine(txtPhysicalAddress.Text + "<br />");
        //strbMail.AppendLine(clsDeliveryInfos.strPhysicalAddressCode + "<br />");
        //strbMail.AppendLine(clsDeliveryInfos.strCity + "<br /><br />");

        strbMail.AppendLine("<b style=\"font-family:Arial; font-size:14px;\"></b><br /><br />");
        strbMail.AppendLine("</td>");
        strbMail.AppendLine("</tr>");
        strbMail.AppendLine("<tr colspan='3' style=\"width: 100%;\">");
        strbMail.AppendLine("<td>");

        strbMail.AppendLine("<table cellpadding=\"3\" cellspacing=\"0\">");
        strbMail.AppendLine("<tr>");
        strbMail.AppendLine("</td>");
        strbMail.AppendLine("<span  style=\"font-family:Arial; font-size:14px;\"><td  align=\"center\">Product Name</td></span>");
        strbMail.AppendLine("<span style=\"font-family:Arial; font-size:14px;\"><td  align=\"center\">Quantity</td></span>");
        strbMail.AppendLine("<span  style=\"font-family:Arial; font-size:14px;\"><td  align=\"center\">Colour</td></span>");
        strbMail.AppendLine("<span  style=\"font-family:Arial; font-size:14px;\"><td  align=\"center\">Size</td></span>");
        strbMail.AppendLine("<span  style=\"font-family:Arial; font-size:14px;\"><td  align=\"center\">Price</td></span>");
        strbMail.AppendLine("</tr>");
        strbMail.AppendLine(BuildItemList());
        strbMail.AppendLine("<span  style=\"font-family:Arial; font-size:14px;\"><tr><td><br /> Total R" + (clsCart.GetLoggedOutOrderTotal().ToString("N2")) + "</td></tr></span>");
        strbMail.AppendLine("</table>");
        strbMail.AppendLine("</td>");
        strbMail.AppendLine("</tr>");
        strbMail.AppendLine("</table>");

        strContent = strbMail.ToString();

        clsCommonFunctions.SendMail(strFrom, strTo, "orders@consciousbirth.co.za", "andrew@softservedigital.co.za", "Conscious Birth - Order", strContent, empty, true);// 
        //orders@consciousbirth.co.za
        //    andrew@softservedigital.co.za
    }

    #endregion
}