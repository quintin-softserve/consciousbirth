﻿<%@ Page Title="Conscious-Birth-MyWishlist" Language="C#" MasterPageFile="~/ConsciousBirth.master" AutoEventWireup="true" CodeFile="Conscious-Birth-My-Gift-Registry-old.aspx.cs" Inherits="Conscious_Birth_MyWishlist" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script src="CMS/scripts/jsGeneric.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div id="divData" runat="server">
        <asp:UpdatePanel ID="udpOrderItemsView" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <center>
                <div class='clr'>
                </div>
                <div style="width: 100%">
                    
                    <div class="feature" runat="server" id="divHeading" visible="true" style='margin-top: 20px;'>
                    <h2 class='headingText' style='margin-top: 15px;'>Wishlist</h2>
                    <br />
                    <p runat="server" id="pMessage">
                        You currently have the following items in added to your Wishlist <asp:Label ID="UserName" runat="server" Text=""></asp:Label>:
                    </p>
                        <div class="viewFullLink" runat="server" id="divNoData" visible="false">
                            <p>
                                Your cart is currently empty. <a href="Conscious-Birth-Products.aspx">Click here to add a Product.</a>
                            </p>
                        </div>
                    </div>
                    <br />
                    <div class="container">
                        <div class="feature" runat="server" id="divGrid" visible="true">
                            <table cellpadding="3" cellspacing="0" class="dgr">
                                <tr class="dgrHeader">
                                    <td align="center" width="100px">&nbsp;
                                    </td>
                                    <td align="center" width="200px">Product Title
                                    </td>
                                    <td align="center" width="100px">Quantity
                                    </td>
                                    <td align="center" width="100px">Colour
                                    </td>
                                    <td align="center" width="100px">Size
                                    </td>
                                    <td align="center" width="100px">Total Cost
                                    </td>
                                    <td align="center" width="100px">Delete
                                    </td>
                                </tr>
                                <asp:Literal runat="server" ID="litGridData"></asp:Literal>
                            </table>
                        </div>
                    </div>
                    <div class="feature" id="divTotalAndButtons" runat="server">
                        <div>
                            Total Incl VAT: R<asp:Label runat="server" ID="lblGridTotal" Style="margin-right: 0px;">0.00</asp:Label>
                        </div>
                        <br />
                        <br style="clear: both;" />
                        <div class="container">
                            <asp:Button runat="server" ID="btnShopMore" CssClass="buttonTemplate buttonArrow centerText textDecoNone"
                                OnClick="btnShopMore_Click" Text="Continue Shopping" />
                            <br />
                            <asp:Button runat="server" ID="btnContinue" CssClass="buttonTemplate buttonArrow centerText textDecoNone"
                                OnClick="btnContinue_Click" Text="Check Out" />
                            <br />
                        </div>
                  
                </div>
                <asp:UpdatePanel runat="server" ID="updStep2" UpdateMode="Always">
                    <ContentTemplate>
                        <div style="width: 100%; margin-bottom: 30px;">
                            <div class="feature" runat="server" id="step2" visible="false">
                                <h2 class="headingText">Delivery Information</h2>
                                <br />
                                <div id="mandatoryDiv" class="mandatoryInvalidDiv" runat="server" visible="false">
                                    <asp:Label ID="lblValidationMessage" runat="server"></asp:Label>
                                </div>
                                <div class="controlDiv">
                                    <div class="imageHolderCommonDiv">
                                        <div class="validationImageMandatory"></div>
                                    </div>
                                    <div class="labelDiv">
                                        Name:
                                    </div>
                                    <div class="fieldDiv">
                                        <asp:TextBox ID="txtContactPerson" runat="server" CssClass="roundedCornerTextBoxMini" onKeyUp="return SetMaxLength(this,50)"/>
                                    </div>
                                    <br class="clear" />
                                </div>
                                <div class="controlDiv">
                                    <div class="imageHolderCommonDiv">
                                        <div class="validationImageMandatory"></div>
                                    </div>
                                    <div class="labelDiv">Contact number:</div>
                                    <div class="fieldDiv">
                                        <asp:TextBox ID="txtContactNumber" runat="server" CssClass="roundedCornerTextBoxMini" onKeyUp="return SetMaxLength(this,20)" onblur="CheckNumeric();"/>
                                    </div>
                                    <br class="clear" />
                                </div>
                                <div class="controlDiv">
                                    <div class="imageHolderCommonDiv">
                                        <div class="validationImageMandatory"></div>
                                    </div>
                                    <div class="labelDiv">
                                        Email Address:
                                    </div>
                                    <div class="fieldDiv">
                                        <asp:TextBox ID="txtEmailAddress" runat="server" CssClass="roundedCornerTextBoxMini" onKeyUp="return SetMaxLength(this,40)" onblur="emailValidator(this);" />
                                    </div>
                                </div>
                                <br class="clear" />
                                <div class="controlDiv" style="margin-bottom: 16px;">
                                    <div class="imageHolderCommonDiv">
                                        <div class="validationImageMandatory"></div>
                                    </div>
                                    <div class="labelDiv">
                                        Physical delivery address:
                                    </div>
                                    <div class="fieldDiv">
                                        <asp:TextBox ID="txtPhysicalAddress" runat="server" CssClass="roundedCornerTextBoxMultiLine2" Height="60px" Width="594px" onKeyUp="return SetMaxLength(this,250)" TextMode="MultiLine" Rows="2" />
                                    </div>
                                    <br class="clear" />
                                </div>
                                <div style='margin: 35px 0px 0px 0px;'>
                                    <asp:Button ID='btnProceedToStep3' runat="server" OnClick="btnProceedToStep3_Click" Text="Process Order" />
                                </div>
                            </div>
                        </div>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnProceedToStep3" />
                    </Triggers>
                </asp:UpdatePanel>
                </center>
                <%--<asp:UpdatePanel runat="server" ID="updStep3" UpdateMode="Always">
                    <ContentTemplate>
                    <div runat="server" id="step3" visible="false">
                        <div style="width:100%;">
                            <div class="feature">
                                PAYMENT INTEGRATION
                            </div>
                        </div>
                    </div>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnProceedToStep3"/>
                    </Triggers>
                </asp:UpdatePanel>--%>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>
