﻿<%@ Page Title="Hypnobirthing® :: Conscious Birth" Language="C#" MasterPageFile="~/ConsciousBirth.master" AutoEventWireup="true" CodeFile="Conscious-Birth-Hypnobirthing.aspx.cs" Inherits="Conscious_Birth_Hypnobirthing" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date(); a = s.createElement(o),
            m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-53160457-1', 'auto');
        ga('send', 'pageview');

    </script>
    <link href="css/style.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="main_wrapper">
        <!-- C O N T E N T -->
        <div class="content_wrapper">
            <div class="page_title_block">
                <div class="container">
                    <h2 class="title">About Hypnobirthing® Antenatal Classes </h2>
                </div>
            </div>
            <div class="container">
                <div class="content_block no-sidebar row">
                    <div class="fl-container span12">
                        <div class="row">
                            <div class="posts-block span12">
                                <div class="contentarea">

                                    <div class="row-fluid">
                                        <div class="span4 module_cont module_text_area module_none_padding">
                                            <img src="img/HB2.png" alt="" />
                                        </div>
                                        <div class="span8 module_cont module_text_area module_none_padding">
                                            <blockquote class="type2">
                                                <h5 style="margin-left: 4%;">HypnoBirthing Antenatal classes</h5>
                                                <h6>Affiliated with the HypnoBirthing® Institute.</h6>
                                                <p>
                                                    HypnoBirthing teaches you the ease of using your own natural birthing instincts.
                                                    <br />
                                                    With HypnoBirthing, you will be aware, fully in control and active, but still profoundly relaxed during your labour.
                                                    <br />


                                                </p>
                                                <h6>What HypnoBirthing is and how it works </h6>
                                                <p>
                                                    The HypnoBirthing Childbirth method is as much a philosophy of birth as it is a<br />
                                                    technique for achieving a satisfying, relaxing, and stress-free method of birthing. 
                                                </p>
                                                <p>
                                                    HypnoBirthing teaches you and your partner to call upon your body’s own natural relaxant, thereby<br />
                                                    lessening, even eliminating, discomfort and the need for medication.<br />
                                                    When a woman is properly prepared for childbirth and when mind and body are in harmony,
                                                    <br />
                                                    nature is free to function in the same well designed manner that it does with all other creatures.<br />
                                                </p>
                                                <p>
                                                    HypnoBirthing teaches you to release all prior programming about birth, how to trust your body and work<br />
                                                    with it, and how to free yourself of harmful emotions, like fear, that cause muscles to become unyielding,<br />
                                                    which causes pain. 
                                                </p>


                                                <h6>You will learn:</h6>

                                                <ul>
                                                    <li>
                                                        <p>Everything you need to know to achieve a safer, easier and more comfortable birth.</p>
                                                    </li>
                                                    <li>
                                                        <p>To explode the myth that pain is a necessary accompaniment to labour.</p>
                                                    </li>
                                                    <li>
                                                        <p>What is wrong with the concept of labour as it exists with most other childbirth methods.</p>
                                                    </li>
                                                    <li>
                                                        <p>Techniques to achieve deep relaxation in order to help you eliminate the Fear-Tension-Pain Syndrome.</p>
                                                    </li>
                                                    <li>
                                                        <p>How your body is naturally designed to conceive, nurture and birth your baby with ease and comfort.</p>
                                                    </li>
                                                    <li>
                                                        <p>To create your body’s own natural relaxant, the only safe labour enhancement.</p>
                                                    </li>
                                                    <li>
                                                        <p>Natural ways to bring your body into labour without artificial, chemical induction.</p>
                                                    </li>
                                                    <li>
                                                        <p>How you and your partner can create a birthing environment that is calm, serene and joyful, rather than tense and stressful.</p>
                                                    </li>
                                                    <li>
                                                        <p>Gentle birthing techniques that allow you to breathe your baby into the world without the violence of hard, physical pushing.</p>
                                                    </li>
                                                    <li>
                                                        <p>To use your natural birthing instincts to birth your baby in a way that mirrors the way that nature intended.</p>
                                                    </li>
                                                </ul>
                                                <h6>A couple will come and share their HypnoBirthing experience with the group.<br />
                                                    A chance to ask questions etc</h6>
                                                <br />
                                                <h6>HypnoBirthing is taught by Theoni Papoutsis:</h6>
                                                <p>
                                                    Theoni is one of only a few certified HypnoBirthing practitioners in South Africa<br />
                                                    and she has 10 years of experience in teaching this course. Using<br />
                                                    HypnoBirthing methods she teaches mothers and partners techniques that<br />
                                                    facilitate safe and satisfying birthing. These include guided imagery,<br />
                                                    visualization, and special breathing.
                                                </p>
                                                <h6>Cost:</h6>
                                                <p>
                                                    Includes HypnoBirthing book, 2 cds, notes, refreshments and light snack.<br />
                                                    R2300 for you and your partner (Mom or Doula is welcome too).
                                                </p>
                                                <h6>Times: </h6>
                                                <p>
                                                   Wednesday evenings 19h00-21h30 for 5 consecutive evenings OR<br />
                                                   Weekend Friday evening 18h30-20h30, Saturday and Sunday 8h30-13h00.
                                                </p>
                                                <h6>Weekend Courses are held over a weekend on a Friday, Saturday and Sunday:</h6>
                                                <ul>
                                                    <li>
                                                        <p>July: 22, 23 & 24</p>
                                                    </li>
                                                    <li>
                                                        <p>September: 16, 17 &18</p>
                                                    </li>
                                                    <li>
                                                        <p>November: 18, 19 & 20</p>
                                                    </li>
                                                    <li>
                                                        <p>December: 2, 3 & 4</p>
                                                    </li>
                                                    <li>
                                                        <p>•	Friday from 18h30-20h30, Saturday from 8h30-13h00 & Sunday from 8h30-13h00</p>
                                                    </li>
                                                </ul>
                                                <h6>Weekday Courses are held over five consecutive Wednesday evenings:</h6>
                                                <ul>
                                                    <li>
                                                        <p>May 25, June:  1, 8,15 & 22</p>
                                                    </li>
                                                    <li>
                                                        <p>July 27, August:  3, 10, 17, 24</p>
                                                    </li>
                                                    <li>
                                                        <p>October 5,12,19, 26 & November 2</p>
                                                    </li>
                                                    <li>
                                                        <p>November 9,16, 23, 30 & December 7</p>
                                                    </li>
                                                    <li>
                                                        <p>Wednesdays from 19h00-22h00</p>
                                                    </li>
                                                </ul>


                                                <h6>Booking is essential <br />
                                                    Theoni on 083 229 3253</h6>
                                                <h6><a style="margin: 0;"href="theoni@consciousbirth.co.za?" target="_top">Enquire now</a></h6>


                                            </blockquote>
                                            <br />

                                            <div class="sectionContainerLeft innerBoxWithShadow">
                                                <div class="span12 module_cont module_text_area module_none_padding">
                                                    <blockquote class="type2">
                                                        <p>
                                                            The hypnobirthing® course was invaluable. My husband and I learnt so much about our pregnancy, our values and what kind of parents 
                                                            we want to be. The course gave us an understanding of what the birthing process should be. Had we not attended the hypnobirthing® 
                                                            classes, we would not be as confident about our baby’s birth. I feel that we are now better equipped to make certain that my baby 
                                                            has a safe, happy and healthy birthing experience. I am empowered as a mother and no longer feel bullied by doctors and clinical 
                                                            birthing practices.
                                                        </p>
                                                        <div class="author italic">Anna, mother of Sam</div>
                                                    </blockquote>
                                                </div>
                                                <br class="clear" />
                                            </div>
                                            <br />

                                            <div class="clear"></div>
                                            <div style="visibility: hidden">
                                                <asp:Button ID="Button1" runat="server" Text="View Calender for Available Dates" class="redirectButton" OnClick="btnViewCalender_Click" />
                                            </div>
                                            <div class="clear"></div>
                                        </div>
                                    </div>
                                    <!-- .row-fluid -->
                                    <div class="module_line_trigger" data-background="#ffeec9 url(img/VV.png) no-repeat center" data-top-padding="bottom_padding_huge" data-bottom-padding="module_big_padding">
                                        <div class="row-fluid">
                                            <div class="span12 module_cont module_promo_text module_huge_padding">
                                                <div class="shortcode_promoblock">
                                                    <div class="promo_button_block type2">
                                                        <a href="Conscious-Birth-Products.aspx" class="promo_button">View our Products </a>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--.module_cont -->
                                            <div class="clear">
                                                <!-- ClearFIX -->
                                            </div>
                                        </div>
                                        <!-- .row-fluid -->
                                    </div>
                                </div>
                                <!-- .contentarea -->
                            </div>
                            <div class="left-sidebar-block span3">
                                <aside class="sidebar">
                                    //Sidebar Text
                                </aside>
                            </div>
                            <!-- .left-sidebar -->
                        </div>
                        <div class="clear">
                            <!-- ClearFix -->
                        </div>
                    </div>
                    <!-- .fl-container -->
                    <div class="right-sidebar-block span3">
                        <aside class="sidebar">
                        </aside>
                    </div>
                    <!-- .right-sidebar -->
                    <div class="clear">
                        <!-- ClearFix -->
                    </div>
                </div>
            </div>
            <!-- .container -->
        </div>
        <!-- .content_wrapper -->
    </div>
    <!-- .main_wrapper -->
</asp:Content>

