﻿<%@ Page Title="Client Portal" Language="C#" AutoEventWireup="true" CodeFile="Login.aspx.cs" Inherits="Login" %>

<!DOCTYPE html>
<!--[if IE 8]> 				 <html class="no-js lt-ie9" lang="en" > <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="en">
<!--<![endif]-->

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Accelerate Login</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/login.css" rel="stylesheet">
    <link href="css/animate-custom.css" rel="stylesheet">


    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
      <script src="js/respond.min.js"></script>
    <![endif]-->

    <script src="js/custom.modernizr.js" type="text/javascript"></script>

    <!--Stylesheet-->
    <%--<link href="css/styleLogin.css" rel="stylesheet" />--%>

    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
    <link rel="icon" href="favicon.ico" type="image/x-icon" />

</head>
<body>
    <form id="form1" runat="server">

        <asp:ScriptManager ID="smgLogin" runat="server" ScriptMode="Release" />
        <center>
            <asp:UpdatePanel ID="udpLogin" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:Panel ID="panelLogin" runat="server" DefaultButton="lnkbtnLogin">
                
                <!-- start Login box -->
    	        <div class="container" id="login-block">
    		        <div class="row">
			            <div class="col-sm-6 col-sm-offset-3"><%--col-md-4 col-md-offset-4--%>
			    	        <h3 class="animated bounceInDown">Welcome</h3>
			               <div class="login-box clearfix animated flipInY">
			        	        <div class="login-logo">
			        		       <img src="img/img-Login.png" width="80%" />
			        	        </div> 
			        	        <hr />
			        	        <div class="login-form">
			        		        <!-- Start Error box -->
			        		        <div class="alert alert-danger hide" id="Alert" runat="server">
								          <button type="button" class="close" data-dismiss="alert"> &times;</button>
								          <h4><asp:Literal ID="litAlertHeading" runat="server"></asp:Literal>!</h4>
								           <asp:Literal ID="litValidationMessage" runat="server" />
							        </div> <!-- End Error box -->
							
						   		         <asp:TextBox ID="txtUsername" runat="server" Width="230px" CssClass="txtUserNameBox" required></asp:TextBox>
						   		         <div id="divPassword" runat="server">
                                                <br />
                                                <asp:TextBox ID="txtPassword" runat="server" TextMode="Password" Width="230px" CssClass="txtPasswordBox"></asp:TextBox>
                                                &nbsp;
                                        </div>
                                    <div class="buttonHolder">
                                        <asp:Button ID="lnkbtnLogin" runat="server" CssClass="btn btn-white" Text="Login" OnClick="lnkbtnLogin_Click" />
                                        <asp:Button ID="lnkbtnSubmit" runat="server" CssClass="btn btn-white" Text="Submit" Visible="false" onClick="lnkbtnSubmit_Click" />
                                    </div>
							        <div class="login-links"> 
					                    <asp:LinkButton ID="lnkbtnForgottenPassword" runat="server" Style="text-decoration: none;" CssClass="linkGreyUnderlined" Text="Forgotten Password?" OnClick="lnkbtnForgottenPassword_Click" />
                                        <asp:LinkButton ID="lnkbtnBackToLogin" runat="server" CssClass="linkGreyUnderlined" Style="text-decoration: none;" Text="Back to login" OnClick="lnkbtnBackToLogin_Click" Visible="false" />
					                    <br />
					                    <a style="text-decoration: none;" href="#">
					                      Don't have an account?
					                    </a>
							        </div>      		
			        	        </div> 			        	
			               </div>
			            </div>
			        </div>
    	        </div>
      	        <!-- End Login box -->

                </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>
        </center>
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/jquery-1.9.1.min.js"><\/script>')</script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/placeholder-shim.min.js"></script>
        <script src="js/custom.js"></script>
    </form>
</body>
</html>
