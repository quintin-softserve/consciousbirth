﻿<%@ Page Title="Pregnancy Yoga For Teachers :: Conscious Birth" Language="C#" MasterPageFile="~/ConsciousBirth.master" AutoEventWireup="true" CodeFile="Conscious-Birth-Pregnancy-Yoga-For-Teachers.aspx.cs" Inherits="Conscious_Birth_Pregnancy_Yoga_For_Teachers" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date(); a = s.createElement(o),
            m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-53160457-1', 'auto');
        ga('send', 'pageview');

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="main_wrapper">
        <!-- C O N T E N T -->
        <div class="content_wrapper">
            <div class="page_title_block">
                <div class="container">
                    <h2 class="title">Pregnancy Yoga For Teachers</h2>
                </div>
            </div>
            <div class="container">
                <div class="content_block no-sidebar row">
                    <div class="fl-container span12">
                        <div class="row">
                            <div class="posts-block span12">
                                <div class="contentarea">
                                    <div class="span12">
                                    </div>
                                    <br />
                                    <br />
                                    <div class="row-fluid">
                                        <div class="span12">
                                            <div class="span4 module_cont module_text_area module_none_padding">
                                                <img src="img/1.jpg" alt="" />
                                            </div>
                                            <div class="span4 module_cont module_text_area module_none_padding">
                                                <img src="img/2.jpg" alt="" />
                                            </div>
                                            <div class="span4 module_cont module_text_area module_none_padding">
                                                <img src="img/3.jpg" alt="" />
                                            </div>

                                        </div>
                                        <div class="clear">
                                            <!-- ClearFIX -->
                                        </div>
                                        <br class="clear" />
                                        <br class="clear" />
                                        <br class="clear" />
                                        <div class="span10" style="text-align: left">
                                            <p>
                                                Join us to learn more about Conscious Pregnancy Yoga as Theoni Papoutsis shares skills, knowledge and expertise at 
                                            a weekend set in the peaceful Melody Hill Retreat in the Magaliesburg.
                                            </p>
                                            <p>
                                                Conscious Pregnancy Yoga uses the ancient wisdom of yoga to prepare a woman for pregnancy, childbirth and parenting 
                                            not only on a physical level, but also on an emotional, mental and spiritual level.
                                            </p>
                                            <p>
                                                The sharing course will be offered in two parts, over two weekends. Part one will cover the basics of pregnancy yoga (posture, 
                                            breath and meditation), conscious conception, pregnancy, birth and the first 40 days after birth. In part two we take a more 
                                            in depth look at pregnancy and birth as well as conscious parenting and the decisions that need to be made that impact the
                                             future adult. (Please see below for a detailed list of topics to be covered).

                                            </p>
                                            <p>
                                                Kundalini Yoga and meditation, as taught by Yogi Bhajan, andGurmukh Kaur Khalsa’s teaching (designer of the The Khalsa Way® for pregnancy)
                                             are the cornerstones of this course. We recall the ancient ways of birthing and mothering handed down through the ages. With Kundalini 
                                            yoga a woman’s awareness grows to meet the demands of growing and nurturing her unborn baby. These tools are designed to empower her so 
                                            that she may truly know herself and what she is capable of; a state of being where she is connected with her body, emotions and spirit, 
                                            so she is able to trust her intuition and instincts in childbirth and motherhood. In doing so she has the capacity to release fears, 
                                            blocks, doubt and old conditioning.
                                            </p>
                                            <ul>
                                                <li>Conscious conception</li>
                                                <li>Physical & mental development of the unborn</li>
                                                <li>Physical & emotional changes of the mother</li>
                                                <li>Discover the key role of hormones in birth</li>
                                                <li>How to teach a pregnancy yoga class</li>
                                                <li>Know what to avoid, what is contra-indicated</li>
                                                <li>Mantras, postures, mudras, breath, meditation, visualization.</li>
                                                <li>120th day of pregnancy</li>
                                                <li>Pregnancy basics</li>
                                                <li>Anatomy & physiology of pregnancy and childbirth</li>
                                                <li>Pregnancy as an opportunity for spiritual growth</li>
                                                <li>Helping women make their own choices during pregnancy and birth</li>
                                                <li>Midwives & Doulas</li>
                                                <li>Labour& birth</li>
                                                <li>Episiotomy</li>
                                                <li>Caesarean Birth</li>
                                                <li>Importance of 40 days after birth</li>
                                                <li>Art of Breastfeeding</li>
                                                <li>Family Bed</li>
                                                <li>Pregnancy loss</li>
                                                <li>Extended breastfeeding</li>
                                                <li>All about nutrition in pregnancy and motherhood</li>
                                                <li>Increase your understanding of the anatomy and physiology of pregnancy and birth</li>
                                                <li>Positions for labour& birth</li>
                                                <li>Supporting a woman in labour& birth</li>
                                                <li>Discover how to help pregnant women to be well-prepared for labour and birth by awakening the intelligence of the body with yoga</li>
                                                <li>Helping woman through grief</li>
                                                <li>Brief guide on Homeopathy, Cranio Sacral therapy</li>
                                                <li>Yoga after birth - muscle toning, posture and emotional well-being</li>
                                            </ul>
                                            <br />
                                            <div class="sectionContainerLeft innerBoxWithShadow">
                                                <div class="span12 module_cont module_text_area module_none_padding">
                                                    <blockquote class="type2">
                                                        <p>
                                                            Thanks Theoni! You're my inspiration!!!! It's your beautiful Yoga classes and amazing, insightful, 
                                                            informative, supportive guidance through my pregnancies that ignited this dream in me. If I can do 
                                                            for others what you did for me I will be one happy woman! 
                                                        </p>
                                                        <div class="author italic">Kerry Soloman</div>
                                                    </blockquote>
                                                </div>
                                                <br class="clear" />
                                            </div>
                                            <br class="clear" />
                                            <br class="clear" />
                                            <h6  class="marginLeft12">About Theoni:</h6>
                                            <p>
                                                Theoni had the privilege to witness her first birth 19 years ago and for the last 14 years has been consistently teaching, 
                                                guiding and nurturing thousands of pregnant women using yoga. She has also been involved in supporting women at births as 
                                                a Doula and has a variety of techniques she draws on (such as pregnancy massage, Hypno-birthing and body alignment) to 
                                                support women through this stage of their life. Theoni is looking forward to sharing her knowledge and insight gained 
                                                from years of experience.
                                            </p>
                                            <br />
                                            <h6 class="marginLeft12">During our time together we will:</h6>
                                            <ul>
                                                <li>Experience pregnancy yoga</li>
                                                <li>Chant & sing</li>
                                                <li>Share delicious vegetarian meals</li>
                                                <li>Gain understanding about conscious birth and pregnancy</li>
                                                <li>Open discussions</li>
                                                <li>View DVD’s</li>
                                            </ul>
                                            <br />
                                            <h6  class="marginLeft12">Who is this weekend designed for?</h6>
                                            <ul>
                                                <li>
                                                Health & Childbirth Educators, midwives, doulas, doctors, nurses.
                                                <li>Yoga teachers of any discipline</li>
                                                <li>Pregnant women or women wanting to become pregnant</li>
                                                <li>Any person wanting to deepen their knowledge of conscious pregnancy</li>
                                            </ul>
                                            <br />
                                            <h6 class="marginLeft12">WHERE:</h6>
                                            <p>Melody Hill Retreat – Magaliesburg</p>
                                            <p>COST including 2 manuals: </p>
                                            <p>FOOD & ACCOMMODATION: </p>
                                            <p>Topics that will be covered:</p>

                                        </div>
                                        <br class="clear" />
                                        <br class="clear" />
                                        <br class="clear" />
                                        <br class="clear" />
                                        <br class="clear" />
                                        <br class="clear" />
                                        <div class="span12">
                                            <div class="span4 module_cont module_text_area module_none_padding">
                                                <img src="img/4.jpg" alt="" />
                                            </div>
                                            <div class="span4 module_cont module_text_area module_none_padding">
                                                <img src="img/5.jpg" alt="" />
                                            </div>
                                            <div class="span4 module_cont module_text_area module_none_padding">
                                                <img src="img/6.jpg" alt="" />
                                            </div>

                                        </div>
                                        <br class="clear" />
                                        <br class="clear" />
                                        <div class="span12">
                                            <h6 class="marginLeft12">Conscious Pregnancy Yoga Weekend Sharing, Part 2 with Theoni Papoutsis</h6>

                                            <h6 class="marginLeft12">DATE:</h6>
                                            <p>to be decided</p>
                                            <h6 class="marginLeft12">WHERE:</h6>
                                            <p>Melody Hill Retreat</p>
                                            <br class="clear" />
                                            <br class="clear" />
                                            <h6 class="marginLeft12">that will be covered:</h6>


                                            <ul>
                                                <li>Useful Natural Remedies in pregnancy</li>
                                                <li>Deepen your understanding of birth physiology</li>
                                                <li>The Yoga Class as a place for emotional support and community building</li>
                                                <li>Postnatal yoga with babies</li>
                                                <li>Circumcision</li>
                                                <li>The Vaccination question</li>
                                                <li>Drug free birth</li>
                                                <li>Motherhood</li>
                                                <li>Parenting with consciousness</li>
                                            </ul>
                                        </div>
                                    </div>
                                    <!-- .row-fluid -->
                                    <div class="module_line_trigger" data-background="#ffeec9 url(img/VV.png) no-repeat center" data-top-padding="bottom_padding_huge" data-bottom-padding="module_big_padding">
                                        <div class="row-fluid">
                                            <div class="span12 module_cont module_promo_text module_huge_padding">
                                                <div class="shortcode_promoblock">
                                                    <div class="promo_button_block type2">
                                                        <a href="Conscious-Birth-Products.aspx" class="promo_button"> View our Products </a>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--.module_cont -->
                                            <div class="clear">
                                                <!-- ClearFIX -->
                                            </div>
                                        </div>
                                        <!-- .row-fluid -->
                                    </div>
                                    <!-- .module_line_trigger -->
                                   <!-- <div class="row-fluid">
                                        <div class="span12 module_cont module_partners center_title module_big_padding2">
                                            <div class="bg_title">
                                                <div class="title_wrapper">
                                                    <a href="javascript:void(0)" class="btn_carousel_left"></a>
                                                    <h5 class="headInModule">Our Partners</h5>
                                                    <a href="javascript:void(0)" class="btn_carousel_right"></a>
                                                </div>
                                            </div>
                                            <div class="module_content sponsors_works carouselslider items5" data-count="5">
                                                <ul>
                                                    <li>
                                                        <div class="item">
                                                            <a href="#charity-html5css3-website-template/3180454" target="_blank">
                                                                <img src="img/pictures/partners2.png" alt=""></a>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="item">
                                                            <a href="#point-business-responsive-wp-theme/4319087" target="_blank">
                                                                <img src="img/pictures/partners4.png" alt=""></a>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="item">
                                                            <a href="#cleanspace-retina-ready-business-wp-theme/3776000" target="_blank">
                                                                <img src="img/pictures/partners1.png" alt=""></a>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="item">
                                                            <a href="#yellowproject-multipurpose-retina-wp-theme/4066662" target="_blank">
                                                                <img src="img/pictures/partners3.png" alt=""></a>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="item">
                                                            <a href="#showroom-portfolio-retina-ready-wp-theme/3473628" target="_blank">
                                                                <img src="img/pictures/partners5.png" alt=""></a>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="item">
                                                            <a href="themeforest.net/item/incipiens-responsive-portfolio-wordpress-theme/2762691" target="_blank">
                                                                <img src="img/pictures/partners6.png" alt=""></a>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="item">
                                                            <a href="themeforest.net/item/hq-photography-responsive-wp-theme/3200962" target="_blank">
                                                                <img src="img/pictures/partners9.png" alt=""></a>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="item">
                                                            <a href="#incipiens-responsive-portfolio-wordpress-theme/2762691" target="_blank">
                                                                <img src="img/pictures/partners11.png" alt=""></a>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- .row-fluid -->

                                </div>
                                <!-- .contentarea -->
                            </div>
                            <div class="left-sidebar-block span3">
                                <aside class="sidebar">
                                    //Sidebar Text
                                </aside>
                            </div>
                            <!-- .left-sidebar -->
                        </div>
                        <div class="clear">
                            <!-- ClearFix -->
                        </div>
                    </div>
                    <!-- .fl-container -->
                    <div class="right-sidebar-block span3">
                        <aside class="sidebar">
                        </aside>
                    </div>
                    <!-- .right-sidebar -->
                    <div class="clear">
                        <!-- ClearFix -->
                    </div>
                </div>
            </div>
            <!-- .container -->
        </div>
        <!-- .content_wrapper -->

    </div>
    <!-- .main_wrapper -->
</asp:Content>

