﻿<%@ Page Title="Blissful Pregnancy :: Conscious Birth" Language="C#" MasterPageFile="~/ConsciousBirth.master" AutoEventWireup="true" CodeFile="Conscious-Birth-Blissful-Pregnancy.aspx.cs" Inherits="Conscious_Birth_Blissful_Pregnancy" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript" src="js/jquery.min.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="main_wrapper">
        <!-- C O N T E N T -->
        <div class="content_wrapper">
            <div class="page_title_block">
                <div class="container">
                    <h2 class="title">Blissful Pregnancy</h2>
                </div>
            </div>
            <div class="container">
                <div class="content_block no-sidebar row">
                    <div class="fl-container span12">
                        <div class="row">
                            <div class="posts-block span12">
                                <div class="contentarea">

                                    <div class="row-fluid">
                                        <div class="span4 module_cont module_text_area module_none_padding">
                                            <img src="img/blissful-pregnancy.png" alt="" />
                                        </div>
                                        <div class="span8 module_cont module_text_area module_none_padding">
                                            <blockquote class="type2">
                                                <h6>What this is and who it is for:</h6>
                                                <p>
                                                    Blissful pregnancy is about preparing for pregnancy so that you can have a healthy and relaxed pregnancy that will benefit you and your baby. 
                                                    This is for anyone who is trying to become pregnant or who is already pregnant. The areas that Theoni can assist you with include:
                                                </p>
                                                <br />
                                                <ul>
                                                    <li><p>Achieving pregnancy through eating the correct food for you, or maximizing health if you are already pregnant.</p></li>
                                                    <li><p>Helping you to make the important decisions that you need to make during pregnancy, such as what kind of birth is right for you.</p></li>
                                                    <li><p>In this modern age of information overload, all the advice on how to behave during pregnancy can be overwhelming and frightening. 
                                                        Theoni can act as a guide to help distinguish between information (which is plentiful) and knowledge (which is less plentiful).</p></li>
                                                    <li><p>Assuring nervous or unsure Mothers that they are doing a good job and offering encouragement when it is needed most.</p></li>
                                                    <li><p>Guiding you to access to your inner wisdom and power and assists you in trusting your own body.</p></li>
                                                    <li><p>Acknowledging fears and worries (from the largest to the smallest) which will be dealt with in the calm and compassion that comes with Theoni’s extensive experience.</p></li>
                                                    <li><p>Achieving greater connection with yourself and your baby.</p></li>
                                                    <li><p>Experiencing better emotional well-being.</p></li>
                                                    <li><p>Offering telephonic support (during office hours) to help deal with niggling concerns that may cause stress.</p></li>
                                                    <li><p>Supplying access to an extensive library of support material, collected over many years, in the form of books and dvds. 
                                                        This comprehensive collection is available exclusively on loan to ladies who sign up for the Blissful Pregnancy Journey.</p></li>
                                                </ul>
                                                <br />
                                            
                                            <p>
                                                Overall, the Blissful Pregnancy journey has been put together to hold you spiritually and emotionally as you unfurl your authentic power 
                                                as a mother or mother-to-be. It is a safe space to bring the real you in all your unknowing, fear and vulnerability to explore and discover 
                                                and be guided by Theoni. A mother herself, Theoni has journeyed with pregnant women for almost 2 decades, learning what it is that they 
                                                really need for themselves at this time of transition. Her experience is invaluable as you begin to charter the unknown.
                                            </p>
                                                
                                            <br />

                                            <h6>What to expect and benefits:</h6>
                                            <br />
                                                <p>When you see Theoni about any issues you may have about becoming pregnant or dealing with the challenges of being pregnant you are in good hands</p>
                                                <p>As a former doula she has years of experience in supporting pregnant women. She understands the complexities of hormone changes and provides warmth and kindness when you need it most</p>
                                                <p>You can expect a tailor made session that helps you with your unique circumstance</p>
                                                <p>The benefit of dealing with any challenges you have by seeing Theoni are that instead of carrying your concerns with you all the way to your birth, you deal with them positively and focus on carrying a healthy child in a calm manner</p>
                                                <p>As a soon-to-be Mother you will need to learn to tap into your own inner knowing and Theoni can help to facilitate the process of you realizing the truth for you and your baby</p>
                                                <p>You will also gain insight and tools as to how to work with your mind and body, and these will stand you in good stead for all spheres of life</p>
                                            <br />

                                                </blockquote>

                                            <div class="sectionContainerLeft innerBoxWithShadow">
                                                <div class="span12 module_cont module_text_area module_none_padding">
                                                    <blockquote class="type2">
                                                        <p>
                                                            Theoni provided an important and nurturing space for me to get to grips with my pregnancy journey. I deeply valued her knowledge and 
                                                            reassurance and felt very supported in our time together. Motherhood is such a significant threshold and finding the space to be present 
                                                            and held has been very empowering.
                                                        </p>
                                                        <div class="author italic">Juliette, mother to a new baby</div>
                                                    </blockquote>
                                                </div>
                                                <br class="clear" />
                                            </div>

                                        </div>
                                    </div>
                                    <!-- .row-fluid -->
                                    <div class="module_line_trigger" data-background="#ffeec9 url(img/VV.png) no-repeat center" data-top-padding="bottom_padding_huge" data-bottom-padding="module_big_padding">
                                        <div class="row-fluid">
                                            <div class="span12 module_cont module_promo_text module_huge_padding">
                                                <div class="shortcode_promoblock">
                                                    <div class="promo_button_block type2">
                                                        <a href="Conscious-Birth-Products.aspx" class="promo_button"> View our Products </a>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--.module_cont -->
                                            <div class="clear">
                                                <!-- ClearFIX -->
                                            </div>
                                        </div>
                                        <!-- .row-fluid -->
                                    </div>
                                    <!-- .module_line_trigger -->
                                    <!--<div class="row-fluid">
                                        <div class="span12 module_cont module_partners center_title module_big_padding2">
                                            <div class="bg_title">
                                                <div class="title_wrapper">
                                                    <a href="javascript:void(0)" class="btn_carousel_left"></a>
                                                    <h5 class="headInModule">Our Partners</h5>
                                                    <a href="javascript:void(0)" class="btn_carousel_right"></a>
                                                </div>
                                            </div>
                                            <div class="module_content sponsors_works carouselslider items5" data-count="5">
                                                <ul>
                                                    <li>
                                                        <div class="item">
                                                            <a href="#charity-html5css3-website-template/3180454" target="_blank">
                                                                <img src="img/pictures/partners2.png" alt=""></a>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="item">
                                                            <a href="#point-business-responsive-wp-theme/4319087" target="_blank">
                                                                <img src="img/pictures/partners4.png" alt=""></a>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="item">
                                                            <a href="#cleanspace-retina-ready-business-wp-theme/3776000" target="_blank">
                                                                <img src="img/pictures/partners1.png" alt=""></a>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="item">
                                                            <a href="#yellowproject-multipurpose-retina-wp-theme/4066662" target="_blank">
                                                                <img src="img/pictures/partners3.png" alt=""></a>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="item">
                                                            <a href="#showroom-portfolio-retina-ready-wp-theme/3473628" target="_blank">
                                                                <img src="img/pictures/partners5.png" alt=""></a>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="item">
                                                            <a href="themeforest.net/item/incipiens-responsive-portfolio-wordpress-theme/2762691" target="_blank">
                                                                <img src="img/pictures/partners6.png" alt=""></a>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="item">
                                                            <a href="themeforest.net/item/hq-photography-responsive-wp-theme/3200962" target="_blank">
                                                                <img src="img/pictures/partners9.png" alt=""></a>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="item">
                                                            <a href="#incipiens-responsive-portfolio-wordpress-theme/2762691" target="_blank">
                                                                <img src="img/pictures/partners11.png" alt=""></a>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- .row-fluid -->

                                </div>
                                <!-- .contentarea -->
                            </div>
                            <div class="left-sidebar-block span3">
                                <aside class="sidebar">
                                    //Sidebar Text
                                </aside>
                            </div>
                            <!-- .left-sidebar -->
                        </div>
                        <div class="clear">
                            <!-- ClearFix -->
                        </div>
                    </div>
                    <!-- .fl-container -->
                    <div class="right-sidebar-block span3">
                        <aside class="sidebar">
                        </aside>
                    </div>
                    <!-- .right-sidebar -->
                    <div class="clear">
                        <!-- ClearFix -->
                    </div>
                </div>
            </div>
            <!-- .container -->
        </div>
        <!-- .content_wrapper -->

    </div>
    <!-- .main_wrapper -->
</asp:Content>

