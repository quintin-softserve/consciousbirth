﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ConsciousBirth.master" AutoEventWireup="true" CodeFile="Conscious-Birth-Category-List.aspx.cs" Inherits="Conscious_Birth_Category_List" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="main_wrapper">
        <!-- C O N T E N T -->
        <div class="content_wrapper">
            <div class="page_title_block">
                <div class="container">
                    <h2 class="title">Category List</h2>
                    <%--<div class="breadcrumbs"><a href="home.html">Home</a><a href="#">Portfolio</a><span>2 columns</span></div>--%>
                </div>
            </div>
            <div class="container">
                <div class="content_block no-sidebar row">
                    <div class="fl-container span12">
                        <div class="row">
                            <div class="posts-block span12">
                                <div class="contentarea">

                                    <div class="row-fluid">
                                        <div class="span12 module_cont module_portfolio module_none_padding">

                                            <div class="portfolio_block image-grid columns4" id="list">
                                                <asp:Repeater ID="rpCategories" runat="server">
                                                    <ItemTemplate>
                                                        <div data-category="<%#Eval ("strTitle") %>" class="<%#Eval ("strTitle") %> element">
                                                            <div class="filter_img gallery_item">
                                                                <a href="<%#Eval ("Link") %>">
                                                                    <img class="gallery-stand-img" src="<%#Eval ("FullPathForImage") %>" alt="" width="570" height="400">
                                                                    <div class="gallery_fadder"></div>
                                                                    <div class="gallery_descr">
                                                                        <h6 class="gallery_title"><%#Eval ("strTitle") %></h6>
                                                                        <p><%#Eval ("strDescription") %></p>
                                                                    </div>
                                                                </a>
                                                            </div>
                                                        </div>
                                                        <!-- .element -->
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                            </div>
                                            <!-- .portfolio_block -->
                                            <div class="clear"></div>
                                            <asp:Literal ID="litGalleryJS" runat="server"></asp:Literal>
                                        </div>
                                        <!-- .module_portfolio -->
                                    </div>
                                    <!-- .row-fluid -->

                                </div>
                                <!-- .contentarea -->
                            </div>
                            <div class="left-sidebar-block span3">
                                <aside class="sidebar">
                                    //Sidebar Text
                                </aside>
                            </div>
                            <!-- .left-sidebar -->
                        </div>
                        <div class="clear">
                            <!-- ClearFix -->
                        </div>
                    </div>
                    <!-- .fl-container -->
                    <div class="right-sidebar-block span3">
                        <aside class="sidebar">
                        </aside>
                    </div>
                    <!-- .right-sidebar -->
                    <div class="clear">
                        <!-- ClearFix -->
                    </div>
                </div>
            </div>
            <!-- .container -->
        </div>
        <!-- .content_wrapper -->

    </div>
    <!-- .main_wrapper -->
</asp:Content>

