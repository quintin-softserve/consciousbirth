﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.Services;
using System.Web.Script.Services;
using System.Web.UI.HtmlControls;
public partial class CMS_AccountUserView : System.Web.UI.Page
{
    clsUsers clsUsers;
    clsAccountUsers clsAccountUsers;

    DataTable dtAccountUsersList;

    List<clsAccountUsers>  lstAccountUsers;

    #region EVENT METHODS

    protected void Page_Load(object sender, EventArgs e)
    {
        //### Check if session clsUser exists
        if (Session["clsUsers"] == null)
        {
            //### Redirect back to login
            Response.Redirect("../CMSLogin.aspx");
        }
        clsUsers = (clsUsers)Session["clsUsers "];

        dgrGrid.ItemCreated += new DataGridItemEventHandler(dgrGrid_PagerPopulate);

        if (!Page.IsPostBack)
        {
            PopulateFormData();
        }
        else
        {
            if (Session["lstAccountUsers;"] == null)
            {
                 lstAccountUsers = new List<clsAccountUsers>();
                Session["lstAccountUsers"] =  lstAccountUsers;
            }
            else
                 lstAccountUsers = (List<clsAccountUsers>)Session[" lstAccountUsers"];

            PopulateFormData();
        }
    }

    protected void lnkbtnSearch_Click(object sender, EventArgs e)
    {
        string FilterExpression = " (strFirstName + ' ' + strSurname + ' ' + strEmailAddress) LIKE '%" + txtSearch.Text + "%'";

        DataTable dtAccountUsersList = clsAccountUsers.GetAccountUsersList(FilterExpression, "");

        Session["dtAccountUsersList"] = dtAccountUsersList;
        PopulateFormData();
    }
    
    protected void lnkbtnAdd_Click(object sender, EventArgs e)
    {
        Response.Redirect("frmAccountUsersAddModify.aspx");
    }

    #endregion

    #region DATA GRID METHODS

    private void PopulateFormData()
    {
        //### Populate datagrid using clsUserList object
        try
        {
            dtAccountUsersList = new DataTable();

            if (Session["dtAccountUsersList"] == null)
                dtAccountUsersList = clsAccountUsers.GetAccountUsersList();
            else
                dtAccountUsersList = (DataTable)Session["dtAccountUsersList"];
                dgrGrid.DataSource = dtAccountUsersList;

            //### Add this check to trap if the last item on the page is deleted
            if (Convert.ToInt16(dgrGrid.CurrentPageIndex) < Convert.ToInt16(Session["AcountUserView_CurrentPageIndex"]))
            {
                dgrGrid.CurrentPageIndex = 0;
            }
            else
            {
                if (Session["AcountUserView_CurrentPageIndex"] != null)
                {
                    dgrGrid.CurrentPageIndex = (int)Session["AcountUserView_CurrentPageIndex"];
                }
            }

            dgrGrid.DataBind();

        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    protected void lnkDeleteItem_Click(object sender, EventArgs e)
    {
        int iAccountUserID = int.Parse((sender as LinkButton).CommandArgument);

        clsAccountUsers.Delete(iAccountUserID);

        PopulateFormData();
    }

    protected void dgrGrid_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
    {
        //### Method is used when the page index is changed.
        try
        {
            Session["AcountUserView_CurrentPageIndex"] = e.NewPageIndex;
            dgrGrid.CurrentPageIndex = e.NewPageIndex;
            PopulateFormData();
        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    protected void dgrGrid_SortCommand(object source, DataGridSortCommandEventArgs e)
    {
        //### Sort Method
        dtAccountUsersList = new DataTable();

        if (Session["dtAccountUsersList"] == null)
            dtAccountUsersList = clsAccountUsers.GetAccountUsersList();
        else
            dtAccountUsersList = (DataTable)Session["dtAccountUsersList"];

        DataView dvTemp = dtAccountUsersList.DefaultView;

        dvTemp.Sort = e.SortExpression;
        dtAccountUsersList = dvTemp.ToTable();
        Session["dtAccountUsersList"] = dtAccountUsersList;

        PopulateFormData();
    }

    public void dgrGrid_PopEditPassLink(object sender, EventArgs e)
    {
        try
        {
            //### Add hyperlink to datagrid item
            HyperLink lnkEditPass = (HyperLink)sender;
            DataGridItem dgrItemEditPass = (DataGridItem)lnkEditPass.Parent.Parent;

            //### Get the iAccountUserID
            int iAccountUserID = 0;
            iAccountUserID = int.Parse(dgrItemEditPass.Cells[0].Text);

            lnkEditPass.Attributes.Add("href", "frmAccountUsersPasswordEdit.aspx?action=edit&iAccountUserID=" + iAccountUserID);
        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    public void dgrGrid_PopEditLink(object sender, EventArgs e)
    {
        try
        {
            //### Add hyperlink to datagrid item
            HyperLink lnkEditItem = (HyperLink)sender;
            DataGridItem dgrItemEdit = (DataGridItem)lnkEditItem.Parent.Parent;

            //### Get the iUserID
           int iAccountUserID = 0;
            iAccountUserID = int.Parse(dgrItemEdit.Cells[0].Text);

            //### Add attributes to edit link
            lnkEditItem.Attributes.Add("href", "frmAccountUsersAddModify.aspx?action=edit&iAccountUserID=" + iAccountUserID);
        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    public void dgrGrid_PopDeleteLink(object sender, EventArgs e)
    {
        try
        {
            //### Add hyperlink to datagrid item
            HyperLink lnkDeleteItem = (HyperLink)sender;
            DataGridItem dgrItemDelete = (DataGridItem)lnkDeleteItem.Parent.Parent;

            //### Get the iUserID
            int iAccountUserID = 0;
            iAccountUserID = int.Parse(dgrItemDelete.Cells[0].Text);

            //### Add attributes to delete link
            lnkDeleteItem.CssClass = "dgrDeleteLink";
            lnkDeleteItem.Attributes.Add("href", "frmAccountUsersAddModify.aspx?action=edit&iAccountUserID=" + iAccountUserID);
        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    public static void dgrGrid_PagerPopulate(object sender, DataGridItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Pager)
        {
            Control[] arrayControls = new Control[e.Item.Controls[0].Controls.Count];

            for (int i = 0; i < e.Item.Controls[0].Controls.Count; i++)
            {
                arrayControls[i] = e.Item.Controls[0].Controls[i];

                //### Check if the first is a previous page button
                if (e.Item.Controls[0].Controls[i] is LinkButton)
                {
                }
            }

            //### Remove the crappy built in span used for paging
            for (int i = 0; i < e.Item.Controls[0].Controls.Count; i++)
            {
                e.Item.Controls[0].Controls.RemoveAt(i);
            }

            for (int i = 0; i < arrayControls.Length; i++)
            {
                if (arrayControls[i] is Label)
                {
                    //### Current page
                    LinkButton lblPager = new LinkButton();
                    HtmlGenericControl divPager = new HtmlGenericControl("div");
                    lblPager.Width = 22;
                    divPager.Attributes.Add("style", "margin-top:10px");
                    lblPager.Attributes.Add("style", "margin-right:2px");
                    divPager.Attributes.Add("style", "height:20px");
                    divPager.InnerText = ((Label)arrayControls[i]).Text;
                    lblPager.Controls.Add(divPager);
                    e.Item.Controls[0].Controls.Add(lblPager);

                }
                if (arrayControls[i] is LinkButton)
                {
                    //### All the other pages
                    LinkButton btnPager = new LinkButton();
                    HtmlGenericControl divPager = new HtmlGenericControl("p");
                    btnPager.Width = 22;
                    btnPager.CommandArgument = ((LinkButton)arrayControls[i]).CommandArgument;
                    btnPager.CommandName = ((LinkButton)arrayControls[i]).CommandName;
                    divPager.Attributes.Add("style", "margin-top:3px");
                    btnPager.Attributes.Add("style", "margin-right:2px");
                    divPager.InnerText = ((LinkButton)arrayControls[i]).Text;
                    btnPager.Controls.Add(divPager);
                    e.Item.Controls[0].Controls.Add(btnPager);
                }
            }
        }
    }

    #endregion

    #region USER FILTERING
    
    /// <summary>
    /// Enables the AutoCompleteExtender
    /// </summary>
    /// <param name="prefixText"></param>
    /// <param name="count"></param>
    /// <returns></returns>
    /// 
    [WebMethod]
    [ScriptMethod]
    public static string[] FilterBusinessRule(string prefixText, int count)
    {
        string[] strReturnList = new string[] { };

        string FilterExpression = " (strFirstName + ' ' + strSurname + ' ' + strEmailAddress) LIKE '%" + prefixText + "%'";
        DataTable dtAccountUsersList = clsAccountUsers.GetAccountUsersList(FilterExpression, "");
        List<string>  lstAccountUsers = new List<string>();

        if (dtAccountUsersList.Rows.Count > 0)
        {
            foreach (DataRow dtrAccountUsersLis in dtAccountUsersList.Rows)
            {
                 lstAccountUsers.Add(AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem("" + dtrAccountUsersLis["strFirstName"].ToString() + " " + dtrAccountUsersLis["strSurname"].ToString(), dtrAccountUsersLis["iAccountUserID"].ToString()));
            }
        }
        else
             lstAccountUsers.Add("No Users Available.");

        strReturnList =  lstAccountUsers.ToArray();

        return strReturnList;
    }

    #endregion
    protected void hfUserID_ValueChanged(object sender, EventArgs e)
    {

    }
    protected void txtSearch_TextChanged(object sender, EventArgs e)
    {

    }
    protected void dgrGrid_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
}
