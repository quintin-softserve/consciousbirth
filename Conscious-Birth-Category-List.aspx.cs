﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Conscious_Birth_Category_List : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        popCategories();
    }

    protected void popCategories()
    {
        StringBuilder strbGalleryJs = new StringBuilder();

        strbGalleryJs.AppendLine("<script>");
        strbGalleryJs.AppendLine("//<![CDATA[");
        strbGalleryJs.AppendLine("items_set = [	//Gallery Data");

        DataTable dtCategories = clsCategories.GetFeaturedCategories();
        dtCategories.Columns.Add("FullPathForImage");
        dtCategories.Columns.Add("Link");
        dtCategories.Columns.Add("classForColumn");


        int iCount = 0;

        foreach (DataRow dtrCategories in dtCategories.Rows)
        {
            if (!(dtrCategories["strMasterImage"].ToString() == "") || (dtrCategories["strMasterImage"] == null) || (dtrCategories["strPathToImages"] == null))
            {
                dtrCategories["FullPathForImage"] = "Categories/" + dtrCategories["strPathToImages"] + "/crop_" + dtrCategories["strMasterImage"];

                dtrCategories["Link"] = "Conscious-Birth-Products.aspx?iCategoryID=" + dtrCategories["iCategoryID"].ToString();
            }
            else
            {
                dtrCategories["FullPathForImage"] = "img/no-image.png";
            }

            ++iCount;
            if (iCount == 1)
            {
                strbGalleryJs.AppendLine("{ src: '" + dtrCategories["FullPathForImage"] + "', url: '" + dtrCategories["Link"] + "', category: '" + dtrCategories["strTitle"] + "', title: '" + dtrCategories["strTitle"] + "', description: '" + dtrCategories["strDescription"] + "' }");
            }
            else
            {
                strbGalleryJs.Append(",");
                strbGalleryJs.AppendLine("{ src: '" + dtrCategories["FullPathForImage"] + "', url: '" + dtrCategories["Link"] + "', category: '" + dtrCategories["strTitle"] + "', title: '" + dtrCategories["strTitle"] + "', description: '" + dtrCategories["strDescription"] + "' }");
            }
        }

        strbGalleryJs.AppendLine("];");
        strbGalleryJs.AppendLine("$('#list').portfolio_addon({");
        strbGalleryJs.AppendLine("type: 1,");
        strbGalleryJs.AppendLine("load_count: 2,");
        strbGalleryJs.AppendLine("items: items_set");
        strbGalleryJs.AppendLine("});");
        strbGalleryJs.AppendLine("//]]");
        strbGalleryJs.AppendLine("</script>");

        rpCategories.DataSource = dtCategories;
        rpCategories.DataBind();

        litGalleryJS.Text = strbGalleryJs.ToString();
    }
}