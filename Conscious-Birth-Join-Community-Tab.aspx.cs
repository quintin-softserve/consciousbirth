﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class web_Conscious_Birth_Join_Community_Tab : System.Web.UI.Page
{
    clsSubscribers clsSubscribers;
   

    #region EVENT METHODS

    protected void Page_Load(object sender, EventArgs e)
    {
        
    }

    protected void lnkbtnBack_Click(object sender, EventArgs e)
    {
        //### Go back to previous page
        Response.Redirect("frmSubscribersView.aspx");
    }


    protected void lnkbtnClear_Click(object sender, EventArgs e)
    {
        txtName.Text = "";

        clsValidation.SetValid(txtName);

        txtSurname.Text = "";
        clsValidation.SetValid(txtSurname);

        txtEmail.Text = "";
        clsValidation.SetValid(txtEmail);
    }

    #endregion

    #region POPULATE DATA METHODS



    #region SAVE DATA METHODS

    private void SaveData()
    {
        //### Only update the password if there is a value
        clsSubscribers clsSubscribersRecord = new clsSubscribers();

        //### Add / Update
        clsSubscribersRecord.dtAdded = Convert.ToDateTime(DateTime.Now.ToString("dd MMM yyyy"));
        clsSubscribersRecord.iAddedBy = -10;
        clsSubscribersRecord.strFirstName = txtName.Text;
        clsSubscribersRecord.strSurname = txtSurname.Text;
        clsSubscribersRecord.strEmailAddress = txtEmail.Text;

        clsSubscribersRecord.Update();

        //### redirect back to view page
        Response.Redirect("frmSubscribersView.aspx");
    }

    #endregion
    protected void btnSubscribe_Click(object sender, EventArgs e)
    {
        SaveData();
    }

    #endregion
}