using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Data.SqlClient;

/// <summary>
/// Summary description for clsUserWishlistLink
/// </summary>
public class clsUserProductWishlistLink
{
    #region MEMBER VARIABLES

    private int m_iUserProductWishlistLinkID;
    private int m_iProductID;
    private int m_iWishListEventID;
    private int m_iQuantity;
    private string m_strColor;
    private string m_strSize;
    private bool m_bisPurchased;
    private bool m_bIsDeleted;

    #endregion

    #region PROPERTIES

    public int iUserProductWishlistLinkID
    {
        get
        {
            return m_iUserProductWishlistLinkID;
        }
    }

    public int iProductID
    {
        get
        {
            return m_iProductID;
        }
        set
        {
            m_iProductID = value;
        }
    }

    public int iWishListEventID
    {
        get
        {
            return m_iWishListEventID;
        }
        set
        {
            m_iWishListEventID = value;
        }
    }

    public int iQuantity
    {
        get
        {
            return m_iQuantity;
        }
        set
        {
            m_iQuantity = value;
        }
    }
    public string strColor
    {
        get
        {
            return m_strColor;
        }
        set
        {
            m_strColor = value;
        }
    }
    public string strSize
    {
        get
        {
            return m_strSize;
        }
        set
        {
            m_strSize = value;
        }
    }
    public bool bisPurchased
    {
        get
        {
            return m_bisPurchased;
        }
        set
        {
            m_bisPurchased = value;
        }
    }

    public bool bIsDeleted
    {
        get
        {
            return m_bIsDeleted;
        }
        set
        {
            m_bIsDeleted = value;
        }
    }



    #endregion

    #region CONSTRUCTORS

    public clsUserProductWishlistLink()
    {
        m_iUserProductWishlistLinkID = 0;
    }

    public clsUserProductWishlistLink(int iUserProductWishlistLinkID)
    {
        m_iUserProductWishlistLinkID = iUserProductWishlistLinkID;
        GetData();
    }

    #endregion

    #region PUBLIC METHODS

    public virtual void Update()
        {
            try
            {
                if (iUserProductWishlistLinkID == 0)
                {
                    //### Assign values to the parameter list for each corresponding column in the DB
                    SqlParameter[] sqlParametersInsert = new SqlParameter[] 
                    {
                        new SqlParameter("@iProductID", m_iProductID),
                        new SqlParameter("@iWishListEventID", m_iWishListEventID),
                        new SqlParameter("@iQuantity", m_iQuantity),
                        new SqlParameter("@strColor", m_strColor),
                        new SqlParameter("@strSize", m_strSize),
                         new SqlParameter("@bisPurchased", m_bisPurchased)
                        

                  };

                  //### Add
                  m_iUserProductWishlistLinkID = (int)clsDataAccess.ExecuteScalar("spUserProductWishlistLinkInsert", sqlParametersInsert);                    
                    }
                    else
                    {
                    //### Assign values to the parameter list for each corresponding column in the DB
                    SqlParameter[] sqlParametersUpdate = new SqlParameter[] 
                    {
                         new SqlParameter("@iUserProductWishlistLinkID", m_iUserProductWishlistLinkID),
                         new SqlParameter("@iProductID", m_iProductID),
                         new SqlParameter("@iWishListEventID", m_iWishListEventID),
                         new SqlParameter("@iQuantity", m_iQuantity),
                         new SqlParameter("@strColor", m_strColor),
                         new SqlParameter("@strSize", m_strSize),
                          new SqlParameter("@bisPurchased", m_bisPurchased)
                    };
                    //### Update
                    clsDataAccess.Execute("spUserProductWishlistLinkUpdate", sqlParametersUpdate);
                    }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    public static void Delete(int iUserProductWishlistLinkID)
    {
        //### Populate
        SqlParameter[] sqlParameter = new SqlParameter[]
        {
            new SqlParameter("@iUserProductWishlistLinkID", iUserProductWishlistLinkID)
        };
        //### Executes delete sp
        clsDataAccess.Execute("spUserProductWishlistLinkDelete", sqlParameter);
    }

    public static void Clear(int iWishListEventID)
    {
        //### Populate
        SqlParameter[] sqlParameter = new SqlParameter[]
        {
            new SqlParameter("@iWishListEventID", iWishListEventID)
        };
        //### Executes delete sp
        clsDataAccess.Execute("spUserProductWishlistLinkClear", sqlParameter);
    }

    public static DataTable GetUserProductWishlistLinkList()
    {
        SqlParameter[] EmptySqlParameter = new SqlParameter[] { };
        return clsDataAccess.GetDataTable("spUserProductWishlistLinkList", EmptySqlParameter);
    }

    public static DataTable GetUserProductWishlistLinkList(string strFilterExpression, string strSortExpression)
    {
        DataView dvUserProductWishlistLinkList = new DataView();

        SqlParameter[] EmptySqlParameter = new SqlParameter[] { };
        dvUserProductWishlistLinkList = clsDataAccess.GetDataView("spUserProductWishlistLinkList", EmptySqlParameter);
        dvUserProductWishlistLinkList.RowFilter = strFilterExpression;
        dvUserProductWishlistLinkList.Sort = strSortExpression;

        return dvUserProductWishlistLinkList.ToTable();
    }
    #endregion

    #region PROTECTED METHODS

    protected virtual void GetData()
    {
        try
        {
            //### Populate
            SqlParameter[] sqlParameter = new SqlParameter[] 
                        {
                            new SqlParameter("@iUserProductWishlistLinkID", m_iUserProductWishlistLinkID)
                        };
            DataRow drRecord = clsDataAccess.GetRecord("spUserProductWishlistLinkGetRecord", sqlParameter);

            m_iProductID = Convert.ToInt32(drRecord["iProductID"]);
            m_iWishListEventID = Convert.ToInt32(drRecord["iWishListEventID"]);
            m_iQuantity = Convert.ToInt32(drRecord["iQuantity"]);
            m_strColor = Convert.ToString(drRecord["strColor"]);
            m_strSize = Convert.ToString(drRecord["strSize"]);
            if (drRecord["bisPurchased"] != DBNull.Value)
            m_bisPurchased = Convert.ToBoolean(drRecord["bisPurchased"]);
            m_bIsDeleted = Convert.ToBoolean(drRecord["bIsDeleted"]);

        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    #endregion

    public static DataTable GetUserProductWishlistLinkList(string p)
    {
        throw new NotImplementedException();
    }
}