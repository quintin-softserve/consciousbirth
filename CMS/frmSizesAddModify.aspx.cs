using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class CMS_frmSizesAddModify : System.Web.UI.Page
{
    clsUsers clsUsers;
    clsSizes clsSizes;
    #region EVENT METHODS

    protected void Page_Load(object sender, EventArgs e)
    {
        //### Check if session clsUser exists
        if (Session["clsUsers"] == null)
        {
            {
                //### Redirect back to login
                Response.Redirect("../CMSLogin.aspx");
            }
        }
        clsUsers = (clsUsers)Session["clsUsers"];

        if (!IsPostBack)
        {

            //### If the iSizeID is passed through then we want to instantiate the object with that iSizeID
            if ((Request.QueryString["iSizeID"] != "") && (Request.QueryString["iSizeID"] != null))
            {
                clsSizes = new clsSizes(Convert.ToInt32(Request.QueryString["iSizeID"]));

                //### Populate the form
                popFormData();
            }
            else
            {
                clsSizes = new clsSizes();
            }
            Session["clsSizes"] = clsSizes;
        }
        else
        {
            clsSizes = (clsSizes)Session["clsSizes"];
        }
    }

    protected void lnkbtnBack_Click(object sender, EventArgs e)
    {
        //### Go back to previous page
        Response.Redirect("frmSizesView.aspx");
    }

    protected void lnkbtnSave_Click(object sender, EventArgs e)
    {
        //### Validate registration process
        bool bCanSave = true;

       bCanSave = clsValidation.IsNullOrEmpty(txtTitle, bCanSave);

        if (bCanSave == true)
        {
            mandatoryDiv.Visible = false;
            lblValidationMessage.Text = "<div class=\"madatoryPaddingDiv\"><img src=\"images/Validation/imgFaceHappy.png\" alt='' title=''/><div class=\"validationMessage\">Size added successfully</div></div>";
            SaveData();
        }
        else
        {
            mandatoryDiv.Visible = true;
            lblValidationMessage.Text = "<div class=\"madatoryPaddingDiv\"><img src=\"images/Validation/imgFaceSad.png\" alt='' title=''/><div class=\"validationMessage\">Please fill out all mandatory fields - Size not added</div></div>";
        }
    }

    protected void lnkbtnClear_Click(object sender, EventArgs e)
    {
        mandatoryDiv.Visible = false;

        txtTitle.Text = "";
        clsValidation.SetValid(txtTitle);
    }

    #endregion 

    #region POPULATE DATA METHODS

    private void popFormData()
    {
         txtTitle.Text = clsSizes.strTitle;
    }
    
    #endregion

    #region SAVE DATA METHODS

    private void SaveData()
    {
        //### Add / Update
        clsSizes.dtAdded = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
        clsSizes.iAddedBy = clsUsers.iUserID;
        clsSizes.dtEdited = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
        clsSizes.iEditedBy = clsUsers.iUserID;
        clsSizes.strTitle = txtTitle.Text;

        clsSizes.Update();

        Session["dtSizesList"] = null;

        //### Go back to view page
        Response.Redirect("frmSizesView.aspx");
    }

    #endregion
}