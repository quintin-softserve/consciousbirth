﻿<%@ Page Title="History :: Conscious Birth" Language="C#" MasterPageFile="~/ConsciousBirth.master" AutoEventWireup="true" CodeFile="Conscious-Birth-History.aspx.cs" Inherits="Conscious_Birth_History" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <title>Conscious Birth - Products</title>






    <script type="text/javascript" src="js/jquery.min.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="main_wrapper">
        <!-- C O N T E N T -->
        <div class="content_wrapper">
            <div class="page_title_block">
                <div class="container">
                    <h2 class="title">History</h2>
                </div>
            </div>
            <div class="container">
                <div class="content_block no-sidebar row">
                    <div class="fl-container span12">
                        <div class="row">
                            <div class="posts-block span12">
                                <div class="contentarea">

                                    <div class="row-fluid">
                                        <div class="span4 module_cont module_text_area module_none_padding">
                                            <img src="img/History.jpg" alt="" />
                                        </div>
                                        <div class="span8 module_cont module_text_area module_none_padding">
                                            <blockquote class="type2">
                                                <h6>History</h6>
                                                <p>
                                                    Theoni’s quest for greater knowledge towards understanding began at the age of 17 when she did her first meditation course in 
                                                    Transcendental Meditation. Since then she has gone on to study Numerology, Tree of Life and Kabbalah under Kate Rheeders. She 
                                                    has completed the Life Line and Life Line counsellor course; learned from Deepak Chopra's primordial sound mediation course; 
                                                    and been trained in Peter Hieblooms Alpha Mind Power course. In 2005 she also completed her Vision Quest with Judy Bekker and 
                                                    Valerie Morris, who introduced the Vision Quest process to South Africa.
                                                </p>
                                                <br />
                                                <p>
                                                    Theoni attended her first birth 19 years ago. She gained experience as a doula after qualifying as a Doula Birth companion and 
                                                    she spent many years working in conjunction with the teachings of Sharon Marsay, an active birth advocate. She has worked alongside 
                                                    many different caregivers, midwives and gynaecologists. With her qualification in Hypnotherapy, through the SA School of Practical 
                                                    Hypnosis and Research, she has expanded her offerings by being one of the pioneers of HypnoBirthing® in South Africa and has trained 
                                                    with the HypnoBirthing Institute. Theoni has been teaching HypnoBirthing® for eight years.
                                                </p>
                                                <br />
                                                <p>
                                                    Most Theoni’s studies have pivoted around pregnancy and birth, which have been central to her journey of discovery.  She undertook a 
                                                    special thesis on pregnancy whilst qualifying as an aromatherapist and reflexologist and this gives her unique insights and a deep 
                                                    understanding of what a therapist should provide. But she has also branched out and added to her specialisations with qualifications 
                                                    in Reiki and Life Alignment, giving her multi-modalities to work with in achieving health and relaxation for pregnant and post-partum women. 
                                                    She has also incorporated Life Alignment and hypnosis into her NLP (Neuro Linguistic Programming) life coaching, thus extending the 
                                                    treatments she provides to beyond the world of pregnancy and birth.
                                                </p>
                                                <br />
                                                <p>
                                                    Theoni is a Hatha Yoga and Kundalini Yoga teacher and she has specialised in Pregnancy Yoga under the mentorship of Aryo Jacobsen, 
                                                    Gurmukh Kaur Khalsa and Sat Hari Kaur. She is the original pregnancy yoga instructor in South Africa and has been teaching consistently 
                                                    for 14 years. Her specialised, tailor-made pregnancy yoga classes deal with the specific physical, emotional and spiritual concerns of 
                                                    pregnancy. The pregnancy yoga classes are also an opportunity for mothers to learn about pregnancy health and gain access to important 
                                                    information on pregnancy. In addition to pregnancy yoga, Theoni has also created a unique Mom’s and Babe’s yoga class that allows Moms 
                                                    and their babies to practice a type of yoga that accommodates them both and allows for playful bonding time. Her own daily spiritual yoga 
                                                    practice enriches the offerings she provides.
                                                </p>
                                                <br />
                                                <p>
                                                    Theoni’s own pregnancy and the birth of her son in 2007 changed her life forever, and her values and beliefs about her world were profoundly 
                                                    and irrevocably transformed. She has continued to search for and find more integrative methods of guiding women through various transitions 
                                                    using NLP life coaching and by giving women practical tools to nurture their sense of self. This kind of nurturing helps women cope better 
                                                    with the complex array of situations found in life. Theoni’s unique guidance enables women to tap into their ability to flow with life and 
                                                    to develop a confident sense of knowing and a better, more fulfilling relationship with their spirits.
                                                </p>
                                                <br />
                                                <p>
                                                    Theoni’s journey as a student, mother, teacher, therapist, counsellor, doula, hypnotherapist, NLP therapist and woman has led her to create Conscious 
                                                    Birth, Conscious Living. Her goal is to empower and support you in illuminating your unique path through the many transitions that you are bound 
                                                    encounter. Let your journey begin...
                                                </p>
                                                <b />
                                            </blockquote>

                                        </div>
                                    </div>
                                    <!-- .row-fluid -->
                                    <div class="module_line_trigger" data-background="#ffeec9 url(img/VV.png) no-repeat center" data-top-padding="bottom_padding_huge" data-bottom-padding="module_big_padding">
                                        <div class="row-fluid">
                                            <div class="span12 module_cont module_promo_text module_huge_padding">
                                                <div class="shortcode_promoblock">
                                                    <div class="promo_button_block type2">
                                                        <a href="Conscious-Birth-Products.aspx" class="promo_button"> View our Products </a>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--.module_cont -->
                                            <div class="clear">
                                                <!-- ClearFIX -->
                                            </div>
                                        </div>
                                        <!-- .row-fluid -->
                                    </div>
                                    <!-- .module_line_trigger -->
                                   <!-- <div class="row-fluid">
                                        <div class="span12 module_cont module_partners center_title module_big_padding2">
                                            <div class="bg_title">
                                                <div class="title_wrapper">
                                                    <a href="javascript:void(0)" class="btn_carousel_left"></a>
                                                    <h5 class="headInModule">Our Partners</h5>
                                                    <a href="javascript:void(0)" class="btn_carousel_right"></a>
                                                </div>
                                            </div>
                                            <div class="module_content sponsors_works carouselslider items5" data-count="5">
                                                <ul>
                                                    <li>
                                                        <div class="item">
                                                            <a href="#charity-html5css3-website-template/3180454" target="_blank">
                                                                <img src="img/pictures/partners2.png" alt=""></a>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="item">
                                                            <a href="#point-business-responsive-wp-theme/4319087" target="_blank">
                                                                <img src="img/pictures/partners4.png" alt=""></a>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="item">
                                                            <a href="#cleanspace-retina-ready-business-wp-theme/3776000" target="_blank">
                                                                <img src="img/pictures/partners1.png" alt=""></a>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="item">
                                                            <a href="#yellowproject-multipurpose-retina-wp-theme/4066662" target="_blank">
                                                                <img src="img/pictures/partners3.png" alt=""></a>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="item">
                                                            <a href="#showroom-portfolio-retina-ready-wp-theme/3473628" target="_blank">
                                                                <img src="img/pictures/partners5.png" alt=""></a>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="item">
                                                            <a href="themeforest.net/item/incipiens-responsive-portfolio-wordpress-theme/2762691" target="_blank">
                                                                <img src="img/pictures/partners6.png" alt=""></a>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="item">
                                                            <a href="themeforest.net/item/hq-photography-responsive-wp-theme/3200962" target="_blank">
                                                                <img src="img/pictures/partners9.png" alt=""></a>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="item">
                                                            <a href="#incipiens-responsive-portfolio-wordpress-theme/2762691" target="_blank">
                                                                <img src="img/pictures/partners11.png" alt=""></a>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- .row-fluid -->

                                </div>
                                <!-- .contentarea -->
                            </div>
                            <div class="left-sidebar-block span3">
                                <aside class="sidebar">
                                    //Sidebar Text
                                </aside>
                            </div>
                            <!-- .left-sidebar -->
                        </div>
                        <div class="clear">
                            <!-- ClearFix -->
                        </div>
                    </div>
                    <!-- .fl-container -->
                    <div class="right-sidebar-block span3">
                        <aside class="sidebar">
                        </aside>
                    </div>
                    <!-- .right-sidebar -->
                    <div class="clear">
                        <!-- ClearFix -->
                    </div>
                </div>
            </div>
            <!-- .container -->
        </div>
        <!-- .content_wrapper -->

    </div>
    <!-- .main_wrapper -->
</asp:Content>

