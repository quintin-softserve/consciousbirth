﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Products_Details : System.Web.UI.Page
{
    clsUsers clsUsers;
    clsProducts clsProducts;
     List<clsProducts> glstProducts;
    protected void Page_Load(object sender, EventArgs e)
    {
        clsProducts = new clsProducts(Convert.ToInt32(Request.QueryString["iProductID"]));
        int iProductID = clsProducts.iProductID;
        popProductDetails(iProductID);
    }


    private void popProductDetails(int iProductID)
    {
        
        StringBuilder sbProductDetails = new StringBuilder();
        string strFullPathToImage = "Products/"+ clsProducts.strPathToImages +"/" + clsProducts.strMasterImage;
        sbProductDetails.AppendLine("");

        sbProductDetails.AppendLine("<div class='cont1 span_4_of_a1'>");
            sbProductDetails.AppendLine("<ul class='add-to-links'>");
                sbProductDetails.AppendLine("<li>");
                sbProductDetails.AppendLine("<img src='" + strFullPathToImage + "' alt='' style='border:1px solid #eee' /><a href='#'></a>");
                sbProductDetails.AppendLine("</li>");
            sbProductDetails.AppendLine("</ul>");
        sbProductDetails.AppendLine(" </div>");


        sbProductDetails.AppendLine("<div class='cont1 span_2_of_a1'>");
            sbProductDetails.AppendLine("<h3 class='m_3'>" + clsProducts.strTitle + "</h3>");
                sbProductDetails.AppendLine("<div class='price_single'>");
                sbProductDetails.AppendLine("<span class='actual'> R " + clsProducts.dblPrice.ToString("N2") + "</span><a href='#'></a>");
                sbProductDetails.AppendLine("</div>");
            sbProductDetails.AppendLine("<p class='m_desc'>"+clsProducts.strDescription+"</p>");   
            sbProductDetails.AppendLine("<div class='tn_form'>");
                sbProductDetails.AppendLine("<form>");
                    sbProductDetails.AppendLine("<input type='submit' value='buy now' title=''>");
                sbProductDetails.AppendLine(" </form>");
            sbProductDetails.AppendLine("</div>");
            sbProductDetails.AppendLine(" <div class='social_single'>");
                sbProductDetails.AppendLine("<ul>");
                    sbProductDetails.AppendLine("<li class='fb'><a href='#'><span></span></a></li>");
                    sbProductDetails.AppendLine("<li class='tw'><a href='#'><span></span></a></li>");
                    sbProductDetails.AppendLine("<li class='g_plus'><a href='#'><span></span></a></li>");
                    sbProductDetails.AppendLine("<li class='rss'><a href='#'><span></span></a></li>");
                sbProductDetails.AppendLine("</ul>");
            sbProductDetails.AppendLine("</div>");
        sbProductDetails.AppendLine("</div>");
        litProducts.Text = sbProductDetails.ToString();
    }
   
   
}