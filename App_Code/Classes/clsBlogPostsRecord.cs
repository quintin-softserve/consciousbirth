
using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Data.SqlClient;

/// <summary>
/// Summary description for clsBlogPosts
/// </summary>
public class clsBlogPosts
{
    #region MEMBER VARIABLES

        private int m_iBlogPostID;
        private DateTime m_dtAdded;
        private int m_iAddedBy;
        private DateTime m_dtEdited;
        private int m_iEditedBy;
        private String m_strStockCode;
        private int m_iBlogTypeID;
        private String m_strTitle;
        private String m_strTagLine;
        private String m_strDescription;
        private String m_strPathToImages;
        private String m_strMasterImage;
        private String m_strVideoLink;
        private String m_strGeoMapLocation;
        private bool m_bDoNotDisplay;
        private bool m_bIsDeleted;
        
    #endregion 

    #region PROPERTIES

        public int iBlogPostID
        {
            get
            {
                return m_iBlogPostID;
            }
        }

        public DateTime dtAdded
        {
            get
            {
                return m_dtAdded;
            }
            set
            {
                m_dtAdded = value;
            }
        }

        public int iAddedBy
        {
            get
            {
                return m_iAddedBy;
            }
            set
            {
                m_iAddedBy = value;
            }
        }

        public DateTime dtEdited
        {
            get
            {
                return m_dtEdited;
            }
            set
            {
                m_dtEdited = value;
            }
        }

        public int iEditedBy
        {
            get
            {
                return m_iEditedBy;
            }
            set
            {
                m_iEditedBy = value;
            }
        }

        public String strStockCode
        {
            get
            {
                return m_strStockCode;
            }
            set
            {
                m_strStockCode = value;
            }
        }

        public int iBlogTypeID
        {
            get
            {
                return m_iBlogTypeID;
            }
            set
            {
                m_iBlogTypeID = value;
            }
        }

        public String strTitle
        {
            get
            {
                return m_strTitle;
            }
            set
            {
                m_strTitle = value;
            }
        }

        public String strTagLine
        {
            get
            {
                return m_strTagLine;
            }
            set
            {
                m_strTagLine = value;
            }
        }

        public String strDescription
        {
            get
            {
                return m_strDescription;
            }
            set
            {
                m_strDescription = value;
            }
        }

        public String strPathToImages
        {
            get
            {
                return m_strPathToImages;
            }
            set
            {
                m_strPathToImages = value;
            }
        }

        public String strMasterImage
        {
            get
            {
                return m_strMasterImage;
            }
            set
            {
                m_strMasterImage = value;
            }
        }

        public String strVideoLink
        {
            get
            {
                return m_strVideoLink;
            }
            set
            {
                m_strVideoLink = value;
            }
        }

        public String strGeoMapLocation
        {
            get
            {
                return m_strGeoMapLocation;
            }
            set
            {
                m_strGeoMapLocation = value;
            }
        }

        public bool bDoNotDisplay
        {
            get
            {
                return m_bDoNotDisplay;
            }
            set
            {
                m_bDoNotDisplay = value;
            }
        }

        public bool bIsDeleted
        {
            get
            {
                return m_bIsDeleted;
            }
            set
            {
                m_bIsDeleted = value;
            }
        }

    
    #endregion
    
    #region CONSTRUCTORS

    public clsBlogPosts()
    {
        m_iBlogPostID = 0;
    }

    public clsBlogPosts(int iBlogPostID)
    {
        m_iBlogPostID = iBlogPostID;
        GetData();
    }

    #endregion

    #region PUBLIC METHODS

        public virtual void Update()
        {
            try
            {
                if (iBlogPostID == 0)
                {
                    //### Assign values to the parameter list for each corresponding column in the DB
                    SqlParameter[] sqlParametersInsert = new SqlParameter[] 
                    {
                        new SqlParameter("@dtAdded", m_dtAdded),
                        new SqlParameter("@iAddedBy", m_iAddedBy),
                        new SqlParameter("@strStockCode", m_strStockCode),
                        new SqlParameter("@iBlogTypeID", m_iBlogTypeID),
                        new SqlParameter("@strTitle", m_strTitle),
                        new SqlParameter("@strTagLine", m_strTagLine),
                        new SqlParameter("@strDescription", m_strDescription),
                        new SqlParameter("@strPathToImages", m_strPathToImages),
                        new SqlParameter("@strMasterImage", m_strMasterImage),
                        new SqlParameter("@strVideoLink", m_strVideoLink),
                        new SqlParameter("@strGeoMapLocation", m_strGeoMapLocation),
                        new SqlParameter("@bDoNotDisplay", m_bDoNotDisplay)                  
                  };

                  //### Add
                  m_iBlogPostID = (int)clsDataAccess.ExecuteScalar("spBlogPostsInsert", sqlParametersInsert);                    
                    }
                    else
                    {
                    //### Assign values to the parameter list for each corresponding column in the DB
                    SqlParameter[] sqlParametersUpdate = new SqlParameter[] 
                    {
                         new SqlParameter("@iBlogPostID", m_iBlogPostID),
                         new SqlParameter("@dtEdited", m_dtEdited),
                         new SqlParameter("@iEditedBy", m_iEditedBy),
                         new SqlParameter("@strStockCode", m_strStockCode),
                         new SqlParameter("@iBlogTypeID", m_iBlogTypeID),
                         new SqlParameter("@strTitle", m_strTitle),
                         new SqlParameter("@strTagLine", m_strTagLine),
                         new SqlParameter("@strDescription", m_strDescription),
                         new SqlParameter("@strPathToImages", m_strPathToImages),
                         new SqlParameter("@strMasterImage", m_strMasterImage),
                         new SqlParameter("@strVideoLink", m_strVideoLink),
                         new SqlParameter("@strGeoMapLocation", m_strGeoMapLocation),
                         new SqlParameter("@bDoNotDisplay", m_bDoNotDisplay)
                    };
                    //### Update
                    clsDataAccess.Execute("spBlogPostsUpdate", sqlParametersUpdate);
                    }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void Delete(int iBlogPostID)
        {
        //### Populate
        SqlParameter[] sqlParameter = new SqlParameter[]
        {
            new SqlParameter("@iBlogPostID", iBlogPostID)
        };
        //### Executes delete sp
        clsDataAccess.Execute("spBlogPostsDelete", sqlParameter);
        }

        public static DataTable GetBlogPostsList()
        {
            SqlParameter[] EmptySqlParameter = new SqlParameter[]{};
            return clsDataAccess.GetDataTable("spBlogPostsList", EmptySqlParameter);
        }
        public static DataTable GetBlogPostsList(string strFilterExpression, string strSortExpression)
        {
            DataView dvBlogPostsList = new DataView();

            SqlParameter[] EmptySqlParameter = new SqlParameter[] { };
            dvBlogPostsList = clsDataAccess.GetDataView("spBlogPostsList", EmptySqlParameter);
            dvBlogPostsList.RowFilter = strFilterExpression;
            dvBlogPostsList.Sort = strSortExpression;

            return dvBlogPostsList.ToTable();
        }
    #endregion

    #region PROTECTED METHODS

        protected virtual void GetData()
        {
            try
                {
                    //### Populate
                    SqlParameter[] sqlParameter = new SqlParameter[] 
                        {
                            new SqlParameter("@iBlogPostID", m_iBlogPostID)
                        };
                DataRow drRecord = clsDataAccess.GetRecord("spBlogPostsGetRecord", sqlParameter);

                m_dtAdded = Convert.ToDateTime(drRecord["dtAdded"]);
                m_iAddedBy = Convert.ToInt32(drRecord["iAddedBy"]);

                if (drRecord["dtEdited"] != DBNull.Value)
                   m_dtEdited = Convert.ToDateTime(drRecord["dtEdited"]);

             if (drRecord["iEditedBy"] != DBNull.Value)
               m_iEditedBy = Convert.ToInt32(drRecord["iEditedBy"]);

                m_strStockCode = drRecord["strStockCode"].ToString();
                m_iBlogTypeID = Convert.ToInt32(drRecord["iBlogTypeID"]);
                m_strTitle = drRecord["strTitle"].ToString();
                m_strTagLine = drRecord["strTagLine"].ToString();
                m_strDescription = drRecord["strDescription"].ToString();
                m_strPathToImages = drRecord["strPathToImages"].ToString();
                m_strMasterImage = drRecord["strMasterImage"].ToString();
                m_strVideoLink = drRecord["strVideoLink"].ToString();
                m_strGeoMapLocation = drRecord["strGeoMapLocation"].ToString();
                m_bDoNotDisplay = Convert.ToBoolean(drRecord["bDoNotDisplay"]);
                m_bIsDeleted = Convert.ToBoolean(drRecord["bIsDeleted"]);
 
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    #endregion
}