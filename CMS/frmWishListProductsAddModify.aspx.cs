﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class CMS_frmWishListProductsAddModify : System.Web.UI.Page
{
//    clsUsers clsUsers;
//    clsUserProductWishlistLink clsUserProductWishlistLink;
//    #region EVENT METHODS

//    protected void Page_Load(object sender, EventArgs e)
//    {
//        //### Check if session clsUser exists
//        if (Session["clsUsers"] == null)
//        {
//            {
//                //### Redirect back to login
//                Response.Redirect("../CMSLogin.aspx");
//            }
//        }
//        clsUsers = (clsUsers)Session["clsUsers"];

//        //### Determines if a javascript delete has been called
//        if (!String.IsNullOrEmpty(Page.Request["__EVENTARGUMENT"]) && (Page.Request["__EVENTARGUMENT"].Split(':')[0] == "iRemoveImages"))
//            DeleteImages(Convert.ToInt32(Page.Request["__EVENTARGUMENT"].Split(':')[1]));

//        if (!IsPostBack)
//        {
//            popSize();
//            popColour();

//            //### If the iProductID is passed through then we want to instantiate the object with that iProductID
//            if ((Request.QueryString["iProductID"] != "") && (Request.QueryString["iProductID"] != null))
//            {
//                clsUserProductWishlistLink = new clsUserProductWishlistLink(Convert.ToInt32(Request.QueryString["iProductID"]));

//                //### Populate the form
//                popFormData();
//            }
//            else
//            {
//                clsUserProductWishlistLink = new clsUserProductWishlistLink();
//            }
//            Session["clsWishlistProducts"] = clsUserProductWishlistLink;
//        }
//        else
//        {
//            //clsUserProductWishlistLink = (clsUserProductWishlistLink)Session["clsWishlistProducts"];
//        }
//    }

//    protected void lnkbtnBack_Click(object sender, EventArgs e)
//    {
//        //### Go back to previous page
//        Response.Redirect("frmWishListProductsView.aspx");
//    }

//    protected void lnkbtnSave_Click(object sender, EventArgs e)
//    {
//        //### Validate registration process
//        bool bCanSave = true;

//        bCanSave = clsValidation.IsNullOrEmpty(txtTitle, bCanSave);
//        bCanSave = clsValidation.IsNullOrEmpty(txtDescription, bCanSave);
//        bCanSave = clsValidation.IsNullOrEmpty(txtPrice, bCanSave);

//        if (bCanSave == true)
//        {
//            mandatoryDiv.Visible = false;
//            lblValidationMessage.Text = "<div class=\"madatoryPaddingDiv\"><img src=\"images/Validation/imgFaceHappy.png\" alt='' title=''/><div class=\"validationMessage\">Product added successfully</div></div>";
//            SaveData();
//        }
//        else
//        {
//            mandatoryDiv.Visible = true;
//            lblValidationMessage.Text = "<div class=\"madatoryPaddingDiv\"><img src=\"images/Validation/imgFaceSad.png\" alt='' title=''/><div class=\"validationMessage\">Please fill out all mandatory fields - Product not added</div></div>";
//        }
//    }

//    protected void lnkbtnClear_Click(object sender, EventArgs e)
//    {
//        mandatoryDiv.Visible = false;

//        txtStockCode.Text = "";
//        clsValidation.SetValid(txtStockCode);
//        txtTitle.Text = "";
//        clsValidation.SetValid(txtTitle);
//        lstSize.SelectedValue = "0";
//        clsValidation.SetValid(lstSize);
//        lstColour.SelectedValue = "0";
//        clsValidation.SetValid(lstColour);
//        txtTagLine.Text = "";
//        clsValidation.SetValid(txtStyle);
//        txtStyle.Text = "";
//        clsValidation.SetValid(txtTagLine);
//        txtDescription.Text = "";
//        clsValidation.SetValid(txtDescription);
//        txtWeight.Text = "";
//        clsValidation.SetValid(txtPrice);
//        txtPrice.Text = "";
//        clsValidation.SetValid(txtWeight);
//        txtContents.Text = "";
//        clsValidation.SetValid(txtContents);
//        txtWarranty.Text = "";
//        clsValidation.SetValid(txtWarranty);
//        txtVideoLink.Text = "";
//        clsValidation.SetValid(txtVideoLink);
//    }

//    #endregion

//    #region POPULATE DATA METHODS

//    //private void popFormData()
//    //{
//    //    txtStockCode.Text = clsUserProductWishlistLink.strStockCode;
//    //    txtTitle.Text = clsWishlistProducts.strTitle;
//    //    txtTagLine.Text = clsWishlistProducts.strTagLine;
//    //    txtDescription.Text = clsWishlistProducts.strDescription.Replace("<br/>", "\n");
//    //    txtPrice.Text = clsWishlistProducts.dblPrice.ToString("N2");

//    //    //### Populates images
//    //    if (!string.IsNullOrEmpty(clsWishlistProducts.strPathToImages))
//    //    {
//    //        lblUniquePath.Text = clsWishlistProducts.strPathToImages;
//    //        getList(clsWishlistProducts.strPathToImages);
//    //        //### Set Current Master Image
//    //        List<string> lstImagesFileNames = (List<string>)Session["lstImagesFileNames"];
//    //        try
//    //        {
//    //            foreach (string strImageFileName in lstImagesFileNames)
//    //            {
//    //                if (strImageFileName == clsWishlistProducts.strMasterImage)
//    //                {
//    //                    RadioButton rdbMasterImage = dlImages.Items[lstImagesFileNames.IndexOf(strImageFileName)].FindControl("rdbMainImage") as RadioButton;
//    //                    rdbMasterImage.Checked = true;
//    //                    break;
//    //                }
//    //            }
//    //        }
//    //        catch { }
//    //    }

      

//    //    //### Populate CheckBox List Selected Values
       
//    //}

//    private void popSize()
//    {
//        DataTable dtSizesList = new DataTable();
//        lstSize.DataSource = clsSizes.GetSizesList("", "strTitle ASC");

//        //### Populates the drop down list with PK and TITLE;
//        lstSize.DataValueField = "iSizeID";
//        lstSize.DataTextField = "strTitle";

//        //### Bind the data to the list;
//        lstSize.DataBind();

//        //### Add default select option;
//        lstSize.Items.Insert(0, new ListItem("--Not Selected--", "0"));
//    }

//    private void popColour()
//    {
//        DataTable dtColourList = new DataTable();
//        lstColour.DataSource = clsColours.GetColoursList("", "strTitle ASC");

//        //### Populates the drop down list with PK and TITLE;
//        lstColour.DataValueField = "iColourID";
//        lstColour.DataTextField = "strTitle";

//        //### Bind the data to the list;
//        lstColour.DataBind();

//        //### Add default select option;
//        lstColour.Items.Insert(0, new ListItem("--Not Selected--", "0"));
//    }

//    private void popColoursChecked(int iProductID)
//    {
//        DataTable dtColoursList = new DataTable();
//        dtColoursList = clsProductColoursLink.GetProductColoursLinkList("iProductID=" + iProductID, "");

//        foreach (DataRow row in dtColoursList.Rows)
//        {
//            foreach (ListItem CheckBoxItem in lstColour.Items)
//            {
//                if (row["iColourID"].ToString() == CheckBoxItem.Value)
//                {
//                    CheckBoxItem.Selected = true;

//                }
//            }
//        }
//    }

//    private void popSizesChecked(int iProductID)
//    {
//        DataTable dtSizesList = new DataTable();
//        dtSizesList = clsProductSizesLink.GetProductSizesLinkList("iProductID=" + iProductID, "");

//        foreach (DataRow row in dtSizesList.Rows)
//        {
//            foreach (ListItem CheckBoxItem in lstSize.Items)
//            {
//                if (row["iSizeID"].ToString() == CheckBoxItem.Value)
//                {
//                    CheckBoxItem.Selected = true;
//                }
//            }
//        }
//    }



//    #endregion

//    #region SAVE DATA METHODS

//    private void SaveData()
//    {
//        //### Add / Update
//        clsWishlistProducts.dtAdded = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
//        clsWishlistProducts.iAddedBy = clsUsers.iUserID;
//        clsWishlistProducts.dtEdited = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
//        clsWishlistProducts.iEditedBy = clsUsers.iUserID;
//        clsWishlistProducts.strStockCode = txtStockCode.Text;
//        clsWishlistProducts.dblPrice = Convert.ToDouble(txtPrice.Text);
//        clsWishlistProducts.strTitle = txtTitle.Text;
//        clsWishlistProducts.strTagLine = txtTagLine.Text;

//        string strDescription = txtDescription.Text;

//        strDescription = strDescription.Replace("\n", "<br/>");

//        clsWishlistProducts.strDescription = strDescription;

//        //### Images related items
//        clsWishlistProducts.strPathToImages = lblUniquePath.Text;
//        clsWishlistProducts.strMasterImage = GetMainImagePath(dlImages);
//        clsWishlistProducts.Update();
//        Session["dtProductsList"] = null;

//        //### Go back to view page
//        Response.Redirect("frmWishListProductsView.aspx");
//    }

//    private void SaveProductColourLink(int iProductID)
//    {
//        clsProductColoursLink.Clear(iProductID);

//        foreach (ListItem item in lstColour.Items)
//        {
//            if (item.Selected)
//            {
//                if (Convert.ToInt32(item.Value) != 0)
//                {
//                    clsProductColoursLink clsProductKeywordsLink = new clsProductColoursLink();
//                    clsProductKeywordsLink.iColourID = Convert.ToInt32(item.Value);

//                    clsProductKeywordsLink.Update();
//                }
//            }
//        }
//    }

//    private void SaveProductSizeLink(int iProductID)
//    {
//        clsProductSizesLink.Clear(iProductID);

//        foreach (ListItem item in lstSize.Items)
//        {
//            if (item.Selected)
//            {
//                if (Convert.ToInt32(item.Value) != 0)
//                {
//                    clsProductSizesLink clsProductKeywordsLink = new clsProductSizesLink();
//                    clsProductKeywordsLink.iSizeID = Convert.ToInt32(item.Value);

//                    clsProductKeywordsLink.Update();
//                }
//            }
//        }
//    }

//    #endregion

//    #region IMAGE METHODS

//    List<string> lstImages;
//    List<string> lstImagesFileNames;
//    int iMaxImages = 11;
//    string strUniqueFullPath = System.Configuration.ConfigurationManager.AppSettings["WebRootFullPath"] + "\\Products";
//    protected void btnUpload_Click(object sender, EventArgs e)
//    {
//        if (Session["lstImages"] == null)
//        {
//            lstImages = new List<string>();
//            Session["lstImages"] = lstImages;
//        }
//        //### Check that they have ONLY HAVE MAX NUMBER OF Images in the datalist
//        if (dlImages.Items.Count == iMaxImages)
//        {
//            mandatoryDiv.Visible = true;
//            lblValidationMessage.Text = "You can only have " + iMaxImages.ToString() + " Images.";
//            getList(lblUniquePath.Text);
//        }
//        else
//        {
//            mandatoryDiv.Visible = false;

//            string strUniquePath;
//            if (lblUniquePath.Text == "")
//            {
//                strUniquePath = GetUniquePath();
//                lblUniquePath.Text = strUniquePath;
//            }
//            else
//            {
//                strUniquePath = lblUniquePath.Text;
//            }
//            UploadImages(strUniquePath);
//            getList(strUniquePath);
//        }
//    }
//    private string GetUniquePath()
//    {
//        int iCount = 1;
//        //### First we need to get the path
//        while (System.IO.Directory.Exists(strUniqueFullPath + "\\Products" + iCount) == true)
//        {
//            iCount++;
//        }
//        return "Products" + iCount;
//    }

//    protected void UploadImages(String strUniquePath)
//    {
//        if (FileUpload.PostedFile.ContentLength > 0 && FileUpload.PostedFile.ContentLength < 1073741824)
//        {

//            //### Upload files to unique folder
//            string strUploadFileName = "";
//            strUploadFileName = System.IO.Path.GetFileName(FileUpload.PostedFile.FileName);
//            string strSaveLocation = "";
//            strSaveLocation = strUniqueFullPath + "\\" + strUniquePath + "\\" + strUploadFileName;

//            if (!System.IO.Directory.Exists(strUniqueFullPath + "\\" + strUniquePath))
//            {
//                System.IO.Directory.CreateDirectory(strUniqueFullPath + "\\" + strUniquePath);
//            }
//            FileUpload.PostedFile.SaveAs(strSaveLocation);

//            CopyAndResizePic(strSaveLocation);
//            getList(strUniqueFullPath + "\\" + strUniquePath);
//        }
//        else
//        {
//            lblValidationMessage.Text = "The file should be between 0 and 1Mb.";
//            lblUploadError.Text = "The file should be between 0 and 1Mb.";
//        }
//    }

//    private void CopyAndResizePic(String strFullPath)
//    {
//        try
//        {

//            String strFileName;
//            String strNewFilePath;

//            //### Main Images
//            String strLrgFileName;

//            strFileName = Path.GetFileName(strFullPath);
//            strNewFilePath = strFullPath.Replace(strFileName, "");
//            strNewFilePath = strNewFilePath + Path.GetFileNameWithoutExtension(strFullPath);
//            strNewFilePath = strNewFilePath + "_lrg";
//            strNewFilePath = strNewFilePath + Path.GetExtension(strFullPath);

//            File.Copy(strFullPath, strNewFilePath);
//            strLrgFileName = Path.GetFileName(strNewFilePath);

//            clsCommonFunctions.ResizeImage(strNewFilePath, strNewFilePath, 600, 600, false);

//            //### Thumbnail
//            String strSmlFileName;

//            strFileName = Path.GetFileName(strFullPath);
//            strNewFilePath = strFullPath.Replace(strFileName, "");
//            strNewFilePath = strNewFilePath + Path.GetFileNameWithoutExtension(strFullPath);
//            strNewFilePath = strNewFilePath + "_sml";
//            strNewFilePath = strNewFilePath + Path.GetExtension(strFullPath);

//            File.Copy(strFullPath, strNewFilePath);
//            strSmlFileName = Path.GetFileName(strNewFilePath);

//            clsCommonFunctions.ResizeImage(strNewFilePath, strNewFilePath, 140, 140, false);

//        }
//        catch (Exception ex) { }
//    }

//    public void getList(String strPathToFolder)
//    {
//        lstImages = new List<string>();
//        lstImagesFileNames = new List<string>();
//        try
//        {
//            string strPath = strPathToFolder;
//            string[] files = Directory.GetFiles(strUniqueFullPath + "\\" + strPath);

//            string iProductID = "";
//            if (!string.IsNullOrEmpty(Request.QueryString["iProductID"]))
//                iProductID = Request.QueryString["iProductID"];

//            int iImagesCount = 0;

//            foreach (string strName in files)
//            {
//                if (strName.IndexOf("_sml") != -1)
//                {
//                    string strHTMLImages = strName.Replace(System.Configuration.ConfigurationManager.AppSettings["WebRootFullPath"] + "\\", "..\\");
//                    strHTMLImages = strHTMLImages.Replace("\\", "/");

//                    //### Generates a javascript postback for the delete method
//                    String strPostBack = Page.ClientScript.GetPostBackEventReference(this, "iRemoveImages:" + iImagesCount);

//                    lstImages.Add(@"<tr style='text-align: centre;'>
//                                        <td style='text-align: centre;'>
//                                            <a class='ImagesColorBox' href='" + strHTMLImages.Replace("_sml", "_lrg") + "'><img src='" + strHTMLImages + @"' alt='' title='' style='border: solid 5px #ffffff;' /></a><br /><br />" +
//                                            "<center><div class='buttonStyle' style='padding-top: 5px;'><a href=\"javascript:" + strPostBack + "\" style='color: #ffffff;'><div class='deleteButton'></div></a></div></center>" + @"
//                                        </td>
//                                    </tr>");
//                    lstImagesFileNames.Add(Path.GetFileName(strName).Replace("_sml", ""));
//                    iImagesCount++;
//                }
//            }
//            dlImages.DataSource = lstImages;
//            dlImages.DataBind();

//            Session["lstImages"] = lstImages;
//            Session["lstImagesFileNames"] = lstImagesFileNames;
//        }
//        catch (Exception ex) { }
//    }

//    private string GetMainImagePath(DataList dtlTarget)
//    {
//        string strReturn = "";

//        foreach (DataListItem dliTarget in dtlTarget.Items)
//        {
//            RadioButton rdbMainImage = (RadioButton)dliTarget.FindControl("rdbMainImage");
//            if (rdbMainImage.Checked)
//            {
//                lstImagesFileNames = (List<string>)Session["lstImagesFileNames"];
//                strReturn = lstImagesFileNames[dliTarget.ItemIndex];
//                break;
//            }
//        }
//        return strReturn;
//    }

//    private void DeleteImages(int iImagesIndex)
//    {

//        //### Deletes all Images related to the target Images.
//        lstImages = (List<string>)Session["lstImages"];
//        lstImagesFileNames = (List<string>)Session["lstImagesFileNames"];

//        lstImages.RemoveAt(iImagesIndex);
//        Session["lstImages"] = lstImages;

//        string[] files = Directory.GetFiles(strUniqueFullPath + "\\" + lblUniquePath.Text);
//        foreach (string file in files)
//        {
//            if (Path.GetFileName(file) == lstImagesFileNames[iImagesIndex].ToString())
//            {
//                //### Remove all Images
//                File.Delete(file);
//                File.Delete(file.Replace(Path.GetExtension(file), "_sml" + Path.GetExtension(file)));
//                File.Delete(file.Replace(Path.GetExtension(file), "_med" + Path.GetExtension(file)));
//                File.Delete(file.Replace(Path.GetExtension(file), "_lrg" + Path.GetExtension(file)));
//                break;
//            }
//        }
//        lstImagesFileNames.RemoveAt(iImagesIndex);
//        ViewState["lstImagesFileNames"] = lstImagesFileNames;
//        getList(lblUniquePath.Text);
//    }

//    #endregion
}