﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Products_Details : System.Web.UI.Page
{
    clsUsers clsUsers;
    clsProducts clsProducts;
    List<clsProducts> glstProducts;

    #region EVENT METHODS

    protected void Page_Load(object sender, EventArgs e)
    {
        //### If the iProductID is passed through then we want to instantiate the object with that iProductID
        if ((Request.QueryString["iProductID"] != "") && (Request.QueryString["iProductID"] != null))
        {
            clsProducts = new clsProducts(Convert.ToInt32(Request.QueryString["iProductID"]));

            //### Populate the form
            popProductDetails(clsProducts.iProductID);
        }
        else
        {
            clsProducts = new clsProducts();
        }
        Session["clsProducts"] = clsProducts;
    }

    protected void btnBuyNow_Click(object sender, EventArgs e)
    {
        //clsCart.addCartItemCookie(clsProducts.iProductID, 1);

        //int iProductID = Convert.ToInt32(((Button)sender).CommandArgument);

        //clsCart.addToCart(clsProducts.iProductID, Int32.Parse(txtQuantity.Text));

        Response.Redirect("Conscious-Birth-Cart.aspx");
    }

    #endregion

    #region POPULATE METHODS

    private void popProductDetails(int iProductID)
   {
        //int iSizeID =  clsProducts.iSizeID;
        //clsSizes clsSizes = new clsSizes(iSizeID);
        //string strSizeTitle = clsSizes.strTitle;
        string strFullPathToImage = "";
       
        StringBuilder sbProductDetails = new StringBuilder();
       
        sbProductDetails.AppendLine("<div class='span6 module_cont module_text_area module_none_padding'>");
        sbProductDetails.AppendLine("<ul class='add-to-links' id='etalage'>");

            if (!(clsProducts.strPathToImages.ToString() == "") || (clsProducts.strPathToImages == null))
            {
                sbProductDetails.AppendLine(getList(clsProducts.strPathToImages));
            }
            if (!(clsProducts.strPathToImages.ToString() == "") || (clsProducts.strPathToImages == null))
            {
                strFullPathToImage = "Products/" + clsProducts.strPathToImages + "/" + clsProducts.strMasterImage;
            }
            else
            {
                strFullPathToImage = "img/no-image.png";
            }
                sbProductDetails.AppendLine("<li>");
                //sbProductDetails.AppendLine("<img class='etalage_thumb_image' src='" + strFullPathToImage + "' alt='' style='border:1px solid #eee' /><img class='etalage_source_image' src='" + strFullPathToImage + "' alt='' style='border:1px solid #eee' /><a href='#'></a>");
                sbProductDetails.AppendLine("<img class='etalage_source_image' src='" + strFullPathToImage + "' alt='' style='border:1px solid #eee' /><a href='#'></a>");
                sbProductDetails.AppendLine("</li>");
            sbProductDetails.AppendLine("</ul>");
        sbProductDetails.AppendLine(" </div>");


        sbProductDetails.AppendLine("<div class='span6 module_cont module_text_area module_none_padding'>");
        sbProductDetails.AppendLine("<h3 class='m_3'>" + clsProducts.strTitle + "</h3>");
        sbProductDetails.AppendLine("<div class='social_single'>");
        sbProductDetails.AppendLine("<ul>");
        sbProductDetails.AppendLine("Colours<br/>");
        sbProductDetails.AppendLine(popColours(iProductID));
        sbProductDetails.AppendLine("</ul>");
        sbProductDetails.AppendLine("</div>");
        //sbProductDetails.AppendLine(" Size: " + strSizeTitle.ToString() + "<br/><br/>");
        sbProductDetails.AppendLine(" Style: " + clsProducts.strStyle.ToString());
                sbProductDetails.AppendLine("<div class='price_single'>");
                sbProductDetails.AppendLine("<br/><span class='actual'> R " + clsProducts.dblPrice.ToString("N2") + "</span><a href='#'></a>");
                sbProductDetails.AppendLine("</div>");
                sbProductDetails.AppendLine("Description<br/>");
            sbProductDetails.AppendLine("<p class='m_desc'>"+clsProducts.strDescription+"</p>");
    // BUTTON CODE WAS HERE  
        sbProductDetails.AppendLine("</div>");

        if ((clsProducts.strVideoLink != null) && (clsProducts.strVideoLink != ""))
        {
                sbProductDetails.AppendLine("<div class='cont1 span_2_of_a1'>");
                sbProductDetails.AppendLine("<br class='clr'/>");
                sbProductDetails.AppendLine("<br class='clr'/>");
                    sbProductDetails.AppendLine("<div><iframe width='100%' height='500' src='" + clsProducts.strVideoLink + "' frameborder='0' allowfullscreen></iframe></div>");
            sbProductDetails.AppendLine(" </div>");
        }
     
        litProducts.Text = sbProductDetails.ToString();
    }

    private string popColours(int iProductID)
    {
        StringBuilder sbProductColoursLink = new StringBuilder();
        int iColourID;
        DataTable dtProductColoursLink;
        dtProductColoursLink = clsProductColoursLink.GetProductColoursLinkList("iProductID=" + iProductID, "");

        foreach (DataRow dtrProductColourLink in dtProductColoursLink.Rows)
        {
            iColourID = Convert.ToInt32(dtrProductColourLink["iColourID"]);

            if (iColourID != 0)
            {
            clsColours clsColours = new clsColours(iColourID);
            sbProductColoursLink.AppendLine("<li style='height: 30px; width: 30px; border: 1px solid #333; list-style:none; background:" + clsColours.strColourCode + ";'></li>");
            }
        }

        return sbProductColoursLink.ToString();
    }

    private string popImages(int iProductID)
    {
        getList(clsProducts.strPathToImages);
        StringBuilder sbImage = new StringBuilder();

        DataTable dtImages = clsProducts.GetProductsList("iProductID="+iProductID,"");
        foreach(DataRow dtrImages in dtImages.Rows)
        {
            string strFullPathToImage = "Products/" + dtrImages["strPathToImages"] + "/" + dtrImages["strMasterImage"];
            sbImage.AppendLine("<li style='margin-right:10px !important'>");
            sbImage.AppendLine("<a href''><img src='" + strFullPathToImage + "' alt=''/></a>");
            sbImage.AppendLine("</li>");
        
        }

        return sbImage.ToString();
    }

    string strUniqueFullPath = System.Configuration.ConfigurationManager.AppSettings["WebRootFullPath"] + "\\Products";

    public string getList(String strPathToFolder)
    {

        StringBuilder sbImage = new StringBuilder();
        try
        {
            string strPath = strPathToFolder;
            string[] files = Directory.GetFiles(strUniqueFullPath + "\\" + strPath);

            string iProductID = "";
            if (!string.IsNullOrEmpty(Request.QueryString["iProductID"]))
                iProductID = Request.QueryString["iProductID"];

            int iImagesCount = 0;

            foreach (string strName in files)
            {
                if (strName.IndexOf("_lrg") != -1)
                {
                    string strHTMLImages = strName.Replace(System.Configuration.ConfigurationManager.AppSettings["WebRootFullPath"] + "\\", "..\\");
                    strHTMLImages = strHTMLImages.Replace("\\", "/");

                    //### Generates a javascript postback for the delete method
                    String strPostBack = Page.ClientScript.GetPostBackEventReference(this, "iRemoveImages:" + iImagesCount);

                    
                    sbImage.AppendLine("<li style=''>");
                    sbImage.AppendLine("<img class='etalage_thumb_image' src='" + strHTMLImages.Replace("_lrg", "") + "' alt='' style='border:1px solid #eee; margin-right:10px !important; list-style:none;' /><img class='etalage_source_image' src='" + strHTMLImages.Replace("_lrg", "") + "' alt='' style='border:1px solid #eee; margin-right:10px !important; list-style:none; ' />");
                    sbImage.AppendLine("</li>");
                    //lstImagesFileNames.Add(Path.GetFileName(strName).Replace("_sml", ""));
                    iImagesCount++;
                   
                }
            }
            
        }
        catch (Exception ex) { }

        return sbImage.ToString();
    }

    #endregion
}