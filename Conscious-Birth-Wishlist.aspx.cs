﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


public partial class Conscious_Birth_UserWishlistView : System.Web.UI.Page
{

    #region EVENT METHODS
    bool validate = false;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Request.QueryString["action"] == "AddToCart")
            {
                clsCart.addToCart(Convert.ToInt32(Request.QueryString["iProductID"]), Convert.ToInt32(Request.QueryString["iQuantity"]), Request.QueryString["strColour"], Request.QueryString["strSize"]);
                Response.Redirect("Conscious-Birth-Cart.aspx");
            }
        }
        HideData.Visible = false;
        validate = false;
        pMessage.Visible = false;

    }
    protected void btnSurcheUser_Click(object sender, EventArgs e)
    {
        DataTable dtAccountUsers = clsAccountUsers.GetAccountUsersList("strEmailAddress='" + UserIdTexbox.Text + "'", "");
        int iAccountUserID = 0;
        string strFirstName = "";
        foreach (DataRow dtrAccountUsers in dtAccountUsers.Rows)
        {
            iAccountUserID = Convert.ToInt32(dtrAccountUsers["iAccountUserID"]);
            strFirstName = Convert.ToString(dtrAccountUsers["strFirstName"]);
        }
        if (iAccountUserID == 0)
        {
            Labeleror.Text = "<p style='margin-top: 49px;'>The Email Adrees does is not valid  </p>";
            validate = true;
        }
        else
        {
            pMessage.Visible = true;
        }
        UserName.Text = strFirstName;
        PopulateWishlist(iAccountUserID);
    }
    private void PopulateWishlist(int iWishlistID)
    {
        StringBuilder sbGrid = new StringBuilder();

        DataTable dtUserWishlistLink = clsUserProductWishlistLink.GetUserProductWishlistLinkList("iUserID='" + iWishlistID + "'", "");
        int iProductID = 0;
        foreach (DataRow dtrUserWishlistLink in dtUserWishlistLink.Rows)
        {
            iProductID = Convert.ToInt32(dtrUserWishlistLink["iProductID"]);
            clsProducts clsProducts = new clsProducts(iProductID);
            double Total = clsProducts.dblPrice * Convert.ToInt32(dtrUserWishlistLink["iQuantity"]);
            sbGrid.Append("<tr>");
            sbGrid.Append("<td style='text-align:center'>");
            sbGrid.Append("<img src= '" + getProductImage(Convert.ToInt32(iProductID)) + "' style='border: none; height: 120px; width: 120px; padding-top: 5px;' />");
            sbGrid.Append("</td>");
            sbGrid.Append("<td style='text-align:center'>");
            sbGrid.Append("<div style='padding-left:10px; padding-right: 10px;'>");
            sbGrid.Append("<span style='font-size: 18px;'>" + clsProducts.strTitle + "</span></div>");
            sbGrid.Append("<br/>");
            sbGrid.Append("</td>");
            sbGrid.Append("<td style='text-align:center'>" + dtrUserWishlistLink["iQuantity"] + "</td>");
            sbGrid.Append("<td style='text-align:center'>" + dtrUserWishlistLink["strColor"] + "</td>");
            sbGrid.Append("<td style='text-align:center'>" + dtrUserWishlistLink["strSize"] + "</td>");
            sbGrid.Append("<td style='text-align:center'>" + Total + "</td>");
            sbGrid.Append("<td style='text-align:center'>");
            sbGrid.Append(" <a href='Conscious-Birth-Wishlist.aspx?action=AddToCart&iProductID=" + clsProducts.iProductID + "&iQuantity=" + dtrUserWishlistLink["iQuantity"] + "&strColour=" + dtrUserWishlistLink["strColor"] + "&strSize=" + dtrUserWishlistLink["strSize"] + "' onclick='return ConfirmRecordDeleteCart()' class='dgrLinkAdd'></a>");
            sbGrid.Append("</td>");
            sbGrid.Append("</tr>");
        }
        if (iProductID != 0)
        {
            HideData.Visible = true;
            litGridData.Text = sbGrid.ToString();
        }
        else
        {
            if(validate == false)
            {
                Labeleror.Text = " <p style='margin-top: 49px;'> Wish list cart is currently empty.";
            }
            
        }

    }
    protected string getProductImage(int iProductID)
    {
        clsProducts clsProducts = new clsProducts(Convert.ToInt32(iProductID));

        string strHTMLImageProduct = "";

        try
        {
            string[] strImagesProduct = Directory.GetFiles(ConfigurationManager.AppSettings["WebRootFullPath"] + "\\Products\\" + clsProducts.strPathToImages);

            foreach (string strImageProduct in strImagesProduct)
            {
                if (strImageProduct.Contains("_sml"))
                {
                    strHTMLImageProduct = strImageProduct.Replace(ConfigurationManager.AppSettings["WebRootFullPath"], "").Replace("\\", "/").Substring(1);
                    break;
                }
            }
        }
        catch
        {
        }

        return strHTMLImageProduct;
    }

    protected void linkAddToCart_Click(object sender, EventArgs e)
    {
        int iProductID = Convert.ToInt32(((LinkButton)sender).CommandArgument);
        clsUserProductWishlistLink clsUserProductWishlistLink = new clsUserProductWishlistLink();
        clsUserProductWishlistLink.GetUserProductWishlistLinkList("iProductID='" + iProductID + "'", "");

        clsCart.addToCart(clsUserProductWishlistLink.iProductID, clsUserProductWishlistLink.iQuantity, clsUserProductWishlistLink.strColor, clsUserProductWishlistLink.strSize);
        Response.Redirect("Conscious-Birth-Cart.aspx");
    }
    #endregion
}












