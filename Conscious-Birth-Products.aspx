﻿<%@ Page Title="Products :: Conscious Birth" Language="C#" MasterPageFile="~/ConsciousBirth.master" AutoEventWireup="true" CodeFile="Conscious-Birth-Products.aspx.cs" Inherits="Products" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <title>Conscious Birth - Products</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
    <link href="css/theme.css" rel="stylesheet" />

    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800' rel='stylesheet' type='text/css' />
    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $(".dropdown img.flag").addClass("flagvisibility");

            $(".dropdown dt a").click(function () {
                $(".dropdown dd ul").toggle();
            });

            $(".dropdown dd ul li a").click(function () {
                var text = $(this).html();
                $(".dropdown dt a span").html(text);
                $(".dropdown dd ul").hide();
                $("#result").html("Selected value is: " + getSelectedValue("sample"));
            });

            function getSelectedValue(id) {
                return $("#" + id).find("dt a span.value").html();
            }

            $(document).bind('click', function (e) {
                var $clicked = $(e.target);
                if (!$clicked.parents().hasClass("dropdown"))
                    $(".dropdown dd ul").hide();
            });


            $("#flagSwitcher").click(function () {
                $(".dropdown img.flag").toggleClass("flagvisibility");
            });
        });
    </script>
    <!-- top scrolling -->
    <script type="text/javascript" src="js/move-top.js"></script>
    <%--<script type="text/javascript" src="js/easing.js"></script>--%>
    <script type="text/javascript">
        jQuery(document).ready(function ($) {
            $(".scroll").click(function (event) {
                event.preventDefault();
                $('html,body').animate({ scrollTop: $(this.hash).offset().top }, 1200);
            });
        });
    </script>
    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date(); a = s.createElement(o),
            m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-53160457-1', 'auto');
        ga('send', 'pageview');

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="main_wrapper">
        <div class="content_wrapper">
            <div class="page_title_block">
                <div class="container">
                    <h2 class="title">Our Products</h2>
                </div>
            </div>
        </div>
        <%--<div class="header-bottom">
                <div class="wrap">
                    <!-- start header menu -->
                    <div class="clear"></div>
                </div>
            </div>--%>
        <div class="main">
            <div class="wrap">
                <div class="content-top">
                    <div class="content-bottom">
                        <div class="box1">
                            <asp:Repeater ID="rpProducts" runat="server">
                                <ItemTemplate>
                                    <div class="<%#Eval ("classForColumn") %>" >
                                        <a href="#">
                                            <div class="view view-fifth">
                                                <div class="top_box">
                                                    <h3 class="m_1"><%#Eval ("strTitle") %></h3>
                                                    <p class="m_2"><%#Eval ("strTagLine") %></p>
                                                    <div class="grid_img">
                                                        <div class="css3">

                                                            <img src='<%#Eval ("FullPathForImage") %>' width="315" height="312" alt="<%#Eval ("strTitle") %>" title="<%#Eval ("strTitle") %>" />
                                                        </div>
                                                        <div class="mask">
                                                            <a class="info" href="Conscious-Birth-Product-Info.aspx?iProductID=<%#Eval("iProductID")%>">Quick View</a>
                                                        </div>
                                                    </div>
                                                    <div class="price">R <%#Eval("dblPrice","{0:#,#.00}")%></div>
                                                </div>
                                            </div>
                                            <ul class="blackButton">
                                                <li>
                                                    <asp:LinkButton ID="btnAddToCart" runat="server" CssClass="active-icon c1" CommandArgument='<%#Eval("iProductID")%>' OnClick="btnAddToCart_Click">ADD TO CART</asp:LinkButton>
                                                </li>
                                            </ul>
                                             <ul class="PienkButton">
                                                <li>
                                                    <asp:LinkButton ID="btnAddToWishList" runat="server" CssClass="active-icon c1" CommandArgument='<%#Eval("iProductID")%>' OnClick="btnAddToWishList_Click">ADD WISHLIST</asp:LinkButton>
                                                </li>
                                            </ul>
                                        </a>
                                    </div>
                                    <%#Eval ("divReset") %>
                                </ItemTemplate>
                            </asp:Repeater>
                            <div class="clear"></div>
                        </div>
                    </div>
                </div>
            </div>
            <a href="#" id="toTop" style="display: block;"><span id="toTopHover" style="opacity: 1;"></span></a>
        </div>
    </div>
</asp:Content>

