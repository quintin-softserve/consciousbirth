﻿<%@ Page Title="FAQ :: Conscious Birth" Language="C#" MasterPageFile="~/ConsciousBirth.master" AutoEventWireup="true" CodeFile="Conscious-Birth-FAQ.aspx.cs" Inherits="Conscious_Birth_FAQ" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
     <script type="text/javascript" src="js/jquery.min.js"></script>
    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date(); a = s.createElement(o),.blog_head .blogpost_title
            m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-53160457-1', 'auto');
        ga('send', 'pageview');

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
     <div class="main_wrapper">
        <!-- C O N T E N T -->
        <div class="content_wrapper">
        	<div class="page_title_block">
            	<div class="container">
                    <h2 class="title">FAQ</h2>
                </div>
            </div>        
            <div class="container">
                <div class="content_block no-sidebar row">
                    <div class="fl-container span12">
                        <div class="row">
                            <div class="posts-block span12">
                                <div class="contentarea">
                                     <div class="row-fluid">
                                        <div class="span12 module_cont module_toggle module_none_padding">
                                        	<div class="bg_title"><h4 class="headInModule">Category 1</h4></div>
                                            <div class="shortcode_toggles_shortcode toggles">
                                                <h5 class="shortcode_toggles_item_title expanded_no" data-count="1">
                                                    Consequat viverra ipsum, sed placerat ligula aliquam eu?
                                                    <span class="ico"></span>
                                                </h5>
                                                <div class="shortcode_toggles_item_body">
                                                    <div class="ip">
														Fusce consequat, nunc sit amet lobortis tempor, est lorem suscipit urna, egestas volutpat diam dolor tincidunt odio. 
                                                        Integer pellentesque dictum lorem, id porttitor odio malesuada eu. Donec vel placerat diam. Ut lorem nisi, condimentum sit amet aliquam ac, 
                                                        rutrum non eros. In ultrices, magna sed ndum pellentesque, neque libero porttitor justo, vitae consequat dolor nibh vel ipsum. 
                                                        Praesent gravida posuere velit, et ornare velit rhoncus at curabitur eget magna libero, condimentum id dapibus sit amet, tincidunt id sem. 
                                                        Phasellus nec libero sed lectus ullamcorper porttitor ut tempus leo. Nulla dignissim vestibulum felis sed lacinia. 
                                                        In pretium felis eu nulla commodo eleifend. Cum sociis natoque penatibus et magnis dis parturient montes. 
                                                    </div>
                                                </div>
                                                <h5 class="shortcode_toggles_item_title expanded_yes" data-count="2">
                                                    Curabitur suscipit hendrerit egets porttitor. Aliquam nusl icien ullamcorper erat eget tempor dolorem pellentesque?
                                                    <span class="ico"></span>
                                                </h5>
                                                <div class="shortcode_toggles_item_body">
                                                    <div class="ip">
														Fusce consequat, nunc sit amet lobortis tempor, est lorem suscipit urna, egestas volutpat diam dolor tincidunt odio. 
                                                        Integer pellentesque dictum lorem, id porttitor odio malesuada eu. Donec vel placerat diam. Ut lorem nisi, condimentum sit amet aliquam ac, 
                                                        rutrum non eros. In ultrices, magna sed ndum pellentesque, neque libero porttitor justo, vitae consequat dolor nibh vel ipsum. 
                                                        Praesent gravida posuere velit, et ornare velit rhoncus at curabitur eget magna libero, condimentum id dapibus sit amet, tincidunt id sem. 
                                                        Phasellus nec libero sed lectus ullamcorper porttitor ut tempus leo. Nulla dignissim vestibulum felis sed lacinia. 
                                                        In pretium felis eu nulla commodo eleifend. Cum sociis natoque penatibus et magnis dis parturient montes. 
                                                    </div>
                                                </div>
                                                <h5 class="shortcode_toggles_item_title expanded_no" data-count="3">
                                                    posuere cubilia curae nulla vestibulum eget ?
                                                    <span class="ico"></span>
                                                </h5>
                                                <div class="shortcode_toggles_item_body">
                                                    <div class="ip">
														Fusce consequat, nunc sit amet lobortis tempor, est lorem suscipit urna, egestas volutpat diam dolor tincidunt odio. 
                                                        Integer pellentesque dictum lorem, id porttitor odio malesuada eu. Donec vel placerat diam. Ut lorem nisi, condimentum sit amet aliquam ac, 
                                                        rutrum non eros. In ultrices, magna sed ndum pellentesque, neque libero porttitor justo, vitae consequat dolor nibh vel ipsum. 
                                                        Praesent gravida posuere velit, et ornare velit rhoncus at curabitur eget magna libero, condimentum id dapibus sit amet, tincidunt id sem. 
                                                        Phasellus nec libero sed lectus ullamcorper porttitor ut tempus leo. Nulla dignissim vestibulum felis sed lacinia. 
                                                        In pretium felis eu nulla commodo eleifend. Cum sociis natoque penatibus et magnis dis parturient montes. 
                                                    </div>
                                                </div>
                                                <h5 class="shortcode_toggles_item_title expanded_no" data-count="3">
                                                    egets sed facilisis magna eros, id dictum arcu. Curabitur suscipit hendrerit?
                                                    <span class="ico"></span>
                                                </h5>
                                                <div class="shortcode_toggles_item_body">
                                                    <div class="ip">
														Fusce consequat, nunc sit amet lobortis tempor, est lorem suscipit urna, egestas volutpat diam dolor tincidunt odio. 
                                                        Integer pellentesque dictum lorem, id porttitor odio malesuada eu. Donec vel placerat diam. Ut lorem nisi, condimentum sit amet aliquam ac, 
                                                        rutrum non eros. In ultrices, magna sed ndum pellentesque, neque libero porttitor justo, vitae consequat dolor nibh vel ipsum. 
                                                        Praesent gravida posuere velit, et ornare velit rhoncus at curabitur eget magna libero, condimentum id dapibus sit amet, tincidunt id sem. 
                                                        Phasellus nec libero sed lectus ullamcorper porttitor ut tempus leo. Nulla dignissim vestibulum felis sed lacinia. 
                                                        In pretium felis eu nulla commodo eleifend. Cum sociis natoque penatibus et magnis dis parturient montes. 
                                                    </div>
                                                </div>
                                            </div><!--.shortcode_toggles_shortcode-->
                                        </div><!-- .module_cont -->
                                    </div><!-- .row-fluid -->	
                                      </div><!-- .row-fluid -->    
                                     <div class="module_line_trigger" data-background="#ffeec9 url(img/VV.png) no-repeat center" data-top-padding="bottom_padding_huge" data-bottom-padding="module_big_padding">
                                        <div class="row-fluid">                                
                                            <div class="span12 module_cont module_promo_text module_huge_padding">
                                                <div class="shortcode_promoblock">
                                                    <div class="promo_button_block type2">
                                                        <a href="Conscious-Birth-Products.aspx" class="promo_button"> View our Products </a>
                                                    </div>
                                                </div>
                                            </div><!--.module_cont -->
                                            <div class="clear"><!-- ClearFIX --></div>                                                                    
                                        </div><!-- .row-fluid -->
                                    </div><!-- .module_line_trigger -->
                                     <!--<div class="row-fluid">
                                        <div class="span12 module_cont module_partners center_title module_big_padding2">
											<div class="bg_title"><div class="title_wrapper"><a href="javascript:void(0)" class="btn_carousel_left"></a><h5 class="headInModule">Our Partners</h5><a href="javascript:void(0)" class="btn_carousel_right"></a></div></div>
                                            <div class="module_content sponsors_works carouselslider items5" data-count="5">
                                                <ul>
                                                    <li>
                                                        <div class="item">
                                                            <a href="#charity-html5css3-website-template/3180454" target="_blank"><img src="img/pictures/partners2.png" alt=""></a>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="item">
                                                            <a href="#point-business-responsive-wp-theme/4319087" target="_blank"><img src="img/pictures/partners4.png" alt=""></a>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="item">
                                                            <a href="#cleanspace-retina-ready-business-wp-theme/3776000" target="_blank"><img src="img/pictures/partners1.png" alt=""></a>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="item">
                                                            <a href="#yellowproject-multipurpose-retina-wp-theme/4066662" target="_blank"><img src="img/pictures/partners3.png" alt=""></a>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="item">
                                                            <a href="#showroom-portfolio-retina-ready-wp-theme/3473628" target="_blank"><img src="img/pictures/partners5.png" alt=""></a>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="item">
                                                            <a href="themeforest.net/item/incipiens-responsive-portfolio-wordpress-theme/2762691" target="_blank"><img src="img/pictures/partners6.png" alt=""></a>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="item">
                                                            <a href="themeforest.net/item/hq-photography-responsive-wp-theme/3200962" target="_blank"><img src="img/pictures/partners9.png" alt=""></a>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="item">
                                                            <a href="#incipiens-responsive-portfolio-wordpress-theme/2762691" target="_blank"><img src="img/pictures/partners11.png" alt=""></a>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div><!-- .row-fluid -->
                                	
                                </div><!-- .contentarea -->
                            </div>
                            <div class="left-sidebar-block span3">
                                <aside class="sidebar">
                                    //Sidebar Text
                                </aside>
                            </div><!-- .left-sidebar -->
                        </div>
                        <div class="clear"><!-- ClearFix --></div>
                    </div><!-- .fl-container -->
                    <div class="right-sidebar-block span3">
                        <aside class="sidebar">

                        </aside>
                    </div><!-- .right-sidebar -->
                    <div class="clear"><!-- ClearFix --></div>
                </div>
            </div><!-- .container -->
        </div><!-- .content_wrapper -->
	
    </div><!-- .main_wrapper -->
</asp:Content>