﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class CMS_frmAccountUsersAddModify : System.Web.UI.Page
{
    clsUsers clsUsers;
    clsAccountUsers clsAccountUsers;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["clsUsers"] == null)
        {
            //### Redirect back to login
            Response.Redirect("../CMSLogin.aspx");
        }
        clsUsers = (clsUsers)Session["clsUsers"];


        if (!IsPostBack)
        {
            //### If the iUserID is passed through then we want to instantiate the obect with that iUserID
            if (Request.QueryString["iAccountUserID"] != "" && Request.QueryString["iAccountUserID"] != null)
            {
                clsAccountUsers = new clsAccountUsers(Convert.ToInt32(Request.QueryString["iAccountUserID"]));

                //### Populate the form
                popFormData();

                popValidFields();

                //### Hide password rows if editing
                if (Request.QueryString["action"] == "edit")
                {
                    divPassword.Attributes.Add("style", "display:none");
                    divConfirmPassword.Attributes.Add("style", "display:none");
                }
            }
            else
            {
                clsAccountUsers = new clsAccountUsers();
            }

            Session["clsAccountUsers"] = clsAccountUsers;
        }
        else
        {
            clsAccountUsers = (clsAccountUsers)Session["clsAccountUsers"];
        }
    }

    protected void lnkbtnBack_Click(object sender, EventArgs e)
    {
        Response.Redirect("frmAccountUserView.aspx");
    }
    protected void lnkbtnSave_Click(object sender, EventArgs e)
    {
        bool bCanSave = true;

        bCanSave = clsValidation.IsNullOrEmpty(txtName);
        bCanSave = clsValidation.IsNullOrEmpty(txtSurname);
        bCanSave = clsValidation.IsNullOrEmpty(txtEmail);

        //### Hide password rows if editing
        if (Request.QueryString["action"] != "edit")
        {
            bCanSave = clsValidation.IsNullOrEmpty(txtPassword);
            bCanSave = clsValidation.IsNullOrEmpty(txtConfirmPassword);
        }

        if ((bCanSave == true) && ((Convert.ToBoolean(hfCanSave.Value)) == true))
        {
            mandatoryDiv.Visible = false;
            lblValidationMessage.Text = "<div class=\"madatoryPaddingDiv\"><img src=\"images/Validation/imgFaceHappy.png\" alt=\"\" title=\"\"/><div class=\"validationMessage\">USER ADDED SUCCESSFULLY</div></div>";
            SaveData();
        }

        else
        {
            mandatoryDiv.Visible = true;
            lblValidationMessage.Text = "<div class=\"madatoryPaddingDiv\"><img src=\"images/Validation/imgFaceSad.png\" alt=\"\" title=\"\"/><div class=\"validationMessage\">Please fill out all mandatory fields - USER NOT ADDED</div></div>";
            popValidFields();
        }
        
}
    
    protected void lnkbtnClear_Click(object sender, EventArgs e)
    {
        mandatoryDiv.Visible = false;

        txtName.Text = "";

        clsValidation.SetValid(txtName);

        txtSurname.Text = "";
        clsValidation.SetValid(txtSurname);

        txtEmail.Text = "";
        clsValidation.SetValid(txtEmail);

        txtPassword.Text = "";
        clsValidation.SetValid(txtPassword);

        txtConfirmPassword.Text = "";
        clsValidation.SetValid(txtConfirmPassword);
     
        txtPhonum.Text = "";
        clsValidation.SetValid(txtPhonum);



    }
    

    #region POPULATE DATA METHODS

    private void popFormData()
    {
        txtName.Text = clsAccountUsers.strFirstName;
        txtSurname.Text = clsAccountUsers.strSurname;
        txtEmail.Text = clsAccountUsers.strEmailAddress;
        txtPhonum.Text = clsAccountUsers.strPhoneNumber;
        hfCanSave.Value = "true";
    }

    private void popValidFields()
    {
        //### Make the relevant mandatory fields green
        string strScript = "";

        if (!String.IsNullOrEmpty(txtName.Text))
            {
                strScript += "setValidFile('" + txtName.ClientID + "',true);";
            }
        if (!String.IsNullOrEmpty(txtSurname.Text))
            {
                strScript += "setValidFile('" + txtSurname.ClientID + "',true);";
            }
        if (!String.IsNullOrEmpty(txtEmail.Text))
            {
                strScript += "setValidFile('" + txtEmail.ClientID + "',true);";
            }
       
        if (!String.IsNullOrEmpty(txtPhonum.Text))
        {
            strScript += "setValidFile('" + txtPhonum.ClientID + "',true);";
        }
        
        ScriptManager.RegisterStartupScript(this, this.GetType(), "mandatoryFields", strScript + strScript, true);
    }
    

    #endregion

    #region SAVE DATA METHODS

    private void SaveData()
    {
        //### Only update the password if there is a value
        if (txtPassword.Text != "")
        {   
            //### Hash the password
            string strHashPassword = clsCommonFunctions.GetMd5Sum(txtPassword.Text);
            clsAccountUsers.strPassword = strHashPassword;

        }

        //### Add / Update
        clsAccountUsers.dtAdded = Convert.ToDateTime(DateTime.Now.ToString("dd MMM yyyy"));
        clsAccountUsers.iAddedBy = 0;
        clsAccountUsers.dtEdited = Convert.ToDateTime(DateTime.Now.ToString("dd MMM yyyy"));
        clsAccountUsers.iEditedBy = 0;
        clsAccountUsers.strFirstName = txtName.Text;
        clsAccountUsers.strSurname = txtSurname.Text;
        clsAccountUsers.strEmailAddress = txtEmail.Text;
        clsAccountUsers.strPhoneNumber = txtPhonum.Text;
       
      

        clsAccountUsers.Update();

        //### redirect back to view page
        Response.Redirect("frmAccountUserView.aspx");
    }

    #endregion

}