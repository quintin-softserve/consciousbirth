﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ConsciousBirth : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        popTestimonials();
         if (Session["clsAccountUsers"] == null)
        {
            //### Redirect back to login
            LoginLabel.Text = "Log in";
        }
        else
         {
             LoginLabel.Text = "Log out";
         }

         litYear.Text = DateTime.Now.Year.ToString();
    }
    protected void btnSubscribeNow_Click(object sender, EventArgs e)
    {
        bool bCanSave = true;

        if (txtName.Text != "" && txtName.Text != null)
        {
            bCanSave = true;
        }
        else
        {
            bCanSave = false;
        }

        if (txtEmail.Text != "" && txtEmail.Text != null)
        {
            bCanSave = true;
        }
        else
        {
            bCanSave = false;
        }

        if (bCanSave == true)
        {
            mandatoryDiv.Visible = true;
            txtName.Attributes.Add("style", "border:none");
            txtEmail.Attributes.Add("style", "border:none");
            lblValidationMessage.Text = "<div class=\"madatoryPaddingDiv\"><img src=\"images/Validation/imgFaceHappy.png\" alt='' style='margin-left: 10px; margin-bottom: 10px; float:left;' title=''/><div class=\"validationMessage\"  style='float: left; margin-left: 10px; margin-top: 18px;'>Thank you for subscribing to Conscious Birth.</div></div>";
            SaveData();
            
        }
        else
        {
            mandatoryDiv.Visible = true;
            txtName.Attributes.Add("style", "border:1px solid #401414");
            txtEmail.Attributes.Add("style", "border:1px solid #401414");
            lblValidationMessage.Text = "<div class=\"madatoryPaddingDiv\"><img src=\"images/Validation/imgFaceSad.png\" alt='' style='margin-left: 10px; margin-bottom: 10px; float:left;' title=''/><div class=\"validationMessage\" style='float: left; margin-left: 10px; margin-top: 18px;'>Please fill out all mandatory fields</div></div>";
        }
    }

    private void SaveData()
    {
        bool bEmailExists = false;
        //### Only update the password if there is a value
        DataTable dtSubscribers = clsSubscribers.GetSubscribersList();
        foreach (DataRow dr in dtSubscribers.Rows)
        {
            if (dr["strEmailAddress"].ToString() == txtEmail.Text)
            {
                bEmailExists = true;
                break;
            }
        }

        if (bEmailExists == false)
        {
            clsSubscribers clsSubscribersRecord = new clsSubscribers();
            bool bIsNewUser = false;

            if (clsSubscribersRecord.iSubscriberID == 0)
                bIsNewUser = true;

            if ((clsCommonFunctions.DoesRecordExist("tblSubscribers", "strEmailAddress='" + txtEmail.Text + "' AND bIsDeleted = 0") == true) && bIsNewUser)
            {
                //rfvEmail.Text = "The email you provided exist. Please enter new email";
            }
            else
            {
                //### Add / Update
                clsSubscribersRecord.dtAdded = Convert.ToDateTime(DateTime.Now.ToString("dd MMM yyyy"));
                clsSubscribersRecord.iAddedBy = -10;
                clsSubscribersRecord.strFirstName = txtName.Text;
                clsSubscribersRecord.strSurname = "";
                clsSubscribersRecord.strEmailAddress = txtEmail.Text;
                //rfvName.Text = "";
                clsSubscribersRecord.Update();
                SendMail();
                SendAdminMail();
                Response.Redirect("Conscious-Birth-home.aspx");
            }
        }
        else
        {
            mandatoryDiv.Visible = true;
            txtName.Attributes.Add("style", "border:1px solid #401414");
            txtEmail.Attributes.Add("style", "border:1px solid #401414");
            lblValidationMessage.Text = "<div class=\"madatoryPaddingDiv\"><img src=\"images/Validation/imgFaceSad.png\" alt='' style='margin-left: 10px; margin-bottom: 10px; float:left;' title=''/><div class=\"validationMessage\" style='float: left; margin-bottom: 15px; margin-left: 10px; margin-top: 18px;'>This email has already been registered. Please try again using a different email address.</div></div>";
        }
        //### redirect back to view page

    }

    protected void SendMail()
    {
        Attachment[] empty = new Attachment[] { };
        StringBuilder strbMail = new StringBuilder();

        string strContent = "";
        string strFrom = "noreply@consciousbirth.co.za";
        string strTo = txtEmail.Text; //clsAccountHolders.strEmailAddress;
        string strName = txtName.Text; //clsAccountHolders.strEmailAddress;

        //string strTo = "renier@softservedigital.co.za";

        strbMail.AppendLine("<table>");
        strbMail.AppendLine("<tr colspan='3' style=\"width: 100%;\">");
        strbMail.AppendLine("<td>");
        strbMail.AppendLine("Dear " + strName + "<br /><br />");
        strbMail.AppendLine("Thank you for subscribing to Conscious Birth.<br /><br />");
        strbMail.AppendLine("</table>");

        strContent = strbMail.ToString();

        clsCommonFunctions.SendMail(strFrom, strTo, "andrew@softservedigital.co.za", "", "Conscious Birth - Subscription", strContent, empty, true);
    }

    protected void SendAdminMail()
    {
        Attachment[] empty = new Attachment[] { };
        StringBuilder strbMail = new StringBuilder();

        //clsAccountHolders clsAccountHoldersRecord = new clsAccountHolders(clsAccountHolders.iAccountHolderID);

        string strContent = "";
        string strFrom = "noreply@consciousbirth.co.za";
        string strTo = "theoni@consciousbirth.co.za"; //clsAccountHolders.strEmailAddress;
        string strName = txtName.Text; //clsAccountHolders.strEmailAddress;

        strbMail.AppendLine("<table>");
        strbMail.AppendLine("<tr colspan='3' style=\"width: 100%;\">");
        strbMail.AppendLine("<td>");
        strbMail.AppendLine("Dear Admin,<br /><br />");
        strbMail.AppendLine("You have a new subscriber, " + strName + ".<br /><br />");
        //strbMail.AppendLine("Full Name: " + clsAccountHolders.strFirstName + " " + clsAccountHolders.strSurname + "<br />");
        //strbMail.AppendLine("Email: <span style='color:#fff;'>" + clsAccountHolders.strEmailAddress + "</span><br />");
        //strbMail.AppendLine("Contact Number: " + clsAccountHoldersRecord.strTelephoneNumber + "<br /><br />");
        //strbMail.AppendLine(clsDeliveryInfos.strPhysicalAddressCode + "<br />");
        //strbMail.AppendLine(clsDeliveryInfos.strCity + "<br /><br />");
        strbMail.AppendLine("</td>");
        strbMail.AppendLine("</tr>");
        strbMail.AppendLine("</table>");

        strContent = strbMail.ToString();

        clsCommonFunctions.SendMail(strFrom, strTo, "andrew@softservedigital.co.za", "", "Conscious Birth - Subscription", strContent, empty, true);
    }

    private void popTestimonials()
    {
        string strCustomerName = "";
        string strTestimonial = "";
        int iCount = 0;

        DataTable dtTestimonials = clsCustomerTestimonials.GetCustomerTestimonialsList("", "");
        StringBuilder strbTestimonial = new StringBuilder();

        foreach (DataRow dtrTestimonialRow in dtTestimonials.Rows)
        {
            ++iCount;

            strTestimonial = (dtrTestimonialRow["strTestimonial"].ToString());
            strCustomerName = (dtrTestimonialRow["strCustomerName"].ToString());

            if (iCount != 1)
                strbTestimonial.AppendLine("<div class='cbp-qtcontent'>");
            else
                strbTestimonial.AppendLine("<div class='cbp-qtcontent current'>");
            strbTestimonial.AppendLine(strTestimonial + "<br /><span>" + strCustomerName + "</span></div>");
        }

        litTestimonial.Text = strbTestimonial.ToString();

    }
}
