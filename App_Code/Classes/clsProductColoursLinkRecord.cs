using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Data.SqlClient;

/// <summary>
/// Summary description for clsProductColoursLink
/// </summary>
public class clsProductColoursLink
{
    #region MEMBER VARIABLES

        private int m_iProductColoursLinkID;
        private int m_iColourID;
        private int m_iProductID;
        private bool m_bIsDeleted;
        
    #endregion 

    #region PROPERTIES

        public int iProductColoursLinkID
        {
            get
            {
                return m_iProductColoursLinkID;
            }
        }

        public int iColourID
        {
            get
            {
                return m_iColourID;
            }
            set
            {
                m_iColourID = value;
            }
        }

        public int iProductID
        {
            get
            {
                return m_iProductID;
            }
            set
            {
                m_iProductID = value;
            }
        }

        public bool bIsDeleted
        {
            get
            {
                return m_bIsDeleted;
            }
            set
            {
                m_bIsDeleted = value;
            }
        }


    
    #endregion
    
    #region CONSTRUCTORS

    public clsProductColoursLink()
    {
        m_iProductColoursLinkID = 0;
    }

    public clsProductColoursLink(int iProductColoursLinkID)
    {
        m_iProductColoursLinkID = iProductColoursLinkID;
        GetData();
    }

    #endregion

    #region PUBLIC METHODS

        public virtual void Update()
        {
            try
            {
                if (iProductColoursLinkID == 0)
                {
                    //### Assign values to the parameter list for each corresponding column in the DB
                    SqlParameter[] sqlParametersInsert = new SqlParameter[] 
                    {
                        new SqlParameter("@iColourID", m_iColourID),
                        new SqlParameter("@iProductID", m_iProductID)                  
                  };

                  //### Add
                  m_iProductColoursLinkID = (int)clsDataAccess.ExecuteScalar("spProductColoursLinkInsert", sqlParametersInsert);                    
                    }
                    else
                    {
                    //### Assign values to the parameter list for each corresponding column in the DB
                    SqlParameter[] sqlParametersUpdate = new SqlParameter[] 
                    {
                         new SqlParameter("@iProductColoursLinkID", m_iProductColoursLinkID),
                         new SqlParameter("@iColourID", m_iColourID),
                         new SqlParameter("@iProductID", m_iProductID)
                    };
                    //### Update
                    clsDataAccess.Execute("spProductColoursLinkUpdate", sqlParametersUpdate);
                    }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void Delete(int iProductColoursLinkID)
        {
        //### Populate
        SqlParameter[] sqlParameter = new SqlParameter[]
        {
            new SqlParameter("@iProductColoursLinkID", iProductColoursLinkID)
        };
        //### Executes delete sp
        clsDataAccess.Execute("spProductColoursLinkDelete", sqlParameter);
        }

        public static void Clear(int iProductID)
        {
            //### Populate
            SqlParameter[] sqlParameter = new SqlParameter[]
        {
            new SqlParameter("@iProductID", iProductID)
        };
            //### Executes delete sp
            clsDataAccess.Execute("spProductColoursLinkClear", sqlParameter);
        }

        public static DataTable GetProductColoursLinkList()
        {
            SqlParameter[] EmptySqlParameter = new SqlParameter[]{};
            return clsDataAccess.GetDataTable("spProductColoursLinkList", EmptySqlParameter);
        }

        public static DataTable GetProductColoursLinkList(string strFilterExpression, string strSortExpression)
        {
            DataView dvProductColoursLinkList = new DataView();

            SqlParameter[] EmptySqlParameter = new SqlParameter[] { };
            dvProductColoursLinkList = clsDataAccess.GetDataView("spProductColoursLinkList", EmptySqlParameter);
            dvProductColoursLinkList.RowFilter = strFilterExpression;
            dvProductColoursLinkList.Sort = strSortExpression;

            return dvProductColoursLinkList.ToTable();
        }
    #endregion

    #region PROTECTED METHODS

        protected virtual void GetData()
        {
            try
                {
                    //### Populate
                    SqlParameter[] sqlParameter = new SqlParameter[] 
                        {
                            new SqlParameter("@iProductColoursLinkID", m_iProductColoursLinkID)
                        };
                DataRow drRecord = clsDataAccess.GetRecord("spProductColoursLinkGetRecord", sqlParameter);

                m_iColourID = Convert.ToInt32(drRecord["iColourID"]);
                m_iProductID = Convert.ToInt32(drRecord["iProductID"]);
                m_bIsDeleted = Convert.ToBoolean(drRecord["bIsDeleted"]);
 
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    #endregion
}