﻿<%@ Page Title="Kahuna Pregnancy Massage :: Conscious Birth" Language="C#" MasterPageFile="~/ConsciousBirth.master" AutoEventWireup="true" CodeFile="Conscious-Birth-Kahuna-Pregnancy-Massage.aspx.cs" Inherits="Conscious_Birth_Kahuna_Pregnancy_Massage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date(); a = s.createElement(o),
            m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-53160457-1', 'auto');
        ga('send', 'pageview');
    </script>
    <link type="text/css" rel="stylesheet" href="/css/theme.css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="main_wrapper">
        <!-- C O N T E N T -->
        <div class="content_wrapper">
            <div class="page_title_block">
                <div class="container">
                    <h2 class="title">Kahuna Pregnancy Massage Experience</h2>
                </div>
            </div>
            <div class="container">
                <div class="content_block no-sidebar row">
                    <div class="fl-container span12">
                        <div class="row">
                            <div class="posts-block span12">
                                <div class="contentarea">

                                    <div class="row-fluid">
                                        <div class="span4 module_cont module_text_area module_none_padding">
                                            <img src="img/kahunapregnancymassage.jpeg" alt="" />
                                        </div>
                                        <div class="span8 module_cont module_text_area module_none_padding">
                                            <blockquote class="type2">
                                                <h6>What this is and who it is for</h6>
                                                <br />
                                                <p>
                                                    The Kahuna pregnancy massage is a Hawaiian full body massage. Using breath and music Theoni creates a nurturing, healing atmosphere 
                                                    and the massage and music soothe both mother and baby. 
                                                </p>
                                                <p>
                                                    This massage is for all pregnant women. To gain the greatest benefit from this type of massage, it is recommended that you book monthly 
                                                    until 30 weeks, every 2 weeks until 36 weeks and weekly until birth. See package options.
                                                </p>
                                                <p>
                                                    The first Kahuna pregnancy massage is longer and much more than the massage. In this session, Theoni offers her personal guidance on a range of 
                                                    pregnancy related issues and concerns. Here you can access expert advice on all your individual pregnancy needs. No two pregnant women are alike and 
                                                    in this special space you will be treated with compassion and care.
                                                </p>
                                                <br />
                                                <h6>What to expect and benefits:</h6>
                                                <br />
                                                <p>
                                                    This massage releases stress and tension held on a physical, emotional and mental level. It is a beautiful, meditative experience that allows a natural, loving bond to develop between 
                                                    mother and baby. Kahuna has all the physical benefits of other massages, and so much more.
                                                </p>
                                                <br />
                                                <h6>Benefits include the following:</h6>
                                                <br />
                                                <ul>
                                                        <li><p>The elimination of waste products through the lymphatic and circulatory systems, which improves energy levels.</p></li>
                                                        <li><p>The easing of the load on mom’s heart through increased blood circulation bringing more oxygen and nutrients to both mother and baby.</p></li>
                                                        <li><p>The alleviation of muscular discomfort such as cramping, stiffness, tension and knots.</p></li>
                                                        <li><p>The relief of depression or anxiety caused by hormonal changes.</p></li>
                                                        <li><p>The alleviation of the typical discomforts experienced during pregnancy, such as backaches, leg cramps, headaches, stiffness on the neck.</p></li>
                                                        <li><p>The promotion of relaxation, which assists the mother to sleep more deeply.</p></li>
                                                </ul>
                                                <br />

                                                <div class="sectionContainerLeft innerBoxWithShadow">
                                                    <div class="span12 module_cont module_text_area module_none_padding">
                                                        <blockquote class="type2">
                                                            <p>
                                                                What an amazing experience! Theoni gave me so much helpful information and answers about my pregnancy. I’m definitely more confident in 
                                                               making my decisions regarding birth etc. The atmosphere is so calming, I immediately forgot about all my stress and worries. The massage 
                                                               not only helped me to relax, but it also cleared my mind completely. I felt like a new person afterwards!
                                                            </p>
                                                            <div class="author italic">Melanie, mother of baby girl</div>
                                                        </blockquote>
                                                    </div>
                                                    <br class="clear" />
                                                </div>
                                            </blockquote>
                                        </div>
                                    </div>

                                </div>
                                <!-- .row-fluid -->
                                <div class="module_line_trigger" data-background="#ffeec9 url(img/VV.png) no-repeat center" data-top-padding="bottom_padding_huge" data-bottom-padding="module_big_padding">
                                    <div class="row-fluid">
                                        <div class="span12 module_cont module_promo_text module_huge_padding">
                                            <div class="shortcode_promoblock">
                                                <div class="promo_button_block type2">
                                                    <a href="Conscious-Birth-Products.aspx" class="promo_button">View our Products </a>
                                                </div>
                                            </div>
                                        </div>
                                        <!--.module_cont -->
                                        <div class="clear">
                                            <!-- ClearFIX -->
                                        </div>
                                    </div>
                                    <!-- .row-fluid -->
                                </div>
                                <!-- .module_line_trigger -->
                                <!-- <div class="row-fluid">
                                    <div class="span12 module_cont module_partners center_title module_big_padding2">
                                        <div class="bg_title">
                                            <div class="title_wrapper">
                                                <a href="javascript:void(0)" class="btn_carousel_left"></a>
                                                <h5 class="headInModule">Our Partners</h5>
                                                <a href="javascript:void(0)" class="btn_carousel_right"></a>
                                            </div>
                                        </div>
                                        <div class="module_content sponsors_works carouselslider items5" data-count="5">
                                            <ul>
                                                <li>
                                                    <div class="item">
                                                        <a href="#charity-html5css3-website-template/3180454" target="_blank">
                                                            <img src="img/pictures/partners2.png" alt=""></a>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="item">
                                                        <a href="#point-business-responsive-wp-theme/4319087" target="_blank">
                                                            <img src="img/pictures/partners4.png" alt=""></a>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="item">
                                                        <a href="#cleanspace-retina-ready-business-wp-theme/3776000" target="_blank">
                                                            <img src="img/pictures/partners1.png" alt=""></a>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="item">
                                                        <a href="#yellowproject-multipurpose-retina-wp-theme/4066662" target="_blank">
                                                            <img src="img/pictures/partners3.png" alt=""></a>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="item">
                                                        <a href="#showroom-portfolio-retina-ready-wp-theme/3473628" target="_blank">
                                                            <img src="img/pictures/partners5.png" alt=""></a>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="item">
                                                        <a href="themeforest.net/item/incipiens-responsive-portfolio-wordpress-theme/2762691" target="_blank">
                                                            <img src="img/pictures/partners6.png" alt=""></a>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="item">
                                                        <a href="themeforest.net/item/hq-photography-responsive-wp-theme/3200962" target="_blank">
                                                            <img src="img/pictures/partners9.png" alt=""></a>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="item">
                                                        <a href="#incipiens-responsive-portfolio-wordpress-theme/2762691" target="_blank">
                                                            <img src="img/pictures/partners11.png" alt=""></a>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <!-- .row-fluid -->
                            </div>
                            <!-- .contentarea -->
                        </div>
                        <div class="left-sidebar-block span3">
                            <aside class="sidebar">
                                //Sidebar Text
                            </aside>
                        </div>
                        <!-- .left-sidebar -->
                    </div>
                    <div class="clear">
                        <!-- ClearFix -->
                    </div>
                </div>
                <!-- .fl-container -->
                <div class="right-sidebar-block span3">
                    <aside class="sidebar">
                    </aside>
                </div>
                <!-- .right-sidebar -->
                <div class="clear">
                    <!-- ClearFix -->
                </div>
            </div>
        </div>
        <!-- .container -->
    </div>
    <!-- .content_wrapper -->

    </div>
    <!-- .main_wrapper -->
</asp:Content>

