﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Conscious_Birth_Gift_Registry : System.Web.UI.Page
{
    #region EVENT METHODS
    bool validate = false;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Request.QueryString["action"] == "AddToCart")
            {
                if (Request.QueryString["value"] == "Yse")
                {
                }
                else if (Request.QueryString["value"] == "No")
                {
                    clsCart.addToCart(Convert.ToInt32(Request.QueryString["iProductID"]), Convert.ToInt32(Request.QueryString["iQuantity"]), Request.QueryString["strColour"], Request.QueryString["strSize"]);
                    HttpCookie cookie = new HttpCookie("iUserProductWishlistLinkID");
                    //Set the cookies value
                    cookie.Value = Request.QueryString["iUserProductWishlistLinkID"];
                    DateTime dtNow = DateTime.Now;
                    TimeSpan tsMinute = new TimeSpan(0, 0, 10, 0);
                    cookie.Expires = dtNow + tsMinute;
                    //Add the cookie
                    Response.Cookies.Add(cookie);
                    Session["clsAccountUserID"] = Request.QueryString["clsAccountUsersID"];
                    Response.Redirect("Conscious-Birth-Cart.aspx");
                }

            }
            labHeader.Text = "Search for gift registry";



        }



    }
    protected void btnSurcheUser_Click(object sender, EventArgs e)
    {
        popUlateAcountUsers();
        //PopulateWishlist(iAccountUserID);
    }
    protected void linkbViewGiftRegestery_Click(object sender, EventArgs e)
    {
        LinkButton btn = (LinkButton)(sender);
        string strAccountUserID = btn.CommandArgument;
        int iAccountUserID = Convert.ToInt32(strAccountUserID);

        PopulateWishlist(iAccountUserID);

    }

    private void popUlateAcountUsers()
    {
        DataTable dtAccountUsers = clsAccountUsers.GetAccountUsersList("strEmailAddress='" + UserIdTexbox.Text + "'", "");
        dtAccountUsers.Columns.Add("dtEvent");
        dtAccountUsers.Columns.Add("EventName");
        dtAccountUsers.Columns.Add("iWishListEventID");


        int iAccountUserID = 0;
        string strFirstName = "";
        foreach (DataRow dtrAccountUsers in dtAccountUsers.Rows)
        {
            iAccountUserID = Convert.ToInt32(dtrAccountUsers["iAccountUserID"]);
            strFirstName = Convert.ToString(dtrAccountUsers["strFirstName"]);
        }
        DataTable dtEvents = clsWishListsEvents.GetWishListsEventsList("iAccountUserID='" + iAccountUserID + "'", "");
        foreach (DataRow dtrAccountUsers in dtAccountUsers.Rows)
        {
            foreach (DataRow dtrWishlistEvents in dtEvents.Rows)
            {
                dtrAccountUsers["dtEvent"] = dtrWishlistEvents["dtEvent"];
                dtrAccountUsers["EventName"] = dtrWishlistEvents["strEventName"];
                dtrAccountUsers["iWishListEventID"] = dtrWishlistEvents["iWishListEventID"];
            }

        }
        if (iAccountUserID == 0)
        {
            Labeleror.Text = "<p style='margin-top: 49px;'>Email address is not valid </p>";
            validate = true;
        }
        else
        {
            HideAcountData.Visible = true;
            hideSearche.Visible = false;
            rpAccountUsers.DataSource = dtAccountUsers;
            rpAccountUsers.DataBind();

        }


    }
    private void PopulateWishlist(int iWishlistID)
    {
        int iWishlistIDs = Convert.ToInt32(Session["clsAccountUsers"]);
        if (iWishlistIDs == null || iWishlistIDs == 0)
        {
            //### Redirect back to login
            Response.Redirect("Conscious-Birth-User-Login.aspx?UserRegester=Current");
        }
        clsWishListsEvents clsWishListsEvents = new clsWishListsEvents(iWishlistID);
        clsAccountUsers clsAccountUsers = new clsAccountUsers(clsWishListsEvents.iAccountUserID);
        labHeader.Text = "Gift registry for&nbsp" + clsAccountUsers.strFirstName;
        StringBuilder sbGrid = new StringBuilder();

        DataTable dtUserWishlistLink = clsUserProductWishlistLink.GetUserProductWishlistLinkList("iWishListEventID='" + iWishlistID + "'", "");
        int iProductID = 0;
        foreach (DataRow dtrUserWishlistLink in dtUserWishlistLink.Rows)
        {
            string value = "";
            if (Convert.ToBoolean(dtrUserWishlistLink["bisPurchased"]) == true)
            {
                value = "Yes";
            }
            else
            {
                value = "No";
            }
            iProductID = Convert.ToInt32(dtrUserWishlistLink["iProductID"]);
            clsProducts clsProducts = new clsProducts(iProductID);
            double Total = clsProducts.dblPrice * Convert.ToInt32(dtrUserWishlistLink["iQuantity"]);
            sbGrid.Append("<tr>");
            sbGrid.Append("<td style='text-align:center'>");
            sbGrid.Append("<img src= '" + getProductImage(Convert.ToInt32(iProductID)) + "' style='border: none; height: 120px; width: 120px; padding-top: 5px;' />");
            sbGrid.Append("</td>");
            sbGrid.Append("<td style='text-align:center'>");
            sbGrid.Append("<div style='padding-left:10px; padding-right: 10px;'>");
            sbGrid.Append("<span style='font-size: 18px;'>" + clsProducts.strTitle + "</span></div>");
            sbGrid.Append("<br/>");
            sbGrid.Append("</td>");
            sbGrid.Append("<td style='text-align:center'>" + value + "</td>");
            sbGrid.Append("<td style='text-align:center'>" + dtrUserWishlistLink["iQuantity"] + "</td>");
            sbGrid.Append("<td style='text-align:center'>" + dtrUserWishlistLink["strColor"] + "</td>");
            sbGrid.Append("<td style='text-align:center'>" + dtrUserWishlistLink["strSize"] + "</td>");
            sbGrid.Append("<td style='text-align:center'>" + Total + "</td>");
            sbGrid.Append("<td style='text-align:center'>");
            if (value == "Yes")
            {
                sbGrid.Append(" <a href='#' class='dgrLinkAdd' style='display: none;'></a>");
            }
            else
            {
                sbGrid.Append(" <a href='Conscious-Birth-Gift-Registry.aspx?action=AddToCart&iProductID=" + clsProducts.iProductID + "&iQuantity=" + dtrUserWishlistLink["iQuantity"] + "&strColour=" + dtrUserWishlistLink["strColor"] + "&strSize=" + dtrUserWishlistLink["strSize"] + "&iUserProductWishlistLinkID=" + dtrUserWishlistLink["iUserProductWishlistLinkID"] + "&clsAccountUsersID=" + clsAccountUsers.iAccountUserID + "&value=" + value + "' onclick='return ConfirmRecordDeleteCart()' class='dgrLinkAdd'></a>");
            }
            sbGrid.Append("</td>");
            sbGrid.Append("</tr>");
        }
        if (iProductID != 0)
        {
            HideAcountData.Visible = false;
            HideData.Visible = true;
            litGridData.Text = sbGrid.ToString();
        }
        else
        {
            if (validate == false)
            {
                Labeleror.Text = " <p style='margin-top: 49px;'> Wish list cart is currently empty.";
            }

        }

    }
    protected string getProductImage(int iProductID)
    {
        clsProducts clsProducts = new clsProducts(Convert.ToInt32(iProductID));

        string strHTMLImageProduct = "";

        try
        {
            string[] strImagesProduct = Directory.GetFiles(ConfigurationManager.AppSettings["WebRootFullPath"] + "\\Products\\" + clsProducts.strPathToImages);

            foreach (string strImageProduct in strImagesProduct)
            {
                if (strImageProduct.Contains("_sml"))
                {
                    strHTMLImageProduct = strImageProduct.Replace(ConfigurationManager.AppSettings["WebRootFullPath"], "").Replace("\\", "/").Substring(1);
                    break;
                }
            }
        }
        catch
        {
        }

        return strHTMLImageProduct;
    }

    protected void linkAddToCart_Click(object sender, EventArgs e)
    {
        int iProductID = Convert.ToInt32(((LinkButton)sender).CommandArgument);
        clsUserProductWishlistLink clsUserProductWishlistLink = new clsUserProductWishlistLink();
        clsUserProductWishlistLink.GetUserProductWishlistLinkList("iProductID='" + iProductID + "'", "");

        clsCart.addToCart(clsUserProductWishlistLink.iProductID, clsUserProductWishlistLink.iQuantity, clsUserProductWishlistLink.strColor, clsUserProductWishlistLink.strSize);
        Response.Redirect("Conscious-Birth-Cart.aspx");
    }
    #endregion
    protected void LinbtnAddToCaret_Click(object sender, EventArgs e)
    {

    }
}