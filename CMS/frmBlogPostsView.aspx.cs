
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.Services;
using System.Web.Script.Services;

/// <summary>
/// Summary description for clsBlogPostsView
/// </summary>
public partial class CMS_clsBlogPostsView : System.Web.UI.Page
{
    #region CLASSES AND VARIABLES

    clsUsers clsUsers;
    DataTable dtBlogPostsList;

    List<clsBlogPosts> glstBlogPosts;

    #endregion

    #region EVENT MODULES

    protected void Page_Load(object sender, EventArgs e)
    {
        //### Check if session clsUser exists
        if (Session["clsUsers"] == null)
        {
            {
                //### Redirect back to login
                Response.Redirect("../CMSLogin.aspx");
            }
        }
        clsUsers = (clsUsers)Session["clsUsers"];

dgrGrid.ItemCreated += new DataGridItemEventHandler(clsCommonFunctions.dgrGrid_PagerPopulate);

        if (!Page.IsPostBack)
        {
            PopulateFormData();
        }
        else
        {
            if (Session["glstBlogPosts"] == null)
            {
                glstBlogPosts = new List<clsBlogPosts>();
                Session["glstBlogPosts"] = glstBlogPosts;
            }
            else
                glstBlogPosts = (List<clsBlogPosts>)Session["glstBlogPosts"];
        }
            PopulateFormData();

    }

    protected void lnkbtnSearch_Click(object sender, EventArgs e)
    {
        string FilterExpression ="(strTitle +' '+ strDescription +' '+ strMasterImage) LIKE '%" + txtSearch.Text + "%'";
        DataTable dtBlogPostsList = clsBlogPosts.GetBlogPostsList(FilterExpression, "");

        Session["dtBlogPostsList"] = dtBlogPostsList;

        PopulateFormData();
    }

    protected void lnkbtnAdd_Click(object sender, EventArgs e)
    {
        Response.Redirect("frmBlogPostsAddModify.aspx");
    }

    #endregion

    #region DATA GRID METHODS

    private void PopulateFormData()
    {
        //### Populate datagrid using clsBlogPostsList object
        try
        {
            dtBlogPostsList = new DataTable();

            if (Session["dtBlogPostsList"] == null)
                dtBlogPostsList = clsBlogPosts.GetBlogPostsList();
            else
                dtBlogPostsList = (DataTable)Session["dtBlogPostsList"];

            dgrGrid.DataSource = dtBlogPostsList;

            //### Add this check to trap if the last item on the page is deleted
            if (Convert.ToInt16(dgrGrid.CurrentPageIndex) < Convert.ToInt16(Session["BlogPostsView_CurrentPageIndex"]))
            {
                dgrGrid.CurrentPageIndex = 0;
            }
            else
            {
                if (Session["BlogPostsView_CurrentPageIndex"] != null)
                {
                    dgrGrid.CurrentPageIndex = (int)Session["BlogPostsView_CurrentPageIndex"];
                }
            }

            dgrGrid.DataBind();

        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    protected void lnkDeleteItem_Click(object sender, EventArgs e)
    {
        int iBlogPostID = int.Parse((sender as LinkButton).CommandArgument);

        clsBlogPosts.Delete(iBlogPostID);

        PopulateFormData();
    }

    protected void dgrGrid_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
    {
        //### Method is used when the page index is changed.
        try
        {
            Session["BlogPostsView_CurrentPageIndex"] = e.NewPageIndex;
            dgrGrid.CurrentPageIndex = e.NewPageIndex;
            PopulateFormData();
        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    protected void dgrGrid_SortCommand(object source, DataGridSortCommandEventArgs e)
    {
        //### Sort Method
        dtBlogPostsList = new DataTable();

        if (Session["dtBlogPostsList"] == null)
            dtBlogPostsList = clsBlogPosts.GetBlogPostsList();
        else
            dtBlogPostsList = (DataTable)Session["dtBlogPostsList"];

        DataView dvTemp = dtBlogPostsList.DefaultView;

        dvTemp.Sort = e.SortExpression;
        dtBlogPostsList = dvTemp.ToTable();
        Session["dtBlogPostsList"] = dtBlogPostsList;

        PopulateFormData();
    }

    public void dgrGrid_PopEditLink(object sender, EventArgs e)
    {
        try
        {
            //### Add hyperlink to datagrid item
            HyperLink lnkEditItem = (HyperLink)sender;
            DataGridItem dgrItemEdit = (DataGridItem)lnkEditItem.Parent.Parent;

            //### Get the iBlogPostID
            int iBlogPostID = 0;
            iBlogPostID = int.Parse(dgrItemEdit.Cells[0].Text);

            //### Add attributes to edit link
            lnkEditItem.Attributes.Add("href", "frmBlogPostsAddModify.aspx?action=edit&iBlogPostID=" + iBlogPostID);
        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

     public void dgrGrid_PopDeleteLink(object sender, EventArgs e)
     {
          try
          {
             //### Add hyperlink to datagrid item
             HyperLink lnkDeleteItem = (HyperLink)sender;
             DataGridItem dgrItemDelete = (DataGridItem)lnkDeleteItem.Parent.Parent;

             //### Get the iBlogPostID
             int iBlogPostID = 0;
             iBlogPostID = int.Parse(dgrItemDelete.Cells[0].Text);

             //### Add attributes to delete link
             lnkDeleteItem.CssClass = "dgrDeleteLink";
             lnkDeleteItem.Attributes.Add("href", "frmBlogPostsView.aspx?action=delete&iBlogPostID=" + iBlogPostID);
           }
           catch (Exception ex)
           {
                 Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
           }
     }

    #endregion

    #region BlogPosts FILTERING

    /// <summary>
    /// Enables the AutoCompleteExtender
    /// </summary>
    /// <param name="prefixText"></param>
    /// <param name="count"></param>
    /// <returns></returns>
    /// 
    [WebMethod]
    [ScriptMethod]
    public static string[] FilterBusinessRule(string prefixText, int count)
    {
        string[] strReturnList = new string[] { };

        string FilterExpression = "(strTitle +' '+ strDescription +' '+ strMasterImage) LIKE '%" + prefixText + "%'";
        DataTable dtBlogPosts = clsBlogPosts.GetBlogPostsList(FilterExpression, "");
        List<string> glstBlogPosts = new List<string>();

        if (dtBlogPosts.Rows.Count > 0)
        {
            foreach (DataRow dtrBlogPosts in dtBlogPosts.Rows)
            {
                glstBlogPosts.Add(AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem("" + dtrBlogPosts["strTitle"].ToString() +' '+"" + dtrBlogPosts["strDescription"].ToString() +' '+"" + dtrBlogPosts["strMasterImage"].ToString(), dtrBlogPosts["iBlogPostID"].ToString()));
            }
        }
        else
            glstBlogPosts.Add("No BlogPosts Available.");
        strReturnList = glstBlogPosts.ToArray();
        return strReturnList;
    }

    #endregion
}
