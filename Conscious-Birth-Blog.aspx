﻿<%@ Page Title="Blog :: Conscious Birth" Language="C#" MasterPageFile="~/ConsciousBirth.master" AutoEventWireup="true" CodeFile="Conscious-Birth-Blog.aspx.cs" Inherits="Conscious_Birth_Blog" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
     <script type="text/javascript" src="js/jquery.min.js"></script>
    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date(); a = s.createElement(o),.blog_head .blogpost_title
            m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-53160457-1', 'auto');
        ga('send', 'pageview');

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
     <div class="main_wrapper">
        <!-- C O N T E N T -->
        <div class="content_wrapper">
        	<div class="page_title_block">
            	<div class="container">
                    <h2 class="title">Blog</h2>
                </div>
            </div>        
            <div class="container">
                <div class="content_block no-sidebar row">
                    <div class="fl-container span12">
                        <div class="row">
                            <div class="posts-block span12">
                                <div class="contentarea">
                                     <div class="row-fluid">                                
                                        <div class="span12 module_cont module_blog module_none_padding">
                                        <asp:Literal ID="litBlog" runat="server"></asp:Literal>

                                            <%--<div class="blog_post_preview">
	                                            <div class="blog_head">
	                                                <span class="blogpost_type_ico post_type_image"></span>                                                    
                                                	<div class="bg_title"><h4><a class="blogpost_title" href="blogpost_full.html">Image Post</a></h4></div>
                                                    <div class="blogpost_meta">
                                                        <span>25 April 2019</span>
                                                        <span>by <a href="#">GT3</a></span>
                                                        <span>In <a href="#">ArtCore</a>, <a href="#">Illustrations</a></span>
                                                        <span>Tags: <a href="#">art</a>, <a href="#">artcore</a>, <a href="#">illustrations</a>, <a href="#">colors</a>, <a href="#">monster</a></span>
                                                    </div>
                                                </div><!-- .blog_head -->
                                                <div class="featured_image_full">
													<img src="img/pictures/blogpost1f.jpg" alt=""/>
                                                </div><!-- .featured_image_full -->
                                                <article class="contentarea">
                                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In eu purus consequat, tempor eros at, malesuada tellus. Proin sodales erat lorem, dictum malesuada justo 
                                                    feugiat vitae. Praesent interdum libero id tempor rutrum. Sed at nibh vel sapien tempor hendrerit. Nam vitae elit augue. Suspendisse commodo, dui sed luctus lobortis, 
                                                    augue libero pellentesque ligula, eget euismod quam mauris non urna. In vitae urna interdum, laoreet tortor nec, vulputate lacus. Proin eu auctor metus. Integer euismod 
                                                    ipsum id mi sodales bibendum. Proin convallis feugiat pulvinar. Vivamus ultricies est tincidunt libero facilisis cursus. Nullam vitae porta nunc. Donec dignissim, dolor 
                                                    et viverra dictum, libero massa congue dolor, ut scelerisque odio eros et nulla.<a href="blogpost_full.html" class="more-link">Read more...</a></p>
                                                </article>
                                            </div>--%><!--.blog_post_preview -->

                                           <%-- <div class="blog_post_preview">
	                                            <div class="blog_head">
	                                                <span class="blogpost_type_ico post_type_video"></span>                                                    
                                                	<div class="bg_title"><h4><a class="blogpost_title" href="blogpost_full.html">Video Post</a></h4></div>
                                                    <div class="blogpost_meta">
                                                        <span>25 April 2019</span>
                                                        <span>by <a href="#">GT3</a></span>
                                                        <span>In <a href="#">ArtCore</a>, <a href="#">Illustrations</a></span>
                                                        <span>Tags: <a href="#">art</a>, <a href="#">artcore</a>, <a href="#">illustrations</a>, <a href="#">colors</a>, <a href="#">monster</a></span>
                                                    </div>
                                                </div><!-- .blog_head -->
                                                <div class="featured_image_full">
													<iframe width="870" height="430" src="<!--http://www.youtube.com/embed/JbeFIfS8uno?controls=1&amp;showinfo=0&amp;modestbranding=1&amp;wmode=opaque"></iframe>
                                                </div><!-- .featured_image_full -->
                                                <article class="contentarea">
                                                    <p>Donec nec laoreet nisi, at scelerisque felis. Donec sed ipsum eu nisi auctor elementum. Quisque est massa, convallis ut neque quis, fermentum posuere felis. 
                                                    Vivamus nunc lacus, sollicitudin consectetur risus non, adipiscing consectetur purus. Donec aliquet volutpat lectus, sit amet suscipit sem euismod at. In pulvinar 
                                                    interdum fringilla. Donec eleifend quam eget nibh ornare rutrum. Vestibulum mattis nec purus non ultricies. Fusce non vehicula ipsum. Pellentesque habitant morbi tristique 
                                                    senectus et netus et malesuada fames ac turpis egestas. Integer diam turpis, feugiat nec hendrerit vitae, vestibulum tincidunt lectus. Sed fringilla lorem quam, et commodo 
                                                    sem egestas in. Sed et erat mi. Duis sodales lectus est, ac interdum orci placerat et. Lorem ipsum dolor sit amet, consectetur adipiscing elit. In eu purus consequat, 
                                                    tempor eros at, malesuada tellus. Proin sodales erat lorem, dictum malesuada justo feugiat vitae. Praesent interdum libero id tempor rutrum. Sed at nibh vel sapien tempor 
                                                    hendrerit. Nam vitae elit augue. Suspendisse commodo, dui sed luctus lobortis, augue libero pellentesque ligula, eget euismod quam mauris non urna. In vitae urna interdum, 
                                                    laoreet tortor nec, vulputate lacus. Proin eu auctor metus. <a href="blogpost_full.html" class="more-link">Read more...</a></p>
                                                </article>
                                            </div>--%><!--.blog_post_preview -->

<%--                                            <div class="blog_post_preview">
	                                            <div class="blog_head">
	                                                <span class="blogpost_type_ico post_type_text"></span>                                                    
                                                	<div class="bg_title"><h4><a class="blogpost_title" href="blogpost_full.html">Text</a></h4></div>
                                                    <div class="blogpost_meta">
                                                        <span>25 April 2019</span>
                                                        <span>by <a href="#">GT3</a></span>
                                                        <span>In <a href="#">ArtCore</a>, <a href="#">Illustrations</a></span>
                                                        <span>Tags: <a href="#">art</a>, <a href="#">artcore</a>, <a href="#">illustrations</a>, <a href="#">colors</a>, <a href="#">monster</a></span>
                                                    </div>
                                                </div><!-- .blog_head -->
                                                <article class="contentarea">
                                                    <p>Suspendisse in erat venenatis, tincidunt sem ut, ultrices nibh. Aliquam bibendum suscipit tempor. Donec a magna ac nisi euismod rhoncus. Donec mauris neque, faucibus 
                                                    sit amet cursus at, fringilla ut lectus. Maecenas purus nibh, placerat vel odio in, lobortis ultrices ante. Aenean lobortis id neque et dapibus. Aenean dapibus augue ac 
                                                    diam aliquet, vel commodo metus feugiat. In sed tellus scelerisque, accumsan urna a, scelerisque magna. Curabitur egestas felis et nisi imperdiet tristique. Donec eu erat 
                                                    vitae justo adipiscing mattis eu eu quam. Quisque porta blandit mi, eget tristique nibh ornare at. Fusce pharetra consequat odio non hendrerit. 
                                                    <a href="blogpost_full.html" class="more-link">Read more...</a></p>
                                                </article>
                                            </div>--%><!--.blog_post_preview -->

                                           <%-- <div class="blog_post_preview">
	                                            <div class="blog_head">
	                                                <span class="blogpost_type_ico post_type_gallery"></span>                                                    
                                                	<div class="bg_title"><h4><a class="blogpost_title" href="blogpost_full.html">Multi Image Post</a></h4></div>
                                                    <div class="blogpost_meta">
                                                        <span>25 April 2019</span>
                                                        <span>by <a href="#">GT3</a></span>
                                                        <span>In <a href="#">ArtCore</a>, <a href="#">Illustrations</a></span>
                                                        <span>Tags: <a href="#">art</a>, <a href="#">artcore</a>, <a href="#">illustrations</a>, <a href="#">colors</a>, <a href="#">monster</a></span>
                                                    </div>
                                                </div>--%><!-- .blog_head -->
                                                <%--<div class="featured_image_full">
                                                    <div class="slider-wrapper theme-default">
                                                        <div class="nivoSlider">
                                                            <img src="img/pictures/blogpost2f.jpg" data-thumb="img/pictures/blogpost2f.jpg" alt="" />
                                                            <img src="img/pictures/blogpost4f.jpg" data-thumb="img/pictures/blogpost4f.jpg" alt="" />
                                                        </div>
                                                        <div class="img_inset"></div>                                                            
                                                    </div><!-- .slider-wrapper  -->
                                                </div>--%><!-- .featured_image_full -->
                                               <%-- <article class="contentarea">
                                                    <p>Nulla urna velit, accumsan porta aliquet sed, cursus at libero. In hac habitasse platea dictumst. Maecenas urna velit, eleifend non euismod sed, congue eu nibh. 
                                                    Pellentesque ligula urna, fermentum eu felis non, aliquet adipiscing est. Proin mollis eros accumsan risus pellentesque, non pulvinar eros pulvinar. Maecenas eget facilisis 
                                                    dui. Etiam non mi felis. Nullam interdum eros erat, at placerat nisi ultricies id. Sed adipiscing tellus et dapibus scelerisque. Integer tempor lacus a molestie 
                                                    pellentesque. Nunc vulputate augue id sem consectetur fringilla. Etiam at volutpat mauris. Proin egestas turpis sed arcu congue lobortis. Class aptent taciti sociosqu ad 
                                                    litora torquent per conubia nostra, per inceptos himenaeos.  Integer euismod ipsum id mi sodales bibendum. Proin convallis feugiat pulvinar. Vivamus ultricies est tincidunt 
                                                    libero facilisis cursus. Nullam vitae porta nunc. Donec dignissim, dolor et viverra dictum, libero massa congue dolor, ut scelerisque odio eros et nulla. Aenean egestas 
                                                    lectus vel nunc vehicula iaculis. Nunc risus tellus, egestas ut lobortis nec, volutpat sed turpis. Duis placerat ligula lacus, quis luctus elit auctor et. Mauris ac magna 
                                                    purus. In imperdiet tellus ac nunc rutrum adipiscing. Fusce blandit ligula quis enim volutpat, sed mollis nunc laoreet.
                                                    <a href="blogpost_full.html" class="more-link">Read more...</a></p>
                                                </article>--%>
                                            </div><!--.blog_post_preview -->
                                            
                                            <ul class="pagerblock">
                                                <!--<li><a href="#"><span class="btn_prev">Previous</span></a></li>-->
                                                <li><a class="current shortcode_button btn_small btn_type1"  href="#">1</a></li>
                                                <li><a href="#" class=" shortcode_button btn_small btn_type2">2</a></li>
                                                <li><a href="#" class=" shortcode_button btn_small btn_type2">3</a></li>
                                                <!--<li><a href="#"><span class="btn_next">Next</span></a></li>-->
                                            </ul><!-- .pagerblock -->
                                        </div>								
                                    </div><!-- .row-fluid -->
                                    <div class="module_line_trigger" data-background="#ffeec9 url(img/VV.png) no-repeat center" data-top-padding="bottom_padding_huge" data-bottom-padding="module_big_padding">
                                        <div class="row-fluid">                                
                                            <div class="span12 module_cont module_promo_text module_huge_padding">
                                                <div class="shortcode_promoblock">
                                                    <div class="promo_button_block type2">
                                                        <a href="Conscious-Birth-Products.aspx" class="promo_button"> View our Products </a>
                                                    </div>
                                                </div>
                                            </div><!--.module_cont -->
                                            <div class="clear"><!-- ClearFIX --></div>                                                                    
                                        </div><!-- .row-fluid -->
                                    </div><!-- .module_line_trigger -->
                                    <!--<div class="row-fluid">
                                        <div class="span12 module_cont module_partners center_title module_big_padding2">
											<div class="bg_title"><div class="title_wrapper"><a href="javascript:void(0)" class="btn_carousel_left"></a><h5 class="headInModule">Our Partners</h5><a href="javascript:void(0)" class="btn_carousel_right"></a></div></div>
                                            <div class="module_content sponsors_works carouselslider items5" data-count="5">
                                                <ul>
                                                    <li>
                                                        <div class="item">
                                                            <a href="#charity-html5css3-website-template/3180454" target="_blank"><img src="img/pictures/partners2.png" alt=""></a>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="item">
                                                            <a href="#point-business-responsive-wp-theme/4319087" target="_blank"><img src="img/pictures/partners4.png" alt=""></a>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="item">
                                                            <a href="#cleanspace-retina-ready-business-wp-theme/3776000" target="_blank"><img src="img/pictures/partners1.png" alt=""></a>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="item">
                                                            <a href="#yellowproject-multipurpose-retina-wp-theme/4066662" target="_blank"><img src="img/pictures/partners3.png" alt=""></a>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="item">
                                                            <a href="#showroom-portfolio-retina-ready-wp-theme/3473628" target="_blank"><img src="img/pictures/partners5.png" alt=""></a>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="item">
                                                            <a href="themeforest.net/item/incipiens-responsive-portfolio-wordpress-theme/2762691" target="_blank"><img src="img/pictures/partners6.png" alt=""></a>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="item">
                                                            <a href="themeforest.net/item/hq-photography-responsive-wp-theme/3200962" target="_blank"><img src="img/pictures/partners9.png" alt=""></a>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="item">
                                                            <a href="#incipiens-responsive-portfolio-wordpress-theme/2762691" target="_blank"><img src="img/pictures/partners11.png" alt=""></a>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div><!-- .row-fluid -->
                                	
                                </div><!-- .contentarea -->
                            </div>
                            <div class="left-sidebar-block span3">
                                <aside class="sidebar">
                                    //Sidebar Text
                                </aside>
                            </div><!-- .left-sidebar -->
                        </div>
                        <div class="clear"><!-- ClearFix --></div>
                    </div><!-- .fl-container -->
                    <div class="right-sidebar-block span3">
                        <aside class="sidebar">

                        </aside>
                    </div><!-- .right-sidebar -->
                    <div class="clear"><!-- ClearFix --></div>
                </div>
            </div><!-- .container -->
        </div><!-- .content_wrapper -->
	
    </div><!-- .main_wrapper -->
</asp:Content>