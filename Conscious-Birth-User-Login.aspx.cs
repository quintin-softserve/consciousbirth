﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Data.SqlClient;
using System.Net.Mail;

public partial class Conscious_Birth_User_Login : System.Web.UI.Page
{
  #region EVENT METHODS

    protected void Page_Load(object sender, EventArgs e)
    {
        txtUsername.Focus();
        if (Session["clsAccountUsers"]  != null)
        {
            //### Redirect back to login
             Session.Clear();
             Response.Redirect("Conscious-Birth-Home.aspx");
        }
        if (Request.QueryString["UserRegester"] == "Current")
        {
            litValidationMessage.Text = "Please register or login to view Gift registry ";
        }
        //### Logout User
        if (Request.QueryString["action"] == "logout")
        {
            //### Clear all session variables
            Session.Clear();
        }
    }

    protected void lnkbtnForgottenPassword_Click(object sender, EventArgs e)
    {
        lnkbtnForgottenPassword.Visible = false;
        lnkbtnBackToLogin.Visible = true;
        divPassword.Visible = false;
        btnLogin.Visible = false;
        btnSubmit.Visible = true;
    }

    protected void lnkbtnBackToLogin_Click(object sender, EventArgs e)
    {
        lnkbtnForgottenPassword.Visible = true;
        lnkbtnBackToLogin.Visible = false;
        divPassword.Visible = true;
        btnLogin.Visible = true;
        btnSubmit.Visible = false;
    }

    protected void btnLogin_Click(object sender, EventArgs e)
    {
        try
        {
            clsAccountUsers clsAccountUsers = new clsAccountUsers(txtUsername.Text, clsCommonFunctions.GetMd5Sum(txtPassword.Text));
            Session["clsAccountUsers"] = clsAccountUsers.iAccountUserID;
            Response.Redirect("Conscious-Birth-Home.aspx");
            mandatoryDiv.Visible = true;
        }
        catch
        {
            litValidationMessage.Text = "<div class=\"mandatoryDiv\"></div><div style=\"position:relative; top:16px; font-size:13px\">Invalid Login Credentials.</div>";
            if ((txtPassword.Text == "") || (txtPassword.Text == null))
            {
                litValidationMessage.Text = "<div class=\"mandatoryDiv\"></div><div class=\"validationLabel\">Login requires a Password.</div>";
            }
            else if ((txtUsername.Text == "") || (txtUsername.Text == null))
            {
                litValidationMessage.Text = "<div class=\"mandatoryDiv\"></div><div class=\"validationLabel\">Login requires an Email.</div>";
            }

            if ((txtUsername.Text == "") && (txtPassword.Text == ""))
            {
                litValidationMessage.Text = "<div class=\"mandatoryDiv\"></div><div class=\"validationLabel\">Login requires an Email and Password.</div>";
            }
        }
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        //### Validate email
        if (txtUsername.Text == "")
        {
            litValidationMessage.Text = "<div class=\"validationImageIncorrectLogin\"></div><div class=\"validationLabel\">Please enter your email address.</div>";
        }
        else
        {
            //### Check if email address exists
            if (clsCommonFunctions.DoesRecordExist("tblAccountUsers", "strEmailAddress = '" + txtUsername.Text + "' AND bIsDeleted = 0") == true)
            {
                //### Change password & Send mail
                string strRandomPassword = "";
                string strPassword = "";
                strRandomPassword = clsCommonFunctions.strCreateRandomPassword(8);
                strPassword = strRandomPassword;
                //### Hash random password
                strRandomPassword = clsCommonFunctions.GetMd5Sum(strRandomPassword);
                ForgottenPassword(txtUsername.Text, strRandomPassword);
                StringBuilder strbContent = new StringBuilder();
                strbContent.AppendLine("Dear User<br /><br />");
                strbContent.AppendLine("Your password has been reset.<br /><br />");
                strbContent.AppendLine("Your temporary password is shown below, please log into your account and change your password to something more suitable and easier to remember:<br /><br />");
                strbContent.AppendLine("Password: " + strPassword + "<br/>");
                string strContent = strbContent.ToString();

                //DataConnection.GetDataObject().ExecuteScalar("spForgottenPassword '" + txtUsername.Text + "', '" + strRandomPassword + "'");
                //clsCommonFunctions.SendMail("noreply@gobundu.co.za", txtUsername.Text, "", "", "Forgotten Password", strContent, true);
                Attachment[] empty = new Attachment[] { };

                try
                {
                    clsCommonFunctions.SendMail("noreply@bespokebouquet.co.za", txtUsername.Text, "", "andrew@softservedigital.co.za", "CMS Forgotten Password", strContent, empty, true);
                }
                catch { }

                //### Redirect
                litValidationMessage.Text = "<div class=\"validationImageCorrectLogin\"></div><div class=\"validationLabel\">Your temporary password has been sent.</div>";
            }
            else
            {
                //### If no email address found
                litValidationMessage.Text = "<div class=\"validationImageIncorrectLogin\"></div><div class=\"validationLabel\">The email you have entered is not a registered <br />email address.</div>";
            }
        }
        mandatoryDiv.Visible = true;
    }

    #endregion

    #region PASSWORD METHODS

    public static void ForgottenPassword(string strEmailAddress, string strRandomPassword)
    {
        //### Populate
        SqlParameter[] sqlParameter = new SqlParameter[]
            {
                new SqlParameter("@strEmailAddress", strEmailAddress),
                new SqlParameter("@strRandomPassword", strRandomPassword)
            };
        //### Executes Password Reset sp
        clsDataAccess.Execute("spForgottenPassword", sqlParameter);
    }

    #endregion
}