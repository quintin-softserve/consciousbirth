﻿<%@ Page Title="Postnatal Massage" Language="C#" MasterPageFile="~/ConsciousBirth.master" AutoEventWireup="true" CodeFile="Conscious-Birth-Postnatal-Massage.aspx.cs" Inherits="Conscious_Birth_Postnatal_Massage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date(); a = s.createElement(o),
            m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-53160457-1', 'auto');
        ga('send', 'pageview');

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="main_wrapper">
        <!-- C O N T E N T -->
        <div class="content_wrapper">
            <div class="page_title_block">
                <div class="container">
                    <h2 class="title">Postnatal Massage</h2>
                </div>
            </div>
            <div class="container">
                <div class="content_block no-sidebar row">
                    <div class="fl-container span12">
                        <div class="row">
                            <div class="posts-block span12">
                                <div class="contentarea">

                                    <div class="row-fluid">
                                        <div class="span4 module_cont module_text_area module_none_padding">
                                            <img src="img/pictures/postnatal-massage1_large.png" alt="" />
                                        </div>
                                        <div class="span8 module_cont module_text_area module_none_padding">
                                            <blockquote class="type2">
                                                <h6>What is this for and who it is for:</h6>
                                                <p>
                                                    Early motherhood is challenging time and Theoni’s postnatal care and massage service will help you ease into this phase comfortably and confidently. 
                                                    Make an appointment for one or more postnatal care and massage sessions within 6 weeks after childbirth – the sooner the better though. Book anytime 
                                                    after day 4 after a natural delivery and after day 7 for caesarean.
                                                </p>
                                                <br />
                                                <h6>What can you expect & benefits:</h6>
                                                <p>
                                                    In the comfort of your home, enjoy a nurturing massage that alleviates discomforts experienced after birth and that facilitates recovery. This service differs from other
                                                     postnatal care offerings in that Theoni supports the postpartum woman with compassion during a vulnerable time and provides personalised advice on whatever challenges that
                                                     particular Mom may be facing – after all, no two Moms experience this stage in quite the same way. She has a wide range of knowledge on what will best support that individual, 
                                                     be it physical or emotional.  If you choose to breastfeed then Theoni can teach you various techniques on how to breastfeed comfortably
                                                    <br />
                                                    <br />
                                                    The massage she performs facilitates a deep relaxation that helps sustain you through night feeds – caring for yourself during this time helps you to care for your baby too.<br />
                                                    <br />
                                                    <ul>
                                                        <li><p>Addressing post-childbirth aches and pains, this massage releases stress, particularly in the shoulders and neck, that may result from carrying and feeding your baby.</p></li>
                                                        <li><p>Helps the body to bounce back to its pre-pregnancy shape more quickly by reducing water retention, shrinking the uterus, reducing cellulite and toning the body.</p></li>
                                                        <li><p>The unique nature of Theoni’s massage also helps women to reclaim their bodies by integrating their unique birth experience.</p></li>
                                                        <li><p>The postnatal care and massage service also makes for the best postnatal gift for friends or family members – enquire about vouchers that can be used for this purpose.</p></li>
                                                
                                                    <li><p> Postnatal support session can be booked without a massage if you simply need extra care, support and expert advice.</p></li>
                                                </ul>
                                                <br />
                                                <div class="sectionContainerLeft innerBoxWithShadow">
                                                    <div class="span12 module_cont module_text_area module_none_padding">
                                                        <blockquote class="type2">
                                                            <p>
                                                                Thank you Theoni! I feel so much better physically as well as more confident of what I am doing as a new mother...thank you. All your practical tips and advice really work. Thank you!                          
                                                            </p>
                                                            <div class="author italic">Rebeca mother to Alex.</div>
                                                        </blockquote>
                                                    </div>
                                                    <br class="clear" />
                                                </div>
                                            </blockquote>
                                        </div>
                                    </div>
                                    <!-- .row-fluid -->
                                    <div class="module_line_trigger" data-background="#ffeec9 url(img/VV.png) no-repeat center" data-top-padding="bottom_padding_huge" data-bottom-padding="module_big_padding">
                                        <div class="row-fluid">
                                            <div class="span12 module_cont module_promo_text module_huge_padding">
                                                <div class="shortcode_promoblock">
                                                    <div class="promo_button_block type2">
                                                        <a href="Conscious-Birth-Products.aspx" class="promo_button"> View our Products </a>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--.module_cont -->
                                            <div class="clear">
                                                <!-- ClearFIX -->
                                            </div>
                                        </div>
                                        <!-- .row-fluid -->
                                    </div>
                                    <!-- .module_line_trigger -->
                                    <!--<div class="row-fluid">
                                        <div class="span12 module_cont module_partners center_title module_big_padding2">
                                            <div class="bg_title">
                                                <div class="title_wrapper">
                                                    <a href="javascript:void(0)" class="btn_carousel_left"></a>
                                                    <h5 class="headInModule">Our Partners</h5>
                                                    <a href="javascript:void(0)" class="btn_carousel_right"></a>
                                                </div>
                                            </div>
                                            <div class="module_content sponsors_works carouselslider items5" data-count="5">
                                                <ul>
                                                    <li>
                                                        <div class="item">
                                                            <a href="#charity-html5css3-website-template/3180454" target="_blank">
                                                                <img src="img/pictures/partners2.png" alt=""></a>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="item">
                                                            <a href="#point-business-responsive-wp-theme/4319087" target="_blank">
                                                                <img src="img/pictures/partners4.png" alt=""></a>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="item">
                                                            <a href="#cleanspace-retina-ready-business-wp-theme/3776000" target="_blank">
                                                                <img src="img/pictures/partners1.png" alt=""></a>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="item">
                                                            <a href="#yellowproject-multipurpose-retina-wp-theme/4066662" target="_blank">
                                                                <img src="img/pictures/partners3.png" alt=""></a>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="item">
                                                            <a href="#showroom-portfolio-retina-ready-wp-theme/3473628" target="_blank">
                                                                <img src="img/pictures/partners5.png" alt=""></a>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="item">
                                                            <a href="themeforest.net/item/incipiens-responsive-portfolio-wordpress-theme/2762691" target="_blank">
                                                                <img src="img/pictures/partners6.png" alt=""></a>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="item">
                                                            <a href="themeforest.net/item/hq-photography-responsive-wp-theme/3200962" target="_blank">
                                                                <img src="img/pictures/partners9.png" alt=""></a>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="item">
                                                            <a href="#incipiens-responsive-portfolio-wordpress-theme/2762691" target="_blank">
                                                                <img src="img/pictures/partners11.png" alt=""></a>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- .row-fluid -->

                                </div>
                                <!-- .contentarea -->
                            </div>
                            <div class="left-sidebar-block span3">
                                <aside class="sidebar">
                                    //Sidebar Text
                                </aside>
                            </div>
                            <!-- .left-sidebar -->
                        </div>
                        <div class="clear">
                            <!-- ClearFix -->
                        </div>
                    </div>
                    <!-- .fl-container -->
                    <div class="right-sidebar-block span3">
                        <aside class="sidebar">
                        </aside>
                    </div>
                    <!-- .right-sidebar -->
                    <div class="clear">
                        <!-- ClearFix -->
                    </div>
                </div>
            </div>
            <!-- .container -->
        </div>
        <!-- .content_wrapper -->
    </div>
    <!-- .main_wrapper -->
</asp:Content>

