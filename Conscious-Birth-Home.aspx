﻿<%@ Page Title="Home :: Conscious Birth" Language="C#" MasterPageFile="~/ConsciousBirth.master" AutoEventWireup="true" CodeFile="Conscious-Birth-Home.aspx.cs" Inherits="Conscious_Birth_Home" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date(); a = s.createElement(o),
            m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-53160457-1', 'auto');
        ga('send', 'pageview');

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="main_wrapper">
        <!-- C O N T E N T -->
        <div class="content_wrapper">
            <div class="container">
                <div class="content_block no-sidebar row">
                    <div class="fl-container span12">
                        <div class="row">
                            <div class="posts-block span12">
                                <div class="contentarea">
                                    <div class="row-fluid">
                                        <div class="span12 module_cont module_revolution_slider first-module module_none_padding">
                                            <script type="text/javascript" src="js/jquery.themepunch.plugins.min.js"></script>
                                            <script type="text/javascript" src="js/jquery.themepunch.revolution.min.js"></script>
                                            <div class="rs-fullscreen">
                                                <div class="fullwidthbanner-container">
                                                    <div class="fullwidthbanner">
                                                        <ul>
                                                            <!-- THE FIRST SLIDE -->
                                                            <li data-masterspeed="400" data-slotamount="7" data-transition="slotzoom-horizontal">
                                                                <img src="img/slider/front-page.png" alt="">
                                                                <!-- THE CAPTIONS OF THE FIRST SLIDE -->
                                                                <div class="caption lfl ltt start"
                                                                    data-x="45"
                                                                    data-y="433"
                                                                    data-endeasing="easeInOutBack"
                                                                    data-endspeed="1000"
                                                                    data-end="4250"
                                                                    data-easing="easeInOutBack"
                                                                    data-start="500"
                                                                    data-speed="750">
                                                                    <span style="font-size: 3em; color: #ffffff; font-family: Arial;">Create space in your busy life</span>
                                                                    <%--<img src="img/cover.jpg" alt="Image 1">--%>
                                                                </div>

                                                                <%--<div class="caption lfl ltl start"
                                                                    data-x="785"
                                                                    data-y="100"
                                                                    data-endeasing="easeInOutBack"
                                                                    data-endspeed="1000"
                                                                    data-end="4250"
                                                                    data-easing="easeInOutBack"
                                                                    data-start="750"
                                                                    data-speed="750">
                                                                    <img src="img/slider/ls2-img1.png" alt="Image 2" />This can be any image or picture
                                                                </div>

                                                                <div class="caption lfl ltb start"
                                                                    data-x="605"
                                                                    data-y="260"
                                                                    data-endeasing="easeInOutBack"
                                                                    data-endspeed="1000"
                                                                    data-end="4250"
                                                                    data-easing="easeInOutBack"
                                                                    data-start="1000"
                                                                    data-speed="750">
                                                                    <img src="img/slider/ls1-img3.png" alt="Image 3">
                                                                    This can be any image or picture
                                                                </div>--%>
                                                            </li>
                                                            <!-- THE SECOND SLIDE -->
                                                            <li data-masterspeed="400" data-slotamount="7" data-transition="papercut">
                                                                <img src="img/slider/1920.png" alt="">

                                                                <%--<div class="caption lft ltb start"
                                                                    data-x="620"
                                                                    data-y="120"
                                                                    data-endeasing="easeInOutCubic"
                                                                    data-endspeed="1000"
                                                                    data-end="4250"
                                                                    data-easing="easeInOutBack"
                                                                    data-start="500"
                                                                    data-speed="1000">
                                                                    <img src="img/slider/ls2-img1.png" alt="Image 1">
                                                                </div>

                                                                <div class="caption lft ltb start"
                                                                    data-x="620"
                                                                    data-y="155"
                                                                    data-endeasing="easeInOutCubic"
                                                                    data-endspeed="1000"
                                                                    data-end="4000"
                                                                    data-easing="easeInOutBack"
                                                                    data-start="750"
                                                                    data-speed="1000">
                                                                    <img src="img/slider/ls2-img2.png" alt="Image 2">
                                                                </div>


                                                                <div class="caption lft ltb start"
                                                                    data-x="765"
                                                                    data-y="225"
                                                                    data-endeasing="easeInOutCubic"
                                                                    data-endspeed="1000"
                                                                    data-end="3750"
                                                                    data-easing="easeInOutBack"
                                                                    data-start="1000"
                                                                    data-speed="1000">
                                                                    <img src="img/slider/ls2-img3.png" alt="Image 3">
                                                                </div>--%>

                                                                <div class="caption lft ltb start"
                                                                    data-x="853"
                                                                    data-y="100"
                                                                    data-endeasing="easeInOutBack"
                                                                    data-endspeed="3000"
                                                                    data-end="4500"
                                                                    data-easing="easeInOutBack"
                                                                    data-start="1250"
                                                                    data-speed="1000">
                                                                    <span style="font-size: 3em; color: #ffffff; font-family: Arial; line-height: 1em;">Relax with confidence<br />
                                                                        into the process of<br />
                                                                        becoming a mother</span>
                                                                    <%--<a href="#">
                                                                        <img src="img/slider/ls2-img4.png" alt="Image 4"></a>--%>
                                                                </div>
                                                            </li>
                                                            <!-- THE THIRD SLIDE -->
                                                            <%--<li data-masterspeed="400" data-slotamount="7" data-transition="3dcurtain-horizontal">
                                                                <img src="img/slider/flower.png" alt="">
                                                                <div class="caption lfb ltt start"
                                                                    data-x="0"
                                                                    data-y="0"
                                                                    data-easing="easeInOutBack"
                                                                    data-start="500"
                                                                    data-speed="1000">
                                                                    <%--  <img src="img/slider/ls3-img1.png" alt="Image 1">--%>


                                                            <%--                                                                <div class="caption lfb ltt start"
                                                                    data-autoplay="false"
                                                                    data-x="300"
                                                                    data-y="25"
                                                                    data-easing="easeInOutBack"
                                                                    data-start="500"
                                                                    data-speed="1000">
                                                                    <iframe width="590" height="450" src="//www.youtube.com/embed/fnbM-KXQhp8" frameborder="0" allowfullscreen></iframe>
                                                                </div>--%>
                                                        </ul>
                                                        <div class="tp-bannertimer tp-bottom"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <script type="text/javascript">
                                                //var tpj=jQuery;
                                                //$.noConflict();
                                                //<![CDATA[
                                                $(document).ready(function () {
                                                    $('.rs-fullscreen').css('margin-left', -1 * ($(window).width() - $('.container').width()) / 2 + 'px').width($(window).width());
                                                    if ($.fn.cssOriginal != undefined)
                                                        $.fn.css = $.fn.cssOriginal;
                                                    var api = $('.fullwidthbanner').revolution({
                                                        delay: 5000,
                                                        startheight: 500,
                                                        startwidth: 1170,
                                                        hideThumbs: 200,
                                                        thumbWidth: 100,	// Thumb With and Height and Amount (only if navigation Tyope set to thumb !)
                                                        thumbHeight: 50,
                                                        thumbAmount: 5,
                                                        navigationType: "none",	//bullet, thumb, none, both (No Thumbs In FullWidth Version !)
                                                        navigationArrows: "verticalcentered",	//nexttobullets, verticalcentered, none
                                                        navigationStyle: "round",	//round,square,navbar
                                                        touchenabled: "on",	// Enable Swipe Function : on/off
                                                        onHoverStop: "on",	// Stop Banner Timet at Hover on Slide on/off
                                                        navOffsetHorizontal: 0,
                                                        navOffsetVertical: 20,
                                                        stopAtSlide: -1,	// Stop Timer if Slide "x" has been Reached. If stopAfterLoops set to 0, then it stops already in the first Loop at slide X which defined. -1 means do not stop at any slide. stopAfterLoops has no sinn in this case.
                                                        stopAfterLoops: -1,	// Stop Timer if All slides has been played "x" times. IT will stop at THe slide which is defined via stopAtSlide:x, if set to -1 slide never stop automatic
                                                        hideCaptionAtLimit: 0,	// It Defines if a caption should be shown under a Screen Resolution ( Basod on The Width of Browser)
                                                        hideAllCaptionAtLilmit: 0,	// Hide all The Captions if Width of Browser is less then this value
                                                        hideSliderAtLimit: 0,	// Hide the whole slider, and stop also functions if Width of Browser is less than this value
                                                        shadow: 0,	//0 = no Shadow, 1,2,3 = 3 Different Art of Shadows  (No Shadow in Fullwidth Version !)
                                                        fullWidth: "on"	// Turns On or Off the Fullwidth Image Centering in FullWidth Modus
                                                    });
                                                    $(document).keyup(function (event) {
                                                        if ((event.keyCode == 37)) {
                                                            api.revprev();
                                                        } else if ((event.keyCode == 39)) {
                                                            api.revnext();
                                                        }
                                                    });
                                                });
                                                //]]
                                            </script>
                                        </div>
                                        <!--.module_cont -->
                                        <div class="clear">
                                            <!-- ClearFIX -->
                                        </div>
                                    </div>
                                    <!-- .row-fluid -->
                                    <%-- <div class="row-fluid">
                                        <div class="span3 module_cont module_iconboxes module_normal_padding">
                                            <div class="shortcode_iconbox">                                                
	                                            <div class="ico">
                                                	<span>*</span>
                                                </div>
                                                <div class="iconbox_body">
                                                    <h6 class="iconbox_title">Fresh &amp; Clean Design</h6>
                                                    Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos per vestibulum.
                                                </div>
                                            </div>
                                        </div><!--.module_cont -->
                                        <div class="span3 module_cont module_iconboxes module_normal_padding">
                                            <div class="shortcode_iconbox">                                                
                                            	<div class="ico">
                                                	<span>0</span>
                                                </div>
                                                <div class="iconbox_body">                                                
                                                    <h6 class="iconbox_title">Fully Responsive</h6>
                                                    Nunc justo lacus, molestie ut porta id, pharetra vestibulum proin sit amet aliquet massa phasellus id.
												</div>
                                            </div>
                                        </div><!--.module_cont -->
                                        <div class="span3 module_cont module_iconboxes module_normal_padding">
                                            <div class="shortcode_iconbox">
                                            	<div class="ico">
                                                	<span>s</span>
                                                </div>
                                                <div class="iconbox_body">                                                
                                                    <h6 class="iconbox_title">Very Flexible</h6>
                                                    Lorem ipsum dolor sit amet a vivamus, consectetur adipiscing elit. Praesent laoreet rutrum malesuada duis.
                                                </div>
                                            </div>
                                        </div><!--.module_cont -->
                                        <div class="span3 module_cont module_iconboxes module_normal_padding">
                                            <div class="shortcode_iconbox">
                                            	<div class="ico">
                                                	<span>w</span>
                                                </div>
                                                <div class="iconbox_body">                                                
                                                    <h6 class="iconbox_title">ONGOING SUPPORT</h6>
                                                    Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos per vestibulum.
                                                </div>
                                            </div>
                                        </div><!--.module_cont -->
                                    </div><!-- .row-fluid -->--%>
                                    <div class="row-fluid">
                                        <div class="span12 module_cont module_wall_grid">
                                            <ul id="og-grid" class="og-grid">
                                                <li>
                                                    <a href="Conscious-Birth-Hypnobirthing.aspx" data-largesrc="img/HB2.png" data-title="Hypnobirthing"
                                                        data-description="HypnoBirthing® teaches you the ease of using your own natural birthing instincts. With HypnoBirthing, you will be aware, fully in control and active, but still profoundly relaxed during your labour.<br/><br/>
                                                            Theoni is one of only a few certified HypnoBirthing® practitioners in South Africa and she has 8 years of experience in 
                                                            teaching this course. Using HypnoBirthing® methods she teaches mothers and partners techniques that facilitate safe and satisfying 
                                                            birthing. These include guided imagery, visualization, and special breathing.">
                                                        <img src="img/HB2.png" alt="img01" />
                                                        <span class="plus_icon"></span>
                                                    </a>
                                                    <div class="og-details">
                                                    </div>
                                                </li>
                                                <%--<li>
                                                    <a href="Conscious-Birth-Baby-Naming-Ceremonies.aspx" data-largesrc="img/pictures/0911_0251_large.png" data-title="Baby Naming Ceremony" data-description="
                                                        Conscious Birth offers a Baby Blessing Ceremony as a non-religious celebration of a new, precious and unique life. Facilitated by Theoni,
                                                         a Baby Blessing Ceremony is a joyous event where you can formally welcome your child into your world of friends and family and express your hopes and intentions for your child.">
                                                        <img src="img/pictures/0911_0251_small.png" alt="img02" />
                                                        <span class="plus_icon"></span>
                                                    </a>
                                                    <div class="og-details">
                                                    </div>
                                                </li>--%>
                                                <li>
                                                    <a href="Conscious-Birth-Blissful-Pregnancy.aspx" data-largesrc="img/blissful-pregnancy.png" data-title="blissful pregnancy" data-description="Blissful pregnancy entails tapping into the feminine power within so that you can overcome any challenge that you are faced with as a woman and discover what it means for you to be a woman in the world.">
                                                        <img src="img/blissful-pregnancy1.png" alt="img01" />
                                                        <span class="plus_icon"></span>
                                                    </a>
                                                    <div class="og-details">
                                                    </div>
                                                </li>
                                                <%--<li>
                                                    <a href="Conscious-Birth-Belly-Casting.aspx" data-largesrc="img/belly-casting1.png" data-title="Belly Casting" data-description="
                                                        Create an impactful memory and celebrate your blossoming belly with a belly casting.">
                                                        <img src="img/belly-casting2.png" alt="img01" />
                                                        <span class="plus_icon"></span>
                                                    </a>
                                                    <div class="og-details">
                                                    </div>
                                                </li>--%>
                                                <li>
                                                    <a href="Conscious-Birth-Kahuna-Massage-Experience-For-Mothers-And-Babies.aspx" data-largesrc="img/kahuna-massage-2.jpg" data-title="kahuna massage" data-description="A unique and customised massage experience to be enjoyed by non-pregnant clients.">
                                                        <img src="img/kahuna-massage-2__.png" alt="img01" />
                                                        <span class="plus_icon"></span>
                                                    </a>
                                                    <div class="og-details">
                                                    </div>
                                                </li>
                                                <li>
                                                    <a href="Conscious-Birth-Pregnancy-Yoga.aspx" data-largesrc="img/pregnancy-yoga.png" data-title="Pregnancy Yoga" data-description="
                                                        Pregnancy yoga is a quiet space for your soul that shifts awareness from your active mind towards a deep, quiet place within you; it develops physical and mental 
                                                        strength and fosters the understanding that your seemingly overwhelming fears can be conquered.">
                                                        <img src="img/pregnancy-yoga.png" alt="img07" />
                                                        <span class="plus_icon"></span>
                                                    </a>
                                                    <div class="og-details">
                                                    </div>
                                                </li>
                                                <br />
                                                <li>
                                                    <a href="Conscious-Birth-Moms-and-Babes-Yoga.aspx" data-largesrc="img/moms-and-babes-yoga1.png" data-title="Mom's and Babes Yoga"
                                                        data-description="Moms and Babes Yoga provides the ultimate bonding 'us-time' for Moms and their babies and an opportunity for Moms to socialise with other Moms.">
                                                        <img src="img/moms-and-babes-yoga2.png" alt="img04" />
                                                        <span class="plus_icon"></span>
                                                    </a>
                                                    <div class="og-details">
                                                    </div>
                                                </li>
                                                <%--<li>
                                                    <a href="Conscious-Birth-Moms-and-Babes-Yoga.aspx" data-largesrc="img/moms-&-babes-yoga2.png" data-title="Moms and babes yoga" data-description="Moms and Babes Yoga provides the ultimate bonding “us-time” for Moms and their babies and an opportunity for Moms to socialise with other Moms.">
                                                        <img src="img/moms-&-babes-yoga2.png" alt="img01" />
                                                        <span class="plus_icon"></span>
                                                    </a>
                                                    <div class="og-details">
                                                    </div>
                                                </li>--%>
                                                <li>
                                                    <a href="Conscious-Birth-Blissful-Mother.aspx" data-largesrc="img/blissful-mother.png" data-title="Blissful Mother" data-description="
                                                       Dealing with all the inherent complexities that come with one of the most important titles in the world, that of Mother.">
                                                        <img src="img/blissful-mother1.png" alt="img08" />
                                                        <span class="plus_icon"></span>
                                                    </a>
                                                    <div class="og-details">
                                                    </div>
                                                </li>
                                                <%--<li>
                                                    <a href="Conscious-Birth-Blessing-Way.aspx" data-largesrc="img/Blessingway-ceremony1.png" data-title="Blessing Way" data-description="
                                                           An empowering ceremony that serves as a rite of passage for the mother-to-be.">
                                                        <img src="img/Blessingway-ceremony2.png" alt="img05" />
                                                        <span class="plus_icon"></span>
                                                    </a>
                                                    <div class="og-details">
                                                    </div>
                                                </li>--%>
                                                <%--<li>
                                                    <a href="Conscious-Birth-Postnatal-Massage.aspx" data-largesrc="img/pictures/postnatal-massage1_large.png" data-title="Post-natal Care and massage" data-description="
                                                            More than a massage, this individualised service includes expert advice and compassionate support aimed at easing the overwhelming strains of postpartum life.">
                                                        <img src="img/pictures/postnatal-massage1_small.png" alt="img06" />
                                                        <span class="plus_icon"></span>
                                                    </a>
                                                    <div class="og-details">
                                                    </div>
                                                </li>--%>
                                                <li>
                                                    <a href="Conscious-Birth-Kahuna-Pregnancy-Massage.aspx" data-largesrc="img/kahunapregnancymassage.jpeg" data-title="Kahuna pregnancy massage experience" data-description="
                                                            The Kahuna Pregnancy Massage, which works on a physical and emotional level, is a must for all pregnant women.">
                                                        <img src="img/kahunapregnancymassage1.jpg" />" alt="img03" />
                                                        <span class="plus_icon"></span>
                                                    </a>
                                                    <div class="og-details">
                                                    </div>
                                                </li>
                                                <li>
                                                    <a href="Conscious-Birth-Blissful-Women.aspx" data-largesrc="img/Blissful-Woman.png" data-title="blissful woman" data-description="Blissful woman entails tapping into the feminine power within so that you can overcome any challenge that you are faced with as a woman and discover what it means for you to be a woman in the world.">
                                                        <img src="img/Blissful-Woman-2.png" alt="img01" />
                                                        <span class="plus_icon"></span>
                                                    </a>
                                                    <div class="og-details">
                                                    </div>
                                                </li>
                                            </ul>
                                            <%--<ul id="og-grid" class="og-grid">
                                                      <li>
                                                    <a href="javascript:void(0)" data-largesrc="img/pictures/large/gallery08.jpg" data-title="Workshops" data-description="
                                                        Conscious Birth’s pregnancy workshops are a journey of connection carefully honed by Theoni to include elements of relaxation, transformation and support; they 
                                                        are soul food for any parents-to-be. The workshops also provide support in the form of knowledge and information on pregnancy, birth and babies. Please check on the 
                                                        calendar to see when the next workshop is scheduled.
                                                        ">
                                                        <img src="img/pictures/gallery08.jpg" alt="img08"/>
                                                        <span class="plus_icon"></span>
                                                    </a>
                                                </li>
                                            </ul>--%>
                                            <script src="js/grid.js"></script>
                                            <script>
                                                $(function () {
                                                    Grid.init();
                                                });
                                            </script>
                                        </div>
                                        <!--.module_cont -->
                                    </div>
                                    <!-- .row-fluid -->
                                    <div class="module_line_trigger" data-background="#ffeec9 url(img/VV.png) no-repeat center" data-top-padding="bottom_padding_huge" data-bottom-padding="module_big_padding">
                                        <div class="row-fluid">
                                            <div class="span12 module_cont module_promo_text module_huge_padding">
                                                <div class="shortcode_promoblock">
                                                    <div class="promo_button_block type2">
                                                       <a href="Conscious-Birth-Products.aspx" class="promo_button"> View our Products </a>
                                                    </div>
                                                    
                                                </div>
                                            </div>
                                            <!--.module_cont -->
                                            <div class="clear">
                                                <!-- ClearFIX -->
                                            </div>
                                        </div>
                                        <!-- .row-fluid -->
                                    </div>
                                    <!-- .module_line_trigger -->
                                    <%-- <div class="row-fluid">
                                        <div class="span12 module_cont module_big_padding2 module_diagramm">
                                            <div class="shortcode_diagramm_shortcode diagramm">
                                                <ul class="skills_list">
                                                    <li class="skill_li">
														<div class="chart" data-percent="75" data-barColor="#91b000" data-trackColor="#cccccc">75<span>%</span></div>
                                                        <h6>Pregnancy Massage</h6>
                                                        <div class="skill_descr">Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos per vestibulum.</div>
													</li>                                                
                                                    <li class="skill_li">
														<div class="chart" data-percent="19" data-barColor="#91b000" data-trackColor="#cccccc">19<span>%</span></div>
                                                        <h6>Postnatal Massage</h6>
                                                        <div class="skill_descr">Lorem ipsum dolor sit amet a vivamus, consectetur adipiscing elit. Praesent laoreet rutrum malesuada dus.</div>
													</li>
                                                    <li class="skill_li">
														<div class="chart" data-percent="58" data-barColor="#91b000" data-trackColor="#cccccc">58<span>%</span></div>
                                                        <h6>Kahuna Massage</h6>
                                                        <div class="skill_descr">A tellus feugiat fermentum mauris eu neque eu mi imperdiet in hac habitasse platea dictumst. Ut in mauris libero.</div>
													</li>
                                                    <li class="skill_li">
														<div class="chart" data-percent="87" data-barColor="#91b000" data-trackColor="#cccccc">87<span>%</span></div>
                                                        <h6>HYPNOBIRTHING</h6>
                                                        <div class="skill_descr">Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos per vestibulum.</div>
													</li>
												</ul>
                                            	<script type="text/javascript" src="js/jquery.easy-pie-chart.js"></script>
                                                <script type="text/javascript">
                                                    $(document).ready(function () {
                                                        $('.skills_list').each(function () {
                                                            $(this).addClass('has-items' + $(this).find('.skill_li').size());
                                                            $(this).find('.skill_li').width(Math.floor(($(this).width() + parseInt($(this).css('margin-left')) * $(this).find('.skill_li').size()) / $(this).find('.skill_li').size()));
                                                            $(this).addClass('ready');
                                                        });
                                                        if (jQuery(window).width() > 760) {
                                                            jQuery('.skill_li').waypoint(function () {
                                                                $('.chart').each(function () {
                                                                    $(this).easyPieChart({
                                                                        barColor: $(this).attr('data-barColor'),
                                                                        trackColor: false,
                                                                        scaleColor: false,
                                                                        lineCap: 'square',
                                                                        lineWidth: 9,
                                                                        size: 88,
                                                                        animate: 1500
                                                                    });
                                                                });
                                                            }, { offset: 'bottom-in-view' });
                                                        } else {
                                                            $('.chart').each(function () {
                                                                $(this).easyPieChart({
                                                                    barColor: $(this).attr('data-barColor'),
                                                                    trackColor: $(this).attr('data-trackColor'),
                                                                    scaleColor: false,
                                                                    lineCap: 'square',
                                                                    lineWidth: 9,
                                                                    size: 88,
                                                                    animate: 1500
                                                                });
                                                            });
                                                        }
                                                    });
												</script>                                                
                                            </div>
                                        </div><!-- .module_cont -->
                                    </div>--%>
                                    <!-- .row-fluid -->
                                    <%--  <div class="row-fluid">
                                        <div class="span12 module_cont module_promo_text module_normal_padding45">
                                            <div class="shortcode_promoblock">
                                                <div class="promo_text_block">
                                                    <h1>Right here we've got something you gonna love!</h1>
                                                </div>
                                            </div>
                                        </div>
                                        <!--.module_cont -->
                                    </div>--%>
                                    <!-- .row-fluid -->
                                    <%-- <div class="row-fluid">
                                        <div class="span12 module_cont module_feature_posts center_title">
                                            <div class="bg_title">
                                                <div class="title_wrapper"><a href="javascript:void(0)" class="btn_carousel_left"></a>
                                                    <h5 class="headInModule">Our blog. Featured Posts</h5>
                                                    <a href="javascript:void(0)" class="btn_carousel_right"></a></div>
                                            </div>
                                            <div class="featured_slider">
                                                <div class="carouselslider featured_posts items4" data-count="4">
                                                    <ul class="item_list">
                                                        <li>
                                                            <div class="item">
                                                                <div class="img_block">
                                                                    <img src="img/pictures/image6.jpg" alt="" width="270" height="190">
                                                                    <div class="gallery_fadder"></div>
                                                                    <span class="carousel_posttype posttype-image"></span>
                                                                </div>
                                                                <div class="carousel_body">
                                                                    <div class="carousel_title">
                                                                        <h6><a href="#">Lorem ipsum dolor</a></h6>
                                                                    </div>
                                                                    <div class="carousel_desc">
                                                                        <div class="exc">Consectetur adipiscing elit ac Praesent laoreet rutrum suada duis ac nisi, ac sapiens ccumsan elementum acnullam</div>
                                                                    </div>
                                                                    <div class="meta"><span>17 April 2019</span><span><a href="#">Read more!</a></span></div>
                                                                </div>
                                                            </div>
                                                        </li>
                                                        <li>
                                                            <div class="item">
                                                                <div class="img_block">
                                                                    <img src="img/pictures/image5.jpg" alt="" width="270" height="190">
                                                                    <div class="gallery_fadder"></div>
                                                                    <span class="carousel_posttype posttype-video"></span>
                                                                </div>
                                                                <div class="carousel_body">
                                                                    <div class="carousel_title">
                                                                        <h6><a href="#">Dolor sit amet</a></h6>
                                                                    </div>
                                                                    <div class="carousel_desc">
                                                                        <div class="exc">Nunc justo lacus, molestie ut porta id pharetra vestibulum proin sit amet aliquet massa phasellus id tristique mi</div>
                                                                    </div>
                                                                    <div class="meta"><span>11 April 2019</span><span><a href="#">Read more!</a></span></div>
                                                                </div>
                                                            </div>
                                                        </li>
                                                        <li>
                                                            <div class="item">
                                                                <div class="img_block">
                                                                    <img src="img/pictures/image4.jpg" alt="" width="270" height="190">
                                                                    <div class="gallery_fadder"></div>
                                                                    <span class="carousel_posttype posttype-gallery"></span>
                                                                </div>
                                                                <div class="carousel_body">
                                                                    <div class="carousel_title">
                                                                        <h6><a href="#">Nunc justo lacus</a></h6>
                                                                    </div>
                                                                    <div class="carousel_desc">
                                                                        <div class="exc">Consectetur adipiscing elit ac Praesent laoreet rutrum suada duis ac nisi, ac sapiens ccumsan elementum acnullam</div>
                                                                    </div>
                                                                    <div class="meta"><span>9 April 2019</span><span><a href="#">Read more!</a></span></div>
                                                                </div>
                                                            </div>
                                                        </li>
                                                        <li>
                                                            <div class="item">
                                                                <div class="img_block">
                                                                    <img src="img/pictures/image3.jpg" alt="" width="270" height="190">
                                                                    <div class="gallery_fadder"></div>
                                                                    <span class="carousel_posttype posttype-image"></span>
                                                                </div>
                                                                <div class="carousel_body">
                                                                    <div class="carousel_title">
                                                                        <h6><a href="#">Lorem ipsum dolor</a></h6>
                                                                    </div>
                                                                    <div class="carousel_desc">
                                                                        <div class="exc">Consectetur adipiscing elit ac Praesent laoreet rutrum suada duis ac nisi, ac sapiens ccumsan elementum acnullam</div>
                                                                    </div>
                                                                    <div class="meta"><span>7 April 2019</span><span><a href="#">Read more!</a></span></div>
                                                                </div>
                                                            </div>
                                                        </li>
                                                        <li>
                                                            <div class="item">
                                                                <div class="img_block">
                                                                    <img src="img/pictures/image2.jpg" alt="" width="270" height="190">
                                                                    <div class="gallery_fadder"></div>
                                                                    <span class="carousel_posttype posttype-link"></span>
                                                                </div>
                                                                <div class="carousel_body">
                                                                    <div class="carousel_title">
                                                                        <h6><a href="#">Dolor sit amet</a></h6>
                                                                    </div>
                                                                    <div class="carousel_desc">
                                                                        <div class="exc">Nunc justo lacus, molestie ut porta id pharetra vestibulum proin sit amet aliquet massa phasellus id tristique mi</div>
                                                                    </div>
                                                                    <div class="meta"><span>7 April 2019</span><span><a href="#">Read more!</a></span></div>
                                                                </div>
                                                            </div>
                                                        </li>
                                                        <li>
                                                            <div class="item">
                                                                <div class="img_block">
                                                                    <img src="img/pictures/image1.jpg" alt="" width="270" height="190">
                                                                    <div class="gallery_fadder"></div>
                                                                    <span class="carousel_posttype posttype-image"></span>
                                                                </div>
                                                                <div class="carousel_body">
                                                                    <div class="carousel_title">
                                                                        <h6><a href="#">Nunc justo lacus</a></h6>
                                                                    </div>
                                                                    <div class="carousel_desc">
                                                                        <div class="exc">Consectetur adipiscing elit ac Praesent laoreet rutrum suada duis ac nisi, ac sapiens ccumsan elementum acnullam</div>
                                                                    </div>
                                                                    <div class="meta"><span>5 April 2019</span><span><a href="#">Read more!</a></span></div>
                                                                </div>
                                                            </div>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="clear"></div>
                                            </div>
                                            <!-- .featured_slider -->
                                        </div>
                                        <!-- .module_cont -->
                                    </div>--%>
                                    <!-- .row-fluid -->
                                    <%--<div class="row-fluid">
                                        <div class="span12 module_cont module_partners center_title module_big_padding2">
                                            <div class="bg_title">
                                                <div class="title_wrapper">
                                                    <a href="javascript:void(0)" class="btn_carousel_left"></a>
                                                    <h5 class="headInModule">Our Partners</h5>
                                                    <a href="javascript:void(0)" class="btn_carousel_right"></a>
                                                </div>
                                            </div>
                                            <div class="module_content sponsors_works carouselslider items5" data-count="5">
                                                <ul>
                                                    <li>
                                                        <div class="item">
                                                            <a href="#charity-html5css3-website-template/3180454" target="_blank">
                                                                <img src="img/pictures/partners2.png" alt=""></a>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="item">
                                                            <a href="#point-business-responsive-wp-theme/4319087" target="_blank">
                                                                <img src="img/pictures/partners4.png" alt=""></a>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="item">
                                                            <a href="#cleanspace-retina-ready-business-wp-theme/3776000" target="_blank">
                                                                <img src="img/pictures/partners1.png" alt=""></a>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="item">
                                                            <a href="#yellowproject-multipurpose-retina-wp-theme/4066662" target="_blank">
                                                                <img src="img/pictures/partners3.png" alt=""></a>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="item">
                                                            <a href="#showroom-portfolio-retina-ready-wp-theme/3473628" target="_blank">
                                                                <img src="img/pictures/partners5.png" alt=""></a>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="item">
                                                            <a href="themeforest.net/item/incipiens-responsive-portfolio-wordpress-theme/2762691" target="_blank">
                                                                <img src="img/pictures/partners6.png" alt=""></a>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="item">
                                                            <a href="themeforest.net/item/hq-photography-responsive-wp-theme/3200962" target="_blank">
                                                                <img src="img/pictures/partners9.png" alt=""></a>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="item">
                                                            <a href="#incipiens-responsive-portfolio-wordpress-theme/2762691" target="_blank">
                                                                <img src="img/pictures/partners11.png" alt=""></a>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>--%>
                                </div>
                                <!-- .contentarea -->
                            </div>
                            <div class="left-sidebar-block span3">
                                <aside class="sidebar">
                                    //Sidebar Text
                                </aside>
                            </div>
                            <!-- .left-sidebar -->
                        </div>
                        <div class="clear">
                            <!-- ClearFix -->
                        </div>
                    </div>
                    <!-- .fl-container -->
                    <div class="right-sidebar-block span3">
                        <aside class="sidebar">
                        </aside>
                    </div>
                    <!-- .right-sidebar -->
                    <div class="clear">
                        <!-- ClearFix -->
                    </div>
                </div>
            </div>
            <!-- .container -->
        </div>
        <!-- .content_wrapper -->
    </div>
</asp:Content>

