using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Data.SqlClient;

/// <summary>
/// Summary description for clsDeliveryInfos
/// </summary>
public class clsDeliveryInfos
{
    #region MEMBER VARIABLES

        private int m_iDeliveryInfoID;
        private DateTime m_dtAdded;
        private int m_iAddedBy;
        private DateTime m_dtEdited;
        private int m_iEditedBy;
        private String m_strContactPerson;
        private String m_strContactNumber;
        private String m_strEmailAddress;
        private String m_strDeliveryAddress;
        private bool m_bIsDeleted;
        
    #endregion 

    #region PROPERTIES

        public int iDeliveryInfoID
        {
            get
            {
                return m_iDeliveryInfoID;
            }
        }

        public DateTime dtAdded
        {
            get
            {
                return m_dtAdded;
            }
            set
            {
                m_dtAdded = value;
            }
        }

        public int iAddedBy
        {
            get
            {
                return m_iAddedBy;
            }
            set
            {
                m_iAddedBy = value;
            }
        }

        public DateTime dtEdited
        {
            get
            {
                return m_dtEdited;
            }
            set
            {
                m_dtEdited = value;
            }
        }

        public int iEditedBy
        {
            get
            {
                return m_iEditedBy;
            }
            set
            {
                m_iEditedBy = value;
            }
        }

        public String strContactPerson
        {
            get
            {
                return m_strContactPerson;
            }
            set
            {
                m_strContactPerson = value;
            }
        }

        public String strContactNumber
        {
            get
            {
                return m_strContactNumber;
            }
            set
            {
                m_strContactNumber = value;
            }
        }

        public String strEmailAddress
        {
            get
            {
                return m_strEmailAddress;
            }
            set
            {
                m_strEmailAddress = value;
            }
        }

        public String strDeliveryAddress
        {
            get
            {
                return m_strDeliveryAddress;
            }
            set
            {
                m_strDeliveryAddress = value;
            }
        }

        public bool bIsDeleted
        {
            get
            {
                return m_bIsDeleted;
            }
            set
            {
                m_bIsDeleted = value;
            }
        }

    
    #endregion
    
    #region CONSTRUCTORS

    public clsDeliveryInfos()
    {
        m_iDeliveryInfoID = 0;
    }

    public clsDeliveryInfos(int iDeliveryInfoID)
    {
        m_iDeliveryInfoID = iDeliveryInfoID;
        GetData();
    }

    #endregion

    #region PUBLIC METHODS

        public virtual void Update()
        {
            try
            {
                if (iDeliveryInfoID == 0)
                {
                    //### Assign values to the parameter list for each corresponding column in the DB
                    SqlParameter[] sqlParametersInsert = new SqlParameter[] 
                    {
                        new SqlParameter("@dtAdded", m_dtAdded),
                        new SqlParameter("@iAddedBy", m_iAddedBy),
                        new SqlParameter("@strContactPerson", m_strContactPerson),
                        new SqlParameter("@strContactNumber", m_strContactNumber),
                        new SqlParameter("@strEmailAddress", m_strEmailAddress),
                        new SqlParameter("@strDeliveryAddress", m_strDeliveryAddress)                  
                  };

                  //### Add
                  m_iDeliveryInfoID = (int)clsDataAccess.ExecuteScalar("spDeliveryInfosInsert", sqlParametersInsert);                    
                    }
                    else
                    {
                    //### Assign values to the parameter list for each corresponding column in the DB
                    SqlParameter[] sqlParametersUpdate = new SqlParameter[] 
                    {
                         new SqlParameter("@iDeliveryInfoID", m_iDeliveryInfoID),
                         new SqlParameter("@dtEdited", m_dtEdited),
                         new SqlParameter("@iEditedBy", m_iEditedBy),
                         new SqlParameter("@strContactPerson", m_strContactPerson),
                         new SqlParameter("@strContactNumber", m_strContactNumber),
                         new SqlParameter("@strEmailAddress", m_strEmailAddress),
                         new SqlParameter("@strDeliveryAddress", m_strDeliveryAddress)
                    };
                    //### Update
                    clsDataAccess.Execute("spDeliveryInfosUpdate", sqlParametersUpdate);
                    }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void Delete(int iDeliveryInfoID)
        {
        //### Populate
        SqlParameter[] sqlParameter = new SqlParameter[]
        {
            new SqlParameter("@iDeliveryInfoID", iDeliveryInfoID)
        };
        //### Executes delete sp
        clsDataAccess.Execute("spDeliveryInfosDelete", sqlParameter);
        }

        public static DataTable GetDeliveryInfosList()
        {
            SqlParameter[] EmptySqlParameter = new SqlParameter[]{};
            return clsDataAccess.GetDataTable("spDeliveryInfosList", EmptySqlParameter);
        }
        public static DataTable GetDeliveryInfosList(string strFilterExpression, string strSortExpression)
        {
            DataView dvDeliveryInfosList = new DataView();

            SqlParameter[] EmptySqlParameter = new SqlParameter[] { };
            dvDeliveryInfosList = clsDataAccess.GetDataView("spDeliveryInfosList", EmptySqlParameter);
            dvDeliveryInfosList.RowFilter = strFilterExpression;
            dvDeliveryInfosList.Sort = strSortExpression;

            return dvDeliveryInfosList.ToTable();
        }
    #endregion

    #region PROTECTED METHODS

        protected virtual void GetData()
        {
            try
                {
                //### Populate
                SqlParameter[] sqlParameter = new SqlParameter[] 
                {
                    new SqlParameter("@iDeliveryInfoID", m_iDeliveryInfoID)
                };
                DataRow drRecord = clsDataAccess.GetRecord("spDeliveryInfosGetRecord", sqlParameter);

                m_dtAdded = Convert.ToDateTime(drRecord["dtAdded"]);
                m_iAddedBy = Convert.ToInt32(drRecord["iAddedBy"]);

                if (drRecord["dtEdited"] != DBNull.Value)
                    m_dtEdited = Convert.ToDateTime(drRecord["dtEdited"]);

                if (drRecord["iEditedBy"] != DBNull.Value)
                    m_iEditedBy = Convert.ToInt32(drRecord["iEditedBy"]);

                m_strContactPerson = drRecord["strContactPerson"].ToString();
                m_strContactNumber = drRecord["strContactNumber"].ToString();
                m_strEmailAddress = drRecord["strEmailAddress"].ToString();
                m_strDeliveryAddress = drRecord["strDeliveryAddress"].ToString();
                m_bIsDeleted = Convert.ToBoolean(drRecord["bIsDeleted"]);
 
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    #endregion
}