using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class CMS_frmColoursAddModify : System.Web.UI.Page
{
    clsUsers clsUsers;
    clsColours clsColours;

    #region EVENT METHODS

    protected void Page_Load(object sender, EventArgs e)
    {
        //### Check if session clsUser exists
        if (Session["clsUsers"] == null)
        {
            {
                //### Redirect back to login
                Response.Redirect("../CMSLogin.aspx");
            }
        }
        clsUsers = (clsUsers)Session["clsUsers"];

        if (!IsPostBack)
        {

            //### If the iColourID is passed through then we want to instantiate the object with that iColourID
            if ((Request.QueryString["iColourID"] != "") && (Request.QueryString["iColourID"] != null))
            {
                clsColours = new clsColours(Convert.ToInt32(Request.QueryString["iColourID"]));

                //### Populate the form
                popFormData();
            }
            else
            {
                clsColours = new clsColours();
            }
            Session["clsColours"] = clsColours;
        }
        else
        {
            clsColours = (clsColours)Session["clsColours"];
        }
    }

    protected void lnkbtnBack_Click(object sender, EventArgs e)
    {
        //### Go back to previous page
        Response.Redirect("frmColoursView.aspx");
    }

    protected void lnkbtnSave_Click(object sender, EventArgs e)
    {
        //### Validate registration process
        bool bCanSave = true;

       bCanSave = clsValidation.IsNullOrEmpty(txtTitle, bCanSave);
       bCanSave = clsValidation.IsNullOrEmpty(txtColourCode, bCanSave);

        if (bCanSave == true)
        {
            mandatoryDiv.Visible = false;
            lblValidationMessage.Text = "<div class=\"madatoryPaddingDiv\"><img src=\"images/Validation/imgFaceHappy.png\" alt='' title=''/><div class=\"validationMessage\">Colour added successfully</div></div>";
            SaveData();
        }
        else
        {
            mandatoryDiv.Visible = true;
            lblValidationMessage.Text = "<div class=\"madatoryPaddingDiv\"><img src=\"images/Validation/imgFaceSad.png\" alt='' title=''/><div class=\"validationMessage\">Please fill out all mandatory fields - Colour not added</div></div>";
        }
    }

    protected void lnkbtnClear_Click(object sender, EventArgs e)
    {
        mandatoryDiv.Visible = false;

        txtTitle.Text = "";
        clsValidation.SetValid(txtTitle);
    }

    #endregion 

    #region POPULATE DATA METHODS

    private void popFormData()
    {
         txtTitle.Text = clsColours.strTitle;
         txtColourCode.Text = clsColours.strColourCode;
    }
    
    #endregion

    #region SAVE DATA METHODS

    private void SaveData()
    {
        //### Add / Update
        clsColours.dtAdded = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
        clsColours.iAddedBy = clsUsers.iUserID;
        clsColours.dtEdited = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
        clsColours.iEditedBy = clsUsers.iUserID;
        clsColours.strTitle = txtTitle.Text;
        clsColours.strColourCode = txtColourCode.Text;

        clsColours.Update();

        Session["dtColoursList"] = null;

        //### Go back to view page
        Response.Redirect("frmColoursView.aspx");
    }

    #endregion
}