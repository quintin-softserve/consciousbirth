﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Conscious_Birth_Thank_You : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        clearCookie();
        SetItemPurchesd();
    }

    private static void clearCookie()
    {
        HttpCookie aCookie = new HttpCookie("OrderItems");
        aCookie.Values["OrderItemInfo"] = null;
        aCookie.Expires = DateTime.Now.AddMonths(-1);
        HttpContext.Current.Response.Cookies.Add(aCookie);
        HttpContext.Current.Response.Cookies["OrderItems"]["OrderItemInfo"] = null;
        HttpContext.Current.Response.Cookies["OrderItems"].Expires = DateTime.Now.AddMonths(-1);
        HttpContext.Current.Session["loggedOutOrder"] = "0";
    }
    private void SetItemPurchesd()
    {

        //Grab the cookie
        HttpCookie cookie = Request.Cookies["iUserProductWishlistLinkID"];
        if (cookie == null)
        {
        }
        else
        {
            String strCookieValue = cookie.Value.ToString();
            clsUserProductWishlistLink clsUserProductWishlistLink = new clsUserProductWishlistLink(Convert.ToInt32(strCookieValue));
            clsUserProductWishlistLink.bisPurchased = true;
            clsUserProductWishlistLink.Update();
            cookie.Values["iUserProductWishlistLinkID"] = null;
            cookie.Expires = DateTime.Now.AddMonths(-1);
            HttpContext.Current.Response.Cookies["iUserProductWishlistLinkID"].Expires = DateTime.Now.AddMonths(-1);
            

        }
    }
}