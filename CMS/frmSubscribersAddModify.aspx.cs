﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class CMS_frmSubscribersAddModify : System.Web.UI.Page
{
    clsSubscribers clsSubscribers;
    clsSubscribers clsSubscribersRecord;
    clsUsers clsUsers;

    #region EVENT METHODS

    protected void Page_Load(object sender, EventArgs e)
    {
        //### Check if session clsSubscriber exists
        if (Session["clsSubscribers"] == null)
        {
            //### Redirect back to login
            //Response.Redirect("../CMSLogin.aspx");
        }
        clsUsers = (clsUsers)Session["clsUsers"];
        clsSubscribers = (clsSubscribers)Session["clsSubscribers"];

        if (!IsPostBack)
        {
            //### If the iSubscriberID is passed through then we want to instantiate the obect with that iSubscriberID
            if (Request.QueryString["iSubscriberID"] != "" && Request.QueryString["iSubscriberID"] != null)
            {
                clsSubscribersRecord = new clsSubscribers(Convert.ToInt32(Request.QueryString["iSubscriberID"]));

                //### Populate the form
                popFormData();

                popValidFields();

                //### Hide password rows if editing
            }
            else
            {
                clsSubscribersRecord = new clsSubscribers();
            }

            Session["clsSubscribersRecord"] = clsSubscribersRecord;
        }
        else
        {
            clsSubscribersRecord = (clsSubscribers)Session["clsSubscribersRecord"];
        }
    }

    protected void lnkbtnBack_Click(object sender, EventArgs e)
    {
        //### Go back to previous page
        Response.Redirect("frmSubscribersView.aspx");
    }

    protected void lnkbtnSave_Click(object sender, EventArgs e)
    {
        //### Validate registration process
        bool bCanSave = true;

        bCanSave = clsValidation.IsNullOrEmpty(txtName);
        bCanSave = clsValidation.IsNullOrEmpty(txtSurname);
        bCanSave = clsValidation.IsNullOrEmpty(txtEmail);

        //### Hide password rows if editing
      
        if ((bCanSave == true) && ((Convert.ToBoolean(hfCanSave.Value)) == true))
        {
            mandatoryDiv.Visible = false;
            lblValidationMessage.Text = "<div class=\"madatoryPaddingDiv\"><img src=\"images/Validation/imgFaceHappy.png\" alt=\"\" title=\"\"/><div class=\"validationMessage\">Subscriber ADDED SUCCESSFULLY</div></div>";
            SaveData();
        }
        else
        {
            mandatoryDiv.Visible = true;
            lblValidationMessage.Text = "<div class=\"madatoryPaddingDiv\"><img src=\"images/Validation/imgFaceSad.png\" alt=\"\" title=\"\"/><div class=\"validationMessage\">Please fill out all mandatory fields - Subscriber NOT ADDED</div></div>";

            popValidFields();
        }
    }

    protected void lnkbtnClear_Click(object sender, EventArgs e)
    {
        mandatoryDiv.Visible = false;

        txtName.Text = "";

        clsValidation.SetValid(txtName);

        txtSurname.Text = "";
        clsValidation.SetValid(txtSurname);

        txtEmail.Text = "";
        clsValidation.SetValid(txtEmail);

    }

    #endregion

    #region POPULATE DATA METHODS

    private void popFormData()
    {
        txtName.Text = clsSubscribersRecord.strFirstName;
        txtSurname.Text = clsSubscribersRecord.strSurname;
        txtEmail.Text = clsSubscribersRecord.strEmailAddress;
        hfCanSave.Value = "true";
    }

    private void popValidFields()
    {
        //### Make the relevant mandatory fields green
        string strScript = "";

        if (!String.IsNullOrEmpty(txtName.Text))
        {
            strScript += "setValidFile('" + txtName.ClientID + "',true);";
        }
        if (!String.IsNullOrEmpty(txtSurname.Text))
        {
            strScript += "setValidFile('" + txtSurname.ClientID + "',true);";
        }
        if (!String.IsNullOrEmpty(txtEmail.Text))
        {
            strScript += "setValidFile('" + txtEmail.ClientID + "',true);";
        }

        ScriptManager.RegisterStartupScript(this, this.GetType(), "mandatoryFields", strScript + strScript, true);
    }

    #endregion

    #region SAVE DATA METHODS

    private void SaveData()
    {
        //### Add / Update
        clsSubscribersRecord.dtAdded = Convert.ToDateTime(DateTime.Now.ToString("dd MMM yyyy"));
        clsSubscribersRecord.iAddedBy = clsUsers.iUserID;
        clsSubscribersRecord.dtEdited = Convert.ToDateTime(DateTime.Now.ToString("dd MMM yyyy"));
        clsSubscribersRecord.iEditedBy = clsUsers.iUserID;
        clsSubscribersRecord.strFirstName = txtName.Text;
        clsSubscribersRecord.strSurname = txtSurname.Text;
        clsSubscribersRecord.strEmailAddress = txtEmail.Text;

        clsSubscribersRecord.Update();

        //### redirect back to view page
        Response.Redirect("frmSubscribersView.aspx");
    }

    #endregion
}