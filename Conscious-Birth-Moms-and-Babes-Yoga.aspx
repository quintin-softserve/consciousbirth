﻿<%@ Page Title="Moms and Babies Yoga :: Conscious Birth" Language="C#" MasterPageFile="~/ConsciousBirth.master" AutoEventWireup="true" CodeFile="Conscious-Birth-Moms-and-Babes-Yoga.aspx.cs" Inherits="Conscious_Birth_Moms_and_Babies_Yoga" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date(); a = s.createElement(o),
            m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-53160457-1', 'auto');
        ga('send', 'pageview');

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="main_wrapper">
        <!-- C O N T E N T -->
        <div class="content_wrapper">
            <div class="page_title_block">
                <div class="container">
                    <h2 class="title">Moms and Babes Yoga</h2>
                </div>
            </div>
            <div class="container">
                <div class="content_block no-sidebar row">
                    <div class="fl-container span12">
                        <div class="row">
                            <div class="posts-block span12">
                                <div class="contentarea">

                                    <div class="row-fluid">
                                        <div class="span4 module_cont module_text_area module_none_padding">
                                            <img src="img/moms-and-babes-yoga1.png" alt="" />
                                        </div>
                                        <div class="span8 module_cont module_text_area module_none_padding">
                                            <blockquote class="type2">
                                                <h6>What this is and who it is for:</h6>
                                                <p>
                                                    Mom’s and Babe’s yoga is a special “us-time” for you and your baby. This is yoga playtime: learn how 
                                                    to get your baby engaged, relaxed and comfortable with this “new” form of play. Theoni creates an atmosphere 
                                                    of total acceptance and fun and the classes focus on yoga that stretches and strengthens you as a mother so 
                                                    that you can find balance in all that you do. You can also connect with other moms about the ups and downs you are experiencing.
                                                </p>
                                                <p>
                                                    In the classes you can also access Theoni’s tips and practical suggestions honed from decades of experience in the world of pregnancy, 
                                                    babies and motherhood. She is a mother herself, so this class provides an opportunity to tap into this source of knowledge when you need it most.
                                                </p>
                                                <p>This class is for Moms and babies who are 6 weeks old up to babies that are crawling.</p>
                                                <br />
                                                <h6>What to expect from the classes:</h6>
                                                <br />
                                                <ul>
                                                        <li><p>Connect with your baby now that they are outside your womb.</p></li>
                                                        <li><p>Strength your body and flexibility.</p></li>
                                                        <li><p>Focus on developing strength in your abdomen, back and pelvic floor.</p></li>
                                                        <li><p>Witness your baby discovering their body.</p></li>
                                                        <li><p>Reduce any stress and anxiety around mothering, and life’s daily demands.</p></li>
                                                        <li><p>Meet other moms and babies, and reconnect with those you may have met during pregnancy yoga.</p></li>
                                                        <li><p>Access knowledge, tips and practical suggestions from Theoni regarding babies and motherhood.</p></li>
                                                </ul>
                                                <br />
                                                <div class="sectionContainerLeft innerBoxWithShadow">
                                                    <div class="span12 module_cont module_text_area module_none_padding">
                                                        <blockquote class="type2">
                                                            <p>
                                                                It is such a special time for both of us. Not only do I get to relax and do some much needed yoga but I also spend quality time bonding with Michael.
                                                                It is such a peaceful, supportive and non-judgmental environment to be in. We have both made wonderful friends and I don't know who gets more 
                                                                excited to 'play' me or Michael! Theoni is a fountain of knowledge and it has been so useful discussing each new miracle of development. I would highly 
                                                                recommend!
                                                            </p>
                                                            <div class="author italic">Bev mother to Michael</div>
                                                        </blockquote>
                                                    </div>
                                                    <br class="clear" />
                                                </div>
                                            </blockquote>
                                        </div>
                                    </div>

                                    <div class="module_line_trigger" data-background="#ffeec9 url(img/VV.png) no-repeat center" data-top-padding="bottom_padding_huge" data-bottom-padding="module_big_padding">
                                        <div class="row-fluid">
                                            <div class="span12 module_cont module_promo_text module_huge_padding">
                                                <div class="shortcode_promoblock">
                                                    <div class="promo_button_block type2">
                                                        <a href="Conscious-Birth-Products.aspx" class="promo_button"> View our Products </a>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--.module_cont -->
                                            <div class="clear">
                                                <!-- ClearFIX -->
                                            </div>
                                        </div>
                                        <!-- .row-fluid -->
                                    </div>
                                    <!-- .module_line_trigger -->
                                    <!-- .row-fluid -->
                                    <%--<div class="row-fluid">
                                        <div class="span12 module_cont module_partners center_title module_big_padding2">
                                            <div class="bg_title">
                                                <div class="title_wrapper">
                                                    <a href="javascript:void(0)" class="btn_carousel_left"></a>
                                                    <h5 class="headInModule">Our Partners</h5>
                                                    <a href="javascript:void(0)" class="btn_carousel_right"></a>
                                                </div>
                                            </div>
                                            <div class="module_content sponsors_works carouselslider items5" data-count="5">
                                                <ul>
                                                    <li>
                                                        <div class="item">
                                                            <a href="#charity-html5css3-website-template/3180454" target="_blank">
                                                                <img src="img/pictures/partners2.png" alt=""></a>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="item">
                                                            <a href="#point-business-responsive-wp-theme/4319087" target="_blank">
                                                                <img src="img/pictures/partners4.png" alt=""></a>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="item">
                                                            <a href="#cleanspace-retina-ready-business-wp-theme/3776000" target="_blank">
                                                                <img src="img/pictures/partners1.png" alt=""></a>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="item">
                                                            <a href="#yellowproject-multipurpose-retina-wp-theme/4066662" target="_blank">
                                                                <img src="img/pictures/partners3.png" alt=""></a>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="item">
                                                            <a href="#showroom-portfolio-retina-ready-wp-theme/3473628" target="_blank">
                                                                <img src="img/pictures/partners5.png" alt=""></a>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="item">
                                                            <a href="themeforest.net/item/incipiens-responsive-portfolio-wordpress-theme/2762691" target="_blank">
                                                                <img src="img/pictures/partners6.png" alt=""></a>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="item">
                                                            <a href="themeforest.net/item/hq-photography-responsive-wp-theme/3200962" target="_blank">
                                                                <img src="img/pictures/partners9.png" alt=""></a>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="item">
                                                            <a href="#incipiens-responsive-portfolio-wordpress-theme/2762691" target="_blank">
                                                                <img src="img/pictures/partners11.png" alt=""></a>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>--%>
                                    <!-- .row-fluid -->
                                </div>
                                <!-- .contentarea -->
                            </div>
                            <div class="left-sidebar-block span3">
                                <aside class="sidebar">
                                    //Sidebar Text
                                </aside>
                            </div>
                            <!-- .left-sidebar -->
                        </div>
                        <div class="clear">
                            <!-- ClearFix -->
                        </div>
                    </div>
                    <!-- .fl-container -->
                    <div class="right-sidebar-block span3">
                        <aside class="sidebar">
                        </aside>
                    </div>
                    <!-- .right-sidebar -->
                    <div class="clear">
                        <!-- ClearFix -->
                    </div>
                </div>
            </div>
            <!-- .container -->
        </div>
        <!-- .content_wrapper -->
    </div>
    <!-- .main_wrapper -->
</asp:Content>

