
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.Services;
using System.Web.Script.Services;

/// <summary>
/// Summary description for clsSizesView
/// </summary>
public partial class CMS_clsSizesView : System.Web.UI.Page
{
    #region CLASSES AND VARIABLES

    clsUsers clsUsers;
    DataTable dtSizesList;

    List<clsSizes> glstSizes;

    #endregion

    #region EVENT MODULES

    protected void Page_Load(object sender, EventArgs e)
    {
        //### Check if session clsUser exists
        if (Session["clsUsers"] == null)
        {
            {
                //### Redirect back to login
                Response.Redirect("../CMSLogin.aspx");
            }
        }
        clsUsers = (clsUsers)Session["clsUsers"];

dgrGrid.ItemCreated += new DataGridItemEventHandler(clsCommonFunctions.dgrGrid_PagerPopulate);

        if (!Page.IsPostBack)
        {
            PopulateFormData();
        }
        else
        {
            if (Session["glstSizes"] == null)
            {
                glstSizes = new List<clsSizes>();
                Session["glstSizes"] = glstSizes;
            }
            else
                glstSizes = (List<clsSizes>)Session["glstSizes"];
        }
            PopulateFormData();

    }

    protected void lnkbtnSearch_Click(object sender, EventArgs e)
    {
        string FilterExpression ="(strTitle) LIKE '%" + txtSearch.Text + "%'";
        DataTable dtSizesList = clsSizes.GetSizesList(FilterExpression, "");

        Session["dtSizesList"] = dtSizesList;

        PopulateFormData();
    }

    protected void lnkbtnAdd_Click(object sender, EventArgs e)
    {
        Response.Redirect("frmSizesAddModify.aspx");
    }

    #endregion

    #region DATA GRID METHODS

    private void PopulateFormData()
    {
        //### Populate datagrid using clsSizesList object
        try
        {
            dtSizesList = new DataTable();

            if (Session["dtSizesList"] == null)
                dtSizesList = clsSizes.GetSizesList();
            else
                dtSizesList = (DataTable)Session["dtSizesList"];

            dgrGrid.DataSource = dtSizesList;

            //### Add this check to trap if the last item on the page is deleted
            if (Convert.ToInt16(dgrGrid.CurrentPageIndex) < Convert.ToInt16(Session["SizesView_CurrentPageIndex"]))
            {
                dgrGrid.CurrentPageIndex = 0;
            }
            else
            {
                if (Session["SizesView_CurrentPageIndex"] != null)
                {
                    dgrGrid.CurrentPageIndex = (int)Session["SizesView_CurrentPageIndex"];
                }
            }

            dgrGrid.DataBind();

        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    protected void lnkDeleteItem_Click(object sender, EventArgs e)
    {
        int iSizeID = int.Parse((sender as LinkButton).CommandArgument);

        clsSizes.Delete(iSizeID);

        PopulateFormData();
    }

    protected void dgrGrid_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
    {
        //### Method is used when the page index is changed.
        try
        {
            Session["SizesView_CurrentPageIndex"] = e.NewPageIndex;
            dgrGrid.CurrentPageIndex = e.NewPageIndex;
            PopulateFormData();
        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    protected void dgrGrid_SortCommand(object source, DataGridSortCommandEventArgs e)
    {
        //### Sort Method
        dtSizesList = new DataTable();

        if (Session["dtSizesList"] == null)
            dtSizesList = clsSizes.GetSizesList();
        else
            dtSizesList = (DataTable)Session["dtSizesList"];

        DataView dvTemp = dtSizesList.DefaultView;

        dvTemp.Sort = e.SortExpression;
        dtSizesList = dvTemp.ToTable();
        Session["dtSizesList"] = dtSizesList;

        PopulateFormData();
    }

    public void dgrGrid_PopEditLink(object sender, EventArgs e)
    {
        try
        {
            //### Add hyperlink to datagrid item
            HyperLink lnkEditItem = (HyperLink)sender;
            DataGridItem dgrItemEdit = (DataGridItem)lnkEditItem.Parent.Parent;

            //### Get the iSizeID
            int iSizeID = 0;
            iSizeID = int.Parse(dgrItemEdit.Cells[0].Text);

            //### Add attributes to edit link
            lnkEditItem.Attributes.Add("href", "frmSizesAddModify.aspx?action=edit&iSizeID=" + iSizeID);
        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

     public void dgrGrid_PopDeleteLink(object sender, EventArgs e)
     {
          try
          {
             //### Add hyperlink to datagrid item
             HyperLink lnkDeleteItem = (HyperLink)sender;
             DataGridItem dgrItemDelete = (DataGridItem)lnkDeleteItem.Parent.Parent;

             //### Get the iSizeID
             int iSizeID = 0;
             iSizeID = int.Parse(dgrItemDelete.Cells[0].Text);

             //### Add attributes to delete link
             lnkDeleteItem.CssClass = "dgrDeleteLink";
             lnkDeleteItem.Attributes.Add("href", "frmSizesView.aspx?action=delete&iSizeID=" + iSizeID);
           }
           catch (Exception ex)
           {
                 Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
           }
     }

    #endregion

    #region Sizes FILTERING

    /// <summary>
    /// Enables the AutoCompleteExtender
    /// </summary>
    /// <param name="prefixText"></param>
    /// <param name="count"></param>
    /// <returns></returns>
    /// 
    [WebMethod]
    [ScriptMethod]
    public static string[] FilterBusinessRule(string prefixText, int count)
    {
        string[] strReturnList = new string[] { };

        string FilterExpression = "(strTitle) LIKE '%" + prefixText + "%'";
        DataTable dtSizes = clsSizes.GetSizesList(FilterExpression, "");
        List<string> glstSizes = new List<string>();

        if (dtSizes.Rows.Count > 0)
        {
            foreach (DataRow dtrSizes in dtSizes.Rows)
            {
                glstSizes.Add(AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem("" + dtrSizes["strTitle"].ToString(), dtrSizes["iSizeID"].ToString()));
            }
        }
        else
            glstSizes.Add("No Sizes Available.");
        strReturnList = glstSizes.ToArray();
        return strReturnList;
    }

    #endregion
}
