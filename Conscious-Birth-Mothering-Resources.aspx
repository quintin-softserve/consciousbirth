﻿<%@ Page Title="Mothering Resources" Language="C#" MasterPageFile="~/ConsciousBirth.master" AutoEventWireup="true" CodeFile="Conscious-Birth-Mothering-Resources.aspx.cs" Inherits="Conscious_Birth_Mothering_Resources" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date(); a = s.createElement(o),
            m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-53160457-1', 'auto');
        ga('send', 'pageview');

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="main_wrapper">
        <!-- C O N T E N T -->
        <div class="content_wrapper">
            <div class="page_title_block">
                <div class="container">
                    <h2 class="title">Mothering Resources</h2>
                </div>
            </div>
            <div class="container">
                <div class="content_block no-sidebar row">
                    <div class="fl-container span12">
                        <div class="row">
                            <div class="posts-block span12">
                                <div class="contentarea">

                                    <div class="row-fluid">
                                        <div class="span4 module_cont module_text_area module_none_padding">
                                            <img src="img/resources-mother.jpg" alt="" />
                                        </div>
                                        <div class="span8 module_cont module_text_area module_none_padding">
                                            <h5>Books</h5>
                                            <p>
                                                <i>The Year after childbirth</i> by Sheila Kitzinger<br />
                                                <i>Coping with Post Natal depression</i> by Sandra L. Wheatley<br />
                                                <i>The Baby Book</i> by William Sears<br />
                                                <i>The Aware Baby</i> by Aletha Solter<br />
                                                <i>Tears & Tantrums</i> by Aletha Solter<br />
                                               <i>Raising our children, raising ourselves</i> by Naomi Aldort<br />
                                               <i>Mother-daughter Wisdom</i> by Dr Christina Northrup<br />
                                                <i>Attachment Parenting</i> by William Sears<br />
                                            </p><br />
                                            
                                            <h5>Dvds</h5>
                                            <p>
                                                What babies want
                                            </p><br />

                                            <h5>Websites</h5>
                                            <p>
                                                <ul>
                                                    <li><a href="www.awareparenting.com" target="_blank">www.awareparenting.com</a></li>
                                                    <li><a href="www.askdrsears.com" target="_blank">www.askdrsears.com</a></li>
                                                    <li><a href="www.llli.org/southafrica" target="_blank">www.llli.org/southafrica</a></li>
                                                    <li><a href="www.lalecheleague.org" target="_blank">www.lalecheleague.org</a></li>
                                                </ul>
                                            </p><br />
                                        </div>
                                    </div>
                                    <!-- .row-fluid -->
                                    <div class="module_line_trigger" data-background="#ffeec9 url(img/VV.png) no-repeat center" data-top-padding="bottom_padding_huge" data-bottom-padding="module_big_padding">
                                        <div class="row-fluid">
                                            <div class="span12 module_cont module_promo_text module_huge_padding">
                                                <div class="shortcode_promoblock">
                                                    <div class="promo_button_block type2">
                                                        <a href="Conscious-Birth-Products.aspx" class="promo_button"> View our Products </a>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--.module_cont -->
                                            <div class="clear">
                                                <!-- ClearFIX -->
                                            </div>
                                        </div>
                                        <!-- .row-fluid -->
                                    </div>
                                    <!-- .module_line_trigger -->
                                    <%--<div class="row-fluid">
                                        <div class="span12 module_cont module_partners center_title module_big_padding2">
                                            <div class="bg_title">
                                                <div class="title_wrapper">
                                                    <a href="javascript:void(0)" class="btn_carousel_left"></a>
                                                    <h5 class="headInModule">Our Partners</h5>
                                                    <a href="javascript:void(0)" class="btn_carousel_right"></a>
                                                </div>
                                            </div>
                                            <div class="module_content sponsors_works carouselslider items5" data-count="5">
                                                <ul>
                                                    <li>
                                                        <div class="item">
                                                            <a href="#charity-html5css3-website-template/3180454" target="_blank">
                                                                <img src="img/pictures/partners2.png" alt=""></a>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="item">
                                                            <a href="#point-business-responsive-wp-theme/4319087" target="_blank">
                                                                <img src="img/pictures/partners4.png" alt=""></a>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="item">
                                                            <a href="#cleanspace-retina-ready-business-wp-theme/3776000" target="_blank">
                                                                <img src="img/pictures/partners1.png" alt=""></a>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="item">
                                                            <a href="#yellowproject-multipurpose-retina-wp-theme/4066662" target="_blank">
                                                                <img src="img/pictures/partners3.png" alt=""></a>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="item">
                                                            <a href="#showroom-portfolio-retina-ready-wp-theme/3473628" target="_blank">
                                                                <img src="img/pictures/partners5.png" alt=""></a>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="item">
                                                            <a href="themeforest.net/item/incipiens-responsive-portfolio-wordpress-theme/2762691" target="_blank">
                                                                <img src="img/pictures/partners6.png" alt=""></a>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="item">
                                                            <a href="themeforest.net/item/hq-photography-responsive-wp-theme/3200962" target="_blank">
                                                                <img src="img/pictures/partners9.png" alt=""></a>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="item">
                                                            <a href="#incipiens-responsive-portfolio-wordpress-theme/2762691" target="_blank">
                                                                <img src="img/pictures/partners11.png" alt=""></a>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>--%>
                                    <!-- .row-fluid -->

                                </div>
                                <!-- .contentarea -->
                            </div>
                            <div class="left-sidebar-block span3">
                                <aside class="sidebar">
                                    //Sidebar Text
                                </aside>
                            </div>
                            <!-- .left-sidebar -->
                        </div>
                        <div class="clear">
                            <!-- ClearFix -->
                        </div>
                    </div>
                    <!-- .fl-container -->
                    <div class="right-sidebar-block span3">
                        <aside class="sidebar">
                        </aside>
                    </div>
                    <!-- .right-sidebar -->
                    <div class="clear">
                        <!-- ClearFix -->
                    </div>
                </div>
            </div>
            <!-- .container -->
        </div>
        <!-- .content_wrapper -->
    </div>
    <!-- .main_wrapper -->
</asp:Content>

