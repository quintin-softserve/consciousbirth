﻿<%@ Page Title="Blissful Women :: Conscious Birth" Language="C#" MasterPageFile="~/ConsciousBirth.master" AutoEventWireup="true" CodeFile="Conscious-Birth-Blissful-Women.aspx.cs" Inherits="Conscious_Birth_Blissful_Women" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript" src="js/jquery.min.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="main_wrapper">
        <!-- C O N T E N T -->
        <div class="content_wrapper">
            <div class="page_title_block">
                <div class="container">
                    <h2 class="title">Blissful women</h2>
                </div>
            </div>
            <div class="container">
                <div class="content_block no-sidebar row">
                    <div class="fl-container span12">
                        <div class="row">
                            <div class="posts-block span12">
                                <div class="contentarea">

                                    <div class="row-fluid">
                                        <div class="span4 module_cont module_text_area module_none_padding">
                                            <img src="img/blissful-woman2.png" alt="" />
                                        </div>
                                        <div class="span8 module_cont module_text_area module_none_padding">
                                            <blockquote class="type2">
                                                <h6>What this is and who it is for:</h6>
                                                <p>
                                                    Blissful woman is a journey, guided by Theoni, which works to help women overcome challenges in any area of life. It is for women, of any age, 
                                                    whether they are mothers or not, who find themselves needing some form of careful and considered guidance in times of difficulty. Theoni can help 
                                                    in the following ways:
                                                </p>
                                                <br />
                                                <ul>
                                                    <li><p>To access your powerful femininity, whether you have children or not, and embracing your feminine self with grace and ease.</p></li>
                                                    <li><p>If you are a mother you may need assistance in embracing the different parts of yourself (mother, wife, working woman etc).</p></li>
                                                    <li><p>If your children are older she can help you reclaiming dormant parts of yourself that were packed away during mothering.</p></li>
                                                    <li><p>By encouraging you if you have lost your confidence in any way and for any reason.</p></li>
                                                    <li><p>Teaching you techniques to focus so that you can achieve your desires.</p></li>
                                                    <li><p>Helping you learn techniques that assist in setting and achieving goals.</p></li>
                                                    <li><p>Showing you how to find new ways of being with a partner through various relationship changes, divorce, or separation.</p></li>
                                                    <li><p>Assisting you as you redefining yourself in any sphere of your life, be it a career change or embarking on doing that thing that you have always wanted to do but don’t know how to get going.</p></li>
                                                </ul>
                                                <br />
                                                <h6>What to expect and benefits:</h6>
                                                <p>
                                                    As a woman, Theoni has a deep understanding of the female psyche and all its fierceness and vulnerabilities. Consulting with her 
                                                    regarding any challenges that you face will result in careful and intuitive guidance that helps you to reconnect with your inner 
                                                    strength and reserve in order to step into the light of your authentic self and bravely overcome your difficulties.
                                                </p>
                                            </blockquote>
                                            <br />
                                            <div class="sectionContainerLeft innerBoxWithShadow">
                                                <div class="span12 module_cont module_text_area module_none_padding">
                                                    <blockquote class="type2">
                                                        <p>
                                                            I have travelled the most incredible and powerful Journey with Theoni, who runs a very special practice, called 
                                                            Conscious Birth, and specializes in Life Alignment. (as well as many other disciplines) 
                                                            I could write reams about how my feeling of confidence has developed, and how I now honor the shift that Theoni facilitated 
                                                            in my world, my life and Me.
                                                        </p>
                                                        <div class="author italic">Isla </div>
                                                    </blockquote>
                                                </div>
                                                <br class="clear" />
                                            </div>
                                        </div>
                                        <br />
                                    </div>
                                    <!-- .row-fluid -->
                                    <div class="module_line_trigger" data-background="#ffeec9 url(img/VV.png) no-repeat center" data-top-padding="bottom_padding_huge" data-bottom-padding="module_big_padding">
                                        <div class="row-fluid">
                                            <div class="span12 module_cont module_promo_text module_huge_padding">
                                                <div class="shortcode_promoblock">
                                                    <div class="promo_button_block type2">
                                                        <a href="Conscious-Birth-Products.aspx" class="promo_button"> View our Products </a>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--.module_cont -->
                                            <div class="clear">
                                                <!-- ClearFIX -->
                                            </div>
                                        </div>
                                        <!-- .row-fluid -->
                                    </div>
                                    <!-- .module_line_trigger -->
                                   <!-- <div class="row-fluid">
                                        <div class="span12 module_cont module_partners center_title module_big_padding2">
                                            <div class="bg_title">
                                                <div class="title_wrapper">
                                                    <a href="javascript:void(0)" class="btn_carousel_left"></a>
                                                    <h5 class="headInModule">Our Partners</h5>
                                                    <a href="javascript:void(0)" class="btn_carousel_right"></a>
                                                </div>
                                            </div>
                                            <div class="module_content sponsors_works carouselslider items5" data-count="5">
                                                <ul>
                                                    <li>
                                                        <div class="item">
                                                            <a href="#charity-html5css3-website-template/3180454" target="_blank">
                                                                <img src="img/pictures/partners2.png" alt=""></a>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="item">
                                                            <a href="#point-business-responsive-wp-theme/4319087" target="_blank">
                                                                <img src="img/pictures/partners4.png" alt=""></a>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="item">
                                                            <a href="#cleanspace-retina-ready-business-wp-theme/3776000" target="_blank">
                                                                <img src="img/pictures/partners1.png" alt=""></a>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="item">
                                                            <a href="#yellowproject-multipurpose-retina-wp-theme/4066662" target="_blank">
                                                                <img src="img/pictures/partners3.png" alt=""></a>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="item">
                                                            <a href="#showroom-portfolio-retina-ready-wp-theme/3473628" target="_blank">
                                                                <img src="img/pictures/partners5.png" alt=""></a>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="item">
                                                            <a href="themeforest.net/item/incipiens-responsive-portfolio-wordpress-theme/2762691" target="_blank">
                                                                <img src="img/pictures/partners6.png" alt=""></a>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="item">
                                                            <a href="themeforest.net/item/hq-photography-responsive-wp-theme/3200962" target="_blank">
                                                                <img src="img/pictures/partners9.png" alt=""></a>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="item">
                                                            <a href="#incipiens-responsive-portfolio-wordpress-theme/2762691" target="_blank">
                                                                <img src="img/pictures/partners11.png" alt=""></a>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- .row-fluid -->

                                </div>
                                <!-- .contentarea -->
                            </div>
                            <div class="left-sidebar-block span3">
                                <aside class="sidebar">
                                    //Sidebar Text
                                </aside>
                            </div>
                            <!-- .left-sidebar -->
                        </div>
                        <div class="clear">
                            <!-- ClearFix -->
                        </div>
                    </div>
                    <!-- .fl-container -->
                    <div class="right-sidebar-block span3">
                        <aside class="sidebar">
                        </aside>
                    </div>
                    <!-- .right-sidebar -->
                    <div class="clear">
                        <!-- ClearFix -->
                    </div>
                </div>
            </div>
            <!-- .container -->
        </div>
        <!-- .content_wrapper -->

    </div>
    <!-- .main_wrapper -->
</asp:Content>

