﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Conscious_Birth_Product_Info : System.Web.UI.Page
{
    bool bHasColours;
    bool bHasSizes;
    clsUsers clsUsers;
    clsProducts clsProducts;
    List<clsProducts> glstProducts;

    #region EVENT METHODS

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["clsAccountUsers"] == null)
        {
            //### Redirect back to login
            LoginLabel.Text = "Log in";
        }
        else
        {
            LoginLabel.Text = "Log out";
        }
        popTestimonials();
        //### If the iProductID is passed through then we want to instantiate the object with that iProductID
        if ((Request.QueryString["iProductID"] != "") && (Request.QueryString["iProductID"] != null))
        {
            clsProducts = new clsProducts(Convert.ToInt32(Request.QueryString["iProductID"]));

            if (!IsPostBack)
            {
                //### Populate the form
                popProductDetails(clsProducts.iProductID);
            }
        }
        else
        {
            clsProducts = new clsProducts();
        }
        Session["clsProducts"] = clsProducts;

    }

    protected void btnBuyNow_Click(object sender, EventArgs e)
    {
        if (ddlColor.SelectedIndex != 0 && ddlSizes.SelectedIndex != 0)
        {
            //clsCart.addCartItemCookie(clsProducts.iProductID, 1);

            //int iProductID = Convert.ToInt32(((Button)sender).CommandArgument);
            if (canPlaceOrder())
            {
                if (ddlSizes.SelectedValue.ToString() == "")
                {
                    string strSize = "One Size Only";
                    clsCart.addToCart(clsProducts.iProductID, Int32.Parse(txtQuantity.Text), ddlColor.SelectedValue.ToString(), strSize);
                }
                else if (ddlColor.SelectedValue.ToString() == "")
                {
                    string strColor = "One Colour Only";
                    clsCart.addToCart(clsProducts.iProductID, Int32.Parse(txtQuantity.Text), strColor, ddlSizes.SelectedValue.ToString());
                }
                else
                {
                    clsCart.addToCart(clsProducts.iProductID, Int32.Parse(txtQuantity.Text), ddlColor.SelectedValue.ToString(), ddlSizes.SelectedValue.ToString());
                }

                Response.Redirect("Conscious-Birth-Cart.aspx");
            }
            else
            {
                mandatoryDiv.Visible = true;
                lblValidationMessage.Text = "Please make sure that you have selected all required information.";
            }
        }
        else
        {
            mandatoryDiv.Visible = true;
            lblValidationMessage.Text = "Please make sure that you have selected all required information.";
        }
    }


    protected void btnSubscribeNow_Click(object sender, EventArgs e)
    {
        bool bCanSave = true;

        if (txtName.Text != "" && txtName.Text != null)
        {
            bCanSave = true;
        }
        else
        {
            bCanSave = false;
        }

        if (txtEmail.Text != "" && txtEmail.Text != null)
        {
            bCanSave = true;
        }
        else
        {
            bCanSave = false;
        }

        if (bCanSave == true)
        {
            mandatoryDiv1.Visible = false;
            lblValidationMessage.Text = "<div class=\"madatoryPaddingDiv\"><img src=\"images/Validation/imgFaceHappy.png\" alt='' title=''/><div class=\"validationMessage\"></div></div>";
            SaveData();
        }
        else
        {
            mandatoryDiv1.Visible = true;
            txtName.Attributes.Add("style", "border:1px solid #401414");
            txtEmail.Attributes.Add("style", "border:1px solid #401414");
            lblValidationMessage.Text = "<div class=\"madatoryPaddingDiv\"><img src=\"images/Validation/imgFaceSad.png\" alt='' style='margin-left: 10px; margin-bottom: 10px; float:left;' title=''/><div class=\"validationMessage\" style='float: left; margin-left: 10px; margin-top: 18px;'>Please fill out all mandatory fields</div></div>";
        }

    }

    private void SaveData()
    {
        //### Only update the password if there is a value
        clsSubscribers clsSubscribersRecord = new clsSubscribers();
        bool bIsNewUser = false;

        if (clsSubscribersRecord.iSubscriberID == 0)
            bIsNewUser = true;

        if ((clsCommonFunctions.DoesRecordExist("tblSubscribers", "strEmailAddress='" + txtEmail.Text + "' AND bIsDeleted = 0") == true) && bIsNewUser)
        {
            //rfvEmail.Text = "The email you provided exist. Please enter new email";
        }
        else
        {
            //### Add / Update
            clsSubscribersRecord.dtAdded = Convert.ToDateTime(DateTime.Now.ToString("dd MMM yyyy"));
            clsSubscribersRecord.iAddedBy = -10;
            clsSubscribersRecord.strFirstName = txtName.Text;
            clsSubscribersRecord.strSurname = "";
            clsSubscribersRecord.strEmailAddress = txtEmail.Text;
            //rfvName.Text = "";
            clsSubscribersRecord.Update();

            Response.Redirect("Conscious-Birth-home.aspx");
        }

        //### redirect back to view page

    }
    protected void btnAddToWishlist_Click(object sender, EventArgs e)
    {
        if (ddlColor.SelectedIndex != 0 && ddlSizes.SelectedIndex != 0)
        {
            //clsCart.addCartItemCookie(clsProducts.iProductID, 1);

            //int iProductID = Convert.ToInt32(((Button)sender).CommandArgument);
            if (canPlaceOrder())
            {
                if (ddlSizes.SelectedValue.ToString() == "")
                {
                    string strSize = "One Size Only";
                    SaveWishlist(Int32.Parse(txtQuantity.Text), ddlColor.SelectedValue.ToString(), strSize);
                }
                else if (ddlColor.SelectedValue.ToString() == "")
                {
                    string strColor = "One Colour Only";
                    SaveWishlist(Int32.Parse(txtQuantity.Text), ddlSizes.SelectedValue.ToString(), strColor);
                }
                else
                {
                    SaveWishlist(Int32.Parse(txtQuantity.Text), ddlSizes.SelectedValue.ToString(), ddlSizes.SelectedValue.ToString());
                }

                Response.Redirect("Conscious-Birth-WishList-Cart.aspx");
            }
            else
            {
                mandatoryDiv.Visible = true;
                lblValidationMessage.Text = "Please make sure that you have selected all required information.";
            }
        }
        else
        {
            mandatoryDiv.Visible = true;
            lblValidationMessage.Text = "Please make sure that you have selected all required information.";
        }

    }
    private void SaveWishlist(int Quantity, string Color, string Size)
    {
        clsUserProductWishlistLink clsUserProductWishlistLink = new clsUserProductWishlistLink();
        clsUserProductWishlistLink.iProductID = clsProducts.iProductID;
        clsUserProductWishlistLink.iWishListEventID = Convert.ToInt32(Session["clsWishlistEvents"]);
        clsUserProductWishlistLink.iQuantity = Quantity;
        clsUserProductWishlistLink.strColor = Color;
        clsUserProductWishlistLink.strSize = Size;
        clsUserProductWishlistLink.Update();
    }


    #endregion

    #region POPULATE METHODS

    private void popProductDetails(int iProductID)
    {
        string strFullPathToImage = "";

        StringBuilder sbProductDetails = new StringBuilder();
        StringBuilder sbProductDetailsLower = new StringBuilder();

        sbProductDetails.AppendLine("<div class='span6 module_cont module_text_area module_none_padding'>");
        sbProductDetails.AppendLine("<ul class='add-to-links' id='etalage'>");

        if (!(clsProducts.strPathToImages.ToString() == "") || (clsProducts.strPathToImages == null))
        {
            sbProductDetails.AppendLine(getList(clsProducts.strPathToImages));
        }
        if (!(clsProducts.strPathToImages.ToString() == "") || (clsProducts.strPathToImages == null))
        {
            strFullPathToImage = "Products/" + clsProducts.strPathToImages + "/" + clsProducts.strMasterImage;
        }
        else
        {
            strFullPathToImage = "img/no-image.png";
        }
        sbProductDetails.AppendLine("<li>");
        //sbProductDetails.AppendLine("<img class='etalage_thumb_image' src='" + strFullPathToImage + "' alt='' style='border:1px solid #eee' /><img class='etalage_source_image' src='" + strFullPathToImage + "' alt='' style='border:1px solid #eee' /><a href='#'></a>");
        sbProductDetails.AppendLine("<img class='etalage_source_image' src='" + strFullPathToImage + "' alt='' style='border:1px solid #eee' /><a href='#'></a>");
        sbProductDetails.AppendLine("</li>");
        sbProductDetails.AppendLine("</ul>");
        sbProductDetails.AppendLine(" </div>");
        sbProductDetails.AppendLine("<div class='span6 module_cont module_text_area module_none_padding'>");
        sbProductDetails.AppendLine("<h3 class='m_3'>" + clsProducts.strTitle + "</h3>");
        sbProductDetails.AppendLine("<br/>");
        sbProductDetails.AppendLine("<div class='social_single'>");
        sbProductDetails.AppendLine("<ul>");
        sbProductDetails.AppendLine("Colours<br/>");
        sbProductDetails.AppendLine(popColourBlocks(iProductID));
        sbProductDetails.AppendLine("</ul>");
        sbProductDetails.AppendLine("</div>");
        sbProductDetails.AppendLine("<br/>");

        List<string> lstSizes = new List<string>();
        lstSizes = popSizesList(iProductID);
        if (lstSizes.Count() > 0)
        {
            if (lstSizes.Count > 1)
            {
                sbProductDetails.AppendLine("Available Sizes: " + lstSizes[0] + "<br/>");
                foreach (string s in lstSizes)
                {
                    sbProductDetails.AppendLine(s + "<br/>");
                }
                //sbProductDetails.AppendLine("<br/>");
            }
            else
            {
                sbProductDetails.AppendLine("Available Sizes: " + lstSizes[0] + "<br/>");
            }
        }
        else
        {
            sbProductDetails.AppendLine("Available Sizes:" + "<br/>");
        }

        //sbProductDetails.AppendLine(" Style: " + clsProducts.strStyle.ToString());
        sbProductDetails.AppendLine("<div class='price_single'>");
        sbProductDetails.AppendLine("<br/><h3 class='m_3'> R " + clsProducts.dblPrice.ToString("N2") + "</h3><a href='#'></a>");
        sbProductDetails.AppendLine("</div>");
        sbProductDetails.AppendLine("<br/>");
        sbProductDetails.AppendLine("<p class='m_desc'>" + clsProducts.strDescription + "</p>");
        // BUTTON CODE WAS HERE  
        sbProductDetails.AppendLine("</div>");

        if ((clsProducts.strVideoLink != null) && (clsProducts.strVideoLink != ""))
        {
            sbProductDetails.AppendLine("<div class='cont1 span_2_of_a1'>");
            sbProductDetails.AppendLine("<br class='clr'/>");
            sbProductDetails.AppendLine("<br class='clr'/>");
            sbProductDetails.AppendLine("<div><iframe width='100%' height='500' src='" + clsProducts.strVideoLink + "' frameborder='0' allowfullscreen></iframe></div>");
            sbProductDetails.AppendLine(" </div>");
        }

        litProducts.Text = sbProductDetails.ToString();
        popColours(iProductID);
        popSizes(iProductID);
    }

    private string popColourBlocks(int iProductID)
    {
        StringBuilder sbProductColoursLink = new StringBuilder();
        int iColourID;
        DataTable dtProductColoursLink;
        dtProductColoursLink = clsProductColoursLink.GetProductColoursLinkList("iProductID=" + iProductID, "");

        foreach (DataRow dtrProductColourLink in dtProductColoursLink.Rows)
        {
            iColourID = Convert.ToInt32(dtrProductColourLink["iColourID"]);

            if (iColourID != 0)
            {
                clsColours clsColours = new clsColours(iColourID);
                sbProductColoursLink.AppendLine("<li style='height: 30px; width: 30px; border: 1px solid #333; list-style:none; background:" + clsColours.strColourCode + ";'></li>");
            }
        }

        return sbProductColoursLink.ToString();
    }

    private void popColours(int iProductID)
    {
        int iColourID;
        DataTable dtProductColoursLink;
        dtProductColoursLink = clsProductColoursLink.GetProductColoursLinkList("iProductID=" + iProductID, "");
        if (dtProductColoursLink.Rows.Count > 0)
        {
            bHasColours = true;
            foreach (DataRow dtrProductColourLink in dtProductColoursLink.Rows)
            {
                iColourID = Convert.ToInt32(dtrProductColourLink["iColourID"]);

                if (iColourID != 0)
                {
                    clsColours clsColours = new clsColours(iColourID);
                    ListItem liColor = new ListItem();
                    liColor.Text = clsColours.strTitle;
                    ddlColor.Items.Add(liColor);
                    //li.Attributes.Add("style", "background-color:" + clsColours.strColourCode + "; width: 30px; height: 30px;");
                    //ddl.Items.Add(li);
                    //sbProductColoursLink.AppendLine("<td style='height: 30px; width: 30px; border: 1px solid #333; list-style:none; background:" + clsColours.strColourCode + ";'></td>");
                }

            }
            ddlColor.Items.Insert(0, new ListItem("--Not Selected--", "0"));
        }
        else
        {
            colourDiv.Visible = false;
            bHasColours = false;
        }
    }

    private void popSizes(int iProductID)
    {
        ddlSizes.Items.Clear();

        int iSizeID = 0;
        DataTable dtProductSizesLink;
        dtProductSizesLink = clsProductSizesLink.GetProductSizesLinkList("iProductID=" + iProductID, "");
        if (dtProductSizesLink.Rows.Count > 0)
        {
            bHasSizes = true;
            foreach (DataRow dtrProductSizesLink in dtProductSizesLink.Rows)
            {
                iSizeID = Convert.ToInt32(dtrProductSizesLink["iSizeID"]);

                if (iSizeID != 0)
                {
                    clsSizes clsSizes = new clsSizes(iSizeID);
                    ListItem liSize = new ListItem();
                    liSize.Text = clsSizes.strTitle;
                    ddlSizes.Items.Add(liSize);

                    //li.Attributes.Add("style", "background-color:" + clsColours.strColourCode + "; width: 30px; height: 30px;");
                    //ddl.Items.Add(li);
                    //sbProductColoursLink.AppendLine("<td style='height: 30px; width: 30px; border: 1px solid #333; list-style:none; background:" + clsColours.strColourCode + ";'></td>");
                }

            }
            ddlSizes.Items.Insert(0, new ListItem("--Not Selected--", "0"));
        }
        else
        {
            sizeDiv.Visible = false;
            bHasSizes = false;
        }
    }

    private List<string> popSizesList(int iProductID)
    {
        List<string> lstSizes = new List<string>();
        int iSizeID = 0;
        DataTable dtProductSizesLink;
        dtProductSizesLink = clsProductSizesLink.GetProductSizesLinkList("iProductID=" + iProductID, "");
        foreach (DataRow dtrProductSizesLink in dtProductSizesLink.Rows)
        {
            iSizeID = Convert.ToInt32(dtrProductSizesLink["iSizeID"]);

            if (iSizeID != 0)
            {
                clsSizes clsSizes = new clsSizes(iSizeID);
                lstSizes.Add(clsSizes.strTitle);
                //li.Attributes.Add("style", "background-color:" + clsColours.strColourCode + "; width: 30px; height: 30px;");
                //ddl.Items.Add(li);
                //sbProductColoursLink.AppendLine("<td style='height: 30px; width: 30px; border: 1px solid #333; list-style:none; background:" + clsColours.strColourCode + ";'></td>");
            }

        }
        return lstSizes;
    }

    private string popImages(int iProductID)
    {
        getList(clsProducts.strPathToImages);
        StringBuilder sbImage = new StringBuilder();

        DataTable dtImages = clsProducts.GetProductsList("iProductID=" + iProductID, "");
        foreach (DataRow dtrImages in dtImages.Rows)
        {
            string strFullPathToImage = "Products/" + dtrImages["strPathToImages"] + "/" + dtrImages["strMasterImage"];
            sbImage.AppendLine("<li style='margin-right:10px !important'>");
            sbImage.AppendLine("<a href''><img src='" + strFullPathToImage + "' alt=''/></a>");
            sbImage.AppendLine("</li>");

        }

        return sbImage.ToString();
    }

    private void popTestimonials()
    {
        string strCustomerName = "";
        string strTestimonial = "";
        int iCount = 0;

        DataTable dtTestimonials = clsCustomerTestimonials.GetCustomerTestimonialsList("", "");
        StringBuilder strbTestimonial = new StringBuilder();

        foreach (DataRow dtrTestimonialRow in dtTestimonials.Rows)
        {
            ++iCount;

            strTestimonial = (dtrTestimonialRow["strTestimonial"].ToString());
            strCustomerName = (dtrTestimonialRow["strCustomerName"].ToString());

            if (iCount != 1)
                strbTestimonial.AppendLine("<div class='cbp-qtcontent'>");
            else
                strbTestimonial.AppendLine("<div class='cbp-qtcontent current'>");
            strbTestimonial.AppendLine(strTestimonial + "<br /><span>" + strCustomerName + "</span></div>");
        }

        litTestimonial.Text = strbTestimonial.ToString();

    }


    string strUniqueFullPath = System.Configuration.ConfigurationManager.AppSettings["WebRootFullPath"] + "\\Products";

    public string getList(String strPathToFolder)
    {

        StringBuilder sbImage = new StringBuilder();
        try
        {
            string strPath = strPathToFolder;
            string[] files = Directory.GetFiles(strUniqueFullPath + "\\" + strPath);

            string iProductID = "";
            if (!string.IsNullOrEmpty(Request.QueryString["iProductID"]))
                iProductID = Request.QueryString["iProductID"];

            int iImagesCount = 0;

            foreach (string strName in files)
            {
                if (strName.IndexOf("_lrg") != -1)
                {
                    string strHTMLImages = strName.Replace(System.Configuration.ConfigurationManager.AppSettings["WebRootFullPath"] + "\\", "..\\");
                    strHTMLImages = strHTMLImages.Replace("\\", "/");

                    //### Generates a javascript postback for the delete method
                    String strPostBack = Page.ClientScript.GetPostBackEventReference(this, "iRemoveImages:" + iImagesCount);


                    sbImage.AppendLine("<li style=''>");
                    sbImage.AppendLine("<img class='etalage_thumb_image' src='" + strHTMLImages.Replace("_lrg", "") + "' alt='' style='border:1px solid #eee; margin-right:10px !important; list-style:none;' /><img class='etalage_source_image' src='" + strHTMLImages.Replace("_lrg", "") + "' alt='' style='border:1px solid #eee; margin-right:10px !important; list-style:none; ' />");
                    sbImage.AppendLine("</li>");
                    //lstImagesFileNames.Add(Path.GetFileName(strName).Replace("_sml", ""));
                    iImagesCount++;

                }
            }

        }
        catch (Exception ex) { }

        return sbImage.ToString();
    }

    private bool canPlaceOrder()
    {
        bool bCanSave = false;
        if (bHasColours && bHasSizes)
        {
            if (ddlColor.SelectedValue.ToString() != null && ddlColor.SelectedValue.ToString() != "" && ddlColor.SelectedValue.ToString() != "--Not Selected--" && ddlColor.SelectedValue.ToString() != "0")
            {
                if (ddlSizes.SelectedValue.ToString() != null && ddlSizes.SelectedValue.ToString() != "" && ddlSizes.SelectedValue.ToString() != "--Not Selected--" && ddlSizes.SelectedValue.ToString() != "0")
                {
                    bCanSave = true;
                }
                else
                {
                    bCanSave = false;
                }
            }
        }
        else if (bHasColours && bHasSizes == false)
        {
            if (ddlColor.SelectedValue.ToString() != null && ddlColor.SelectedValue.ToString() != "" && ddlColor.SelectedValue.ToString() != "--Not Selected--" && ddlColor.SelectedValue.ToString() != "0")
            {
                bCanSave = true;
            }
            else
            {
                bCanSave = false;
            }
        }
        else if (bHasSizes && bHasColours == false)
        {
            if (ddlSizes.SelectedValue.ToString() != null && ddlSizes.SelectedValue.ToString() != "" && ddlSizes.SelectedValue.ToString() != "--Not Selected--" && ddlSizes.SelectedValue.ToString() != "0")
            {
                bCanSave = true;
            }
            else
            {
                bCanSave = false;
            }
        }
        else if (bHasColours == false && bHasSizes == false)
        {
            bCanSave = true;
        }
        else
        {
            bCanSave = false;
        }
        return bCanSave;
    }

    #endregion
}