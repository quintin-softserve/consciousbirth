﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class web_Conscious_Birth_User_Register : System.Web.UI.Page
{
    clsAccountUsers clsAccountUsers;

    protected void Page_Load(object sender, EventArgs e)
     {
        if (!IsPostBack)
        {
            if (Request.QueryString["AddEvent"] == "Current")
            {
                addevent.Visible = true;
                HideRegester.Visible = false;
            }
           
            else
            {


                //### If the iUserID is passed through then we want to instantiate the obect with that iUserID
                if (Request.QueryString["iAccountUserID"] != "" && Request.QueryString["iAccountUserID"] != null)
                {
                    clsAccountUsers = new clsAccountUsers(Convert.ToInt32(Request.QueryString["iAccountUserID"]));

                    //### Populate the form
                    popFormData();

                    popValidFields();

                    //### Hide password rows if editing
                    if (Request.QueryString["action"] == "edit")
                    {
                        divPassword.Attributes.Add("style", "display:none");
                        divConfirmPassword.Attributes.Add("style", "display:none");
                    }
                }
                else
                {
                    clsAccountUsers = new clsAccountUsers();
                }

                Session["clsAccountUsers"] = clsAccountUsers;


            }
        }
        else
        {
            //clsAccountUsers = (clsAccountUsers)Session["clsAccountUsers"];
        }
    }

    protected void lnkbtnBack_Click(object sender, EventArgs e)
    {
        Response.Redirect("frmAccountUserView.aspx");
    }
    protected void btnSubmit_Click1(object sender, EventArgs e)
    {
        SaveData();
    }
    protected void btnSubmitEvent_Click(object sender, EventArgs e)
    {
        int iWishlistID = Convert.ToInt32(Session["clsAccountUsers"]);
        string dt = Request.Form[txtDate.UniqueID];

        clsWishListsEvents clsWishListsEvents = new clsWishListsEvents();
        clsWishListsEvents.dtAdded = Convert.ToDateTime(DateTime.Now.ToString("dd MMM yyyy"));
        clsWishListsEvents.dtEdited = Convert.ToDateTime(DateTime.Now.ToString("dd MMM yyyy"));
        clsWishListsEvents.iAddedBy = 0;
        clsWishListsEvents.dtEvent = Convert.ToDateTime(dt);
        clsWishListsEvents.strAddressOfEvent = txtAddressOfEvent.Text;
        clsWishListsEvents.strEventName = TextBox1.Text;
        clsWishListsEvents.iAccountUserID = iWishlistID;
        clsWishListsEvents.Update();

    }

    protected void lnkbtnClear_Click(object sender, EventArgs e)
    {
        mandatoryDiv.Visible = false;

        txtName.Text = "";

        clsValidation.SetValid(txtName);

        txtSurname.Text = "";
        clsValidation.SetValid(txtSurname);

        txtEmail.Text = "";
        clsValidation.SetValid(txtEmail);

        txtPassword.Text = "";
        clsValidation.SetValid(txtPassword);

        txtConfirmPassword.Text = "";
        clsValidation.SetValid(txtConfirmPassword);
    }

    #region POPULATE DATA METHODS

    private void popFormData()
    {
        txtName.Text = clsAccountUsers.strFirstName;
        txtSurname.Text = clsAccountUsers.strSurname;
        txtEmail.Text = clsAccountUsers.strEmailAddress;
        hfCanSave.Value = "true";
    }

    private void popValidFields()
    {
        //### Make the relevant mandatory fields green
        string strScript = "";

        if (!String.IsNullOrEmpty(txtName.Text))
        {
            strScript += "setValidFile('" + txtName.ClientID + "',true);";
        }
        if (!String.IsNullOrEmpty(txtSurname.Text))
        {
            strScript += "setValidFile('" + txtSurname.ClientID + "',true);";
        }
        if (!String.IsNullOrEmpty(txtEmail.Text))
        {
            strScript += "setValidFile('" + txtEmail.ClientID + "',true);";
        }

        ScriptManager.RegisterStartupScript(this, this.GetType(), "mandatoryFields", strScript + strScript, true);
    }

    #endregion

    #region SAVE DATA METHODS

    private void SaveData()
    {
        clsAccountUsers clsAccountUsers = new clsAccountUsers();
        //string dt = Request.Form[txtDate.UniqueID];
        //### Only update the password if there is a value
        if (txtPassword.Text != "")
        {
            //### Hash the password
            string strHashPassword = clsCommonFunctions.GetMd5Sum(txtPassword.Text);
            //### Add / Update
            clsAccountUsers.dtAdded = Convert.ToDateTime(DateTime.Now.ToString("dd MMM yyyy"));
            clsAccountUsers.iAddedBy = 0;
            clsAccountUsers.dtEdited = Convert.ToDateTime(DateTime.Now.ToString("dd MMM yyyy"));
            clsAccountUsers.iEditedBy = 0;
            clsAccountUsers.strFirstName = txtName.Text;
            clsAccountUsers.strSurname = txtSurname.Text;
            clsAccountUsers.strEmailAddress = txtEmail.Text;
            clsAccountUsers.strPassword = strHashPassword;
            clsAccountUsers.strPhoneNumber = txtPhoneNumber.Text;
            clsAccountUsers.Update();
            Session["clsAccountUsers"] = clsAccountUsers.iAccountUserID;
            //### redirect back to view page
            Response.Redirect("Conscious-Birth-Thank-You.aspx");
        }
    }

    #endregion
}