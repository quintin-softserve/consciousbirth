using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class CMS_frmBlogPostsAddModify : System.Web.UI.Page
{
    clsUsers clsUsers;
    clsBlogPosts clsBlogPosts;
  
    #region EVENT METHODS

    protected void Page_Load(object sender, EventArgs e)
    {
        //### Check if session clsUser exists
        if (Session["clsUsers"] == null)
        {
            {
                //### Redirect back to login
                Response.Redirect("../CMSLogin.aspx");
            }
        }
        clsUsers = (clsUsers)Session["clsUsers"];

             //### Determines if a javascript delete has been called
             if (!String.IsNullOrEmpty(Page.Request["__EVENTARGUMENT"]) && (Page.Request["__EVENTARGUMENT"].Split(':')[0] == "iRemoveImages"))
             DeleteImages(Convert.ToInt32(Page.Request["__EVENTARGUMENT"].Split(':')[1]));
       
        if (!IsPostBack)
        {
             popBlogType();

            //### If the iBlogPostID is passed through then we want to instantiate the object with that iBlogPostID
            if ((Request.QueryString["iBlogPostID"] != "") && (Request.QueryString["iBlogPostID"] != null))
            {
                clsBlogPosts = new clsBlogPosts(Convert.ToInt32(Request.QueryString["iBlogPostID"]));

                //### Populate the form
                popFormData();
            }
            else
            {
                clsBlogPosts = new clsBlogPosts();
            }
            Session["clsBlogPosts"] = clsBlogPosts;
        }
        else
        {
            clsBlogPosts = (clsBlogPosts)Session["clsBlogPosts"];
        }
    }

    protected void lnkbtnBack_Click(object sender, EventArgs e)
    {
        //### Go back to previous page
        Response.Redirect("frmBlogPostsView.aspx");
    }

    protected void lnkbtnSave_Click(object sender, EventArgs e)
    {
        //### Validate registration process
        bool bCanSave = true;

           if (lstBlogType.SelectedIndex == 1)
           {
             bCanSave = clsValidation.IsNullOrEmpty(txtTitle, bCanSave);
             bCanSave = clsValidation.IsNullOrEmpty(txtDescription, bCanSave);
           }
          if(lstBlogType.SelectedIndex == 2)
        {
            bCanSave = clsValidation.IsNullOrEmpty(txtTitle, bCanSave);
            bCanSave = clsValidation.IsNullOrEmpty(txtDescription, bCanSave);
        }

       if (lstBlogType.SelectedIndex == 3)
       {
           bCanSave = clsValidation.IsNullOrEmpty(txtTitle, bCanSave);
           bCanSave = clsValidation.IsNullOrEmpty(txtDescription, bCanSave);
           bCanSave = clsValidation.IsNullOrEmpty(txtVideoLink, bCanSave);
       }
          

           if (bCanSave == true)
           {
               mandatoryDiv.Visible = false;
               lblValidationMessage.Text = "<div class=\"madatoryPaddingDiv\"><img src=\"images/Validation/imgFaceHappy.png\" alt='' title=''/><div class=\"validationMessage\">BlogPost added successfully</div></div>";
               SaveData();
           }
           else
           {
               mandatoryDiv.Visible = true;
               lblValidationMessage.Text = "<div class=\"madatoryPaddingDiv\"><img src=\"images/Validation/imgFaceSad.png\" alt='' title=''/><div class=\"validationMessage\">Please fill out all mandatory fields - BlogPost not added</div></div>";
           }
    }

    protected void lnkbtnClear_Click(object sender, EventArgs e)
    {
        mandatoryDiv.Visible = false;

        //txtStockCode.Text = "";
        //clsValidation.SetValid(txtStockCode);
        lstBlogType.SelectedValue = "0";
        clsValidation.SetValid(lstBlogType);
        txtTitle.Text = "";
        clsValidation.SetValid(txtTitle);
        txtTagLine.Text = "";
        clsValidation.SetValid(txtTagLine);
        txtDescription.Text = "";
        clsValidation.SetValid(txtDescription);
        txtVideoLink.Text = "";
        clsValidation.SetValid(txtVideoLink);
        txtGeoMapLocation.Text = "";
        clsValidation.SetValid(txtGeoMapLocation);
    }

    #endregion 

    #region POPULATE DATA METHODS

    private void popFormData()
    {
         //txtStockCode.Text = clsBlogPosts.strStockCode;
         lstBlogType.SelectedValue = clsBlogPosts.iBlogTypeID.ToString();
         txtTitle.Text = clsBlogPosts.strTitle;
         txtTagLine.Text = clsBlogPosts.strTagLine;
         txtDescription.Text = clsBlogPosts.strDescription;

         //### Populates images
         if (!string.IsNullOrEmpty(clsBlogPosts.strPathToImages))
         {
             lblUniquePath.Text = clsBlogPosts.strPathToImages;
             getList(clsBlogPosts.strPathToImages);
             //### Set Current Master Image
             List<string> lstImagesFileNames = (List<string>)Session["lstImagesFileNames"];
             try
             {
                 foreach (string strImageFileName in lstImagesFileNames)
                 {
                     if (strImageFileName == clsBlogPosts.strMasterImage)
                     {
                         RadioButton rdbMasterImage = dlImages.Items[lstImagesFileNames.IndexOf(strImageFileName)].FindControl("rdbMainImage") as RadioButton;
                         rdbMasterImage.Checked = true;
                         break;
                     }
                 }
             }
             catch { }
         }
         txtVideoLink.Text = clsBlogPosts.strVideoLink;
         txtGeoMapLocation.Text = clsBlogPosts.strGeoMapLocation;
    }
    
    private void popBlogType()
    {
         DataTable dtBlogTypesList = new DataTable();
         lstBlogType.DataSource = clsBlogTypes.GetBlogTypesList();

         //### Populates the drop down list with PK and TITLE;
         lstBlogType.DataValueField = "iBlogTypeID";
         lstBlogType.DataTextField = "strTitle";

         //### Bind the data to the list;
         lstBlogType.DataBind();

         //### Add default select option;
         lstBlogType.Items.Insert(0, new ListItem("--Not Selected--", "0"));
    }
    
    #endregion

    #region SAVE DATA METHODS

    private void SaveData()
    {
        //### Add / Update
        clsBlogPosts.dtAdded = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
        clsBlogPosts.iAddedBy = clsUsers.iUserID;
        clsBlogPosts.dtEdited = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
        clsBlogPosts.iEditedBy = clsUsers.iUserID;
        clsBlogPosts.strStockCode = "";
        clsBlogPosts.iBlogTypeID = Convert.ToInt32(lstBlogType.SelectedValue.ToString());
        clsBlogPosts.strTitle = txtTitle.Text;
        clsBlogPosts.strTagLine = txtTagLine.Text;
        clsBlogPosts.strDescription = txtDescription.Text;

        //### Images related items
        clsBlogPosts.strPathToImages = lblUniquePath.Text;
        clsBlogPosts.strMasterImage = GetMainImagePath(dlImages);
        clsBlogPosts.strVideoLink = txtVideoLink.Text;
        clsBlogPosts.strGeoMapLocation = txtGeoMapLocation.Text;

        clsBlogPosts.Update();

        Session["dtBlogPostsList"] = null;

        //### Go back to view page
        Response.Redirect("frmBlogPostsView.aspx");
    }

    #endregion

    #region IMAGE METHODS

    List<string> lstImages;
    List<string> lstImagesFileNames;
    int iMaxImages = 3;
    string strUniqueFullPath = System.Configuration.ConfigurationManager.AppSettings["WebRootFullPath"] + "\\BlogPosts";
    protected void btnUpload_Click(object sender, EventArgs e)
    {
        if (Session["lstImages"] == null)
        {
            lstImages = new List<string>();
            Session["lstImages"] = lstImages;
        }
        //### Check that they have ONLY HAVE MAX NUMBER OF Images in the datalist
        if (dlImages.Items.Count == iMaxImages)
        {
            mandatoryDiv.Visible = true;
            lblValidationMessage.Text = "You can only have " + iMaxImages.ToString() + " Images.";
            getList(lblUniquePath.Text);
        }
        else
        {
            mandatoryDiv.Visible = false;

            string strUniquePath;
            if (lblUniquePath.Text == "")
            {
                strUniquePath = GetUniquePath();
                lblUniquePath.Text = strUniquePath;
            }
            else
            {
                strUniquePath = lblUniquePath.Text;
            }
            UploadImages(strUniquePath);
            getList(strUniquePath);
        }
    }
    private string GetUniquePath()
    {
        int iCount = 1;
        //### First we need to get the path
        while (System.IO.Directory.Exists(strUniqueFullPath + "\\BlogPosts" + iCount) == true)
        {
            iCount++;
        }
        return "BlogPosts" + iCount;
    }

    protected void UploadImages(String strUniquePath)
    {
        if (FileUpload.PostedFile.ContentLength > 0 && FileUpload.PostedFile.ContentLength < 1073741824)
        {

            //### Upload files to unique folder
            string strUploadFileName = "";
            strUploadFileName = System.IO.Path.GetFileName(FileUpload.PostedFile.FileName);
            string strSaveLocation = "";
            strSaveLocation = strUniqueFullPath + "\\" + strUniquePath + "\\" + strUploadFileName;

            if (!System.IO.Directory.Exists(strUniqueFullPath + "\\" + strUniquePath))
            {
                System.IO.Directory.CreateDirectory(strUniqueFullPath + "\\" + strUniquePath);
            }
            FileUpload.PostedFile.SaveAs(strSaveLocation);

            CopyAndResizePic(strSaveLocation);
            getList(strUniqueFullPath + "\\" + strUniquePath);
        }
        else
        {
            lblValidationMessage.Text = "The file should be between 0 and 1Mb.";
            lblUploadError.Text = "The file should be between 0 and 1Mb.";
        }
    }

    private void CopyAndResizePic(String strFullPath)
    {
        try
        {

            String strFileName;
            String strNewFilePath;

            //### Main Images
            String strLrgFileName;

            strFileName = Path.GetFileName(strFullPath);
            strNewFilePath = strFullPath.Replace(strFileName, "");
            strNewFilePath = strNewFilePath + Path.GetFileNameWithoutExtension(strFullPath);
            strNewFilePath = strNewFilePath + "_lrg";
            strNewFilePath = strNewFilePath + Path.GetExtension(strFullPath);

            File.Copy(strFullPath, strNewFilePath);
            strLrgFileName = Path.GetFileName(strNewFilePath);

            clsCommonFunctions.ResizeImage(strNewFilePath, strNewFilePath, 600, 600, false);

            //### Thumbnail
            String strSmlFileName;

            strFileName = Path.GetFileName(strFullPath);
            strNewFilePath = strFullPath.Replace(strFileName, "");
            strNewFilePath = strNewFilePath + Path.GetFileNameWithoutExtension(strFullPath);
            strNewFilePath = strNewFilePath + "_sml";
            strNewFilePath = strNewFilePath + Path.GetExtension(strFullPath);

            File.Copy(strFullPath, strNewFilePath);
            strSmlFileName = Path.GetFileName(strNewFilePath);

            clsCommonFunctions.ResizeImage(strNewFilePath, strNewFilePath, 140, 140, false);
            
        }
        catch (Exception ex){}
    }

    public void getList(String strPathToFolder)
    {
        lstImages = new List<string>();
        lstImagesFileNames = new List<string>();
        try
        {
            string strPath = strPathToFolder;
            string[] files = Directory.GetFiles(strUniqueFullPath + "\\" + strPath);

            string iBlogPostID = "";
            if (!string.IsNullOrEmpty(Request.QueryString["iBlogPostID"]))
                iBlogPostID = Request.QueryString["iBlogPostID"];

            int iImagesCount = 0;

            foreach (string strName in files)
            {
                if (strName.IndexOf("_sml") != -1)
                {
                    string strHTMLImages = strName.Replace(System.Configuration.ConfigurationManager.AppSettings["WebRootFullPath"] + "\\", "..\\");
                    strHTMLImages = strHTMLImages.Replace("\\", "/");

                    //### Generates a javascript postback for the delete method
                    String strPostBack = Page.ClientScript.GetPostBackEventReference(this, "iRemoveImages:" + iImagesCount);

                    lstImages.Add(@"<tr style='text-align: centre;'>
                                        <td style='text-align: centre;'>
                                            <a class='ImagesColorBox' href='" + strHTMLImages.Replace("_sml", "_lrg") + "'><img src='" + strHTMLImages + @"' alt='' title='' style='border: solid 5px #ffffff;' /></a><br /><br />" +
                                            "<center><div class='buttonStyle' style='padding-top: 5px;'><a href=\"javascript:" + strPostBack + "\" style='color: #ffffff;'><div class='deleteButton'></div></a></div></center>" + @"
                                        </td>
                                    </tr>");
                    lstImagesFileNames.Add(Path.GetFileName(strName).Replace("_sml", ""));
                    iImagesCount++;
                }
            }
            dlImages.DataSource = lstImages;
            dlImages.DataBind();

            Session["lstImages"] = lstImages;
            Session["lstImagesFileNames"] = lstImagesFileNames;
        }
        catch (Exception ex) {}
    }

    private string GetMainImagePath(DataList dtlTarget)
    {
        string strReturn = "";

        foreach (DataListItem dliTarget in dtlTarget.Items)
        {
            RadioButton rdbMainImage = (RadioButton)dliTarget.FindControl("rdbMainImage");
            if (rdbMainImage.Checked)
            {
                lstImagesFileNames = (List<string>)Session["lstImagesFileNames"];
                strReturn = lstImagesFileNames[dliTarget.ItemIndex];
                break;
            }
        }
        return strReturn;
    }

    private void DeleteImages(int iImagesIndex)
    {

        //### Deletes all Images related to the target Images.
        lstImages = (List<string>)Session["lstImages"];
        lstImagesFileNames = (List<string>)Session["lstImagesFileNames"];

        lstImages.RemoveAt(iImagesIndex);
        Session["lstImages"] = lstImages;

        string[] files = Directory.GetFiles(strUniqueFullPath + "\\" + lblUniquePath.Text);
        foreach (string file in files)
        {
            if (Path.GetFileName(file) == lstImagesFileNames[iImagesIndex].ToString())
            {
                //### Remove all Images
                File.Delete(file);
                File.Delete(file.Replace(Path.GetExtension(file), "_sml" + Path.GetExtension(file)));
                File.Delete(file.Replace(Path.GetExtension(file), "_med" + Path.GetExtension(file)));
                File.Delete(file.Replace(Path.GetExtension(file), "_lrg" + Path.GetExtension(file)));
                break;
            }
        }
        lstImagesFileNames.RemoveAt(iImagesIndex);
        ViewState["lstImagesFileNames"] = lstImagesFileNames;
        getList(lblUniquePath.Text);
    }

    #endregion
}