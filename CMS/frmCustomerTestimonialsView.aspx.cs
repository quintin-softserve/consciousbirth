
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.Services;
using System.Web.Script.Services;

/// <summary>
/// Summary description for clsCustomerTestimonialsView
/// </summary>
public partial class CMS_clsCustomerTestimonialsView : System.Web.UI.Page
{
    #region CLASSES AND VARIABLES

    clsUsers clsUsers;
    DataTable dtCustomerTestimonialsList;

    List<clsCustomerTestimonials> glstCustomerTestimonials;

    #endregion

    #region EVENT MODULES

    protected void Page_Load(object sender, EventArgs e)
    {
        //### Check if session clsUser exists
        if (Session["clsUsers"] == null)
        {
            {
                //### Redirect back to login
                Response.Redirect("../CMSLogin.aspx");
            }
        }
        clsUsers = (clsUsers)Session["clsUsers"];

dgrGrid.ItemCreated += new DataGridItemEventHandler(clsCommonFunctions.dgrGrid_PagerPopulate);

        if (!Page.IsPostBack)
        {
            PopulateFormData();
        }
        else
        {
            if (Session["glstCustomerTestimonials"] == null)
            {
                glstCustomerTestimonials = new List<clsCustomerTestimonials>();
                Session["glstCustomerTestimonials"] = glstCustomerTestimonials;
            }
            else
                glstCustomerTestimonials = (List<clsCustomerTestimonials>)Session["glstCustomerTestimonials"];
        }
            PopulateFormData();

    }

    protected void lnkbtnSearch_Click(object sender, EventArgs e)
    {
        string FilterExpression ="(strCustomerName +' '+ strTestimonial) LIKE '%" + txtSearch.Text + "%'";
        DataTable dtCustomerTestimonialsList = clsCustomerTestimonials.GetCustomerTestimonialsList(FilterExpression, "");

        Session["dtCustomerTestimonialsList"] = dtCustomerTestimonialsList;

        PopulateFormData();
    }

    protected void lnkbtnAdd_Click(object sender, EventArgs e)
    {
        Response.Redirect("frmCustomerTestimonialsAddModify.aspx");
    }

    #endregion

    #region DATA GRID METHODS

    private void PopulateFormData()
    {
        //### Populate datagrid using clsCustomerTestimonialsList object
        try
        {
            dtCustomerTestimonialsList = new DataTable();

            if (Session["dtCustomerTestimonialsList"] == null)
                dtCustomerTestimonialsList = clsCustomerTestimonials.GetCustomerTestimonialsList();
            else
                dtCustomerTestimonialsList = (DataTable)Session["dtCustomerTestimonialsList"];

            dgrGrid.DataSource = dtCustomerTestimonialsList;

            //### Add this check to trap if the last item on the page is deleted
            if (Convert.ToInt16(dgrGrid.CurrentPageIndex) < Convert.ToInt16(Session["CustomerTestimonialsView_CurrentPageIndex"]))
            {
                dgrGrid.CurrentPageIndex = 0;
            }
            else
            {
                if (Session["CustomerTestimonialsView_CurrentPageIndex"] != null)
                {
                    dgrGrid.CurrentPageIndex = (int)Session["CustomerTestimonialsView_CurrentPageIndex"];
                }
            }

            dgrGrid.DataBind();

        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    protected void lnkDeleteItem_Click(object sender, EventArgs e)
    {
        int iCustomerTestimonialID = int.Parse((sender as LinkButton).CommandArgument);

        clsCustomerTestimonials.Delete(iCustomerTestimonialID);

        PopulateFormData();
    }

    protected void dgrGrid_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
    {
        //### Method is used when the page index is changed.
        try
        {
            Session["CustomerTestimonialsView_CurrentPageIndex"] = e.NewPageIndex;
            dgrGrid.CurrentPageIndex = e.NewPageIndex;
            PopulateFormData();
        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    protected void dgrGrid_SortCommand(object source, DataGridSortCommandEventArgs e)
    {
        //### Sort Method
        dtCustomerTestimonialsList = new DataTable();

        if (Session["dtCustomerTestimonialsList"] == null)
            dtCustomerTestimonialsList = clsCustomerTestimonials.GetCustomerTestimonialsList();
        else
            dtCustomerTestimonialsList = (DataTable)Session["dtCustomerTestimonialsList"];

        DataView dvTemp = dtCustomerTestimonialsList.DefaultView;

        dvTemp.Sort = e.SortExpression;
        dtCustomerTestimonialsList = dvTemp.ToTable();
        Session["dtCustomerTestimonialsList"] = dtCustomerTestimonialsList;

        PopulateFormData();
    }

    public void dgrGrid_PopEditLink(object sender, EventArgs e)
    {
        try
        {
            //### Add hyperlink to datagrid item
            HyperLink lnkEditItem = (HyperLink)sender;
            DataGridItem dgrItemEdit = (DataGridItem)lnkEditItem.Parent.Parent;

            //### Get the iCustomerTestimonialID
            int iCustomerTestimonialID = 0;
            iCustomerTestimonialID = int.Parse(dgrItemEdit.Cells[0].Text);

            //### Add attributes to edit link
            lnkEditItem.Attributes.Add("href", "frmCustomerTestimonialsAddModify.aspx?action=edit&iCustomerTestimonialID=" + iCustomerTestimonialID);
        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

     public void dgrGrid_PopDeleteLink(object sender, EventArgs e)
     {
          try
          {
             //### Add hyperlink to datagrid item
             HyperLink lnkDeleteItem = (HyperLink)sender;
             DataGridItem dgrItemDelete = (DataGridItem)lnkDeleteItem.Parent.Parent;

             //### Get the iCustomerTestimonialID
             int iCustomerTestimonialID = 0;
             iCustomerTestimonialID = int.Parse(dgrItemDelete.Cells[0].Text);

             //### Add attributes to delete link
             lnkDeleteItem.CssClass = "dgrDeleteLink";
             lnkDeleteItem.Attributes.Add("href", "frmCustomerTestimonialsView.aspx?action=delete&iCustomerTestimonialID=" + iCustomerTestimonialID);
           }
           catch (Exception ex)
           {
                 Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
           }
     }

    #endregion

    #region CustomerTestimonials FILTERING

    /// <summary>
    /// Enables the AutoCompleteExtender
    /// </summary>
    /// <param name="prefixText"></param>
    /// <param name="count"></param>
    /// <returns></returns>
    /// 
    [WebMethod]
    [ScriptMethod]
    public static string[] FilterBusinessRule(string prefixText, int count)
    {
        string[] strReturnList = new string[] { };

        string FilterExpression = "(strCustomerName +' '+ strTestimonial) LIKE '%" + prefixText + "%'";
        DataTable dtCustomerTestimonials = clsCustomerTestimonials.GetCustomerTestimonialsList(FilterExpression, "");
        List<string> glstCustomerTestimonials = new List<string>();

        if (dtCustomerTestimonials.Rows.Count > 0)
        {
            foreach (DataRow dtrCustomerTestimonials in dtCustomerTestimonials.Rows)
            {
                glstCustomerTestimonials.Add(AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem("" + dtrCustomerTestimonials["strCustomerName"].ToString() +' '+"" + dtrCustomerTestimonials["strTestimonial"].ToString(), dtrCustomerTestimonials["iCustomerTestimonialID"].ToString()));
            }
        }
        else
            glstCustomerTestimonials.Add("No CustomerTestimonials Available.");
        strReturnList = glstCustomerTestimonials.ToArray();
        return strReturnList;
    }

    #endregion
}
