using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class CMS_frmBlogTypesAddModify : System.Web.UI.Page
{
    clsUsers clsUsers;
    clsBlogTypes clsBlogTypes;
    #region EVENT METHODS

    protected void Page_Load(object sender, EventArgs e)
    {
        //### Check if session clsUser exists
        if (Session["clsUsers"] == null)
        {
            {
                //### Redirect back to login
                Response.Redirect("../CMSLogin.aspx");
            }
        }
        clsUsers = (clsUsers)Session["clsUsers"];

        if (!IsPostBack)
        {

            //### If the iBlogTypeID is passed through then we want to instantiate the object with that iBlogTypeID
            if ((Request.QueryString["iBlogTypeID"] != "") && (Request.QueryString["iBlogTypeID"] != null))
            {
                clsBlogTypes = new clsBlogTypes(Convert.ToInt32(Request.QueryString["iBlogTypeID"]));

                //### Populate the form
                popFormData();
            }
            else
            {
                clsBlogTypes = new clsBlogTypes();
            }
            Session["clsBlogTypes"] = clsBlogTypes;
        }
        else
        {
            clsBlogTypes = (clsBlogTypes)Session["clsBlogTypes"];
        }
    }

    protected void lnkbtnBack_Click(object sender, EventArgs e)
    {
        //### Go back to previous page
        Response.Redirect("frmBlogTypesView.aspx");
    }

    protected void lnkbtnSave_Click(object sender, EventArgs e)
    {
        //### Validate registration process
        bool bCanSave = true;

       bCanSave = clsValidation.IsNullOrEmpty(txtTitle, bCanSave);

        if (bCanSave == true)
        {
            mandatoryDiv.Visible = false;
            lblValidationMessage.Text = "<div class=\"madatoryPaddingDiv\"><img src=\"images/Validation/imgFaceHappy.png\" alt='' title=''/><div class=\"validationMessage\">BlogType added successfully</div></div>";
            SaveData();
        }
        else
        {
            mandatoryDiv.Visible = true;
            lblValidationMessage.Text = "<div class=\"madatoryPaddingDiv\"><img src=\"images/Validation/imgFaceSad.png\" alt='' title=''/><div class=\"validationMessage\">Please fill out all mandatory fields - BlogType not added</div></div>";
        }
    }

    protected void lnkbtnClear_Click(object sender, EventArgs e)
    {
        mandatoryDiv.Visible = false;

        txtTitle.Text = "";
        clsValidation.SetValid(txtTitle);
        txtContent.Text = "";
        clsValidation.SetValid(txtContent);
    }

    #endregion 

    #region POPULATE DATA METHODS

    private void popFormData()
    {
         txtTitle.Text = clsBlogTypes.strTitle;
         txtContent.Text = clsBlogTypes.strContent;
    }
    
    #endregion

    #region SAVE DATA METHODS

    private void SaveData()
    {
        //### Add / Update
        clsBlogTypes.dtAdded = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
        clsBlogTypes.iAddedBy = clsUsers.iUserID;
        clsBlogTypes.dtEdited = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
        clsBlogTypes.iEditedBy = clsUsers.iUserID;
        clsBlogTypes.strTitle = txtTitle.Text;
        clsBlogTypes.strContent = txtContent.Text;

        clsBlogTypes.Update();

        Session["dtBlogTypesList"] = null;

        //### Go back to view page
        Response.Redirect("frmBlogTypesView.aspx");
    }

    #endregion
}