﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.Services;
using System.Web.Script.Services;
using System.Web.UI.HtmlControls;

public partial class CMS_frmSubscribersView : System.Web.UI.Page
{
    clsSubscribers clsSubscribers;
    DataTable dtSubscribersList;

    List<clsSubscribers> glstSubscribers;

    #region EVENT METHODS

    protected void Page_Load(object sender, EventArgs e)
    {
        //### Check if session clsSubscriber exists
        if (Session["clsSubscribers"] == null)
        {
            //### Redirect back to login
            //Response.Redirect("../CMSLogin.aspx");
        }
        clsSubscribers = (clsSubscribers)Session["clsSubscribers"];

        dgrGrid.ItemCreated += new DataGridItemEventHandler(dgrGrid_PagerPopulate);

        if (!Page.IsPostBack)
        {
            PopulateFormData();
        }
        else
        {
            if (Session["glstSubscribers"] == null)
            {
                glstSubscribers = new List<clsSubscribers>();
                Session["glstSubscribers"] = glstSubscribers;
            }
            else
                glstSubscribers = (List<clsSubscribers>)Session["glstSubscribers"];

            PopulateFormData();
        }
    }

    protected void lnkbtnSearch_Click(object sender, EventArgs e)
    {
        string FilterExpression = " (strFirstName + ' ' + strSurname + ' ' + strEmailAddress) LIKE '%" + txtSearch.Text + "%'";

        DataTable dtSubscribersList = clsSubscribers.GetSubscribersList(FilterExpression, "");

        Session["dtSubscribersList"] = dtSubscribersList;
        PopulateFormData();
    }
    
    protected void lnkbtnAdd_Click(object sender, EventArgs e)
    {
        Response.Redirect("frmSubscribersAddModify.aspx");
    }

    #endregion

    #region DATA GRID METHODS

    private void PopulateFormData()
    {
        //### Populate datagrid using clsSubscriberList object
        try
        {
            dtSubscribersList = new DataTable();

            if (Session["dtSubscribersList"] == null)
                dtSubscribersList = clsSubscribers.GetSubscribersList();
            else
                dtSubscribersList = (DataTable)Session["dtSubscribersList"];

            dgrGrid.DataSource = dtSubscribersList;

            //### Add this check to trap if the last item on the page is deleted
            if (Convert.ToInt16(dgrGrid.CurrentPageIndex) < Convert.ToInt16(Session["SubscriberView_CurrentPageIndex"]))
            {
                dgrGrid.CurrentPageIndex = 0;
            }
            else
            {
                if (Session["SubscriberView_CurrentPageIndex"] != null)
                {
                    dgrGrid.CurrentPageIndex = (int)Session["SubscriberView_CurrentPageIndex"];
                }
            }

            dgrGrid.DataBind();

        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    protected void lnkDeleteItem_Click(object sender, EventArgs e)
    {
        int iSubscriberID = int.Parse((sender as LinkButton).CommandArgument);

        clsSubscribers.Delete(iSubscriberID);

        PopulateFormData();
    }

    protected void dgrGrid_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
    {
        //### Method is used when the page index is changed.
        try
        {
            Session["SubscriberView_CurrentPageIndex"] = e.NewPageIndex;
            dgrGrid.CurrentPageIndex = e.NewPageIndex;
            PopulateFormData();
        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    protected void dgrGrid_SortCommand(object source, DataGridSortCommandEventArgs e)
    {
        //### Sort Method
        dtSubscribersList = new DataTable();

        if (Session["dtSubscribersList"] == null)
            dtSubscribersList = clsSubscribers.GetSubscribersList();
        else
            dtSubscribersList = (DataTable)Session["dtSubscribersList"];

        DataView dvTemp = dtSubscribersList.DefaultView;

        dvTemp.Sort = e.SortExpression;
        dtSubscribersList = dvTemp.ToTable();
        Session["dtSubscribersList"] = dtSubscribersList;

        PopulateFormData();
    }

    public void dgrGrid_PopEditPassLink(object sender, EventArgs e)
    {
        try
        {
            //### Add hyperlink to datagrid item
            HyperLink lnkEditPass = (HyperLink)sender;
            DataGridItem dgrItemEditPass = (DataGridItem)lnkEditPass.Parent.Parent;

            //### Get the iSubscriberID
            int iSubscriberID = 0;
            iSubscriberID = int.Parse(dgrItemEditPass.Cells[0].Text);

            //### Add attributes to edit link
            lnkEditPass.Attributes.Add("href", "frmSubscribersPasswordEdit.aspx?iSubscriberID=" + iSubscriberID);
        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    public void dgrGrid_PopEditLink(object sender, EventArgs e)
    {
        try
        {
            //### Add hyperlink to datagrid item
            HyperLink lnkEditItem = (HyperLink)sender;
            DataGridItem dgrItemEdit = (DataGridItem)lnkEditItem.Parent.Parent;

            //### Get the iSubscriberID
            int iSubscriberID = 0;
            iSubscriberID = int.Parse(dgrItemEdit.Cells[0].Text);

            //### Add attributes to edit link
            lnkEditItem.Attributes.Add("href", "frmSubscribersAddModify.aspx?action=edit&iSubscriberID=" + iSubscriberID);
        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    public void dgrGrid_PopDeleteLink(object sender, EventArgs e)
    {
        try
        {
            //### Add hyperlink to datagrid item
            HyperLink lnkDeleteItem = (HyperLink)sender;
            DataGridItem dgrItemDelete = (DataGridItem)lnkDeleteItem.Parent.Parent;

            //### Get the iSubscriberID
            int iSubscriberID = 0;
            iSubscriberID = int.Parse(dgrItemDelete.Cells[0].Text);

            //### Add attributes to delete link
            lnkDeleteItem.CssClass = "dgrDeleteLink";
            lnkDeleteItem.Attributes.Add("href", "frmSubscribersView.aspx?action=delete&iSubscriberID=" + iSubscriberID);
        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    public static void dgrGrid_PagerPopulate(object sender, DataGridItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Pager)
        {
            Control[] arrayControls = new Control[e.Item.Controls[0].Controls.Count];

            for (int i = 0; i < e.Item.Controls[0].Controls.Count; i++)
            {
                arrayControls[i] = e.Item.Controls[0].Controls[i];

                //### Check if the first is a previous page button
                if (e.Item.Controls[0].Controls[i] is LinkButton)
                {
                }
            }

            //### Remove the crappy built in span used for paging
            for (int i = 0; i < e.Item.Controls[0].Controls.Count; i++)
            {
                e.Item.Controls[0].Controls.RemoveAt(i);
            }

            for (int i = 0; i < arrayControls.Length; i++)
            {
                if (arrayControls[i] is Label)
                {
                    //### Current page
                    LinkButton lblPager = new LinkButton();
                    HtmlGenericControl divPager = new HtmlGenericControl("div");
                    lblPager.Width = 22;
                    divPager.Attributes.Add("style", "margin-top:10px");
                    lblPager.Attributes.Add("style", "margin-right:2px");
                    divPager.Attributes.Add("style", "height:20px");
                    divPager.InnerText = ((Label)arrayControls[i]).Text;
                    lblPager.Controls.Add(divPager);
                    e.Item.Controls[0].Controls.Add(lblPager);

                }
                if (arrayControls[i] is LinkButton)
                {
                    //### All the other pages
                    LinkButton btnPager = new LinkButton();
                    HtmlGenericControl divPager = new HtmlGenericControl("p");
                    btnPager.Width = 22;
                    btnPager.CommandArgument = ((LinkButton)arrayControls[i]).CommandArgument;
                    btnPager.CommandName = ((LinkButton)arrayControls[i]).CommandName;
                    divPager.Attributes.Add("style", "margin-top:3px");
                    btnPager.Attributes.Add("style", "margin-right:2px");
                    divPager.InnerText = ((LinkButton)arrayControls[i]).Text;
                    btnPager.Controls.Add(divPager);
                    e.Item.Controls[0].Controls.Add(btnPager);
                }
            }
        }
    }

    #endregion

    #region Subscriber FILTERING
    
    /// <summary>
    /// Enables the AutoCompleteExtender
    /// </summary>
    /// <param name="prefixText"></param>
    /// <param name="count"></param>
    /// <returns></returns>
    /// 
    [WebMethod]
    [ScriptMethod]
    public static string[] FilterBusinessRule(string prefixText, int count)
    {
        string[] strReturnList = new string[] { };

        string FilterExpression = " (strFirstName + ' ' + strSurname + ' ' + strEmailAddress) LIKE '%" + prefixText + "%'";
        DataTable dtSubscribers = clsSubscribers.GetSubscribersList(FilterExpression, "");
        List<string> glstSubscribers = new List<string>();

        if (dtSubscribers.Rows.Count > 0)
        {
            foreach (DataRow dtrSubscribers in dtSubscribers.Rows)
            {
                glstSubscribers.Add(AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem("" + dtrSubscribers["strFirstName"].ToString() + " " + dtrSubscribers["strSurname"].ToString(), dtrSubscribers["iSubscriberID"].ToString()));
            }
        }
        else
            glstSubscribers.Add("No Subscribers Available.");

        strReturnList = glstSubscribers.ToArray();

        return strReturnList;
    }

    #endregion
}