﻿<%@ Page Title="My Gift Registry" Language="C#" MasterPageFile="~/ConsciousBirth.master" AutoEventWireup="true" CodeFile="Conscious-Birth-My-Gift-Registry.aspx.cs" Inherits="Conscious_Birth_Wishlist" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script src="CMS/scripts/jsGeneric.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div id="divData" runat="server">
        <asp:UpdatePanel ID="udpOrderItemsView" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <center>
                <div class='clr'></div>
                <div style="width: 100%">                 
                    <h2 class='headingText' style='margin-top: 15px;'><asp:Label ID="Username" runat="server" Text=""></asp:Label>My Wishlist</h2>
                    <br />
                  
                        <asp:Label ID="labText" runat="server" Text=""></asp:Label>
                </div>
                    <br />
                         <div id ="HideAcountData" visible="false" runat="server" >
                        <div class="container">
                            <div class="feature" runat="server" id="div1" visible="true">
                                <table cellpadding="3" cellspacing="0" class="dgr">
                                    <tr class="dgrHeader">
                                    <td align="center" width="200px">Event Name
                                    </td>
                                    <td align="center" width="100px">Address
                                    </td>
                                    <td align="center" width="100px">Date of Event
                                    </td>
                                    <td align="center" width="100px">View List
                                    </td>
                                    </tr>
                                    <asp:Repeater ID="rpAccountUsers" runat="server">
                                        <ItemTemplate>
                                            <tr>
                                                <td style="text-align:center ;border: none; height: 67px; width: 67px; padding-top: 5px;">
                                                    <%#Eval ("strEventName") %>
                                                </td>
                                                <td style="text-align:center">
                                                    <%#Eval ("strAddressOfEvent") %>
                                                </td>
                                                <td style="text-align:center">
                                                    <%#Eval ("dtEvent", "{0:MM/dd/yyyy}") %>
                                                </td>
                                                <td style='text-align:center'>
                                                    <asp:LinkButton ID="linkbViewGiftRegestery" Style="color:white; border-radius: 5px;" OnClick="linkbViewGiftRegestery_Click" CommandArgument=<%#Eval ("iWishListEventID") %> CssClass="PienkButton" runat="server">View registry</asp:LinkButton>
                                                </td>
                                            </tr>
                                            
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </table>
                            </div>
                        </div>
                            
                    </div>
                    <div id="HideButton" runat="server">
                        <asp:Button ID="btnSubmit" runat="server" Text="Add Event" class="" OnClick="btnSubmit_Click" />
                    </div>
                    <div id="HideData" runat="server">
                    <div class="container">
                        <div class="feature" runat="server" id="divGrid" visible="true">
                            <table cellpadding="3" cellspacing="0" class="dgr">
                                <tr class="dgrHeader">
                                    <td align="center" width="100px">&nbsp;
                                    </td>
                                    <td align="center" width="200px">Product Title
                                    </td>
                                    <td align="center" width="100px">Quantity
                                    </td>
                                    <td align="center" width="100px">Colour
                                    </td>
                                    <td align="center" width="100px">Size
                                    </td>
                                    <td align="center" width="100px">Total Cost
                                    </td>
                                    <td style="text-align:center; width:100px">Delete
                                    </td>
                                </tr>
                               <asp:Literal ID="litWishlist" runat="server"></asp:Literal>
                            </table>
                        </div>
                    </div>
                    <div class="feature" id="divTotalAndButtons" runat="server">
                        <div>
                            Total Incl VAT: R<asp:Label runat="server" ID="lblGridTotal" Style="margin-right: 0px;">0.00</asp:Label>
                        </div>
                        <br />
                        <br style="clear: both;" />
                        <div class="container">
                            
                            <br />
                         
                          
                            </div>
                </div>
                    </div>
                <asp:UpdatePanel runat="server" ID="updStep2" UpdateMode="Always">
                    <ContentTemplate>
                        <div style="width: 100%; margin-bottom: 30px;">
                            <div class="feature" runat="server" id="step2" visible="false">
                                <h2 class="headingText">Delivery Information</h2>
                                <br />
                                <div id="mandatoryDiv" class="mandatoryInvalidDiv" runat="server" visible="false">
                                    <asp:Label ID="lblValidationMessage" runat="server"></asp:Label>
                                </div>
                                <div class="controlDiv">
                                    <div class="imageHolderCommonDiv">
                                        <div class="validationImageMandatory"></div>
                                    </div>
                                    <div class="labelDiv">
                                        Name:
                                    </div>
                                    <div class="fieldDiv">
                                        <asp:TextBox ID="txtContactPerson" runat="server" CssClass="roundedCornerTextBoxMini" onKeyUp="return SetMaxLength(this,50)"/>
                                    </div>
                                    <br class="clear" />
                                </div>
                                <div class="controlDiv">
                                    <div class="imageHolderCommonDiv">
                                        <div class="validationImageMandatory"></div>
                                    </div>
                                    <div class="labelDiv">Contact number:</div>
                                    <div class="fieldDiv">
                                        <asp:TextBox ID="txtContactNumber" runat="server" CssClass="roundedCornerTextBoxMini" onKeyUp="return SetMaxLength(this,20)" onblur="CheckNumeric();"/>
                                    </div>
                                    <br class="clear" />
                                </div>
                                <div class="controlDiv">
                                    <div class="imageHolderCommonDiv">
                                        <div class="validationImageMandatory"></div>
                                    </div>
                                    <div class="labelDiv">
                                        Email Address:
                                    </div>
                                    <div class="fieldDiv">
                                        <asp:TextBox ID="txtEmailAddress" runat="server" CssClass="roundedCornerTextBoxMini" onKeyUp="return SetMaxLength(this,40)" onblur="emailValidator(this);" />
                                    </div>
                                </div>
                                <br class="clear" />
                                <div class="controlDiv" style="margin-bottom: 16px;">
                                    <div class="imageHolderCommonDiv">
                                        <div class="validationImageMandatory"></div>
                                    </div>
                                    <div class="labelDiv">
                                        Physical delivery address:
                                    </div>
                                    <div class="fieldDiv">
                                        <asp:TextBox ID="txtPhysicalAddress" runat="server" CssClass="roundedCornerTextBoxMultiLine2" Height="60px" Width="594px" onKeyUp="return SetMaxLength(this,250)" TextMode="MultiLine" Rows="2" />
                                    </div>
                                    <br class="clear" />
                                </div>
                                <%--                        <div class="feature" style="padding:30px !important;">
                            <div style="float:left;margin:5px 30px 0px 0px;">
                                Discount code:  &nbsp;&nbsp;&nbsp;<asp:TextBox runat="server" ID="txtDiscountCode" style="text-transform:none;"></asp:TextBox>
                            </div>
                            <div style="float:left;">
                                <asp:LinkButton runat="server" ID="btnDiscount" 
                                    CssClass="buttonTemplate buttonArrow centerText textDecoNone" 
                                    Text="Validate Discount" onclick="btnDiscount_Click" ></asp:LinkButton>
                            </div>
                            <div style="clear:both"></div>
                            <asp:Literal runat="server" ID="litDiscount"></asp:Literal>
                        </div>--%>
                                <%--                        <div class="controlDiv" style='margin-bottom:15px;'>
                            <div class="imageHolderCommonDiv">
                                <div class="validationImageMandatory"></div>
                            </div>
                            <div style="width:100%;text-align:left;">
                            <br />
                                
                                <div runat="server" id="divCheckBoxes">
                                <asp:CheckBox runat="server" ID="cbLeaveWithNeighbours" Text="I do not mind if you leave the flowers with the neighbours / colleagues / family."/><br />
                                <asp:CheckBox runat="server" ID="cbSurprise" Text="This is a surprise. Do not phone the person."/><br />
                                <asp:CheckBox runat="server" ID="cbDeliveryPicture" Text="I would like to receive my delivery picture."/>
                                </div>
                            </div>
                            <br class="clear" />
                        </div>--%>
                                <div style='margin: 35px 0px 0px 0px;'>
                             
                                </div>
                              
                              
                            </div>
                        </div>
                    </ContentTemplate>
                    <Triggers>
                <%--        <asp:LinkButton ControlID="linkDeliet" />--%>
                    </Triggers>

                </asp:UpdatePanel>
                </center>
                <%--<asp:UpdatePanel runat="server" ID="updStep3" UpdateMode="Always">
                    <ContentTemplate>
                    <div runat="server" id="step3" visible="false">
                        <div style="width:100%;">
                            <div class="feature">
                                PAYMENT INTEGRATION
                            </div>
                        </div>
                    </div>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnProceedToStep3"/>
                    </Triggers>
                </asp:UpdatePanel>--%>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>
