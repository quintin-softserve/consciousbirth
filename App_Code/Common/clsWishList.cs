﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

/// <summary>
/// Summary description for clsWishList
/// </summary>
public class clsWishList
{
    int iWishListCount = 0;

    //### WishList Class.
    public clsWishList()
    {
    }

    public delegate void WishListChanged(EventArgs e);

    public static event WishListChanged myWishListEvent;

    #region WishList SETUP METHODS

    //### Static method to create a new order session and a new order entry into tblOrders.
    public static void WishListSetup(int iProductID, int iQuantityID, string strColour, string strSize)
    {
        if (HttpContext.Current.Session["loggedOutOrder"].ToString() != "" && HttpContext.Current.Session["loggedOutOrder"] != null)
        {
            clsOrders clsOrders = new clsOrders();

            if (HttpContext.Current.Session["clsAccountHolders"] == null)
            {
                // ID = -10 for orders added by users that are once off customers
                clsOrders.dtAdded = DateTime.Now;
                clsOrders.iAddedBy = -10;
                clsOrders.iOrderStatusID = 6; //FOR ORDERS THAT HAVE BEEN ADDED TO THE SYSTEM BUT PAYMENT BEFORE PAYMENT
                clsOrders.strOrderNumber = clsCommonFunctions.GetGeneratedNumber("tblOrders", "strOrderNumber", "ORD-", 4);
                clsOrders.dblTotalAmount = Convert.ToDouble(GetLoggedOutOrderTotal().ToString("N2"));
                clsOrders.dblDeliveryCost = 0.00;
                clsOrders.iDeliveryInfoID = 0;
                //clsOrders.iDeliveryInfoID = iDeliveryInfoID;

                clsOrders.Update();


                        clsOrderItems clsOrderItem = new clsOrderItems();
                        clsOrderItem.iQuantity = iQuantityID;
                        clsOrderItem.dtAdded = DateTime.Now;
                        clsOrderItem.dtEdited = DateTime.Now;
                        clsOrderItem.iEditedBy = 0;
                        clsOrderItem.iProductID = iProductID;
                        clsOrderItem.strColour = strColour;
                        clsOrderItem.strSize = strSize;
                        clsOrderItem.iAddedBy = 0;
                        clsOrderItem.iOrderID = clsOrders.iOrderID;
                        clsOrderItem.Update();

                //### Save the OrderID to the session
                HttpContext.Current.Session["iOrderID"] = clsOrders.iOrderID;
                HttpContext.Current.Session["clsOrders"] = clsOrders;
            }
            //else if (HttpContext.Current.Session["clsAccountHolders"] != null)
            //{
            //    clsAccountHolders = (clsAccountHolders)HttpContext.Current.Session["clsAccountHolders"];

            //    clsOrders.dtAdded = DateTime.Now;
            //    clsOrders.iAddedBy = clsAccountHolders.iAccountHolderID;
            //    clsOrders.iOrderStatusID = 6; //FOR ORDERS THAT HAVE BEEN ADDED TO THE SYSTEM BUT PAYMENT BEFORE PAYMENT
            //    clsOrders.iAccountHolderID = clsAccountHolders.iAccountHolderID;
            //    clsOrders.strOrderNumber = clsCommonFunctions.GetGeneratedNumber("tblOrders", "strOrderNumber", "ORD-", 4);
            //    clsOrders.dblTotalAmount = Convert.ToDouble(GetLoggedOutOrderTotal().ToString("N2"));
            //    clsOrders.dblDeliveryCost = 0.00;
            //    clsOrders.iDeliveryInfoID = 0;
            //    //clsOrders.iDeliveryInfoID = clsAccountHolders.iDeliveryInfoID;

            //    clsOrders.Update();

            //    //### Save the OrderID to the session
            //    HttpContext.Current.Session["iOrderID"] = clsOrders.iOrderID;
            //    HttpContext.Current.Session["clsOrders"] = clsOrders;
            //}
        }
    }

    public static int WishListSetup(int iDeliveryInfoID, double dblDeliveryCharge)
    {
        int iCurrentOrderID = 0;

        if (HttpContext.Current.Session["loggedOutOrder"] != "" && HttpContext.Current.Session["loggedOutOrder"] != null)
        {
            //clsAccountHolders clsAccountHolders;
            clsOrders clsOrders = new clsOrders();

            if (HttpContext.Current.Session["clsAccountHolders"] == null)
            {
                // ID = -10 for orders added by users that are once off customers
                clsOrders.dtAdded = DateTime.Now;
                clsOrders.iAddedBy = -10;
                clsOrders.iOrderStatusID = 6; //FOR ORDERS THAT HAVE BEEN ADDED TO THE SYSTEM BEFORE PAYMENT
                //clsOrders.iAccountHolderID = -10;
                clsOrders.strOrderNumber = clsCommonFunctions.GetGeneratedNumber("tblOrders", "strOrderNumber", "ORD-", 4);
                clsOrders.dblTotalAmount = Convert.ToDouble(GetLoggedOutOrderTotal().ToString("N2")) + dblDeliveryCharge;
                clsOrders.dblDeliveryCost = dblDeliveryCharge;
                clsOrders.iDeliveryInfoID = iDeliveryInfoID;

                clsOrders.Update();

                //### Save the OrderID to the session
                HttpContext.Current.Session["iOrderID"] = clsOrders.iOrderID;
                HttpContext.Current.Session["clsOrders"] = clsOrders;

                iCurrentOrderID = clsOrders.iOrderID;
            }
            else if (HttpContext.Current.Session["clsAccountHolders"] != null)
            {
                //clsAccountHolders = (clsAccountHolders)HttpContext.Current.Session["clsAccountHolders"];

                clsOrders.dtAdded = DateTime.Now;
                clsOrders.iAddedBy = -10;//clsAccountHolders.iAccountHolderID;
                clsOrders.iOrderStatusID = 6; //FOR ORDERS THAT HAVE BEEN ADDED TO THE SYSTEM BEFORE PAYMENT
                //clsOrders.iAccountHolderID = clsAccountHolders.iAccountHolderID;
                clsOrders.strOrderNumber = clsCommonFunctions.GetGeneratedNumber("tblOrders", "strOrderNumber", "ORD-", 4);
                clsOrders.dblTotalAmount = Convert.ToDouble(GetLoggedOutOrderTotal().ToString("N2"));
                clsOrders.dblDeliveryCost = dblDeliveryCharge;
                clsOrders.iDeliveryInfoID = iDeliveryInfoID;

                clsOrders.Update();

                //### Save the OrderID to the session
                HttpContext.Current.Session["iOrderID"] = clsOrders.iOrderID;
                HttpContext.Current.Session["clsOrders"] = clsOrders;

                iCurrentOrderID = clsOrders.iOrderID;
            }
        }
        return iCurrentOrderID;
    }

    #endregion

    #region ADD TO WishList METHODS

    //### Static method to add items to the user's WishList.
    public static void addToWishList(int iProductID, int iQuantity, string strColors, string strSizes)
    {
        //### Check for an Order in session
        if (HttpContext.Current.Session["iOrderID"] == null)
        {
            if (HttpContext.Current.Session["clsAccountHolders"] != null)
            {
                //### If there is no order in session and the user is logged in, create a new order entry to tblOrders.
                WishListSetup(iProductID, iQuantity, strColors, strSizes);
            }
        }

        //### Check to see if the user is logged in.
        if (HttpContext.Current.Session["clsAccountHolders"] != null)
        {
            //### If the user is logged in, create the order as normal.
            //addWishListItem(iProductID, iQuantity); --->Replacing to be handled by the cookie
            addWishListItemCookie(iProductID, iQuantity, strColors, strSizes);
        }
        else
        {
            //### If the user is not logged in, create a logged out order(in session order + cookie order).
            //### The cookie gives the user the ability to register and continue with the order within 7 days.
           
            addWishListItemCookie(iProductID, iQuantity, strColors, strSizes);
        }
    }

    //### Static method to add WishList items to tblOrderItems.
    public static void addWishListItem(int iProductID, int iQuantity)
    {
        int iOrderID = Convert.ToInt32(HttpContext.Current.Session["iOrderID"]);

        //### Check if the order item exists.
        //if (!clsCommonFunctions.DoesRecordExist("tblOrderItems", "iOrderID = " + iOrderID + " AND iProductID = " + iProductID + " AND bIsDeleted=0"))
        //{
        //    //### If the order item exists create a new entry.
        //    clsOrderItems clsOrderItems = new clsOrderItems();

        //    clsOrderItems.dtAdded = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
        //    if ((HttpContext.Current.Session["clsAccountHolders"] != null))
        //    {
        //        clsAccountHolders clsAccountHolders = (clsAccountHolders)HttpContext.Current.Session["clsAccountHolders"];
        //        clsOrderItems.iAddedBy = clsAccountHolders.iAccountHolderID;
        //    }
        //    else
        //        clsOrderItems.iAddedBy = 0;

        //    clsOrderItems.iOrderID = iOrderID;
        //    clsOrderItems.iProductID = iProductID;
        //    clsOrderItems.iQuantity = iQuantity;

        //    clsOrderItems.Update();
        //}
    }

    #endregion

    #region ADD TO WishList COOKIE METHODS

    //### Static method to add a WishList item to a cookie.
    public static void addWishListItemCookie(int iProductID, int iQuantity, string strColors, string strSizes)
    {
        //### Trap for logged out order in session.
        if (HttpContext.Current.Session["loggedOutOrder"] == null)
        {
            //### If there is no order in session check to see if a cookie exists.
            if (HttpContext.Current.Request.Cookies["OrderItems"] != null)
            {
                //### If a cookie exists, add the cookie values to the current session.
                if (HttpContext.Current.Request.Cookies["OrderItems"]["OrderItemInfo"] != null)
                {
                    HttpContext.Current.Session["loggedOutOrder"] = HttpContext.Current.Request.Cookies["OrderItems"]["OrderItemInfo"];
                    //HttpContext.Current.Request.Cookies["OrderItems"]["OrderItemInfo"] = null;
                    //HttpContext.Current.Session["loggedOutOrder"] = null;
                    //HttpContext.Current.Session["loggedOutOrder"] = HttpContext.Current.Request.Cookies["OrderItems"]["OrderItemInfo"];
                    //### Loop through this method to add the current item to the existing list of items.
                    addWishListItemCookie(iProductID, iQuantity, strColors, strSizes);
                }
                else
                {
                    //### If no cookie exists, create a completely new session.
                    HttpContext.Current.Session["loggedOutOrder"] = iProductID + "|" + iQuantity + "|" + strColors + "|" + strSizes;
                }
            }
            else
            {
                //### If no cookie exists, create a completely new session.
                HttpContext.Current.Session["loggedOutOrder"] = iProductID + "|" + iQuantity + "|" + strColors + "|" + strSizes;
            }
        }
        else
        {
            //HttpContext.Current.Session["loggedOutOrder"] = iProductID + "|" + iQuantity + "|" + strColors + "|" + strSizes;
            //### If there is an order in session add a new WishList item to that session.
            string strWishListValues = "";
            string iProdID = "";
            string iQnty = "";
            string strColor = "";
            string strSize = "";

            //### Trap for empty Session.
            if (!String.IsNullOrEmpty(HttpContext.Current.Session["loggedOutOrder"].ToString()))
            {
                strWishListValues = HttpContext.Current.Session["loggedOutOrder"].ToString();
            }

            //### Trap for multiple WishList items (if the string contains a "," then we know there are multiple WishList items.)
            if (strWishListValues.Contains(","))
            {
                //### Split the WishList items
                string[] strWishListItems = strWishListValues.Split(new char[] { ',' });
                string strWishListTemp = strWishListValues;
                bool bfound = false;

                foreach (string value in strWishListItems)
                {
                    //### Trap for empty WishList item values.
                    if (!String.IsNullOrEmpty(value))
                    {
                        //### Splite WishList item details (ID, Quantity, Product Type)
                        string[] strWishListItemDetails = value.ToString().Split(new char[] { '|' });

                        iProdID = strWishListItemDetails[0];
                        iQnty = strWishListItemDetails[1];
                        strColor = strWishListItemDetails[2];
                        strSize = strWishListItemDetails[3];

                        if ((int.Parse(iProdID) == iProductID && strColor != "" && strColor == strColors && strSizes == "One Size Only") || (int.Parse(iProdID) == iProductID && strSize != "" && strSize == strSizes && strColors == "One Colour Only") || (int.Parse(iProdID) == iProductID && strSize != "One Size Only" && strSize == strSizes && strColor != "One Colour Only" && strColor == strColors)) 
                        {
                            bfound = true;
                            break;
                        }
                    }
                    ////### If the current product(item) being added does not exists in the session, add the new item. iProductID.ToString() != iProdID
                    //if (!strWishListTemp.Contains("," + iProductID.ToString() + "|" + iQnty) && !strWishListTemp.Contains(iProductID.ToString() + "|" + iQnty))
                    //{
                    //}
                    ////### If the current product(item) being added does exist in the session, update the existing item's quantity.
                    //else
                    //{
                    //    bfound = true;
                    //    break;
                    //}
                }

                if (bfound)
                {
                    strWishListValues = strWishListValues.Replace(iProductID.ToString() + "|" + iQnty + "|" + strColor + "|" + strSize, (iProductID.ToString() + "|" + (Convert.ToInt32(iQnty) + iQuantity) + "|" + strColors + "|" + strSizes));
                    //strWishListValues += "," + iProductID.ToString() + "|" + (Convert.ToInt32(iQnty) + iQuantity) + "|" + addType;
                }
                else
                {
                    strWishListValues += "," + iProductID.ToString() + "|" + iQuantity + "|" + strColors + "|" + strSizes;
                }


            }
            //### If there is only a single item in the WishList, add the new item to the WishList and add a "," to split the item from the existing item.
            else
            {
                if (strWishListValues.Contains("|"))
                {
                    //### Split the WishList item's details (ID, Quantity, Product Type).
                    string[] strWishListItemDetails = strWishListValues.ToString().Split(new char[] { '|' });

                    iProdID = strWishListItemDetails[0];
                    iQnty = strWishListItemDetails[1];
                    strColor = strWishListItemDetails[2];
                    strSize = strWishListItemDetails[3];

                    //### If the current product(item) being added does not exists in the session, add the new item.
                    if (!strWishListValues.Contains("," + iProductID.ToString() + "|" + iQnty + "|" + strColors + "|" + strSizes) && !strWishListValues.Contains(iProductID.ToString() + "|" + iQnty + "|" + strColors + "|" + strSizes))
                    {
                        strWishListValues += "," + iProductID.ToString() + "|" + iQuantity + "|" + strColors + "|" + strSizes;
                    }
                    //### If the current product(item) being added does exist in the session, update the existing item's quantity.
                    else
                    {
                        strWishListValues = strWishListValues.Replace(iProductID.ToString() + "|" + iQnty + "|" + strColor + "|" + strSize, "");
                        strWishListValues += iProductID.ToString() + "|" + (Convert.ToInt32(iQnty) + iQuantity) + "|" + strColors + "|" + strSizes;
                    }
                }
                else
                {
                    strWishListValues = iProductID.ToString() + "|" + iQuantity + "|" + strColors + "|" + strSizes; ;
                }
            }

            //### Update the session.
            HttpContext.Current.Session["loggedOutOrder"] = strWishListValues;
        }

        //### If the user has no cookies available, create a new cookie.
        if (HttpContext.Current.Request.Cookies["OrderItems"] == null)
        {
            HttpCookie aCookie = new HttpCookie("OrderItems");

            aCookie.Values["OrderItemInfo"] = HttpContext.Current.Session["loggedOutOrder"].ToString();
            aCookie.Expires = DateTime.Now.AddDays(7);

            HttpContext.Current.Response.Cookies.Add(aCookie);
        }
        //### If the user has a cookie, update the existing cookie.
        else
        {
            if (HttpContext.Current.Request.Cookies["OrderItems"]["OrderItemInfo"] != null)
            {
                //### We cannot add values to an existing cookie, therefore we need we delete the cookie 
                //### to create a new one later on

                HttpContext.Current.Response.Cookies["OrderItems"].Expires = DateTime.Now;
            }

            //### Write the updated "logged out order" session to a new cookie. 
            HttpCookie aCookie = new HttpCookie("OrderItems");

            aCookie.Values["OrderItemInfo"] = HttpContext.Current.Session["loggedOutOrder"].ToString();
            aCookie.Expires = DateTime.Now.AddDays(7);

            HttpContext.Current.Response.Cookies.Add(aCookie);
        }

        //myWishListEvent(new EventArgs());
    }

    //### Static method to add a WishList item from a session or cookie into the user's WishList once the user logs in.
    //public static void addWishListItemFromSessionOrCookie()
    //{
    //    string iProductID = "";
    //    string iQuantity = "";

    //    //### Check to see that a session exists.
    //    if (HttpContext.Current.Session["loggedOutOrder"] != null)
    //    {
    //        //### If there is no order in session, create a new order.
    //        if (HttpContext.Current.Session["iOrderID"] == null)
    //        {
    //            //WishListSetup();
    //        }

    //        //### Split the WishList items.
    //        string[] strWishListItems = HttpContext.Current.Session["loggedOutOrder"].ToString().Split(new char[] { ',' });

    //        foreach (string value in strWishListItems)
    //        {
    //            if (!String.IsNullOrEmpty(value))
    //            {
    //                //### Split the WishList item's details.
    //                string[] strWishListItemDetails = value.ToString().Split(new char[] { '|' });

    //                iProductID = strWishListItemDetails[0];
    //                iQuantity = strWishListItemDetails[1];
    //            }

    //            //### For each WishList item in the session, add the item to the WishList.
    //            addWishListItem(Convert.ToInt32(iProductID), Convert.ToInt32(iQuantity));
    //        }

    //        //### Once the order has been made, kill the logged out order session.
    //        HttpContext.Current.Session["loggedOutOrder"] = null;
    //    }
    //    else
    //    {
    //        //### If there is no logged out order in session, check to see if the user has an order cookie.
    //        if (HttpContext.Current.Request.Cookies["OrderItems"] != null)
    //        {
    //            //### If there is no order in session, create a new order.
    //            if (HttpContext.Current.Session["iOrderID"] == null)
    //            {
    //                //WishListSetup();
    //            }

    //            //### Split the WishList items in the cookie.
    //            string[] strWishListItems = HttpContext.Current.Request.Cookies["OrderItems"]["OrderItemInfo"].ToString().Split(new char[] { ',' });

    //            foreach (string value in strWishListItems)
    //            {
    //                if (!String.IsNullOrEmpty(value))
    //                {
    //                    string[] strWishListItemDetails = value.ToString().Split(new char[] { '|' });

    //                    iProductID = strWishListItemDetails[0];
    //                    iQuantity = strWishListItemDetails[1];
    //                }

    //                //### For each WishList item in the cookie, add the WishList item to the WishList.
    //                addWishListItem(Convert.ToInt32(iProductID), Convert.ToInt32(iQuantity));
    //            }
    //            //### Once the order has been made, kill the cookie.
    //            HttpContext.Current.Response.Cookies["OrderItems"].Expires = DateTime.Now;
    //        }
    //    }
    //}

    #endregion

    #region GET COUNT METHODS

    //### Static method to get the WishList count for the logged in user.
    public static int GetWishListCount(int iOrderID)
    {
        int iWishListCount = 0;

        //clsOrderItems clsOrderItems = new clsOrderItems();
        //DataTable dtOrderItems = clsOrderItems.GetOrderItemsList("iOrderID = " + iOrderID + "", "");

        //iWishListCount = dtOrderItems.Rows.Count;

        return iWishListCount;
    }

    //### Static method to get the WishList count for the logged out user.
    public static int GetLoggedOutWishListCount()
    {
        getWishList();

        string strWishListValues = "";

        string iProdID = "";
        string iQnty = "";
        string strColour = "";
        string strSize = "";

        int iWishListCount = 0;

        if (HttpContext.Current.Session["loggedOutOrder"] != null)
        {
            strWishListValues = HttpContext.Current.Session["loggedOutOrder"].ToString();

            //### Trap for multiple WishList items (if the string contains a "," then we know there are multiple WishList items.)
            if (strWishListValues.Contains(","))
            {
                //### Split the WishList items
                string[] strWishListItems = strWishListValues.Split(new char[] { ',' });

                foreach (string value in strWishListItems)
                {
                    //### Trap for empty WishList item values.
                    if (!String.IsNullOrEmpty(value))
                    {
                        //### Splite WishList item details (ID, Quantity, Product Type)
                        string[] strWishListItemDetails = value.ToString().Split(new char[] { '|' });

                        iProdID = strWishListItemDetails[0];
                        iQnty = strWishListItemDetails[1];
                        strColour = strWishListItemDetails[2];
                        strSize = strWishListItemDetails[3];
                    }

                    //### For each WishList item in the session, add the quantity to the count.
                    iWishListCount++;
                }
            }
            else
            {
                if (strWishListValues.Contains("|"))
                {

                    //### Split the WishList item's details (ID, Quantity, Product Type).
                    string[] strWishListItemDetails = strWishListValues.ToString().Split(new char[] { '|' });

                    iProdID = strWishListItemDetails[0];
                    iQnty = strWishListItemDetails[1];
                    strColour = strWishListItemDetails[2];
                    strSize = strWishListItemDetails[3];

                    iWishListCount++;
                }
            }
        }

        return iWishListCount;
    }

    #endregion

    #region GET WishList ITEMS

    /// <summary>
    /// Gets all the items in the Cookie/Session
    /// </summary>
    /// <returns>DataTable</returns>
    /// 
    public static DataTable WishList()
    {
        DataTable dtWishList = new DataTable("tblWishList");

        if (HttpContext.Current.Session["clsAccountHolders"] != null)
        {
            int iOrderID;
            iOrderID = Convert.ToInt32(HttpContext.Current.Session["iOrderID"]);

            //string iProdID = "";
            //string iQnty = "";

            //### Build a Datatable to return the data from the cookie.        
            //object[] objWishListItem = new object[2];

            //dtWishList.Columns.Add("iProductID");
            //dtWishList.Columns.Add("iQuantity");

            //foreach (DataRow r in clsOrderItems.GetOrderItemsList("iOrderID = " + iOrderID.ToString(), "").Rows)
            //{
            //    objWishListItem[0] = r["iProductID"];
            //    objWishListItem[1] = r["iQuantity"];

            //    dtWishList.Rows.Add(objWishListItem);
            //}
        }
        else
        {
            //if (HttpContext.Current.Session["clsAccountHolders"] == null)
            //{
            //}

            //Puts order item info into session loggedOutOrder
            getWishList();

            string strWishListValues = "";
            string iProdID = "";
            string iQnty = "";
            string strColour = "";
                string strSize = "";

            int iWishListCount = 0;

            //### Build a Datatable to return the data from the cookie.
            object[] objWishListItem = new object[4];

            dtWishList.Columns.Add("iProductID");
            dtWishList.Columns.Add("iQuantity");
            dtWishList.Columns.Add("strColour");
            dtWishList.Columns.Add("strSize");

            if (HttpContext.Current.Session["loggedOutOrder"] != null)
            {
                strWishListValues = HttpContext.Current.Session["loggedOutOrder"].ToString();

                //### Trap for multiple WishList items (if the string contains a "," then we know there are multiple WishList items.)
                if (strWishListValues.Contains(","))
                {
                    //### Split the WishList items
                    string[] strWishListItems = strWishListValues.Split(new char[] { ',' });

                    foreach (string value in strWishListItems)
                    {
                        //### Trap for empty WishList item values.
                        if (!String.IsNullOrEmpty(value))
                        {
                            //### Splite WishList item details (ID, Quantity, Product Type)
                            string[] strWishListItemDetails = value.ToString().Split(new char[] { '|' });

                            iProdID = strWishListItemDetails[0];
                            iQnty = strWishListItemDetails[1];
                            strColour = strWishListItemDetails[2];
                            strSize = strWishListItemDetails[3];
                        }

                        //### For each WishList item in the session, add the quantity to the count.
                        iWishListCount += Convert.ToInt32(iQnty);

                        //### Set the details of the item to the objWishListItem (datarow)
                        objWishListItem[0] = iProdID;
                        objWishListItem[1] = iQnty;
                        objWishListItem[2] = strColour;
                        objWishListItem[3] = strSize;


                        dtWishList.Rows.Add(objWishListItem);
                    }
                }
                else
                {
                    //### Split the WishList item's details (ID, Quantity, Product Type).
                    string[] strWishListItemDetails = strWishListValues.ToString().Split(new char[] { '|' });

                    iProdID = strWishListItemDetails[0];
                    iQnty = strWishListItemDetails[1];
                    strColour = strWishListItemDetails[2];
                    strSize = strWishListItemDetails[3];

                    iWishListCount += Convert.ToInt32(iQnty);

                    //### Set the details of the item to the objWishListItem (datarow)
                    objWishListItem[0] = iProdID;
                    objWishListItem[1] = iQnty;
                    objWishListItem[2] = strColour;
                    objWishListItem[3] = strSize;

                    dtWishList.Rows.Add(objWishListItem);
                }
            }
            else
            {
            }
        }
        return dtWishList;
    }

    #endregion

    #region PRIVATE METHODS

    /// <summary>
    /// Gets the details of the cookie,puts it into the session
    /// </summary>
    /// 
    private static HttpContext getWishList()
    {
        if (HttpContext.Current.Session["loggedOutOrder"] != "0")
        {
            if (HttpContext.Current.Session["loggedOutOrder"] == null)
            {
                //### If there is no order in session check to see if a cookie exists.
                if (HttpContext.Current.Request.Cookies["OrderItems"] != null)
                {
                    if (HttpContext.Current.Request.Cookies["OrderItems"]["OrderItemInfo"] != null)
                    {
                        //### If a cookie exists, add the cookie values to the current session.
                        HttpContext.Current.Session["loggedOutOrder"] = HttpContext.Current.Request.Cookies["OrderItems"]["OrderItemInfo"];
                    }
                }
            }
        }
        else
        {
            HttpContext.Current.Session["loggedOutOrder"] = null;
            HttpContext.Current.Response.Cookies["OrderItems"].Expires = DateTime.Now.AddDays(-10);
        }

        return null;
    }
    
    #endregion

    #region REMOVE WishList ITEMS

    public static void removeItemFromWishList(int iProductID, int iQuantity, string strColour, string strSize)//, int iQuantity)
    {
        //### Check for an Order in session
        if (HttpContext.Current.Session["iOrderID"] == null)
        {
            if (HttpContext.Current.Session["clsAccountHolders"] != null)
            {
                //### If there is no order in session and the user is logged in, create a new order entry to tblOrders.
                //WishListSetup();
            }
        }

        //### Check to see if the user is logged in.
        if (HttpContext.Current.Session["clsAccountHolders"] != null)
        {
            //### If the user is logged in, create the order as normal.
            //addWishListItem(iProductID);
            removeItemFromCookie(iProductID, iQuantity, strColour, strSize);
        }
        else
        {
            //### If the user is not logged in, create a logged out order(in session order + cookie order).
            //### The cookie gives the user the ability to register and continue with the order within 7 days.
            removeItemFromCookie(iProductID, iQuantity, strColour, strSize);
        }
    }

    private static void removeItemFromCookie(int iProductID, int iQuantities, string strColours, string strSizes)
    {
        //### Trap for logged out order in session.
        if (HttpContext.Current.Session["loggedOutOrder"] == null)
        {
            //### If there is no order in session check to see if a cookie exists.
            if (HttpContext.Current.Request.Cookies["OrderItems"] != null)
            {
                if (HttpContext.Current.Request.Cookies["OrderItems"]["OrderItemInfo"] != null)
                {
                    //### If a cookie exists, add the cookie values to the current session.
                    HttpContext.Current.Session["loggedOutOrder"] = HttpContext.Current.Request.Cookies["OrderItems"]["OrderItemInfo"];

                    //### Loop through this method to add the current item to the existing list of items.
                    removeItemFromCookie(iProductID, iQuantities, strColours, strSizes);
                }
                else
                {
                }
            }
            else
            {
            }
        }
        else
        {
            //### If there is an order in session add a new WishList item to that session.
            string strWishListValues = "";

            string iProdID = "";
            string iQnty = "";
            string strColour = "";
            string strSize = "";
            //string strPrdTyp = "";

            //### Trap for empty Session.
            if (!String.IsNullOrEmpty(HttpContext.Current.Session["loggedOutOrder"].ToString()))
            {
                strWishListValues = HttpContext.Current.Session["loggedOutOrder"].ToString();
            }

            //### Trap for multiple WishList items (if the string contains a "," then we know there are multiple WishList items.)
            if (strWishListValues.Contains(","))
            {
                //### Split the WishList items
                string[] strWishListItems = strWishListValues.Split(new char[] { ',' });
                string strWishListTemp = strWishListValues;

                foreach (string value in strWishListItems)
                {
                    //### Trap for empty WishList item values.
                    if (!(String.IsNullOrEmpty(value)))
                    {
                        //### Split WishList item details (ID, Quantity, Product Type)
                        string[] strWishListItemDetails = value.ToString().Split(new char[] { '|' });

                        iProdID = strWishListItemDetails[0];
                        iQnty = strWishListItemDetails[1];
                        strColour = strWishListItemDetails[2];
                        strSize = strWishListItemDetails[3];
                    }

                    //### If the current product(item) being added does not exists in the session, add the new item. iProductID.ToString() != iProdID
                    if (!strWishListTemp.Contains("," + iProdID + "|" + iQnty + "|" + strColour + "|" + strSize) && !strWishListTemp.Contains(iProductID.ToString() + "|" + iQnty + "|" + strColour + "|" + strSize))
                    {
                        //strWishListValues += value;
                    }
                    //### If the current product(item) being added does exist in the session, update the existing item's quantity.
                    else
                    {
                        if (strWishListTemp.Contains("," + iProductID.ToString() + "|" + iQuantities + "|" + strColours + "|" + strSizes))
                        {
                            strWishListValues = strWishListValues.Replace("," + iProductID.ToString() + "|" + iQuantities + "|" + strColours + "|" + strSizes, "");
                            break;
                        }
                        else
                        {
                            if (strWishListTemp.Contains(iProductID.ToString() + "|" + iQuantities + "|" + strColours + "|" + strSizes + ","))
                            {
                                strWishListValues = strWishListValues.Replace(iProductID.ToString() + "|" + iQuantities + "|" + strColours + "|" + strSizes + ",", "");
                                break;
                            }

                        }
                    }
                }
            }
            else //### If there is only a single item in the WishList, add the new item to the WishList and add a "," to split the item from the existing item.
            {
                //### Split the WishList item's details (ID, Quantity, Product Type).
                string[] strWishListItemDetails = strWishListValues.ToString().Split(new char[] { '|' });

                iProdID = strWishListItemDetails[0];
                iQnty = strWishListItemDetails[1];
                strColour = strWishListItemDetails[2];
                strSize = strWishListItemDetails[3];

                strWishListValues = strWishListValues.Replace(iProductID.ToString() + "|" + iQnty + "|" + strColour + "|" + strSize, "");
            }

            //### Update the session.
            HttpContext.Current.Session["loggedOutOrder"] = strWishListValues;
            if (strWishListValues != "")
            {
                //### Write the updated "logged out order" session to a new cookie. 
                HttpCookie aCookie = new HttpCookie("OrderItems");

                aCookie.Values["OrderItemInfo"] = HttpContext.Current.Session["loggedOutOrder"].ToString();
                aCookie.Expires = DateTime.Now.AddDays(7);

                HttpContext.Current.Response.Cookies.Add(aCookie);
            }
            else
            {
                HttpContext.Current.Response.Cookies["OrderItems"]["OrderItemInfo"] = null;
                HttpContext.Current.Response.Cookies["OrderItems"].Expires = DateTime.Now;
                HttpContext.Current.Session["loggedOutOrder"] = null;
            }
        }
    }

    public static void FinalizeWishList()
    {
        HttpContext.Current.Request.Cookies.Remove("OrderItems");
    }

    #endregion

    #region LOGIN

    public static void doLogin()
    {
        //WishListSetup();

        if (HttpContext.Current.Session["loggedOutOrder"] != null)
        {
            //### If the user added items to the WishList while logged out then the loggedOutOrder session should not be empty(if the session is stil alive)
            //### Create the order item in the DB
            createOrder(HttpContext.Current.Session["loggedOutOrder"].ToString());
        }
        else
        {
            //### The user might not have added items to the WishList loged out, or the session was lost, get the items from the cookie.
            //### This method will put the cookie back in the session for us.
            getWishList();
            if (HttpContext.Current.Session["loggedOutOrder"] != null)
            {
                //### Create the order item in the DB
                createOrder(HttpContext.Current.Session["loggedOutOrder"].ToString());
            }
        }

        //### After processing the loged out order clear the cookie so that if you log out and back in you dont get a duplicate order.
        clearCookie();
    }

    private static void createOrder(string strOrder)
    {
        if (strOrder != "")
        {
            //int iOrderID = 0;
            //clsAccountHolders clsAccountHolders;
            //clsAccountHolders = (clsAccountHolders)HttpContext.Current.Session["clsAccountHolders"];
            //iOrderID = Convert.ToInt32(HttpContext.Current.Session["iOrderID"]);
            
            //foreach (string item in strOrder.Split(','))
            //{
            //    int iProductID = 0;
            //    int iQuantity = 0;

            //    iProductID = int.Parse(item.Split('|')[0]);
            //    iQuantity = int.Parse(item.Split('|')[1]);

            //    if (!clsCommonFunctions.DoesRecordExist("tblOrderItems", "iOrderID = " + iOrderID + " AND iProductID = " + iProductID + " AND bIsDeleted=0"))
            //    {
            //        //### If the order item exists create a new entry.
            //        clsOrderItems clsOrderItems = new clsOrderItems();

            //        clsOrderItems.dtAdded = DateTime.Now;
            //        clsOrderItems.iAddedBy = clsAccountHolders.iAccountHolderID;
            //        clsOrderItems.iOrderID = iOrderID;
            //        clsOrderItems.iProductID = iProductID;
            //        //clsOrderItems.iOrderItemStatus = 0;
            //        clsOrderItems.iQuantity += iQuantity;
            //        //clsOrderItems.bIsJewellery = bIsJewellery;
            //        clsOrderItems.Update();
            //    }
            //    else
            //    {
            //        //### If the order item entry already exists, update the item's quantity.
            //        clsOrderItems clsOrderItems = new clsOrderItems(iOrderID);

            //        clsOrderItems.dtEdited = DateTime.Now;
            //        clsOrderItems.iEditedBy = clsAccountHolders.iAccountHolderID;
            //        clsOrderItems.iOrderID = iOrderID;
            //        //clsOrderItems.iOrderItemStatus = 0;
            //        clsOrderItems.iQuantity += iQuantity;
            //        //clsOrderItems.bIsJewellery = bIsJewellery;
            //        clsOrderItems.Update();
            //    }

            //}
        }
    }

    private static void clearCookie()
    {
        HttpContext.Current.Response.Cookies["OrderItems"]["OrderItemInfo"] = null;
        HttpContext.Current.Response.Cookies["OrderItems"].Expires = DateTime.Now.AddMonths(-1);
        HttpContext.Current.Session["loggedOutOrder"] = "0";
    }

    #endregion

    #region GET TOTALS METHOD

    //### Get the total cost for the order.
    public static double GetOrderTotal(int iOrderID)
    {
        Double dblOrderTotal = 0.00;

        //clsOrders clsOrders = new clsOrders(iOrderID);

        //dblOrderTotal = clsOrders.dblTotalAmount;

        return dblOrderTotal;
    }

    //### Get the total cost of the logged out order.
    public static double GetLoggedOutOrderTotal()
    {
        double dblOrderTotal = 0.00;

        string strWishListValues = "";

        string iProdID = "";
        string iQnty = "";
        //string strAtp = "";

        if (!String.IsNullOrEmpty(HttpContext.Current.Session["loggedOutOrder"].ToString()))
        {
            strWishListValues = HttpContext.Current.Session["loggedOutOrder"].ToString();
        }

        //### Trap for multiple WishList items (if the string contains a "," then we know there are multiple WishList items.)
        if (strWishListValues.Contains(","))
        {
            //### Split the WishList items
            string[] strWishListItems = strWishListValues.Split(new char[] { ',' });

            foreach (string value in strWishListItems)
            {
                //### Trap for empty WishList item values.
                if (!(String.IsNullOrEmpty(value)))
                {
                    //### Split WishList item details (ID, Quantity, Product Type)
                    string[] strWishListItemDetails = value.ToString().Split(new char[] { '|' });

                    iProdID = strWishListItemDetails[0];
                    iQnty = strWishListItemDetails[1];
                }

                if (!(String.IsNullOrEmpty(iProdID)))
                {
                    clsProducts clsProducts = new clsProducts(Convert.ToInt32(iProdID));
                    dblOrderTotal += (clsProducts.dblPrice) * Convert.ToInt32(iQnty);
                }
            }
        }
        else
        {
            //### Split the WishList item's details (ID, Quantity).
            string[] strWishListItemDetails = strWishListValues.ToString().Split(new char[] { '|' });

            iProdID = strWishListItemDetails[0];
            iQnty = strWishListItemDetails[1];

            clsProducts clsProducts = new clsProducts(Convert.ToInt32(iProdID));
            dblOrderTotal += (clsProducts.dblPrice) * Convert.ToInt32(iQnty);

            //if (strAtp == "jewellery")
            //{
            //    clsJewelleries clsJewelleries = new clsJewelleries(Convert.ToInt32(iProdID));
            //    dblOrderTotal += (clsJewelleries.dblRetailPrice) * Convert.ToInt32(iQnty);
            //}
            //else
            //{
            //    clsWatches clsWatches = new clsWatches(Convert.ToInt32(iProdID));
               
            //    if (clsWatches.bIsOnSpecial == true)
            //    {
            //        dblOrderTotal += (clsWatches.dblSpecialPrice) * Convert.ToInt32(iQnty);
            //    }
            //    else
            //    {
            //        if (clsWatches.dblYourPrice != 0.00)
            //        {
            //            dblOrderTotal += (clsWatches.dblYourPrice) * Convert.ToInt32(iQnty);
            //        }
            //        else
            //        {
            //            dblOrderTotal += (clsWatches.dblRetailPrice) * Convert.ToInt32(iQnty);
            //        }
            //    }
            //}
        }

        return dblOrderTotal;
    }

    //### Get the total vat of the logged out order.
    public static double GetLoggedOutOrderVat()
    {
        double dblVat = 0.00;
        double dblOrderTotal = 0.00;

        dblOrderTotal = GetLoggedOutOrderTotal();

        dblVat = Convert.ToDouble(dblOrderTotal * (Convert.ToDouble(14) / Convert.ToDouble(100)));

        return dblVat;
    }

    #endregion
}